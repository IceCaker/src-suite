package com.bmit.project.srrc.service;

import com.bmit.platform.soupe.model.Page;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * Created by IceCaker on 2017/11/21.
 */
public interface SRRCPageService {
    Set<Page> getUserPages(HttpServletRequest request);
}
