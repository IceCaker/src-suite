package com.bmit.project.srrc.service.internal;

import com.bmit.platform.soupe.model.BaseUser;
import com.bmit.platform.soupe.model.Page;
import com.bmit.platform.soupe.model.Role;
import com.bmit.platform.soupe.repository.jdbc.PageRoleJdbcRepository;
import com.bmit.platform.soupe.repository.jdbc.UserRoleJdbcRepository;
import com.bmit.platform.soupe.repository.jpa.PageRepository;
import com.bmit.platform.soupe.service.PageManager;
import com.bmit.platform.soupe.util.UserUtil;
import com.bmit.project.srrc.service.SRRCPageService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.lang.ref.SoftReference;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by IceCaker on 2017/11/21.
 */
@Service
public class DefaultSRRCPageService implements SRRCPageService {

    @Autowired
    private PageManager pageManager;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private UserRoleJdbcRepository userRoleJdbcRepository;

    @Autowired
    private PageRoleJdbcRepository pageRoleJdbcRepository;

    @Override
    public Set<Page> getUserPages(HttpServletRequest request) {

        Set<Page> result = getUserPagesInSession(request);
        if (result != null) {
            return result;
        }

        Set<Page> pages = getUserPagesInDataBase(request);
        request.getSession().setAttribute("userPages", new SoftReference<Set<Page>>(pages));

        return pages;
    }

    private void loadChildrenPages(Page page, Long userId) {
        if (!page.isLeaf()) {
            Set<Page> pages = pageManager.getPages(page.getId(), 2, userId);
            for (Page childPage : pages) {
                loadChildrenPages(childPage, userId);
            }
            page.setChildren((LinkedHashSet<Page>) pages);
        }
    }

    private Set<Page> getUserPagesInDataBase(HttpServletRequest request) {
        Set<Page> userPages = Sets.newLinkedHashSet();
        List<Page> allPages = pageRepository.findAll(
                new Sort(
                        new Sort.Order(Sort.Direction.ASC, "sortCode"),
                        new Sort.Order(Sort.Direction.ASC, "priority")
                )
        );
        Set<String> authorizedPageIds = getAuthorizedPageIds();

        List<Page> tempTree = Lists.newArrayList();
        for (Page page : allPages) {
            if (authorizedPageIds.contains(page.getId())) {
                if (page.getSortCode() == null) {
                    continue;
                }
                while (tempTree.size() > 0
                        && tempTree.get(tempTree.size() - 1).getSortCode().length() >= page.getSortCode().length()) {
                    tempTree.remove(tempTree.size() - 1);
                }
                if (tempTree.size() == 0) {
                    userPages.add(page);
                    tempTree.add(page);
                } else {
                    Page parentPage = tempTree.get(tempTree.size() - 1);
                    if (parentPage.getChildren() == null) {
                        parentPage.setChildren(new LinkedHashSet<Page>());
                    }
                    parentPage.getChildren().add(page);
                    tempTree.add(page);
                }
            }
        }

        return userPages;
    }

    private Set<String> getAuthorizedPageIds() {

        Set<String> result = Sets.newHashSet();

        BaseUser user = UserUtil.getCurrentUser();

        List<Role> roles = userRoleJdbcRepository.findRolesByUserId(user.getId());
        for (Role role : roles) {
            List<Page> pages = pageRoleJdbcRepository.findPagesByRoleId(role.getId());
            for (Page page : pages) {
                result.add(page.getId());
            }
        }

        return result;
    }

    private Set<Page> getUserPagesInSession(HttpServletRequest request) {

        if (request.getSession().getAttribute("userPages") != null) {
            return ((SoftReference<Set<Page>>) request.getSession().getAttribute("userPages")).get();
        }
        return null;
    }
}
