package com.bmit.project.srrc;

import com.bmit.platform.soupe.application.AbstractApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SRRCApplication extends AbstractApplication {

    public static void main(String[] args) {
        SpringApplication.run(SRRCApplication.class, args);
    }

}
