# Shared Service Center UI

## Build Setup

```
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# serve in production env
npm run start
```

## To Enable Server-Side Rendering Feature

Using following scripts in package.json

```
"dev": "node server",
"build": "npm run build:client && npm run build:server",
"build:client": "cross-env NODE_ENV=production webpack --config build/webpack.client.config.js --progress --hide-modules",
"build:server": "cross-env NODE_ENV=production webpack --config build/webpack.server.config.js --progress --hide-modules",
"start": "cross-env NODE_ENV=production node server"
```
