package com.bmit.project.srrc.ui.controller;

import com.bmit.platform.soupe.annotation.PageConfiguration;
import com.bmit.platform.soupe.ui.controller.AbstractUIController;
import com.bmit.project.srrc.util.SrrcModule;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@ApplicationScope
public class SRRCExpenseEntryUIController extends AbstractUIController {

    public static final String MODERN_UI_ENTRY = "srrc.expense.entry";

    @PageConfiguration(
            id = MODERN_UI_ENTRY,
            isMenu = true
    )
    @RequestMapping(value = {"/srrc", "/srrc/**"})
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        return service(request, response);
    }

    @Override
    protected String getViewKey() {
        return SrrcModule.BMS_UI_COMPATIBILITY_VIEW_EXPENSE_COMMON_VIEW;
    }

}
