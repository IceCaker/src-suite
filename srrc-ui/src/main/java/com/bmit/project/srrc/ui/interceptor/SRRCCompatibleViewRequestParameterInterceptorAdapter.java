package com.bmit.project.srrc.ui.interceptor;

import com.bmit.platform.soupe.model.Page;
import com.bmit.project.srrc.service.SRRCPageService;
import com.bmit.project.srrc.ui.util.SRRCProjectCompatibleViewPageUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

public class SRRCCompatibleViewRequestParameterInterceptorAdapter extends HandlerInterceptorAdapter {

    private static Log logger = LogFactory.getLog(SRRCCompatibleViewRequestParameterInterceptorAdapter.class);

    private SRRCPageService srrcPageService;

    public SRRCCompatibleViewRequestParameterInterceptorAdapter(
            SRRCPageService srrcPageService) {
        this.srrcPageService = srrcPageService;
    }

    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        if (modelAndView == null || !modelAndView.hasView()) {
            return;
        }
        try {
            Set<Page> userPages = srrcPageService.getUserPages(request);

            request.setAttribute("userPages", userPages);
            request.setAttribute("userPagesHtml",
                    SRRCProjectCompatibleViewPageUtil.getPagesAsHtml(userPages));
        } catch (Exception ex) {
            logger.warn("Set parameters to request fail, " + ex.getMessage(), ex);
        }
    }

}
