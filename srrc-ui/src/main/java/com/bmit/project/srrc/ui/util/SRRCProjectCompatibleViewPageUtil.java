package com.bmit.project.srrc.ui.util;

import com.bmit.platform.soupe.model.Page;
import com.bmit.platform.soupe.util.LocaleUtil;

import java.util.Locale;
import java.util.Set;

public final class SRRCProjectCompatibleViewPageUtil {

    private static final int DEFAULT_LI_WIDTH = 110;

    public static String getPagesAsHtml(Set<Page> pages) {
        StringBuilder htmlCode = new StringBuilder();

        Locale locale = LocaleUtil.getCurrentLocale();

        //htmlCode.append("<!--<![CDATA[-->");
        writeHtml(pages, htmlCode, locale, 3);
        //htmlCode.append("<!--]]>-->");
        writeSpaceAndNextLine(htmlCode, 2);
        return htmlCode.toString();
    }

    private static void writeHtml(Set<Page> pages, StringBuilder htmlCode, Locale locale, int level) {
        int maxLength = 0;
        for (Page page : pages) {
            if (maxLength <= page.getTitle(locale).length()) {
                maxLength = page.getTitle(locale).length();
            }
        }
        maxLength = maxLength * 13 + 20;
        if (maxLength < DEFAULT_LI_WIDTH) {
            maxLength = DEFAULT_LI_WIDTH;
        }
        for (Page page : pages) {
            writeSpaceAndNextLine(htmlCode, level);
            htmlCode.append("<li><a style=\"width:").append(maxLength).append("px;\" href=\"");
            htmlCode.append(page.getLink()).append("\">");
            htmlCode.append(page.getTitle(locale)).append("</a>");
            if (page.getChildren() != null && page.getChildren().size() > 0) {
                writeSpaceAndNextLine(htmlCode, level + 1);
                htmlCode.append("<ul>");
                writeHtml(page.getChildren(), htmlCode, locale, level + 2);
                writeSpaceAndNextLine(htmlCode, level + 1);
                htmlCode.append("</ul>");
                writeSpaceAndNextLine(htmlCode, level);
            }
            htmlCode.append("</li>");
        }
    }

    private static void writeSpaceAndNextLine(StringBuilder htmlCode, int level) {
        htmlCode.append("\n");
        while (level > 0) {
            htmlCode.append("    ");
            level--;
        }
    }
}
