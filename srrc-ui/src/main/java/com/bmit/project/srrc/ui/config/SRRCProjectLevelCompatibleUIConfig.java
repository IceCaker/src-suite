package com.bmit.project.srrc.ui.config;

import com.bmit.platform.soupe.ui.config.AbstractSoupeWebConfig;
import com.bmit.project.srrc.service.SRRCPageService;
import com.bmit.project.srrc.ui.interceptor.SRRCCompatibleViewRequestParameterInterceptorAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

@Configuration
public class SRRCProjectLevelCompatibleUIConfig extends AbstractSoupeWebConfig {

    @Autowired
    private SRRCPageService srrcPageService;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        SRRCCompatibleViewRequestParameterInterceptorAdapter requestCommonParamInterceptor
                = new SRRCCompatibleViewRequestParameterInterceptorAdapter(srrcPageService);

        registry.addInterceptor(requestCommonParamInterceptor);
    }

}
