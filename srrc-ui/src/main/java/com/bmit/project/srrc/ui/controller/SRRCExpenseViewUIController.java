package com.bmit.project.srrc.ui.controller;

import com.bmit.platform.soupe.annotation.PageConfiguration;
import com.bmit.platform.soupe.ui.controller.AbstractUIController;
import com.bmit.project.srrc.util.SrrcModule;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@ApplicationScope
class SRRCExpenseViewUIController extends AbstractUIController {

    @PageConfiguration(
            id = SrrcModule.BMS_SRRC_EXPENSE_VIEW_FEATURE_KEY,
            isMenu = false
    )
    @RequestMapping(value = SrrcModule.MODULE_MAPPING_ROOT + "expense/view/ui")
    public ModelAndView index(HttpServletRequest request, Model model, HttpServletResponse response) {
        return service(request, response);
    }

    @Override
    protected String getViewKey() {
        return SrrcModule.BMS_UI_COMPATIBILITY_VIEW_EXPENSE_COMMON_VIEW;
    }

}
