package com.bmit.project.srrc.ui.component;

import com.bmit.platform.soupe.ui.component.ThymeleafLayoutTemplateProvider;
import org.thymeleaf.templatemode.StandardTemplateModeHandlers;

public class SRRCThymeleafLayoutTemplateProvider implements ThymeleafLayoutTemplateProvider {

    @Override
    public String getLayoutTemplatePath(String templateMode) {
        if (StandardTemplateModeHandlers.XHTML.getTemplateModeName().equals(templateMode)) {
            return "xhtml/project/srrc/layout";
        } else if (StandardTemplateModeHandlers.HTML5.getTemplateModeName().equals(templateMode)) {
            return "html5/product/erp/layout";
        }

        return "xhtml/project/srrc/layout";
    }

}
