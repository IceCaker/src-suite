Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  "Common.FlexibleToolbar"
]);

Ext.application({
  name: 'Project',
  appFolder: contextPath + '/static/js/project/pkuih/project',
  controllers: ["ProjectPrintController"],
  launch: function () {
    Ext.tip.QuickTipManager.init();

    var height = window.document.body.clientHeight;
    var width = 1024;

    var initParams = pageInitParam;
    var urlParams = Ext.urlDecode(location.search.slice(1));
    var optionValue = "," + urlParams.optionValue + ",";

    var panels = [];

    if (optionValue.indexOf(",projectBasePanel,") > -1) {
      var projectBasePanel = Ext.widget("projectBasePanel", {
        title: '基本信息',
        height: 470,
        width: width,
        margin: 5,
        getProjectId: function () {
          return pageInitParam.projectId
        },
        isReadOnly: function () {
          return true
        }
      });
      projectBasePanel.loadData();
      panels.push(projectBasePanel);
    }

    if (optionValue.indexOf(",projectBillPanel,") > -1) {
      var projectBillPanel = Ext.widget("projectBillPanel", {
        minHeight: 200,
        autoHeight: true,
        width: width,
        margin: 5,
        getProjectId: function () {
          return pageInitParam.projectId
        },
        isReadOnly: function () {
          return true
        }
      });
      projectBillPanel.loadData();
      panels.push(projectBillPanel);
    }

    if (optionValue.indexOf(",projectProcessPanel,") > -1) {
      var projectProcessPanel = Ext.widget("projectProcessPanel", {
        height: 400,
        width: width,
        margin: 5,
        getProjectId: function () {
          return pageInitParam.projectId
        },
        isReadOnly: function () {
          return true
        }
      });
      projectProcessPanel.loadData();
      panels.push(projectProcessPanel);
    }

    if (optionValue.indexOf(",projectVersionPanel,") > -1) {
      var projectVersionPanel = Ext.widget("projectVersionPanel", {
        minHeight: 200,
        autoHeight: true,
        width: width,
        margin: 5,
        showOperate: false,
        getProjectId: function () {
          return pageInitParam.projectId
        },
        isReadOnly: function () {
          return true
        }
      });
      projectVersionPanel.loadData();
      panels.push(projectVersionPanel);
    }

    /*    Ext.create('Ext.panel.Panel', {
     bodyPadding: 5,
     layout: 'column',
     defaults: {
     layout: 'anchor',
     defaults: {
     anchor: '100%'
     }
     },
     items: panels
     }).render(Ext.getBody());*/

    /*    Ext.create("Ext.panel.Panel", {
     layout: {
     align: 'middle',
     pack: 'center',
     type: 'anchor'
     },
     items: panels
     }).render(Ext.getBody());*/

    for (var i = 0; i < panels.length; i++) {
      panels[i].render(Ext.getBody());
    }
  }
});