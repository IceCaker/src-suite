Ext.require([
  'Common.combo.Organization',
  'Common.combo.Period'
]);
Ext.define('Project.view.task.ProjectTaskGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.projectTaskGrid',
  title: '项目信息',
  autoScroll: true,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  autoWidth: true,
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  store: Ext.create('Common.data.ApiGridStore', {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'code', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'status', type: 'string'},
      {name: 'statusName', type: 'string'},
      {name: 'nextStatus', type: 'string'},
      {name: 'nextStatusName', type: 'string'},
      {name: 'dutyOrgName', type: 'string'},
      {name: 'costAmount', type: 'double'},
      {name: 'costTaxAmount', type: 'double'},
      {name: 'creatorName', type: 'string'},
      {name: 'processId', type: 'int'},
      {name: 'isSave', type: 'boolean'},
      {name: 'isCancel', type: 'boolean'},
      {name: 'isAdjust', type: 'boolean'}
    ],
    api: "project",
    joins: [
      {
        type: 'organization',
        fields: {
          name: 'organizationName',
          sortCode: 'organizationSortCode'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      },
      {
        type: 'user',
        fields: {
          name: 'managerName'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'managerId'
          }
        ]
      }
    ],
    sorts: [
      {
        field: 'createTime',
        direction: 'DESC'
      },
      {
        field: 'name',
        direction: 'ASC'
      }
    ]
  }),
  selModel: Ext.create('Ext.selection.CheckboxModel', {mode: 'SIMPLE'}),
  columns: [
    {text: '项目编码', dataIndex: 'code', align: 'left', width: 120, sortable: false},
    {
      text: '项目名称', dataIndex: 'name', align: 'left', minWidth: 140, flex: 1,
      renderer: function(value, meta, record) {
        return '<a onclick="Project.view.task.ProjectTaskGrid.view('
          + record.data.id + ')">' + value + '</a>';
      }
    },
    {text: '主管部门', dataIndex: 'dutyOrgName', align: 'left', width: 140, sortable: false},
    {
      text: '成本预算（元）', dataIndex: 'costAmount', align: 'right', width: 110,
      renderer: Ext.util.Format.numberRenderer('0,0.00')
    },
    {text: '创建人', dataIndex: 'creatorName', align: 'center', width: 80, sortable: false},
    {text: '状态', dataIndex: 'statusName', align: 'center', width: 120, sortable: false}
  ],
  initComponent: function() {

    this.addEvents({'nameClick': true});
    ;
    this.callParent(arguments);
  },
  statics: {
    view: function(id) {
      Ext.ComponentQuery.query('projectTaskGrid')[0].fireEvent('nameClick', id);
    }
  }
});
