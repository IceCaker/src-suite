Ext.define("Divide.view.MainPanel", {
  extend: "Ext.panel.Panel",
  alias: "widget.divideMainPanel",
  layout: 'border',
  border: false,
  bodyStyle: {
    background: "#FFFFFF"
  },
  bodyPadding: 5,
  items: []
});
