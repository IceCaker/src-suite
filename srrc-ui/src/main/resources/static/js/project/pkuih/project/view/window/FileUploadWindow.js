Ext.define("Project.view.window.FileUploadWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.fileUploadWindow",
  title: "文件上传",
  plain: true,
  layout: 'fit',
  closeAction: 'destroy',
  modal:true,
  getResourceId: function(){
    return '';
  },
  uploadFunc: function(){},
  items: [{
    xtype: "form",
    frame: false,
    width: 400,
    bodyPadding: "10 20 10 20",
    closeAction: 'destroy',
    fieldDefaults: {
      labelAlign: 'left',
      labelWidth: 80,
      anchor: '100%'
    },
    bodyStyle: "border: 1px silver solid",
    items: [
      {
        xtype: 'textfield', name: 'showName', fieldLabel: "文件名", allowBlank: false, readOnly: true
      },
      {
        xtype: 'hiddenfield', name: 'name', allowBlank: false
      },
      {
        xtype: 'textareafield', name: 'description', fieldLabel: "描述", maxLength:50, allowBlank: true
      },
      {
        xtype: 'filefield',
        name: 'file',
        fieldLabel: "文件路径",
        buttonText: "请选择文件..",
        allowBlank: false,
        listeners:{
          change:function(fileCom, value, optis){
            var file = value.toString();
            if(file != null && file != ''){
              var fileNameToString = file.split('\\');
              var fileName = fileNameToString[fileNameToString.length-1];

              var file_name = this.up("form").down("textfield");
              file_name.setValue(fileName)
              var hidden_name = this.up("form").down("hiddenfield");
              hidden_name.setValue(fileName);
            }
          }
        }
      }
    ]
  }],
  buttons:[{
    text: "上传",
    handler: function(){
      var $resourceId = this.up("window").getResourceId();
      var $this = this;
      var form = this.up("window").down("form");
      if (!form.getForm().isValid()) {
        return false;
      }
      var fileName = form.down("textfield").getValue();
      if(fileName == null || fileName == ''){
        Ext.Msg.alert("文件名不能为空！");
        return;
      }
      var file = form.down("filefield").getValue();
      if(file == null || file == ''){
        Ext.Msg.alert("请选择上传文件");
        return;
      }
      var mag = Ext.MessageBox.show({
        msg: "正在上传附件...",
        width: 300,
        wait: true,
        waitConfig: {interval: 200},
        icon: Ext.MessageBox.wait,
        iconHeight: 50
      });
      form.getForm().submit({
        url: contextPath + '/foundation/service/document/upload',
        params: {
          resourceId: $resourceId
        },
        success: function (form, action) {
          mag.hide();
          if(action.result.success){
            $this.up("window").uploadFunc();
            $this.up("window").close();
          }else{
            Common.Msg.Error("上传文件异常！");
          }
        }
      });
    }
  }, {
    text: "取消",
    handler: function(){
      this.up("window").close();
    }
  }],
  showAndRevertFunc: function(getResourceIdFunc, func){
    this.getResourceId = getResourceIdFunc;
    this.uploadFunc = func;
    this.show();
  }
});
