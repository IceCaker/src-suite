Ext.define('Project.view.combo.ExpenseItem', {
  extend: 'Ext.form.ComboBox',
  alias: 'widget.expenseItemCombo',
  width: 180,
  labelWidth: 65,
  fieldLabel: '费用项',
  emptyText: '请选择',
  typeAhead: false,
  editable: false,
  name: 'expenseItemId',
  store: Ext.create('Ext.data.Store', {
    fields: ['id', 'code', 'name'],
    proxy: {
      type: 'ajax',
      url: contextPath + '/api/2.0.0/cql',
      actionMethods: {
        read: 'POST'
      },
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      paramsAsJson: true,
      reader: {
        type: 'json',
        root: 'data'
      }
    },
    autoLoad: true,
    listeners: {
      beforeload: function(store) {
        var dynamicQuery = {
          queryType: 'read',
          type: 'ExpenseItem',
          sorts: [{
            field: 'sortCode',
            direction: 'ASC'
          }],
          page: 1,
          size: 1000000
        };
        store.proxy.extraParams = dynamicQuery;
      }
    }
  }),
  queryMode: 'local',
  displayField: 'name',
  valueField: 'id',
  readCookie: true,
  initComponent: function() {
    var $this = this;

    this.getStore().on('load', function(store) {
      var expenseItemId = $this.value;

      var isSelect = false;
      for (var i = 0; i < store.getTotalCount(); i++) {
        if (store.getAt(i).data.id == expenseItemId) {
          $this.select(store.getAt(i));
          isSelect = true;
          break;
        }
      }
      if (!isSelect) {
        $this.select(store.getAt(0));
      }
    });

    this.callParent(arguments);
  }
});