Ext.define('Financial.view.FinancialDataPanel', {
  extend: 'Ext.panel.Panel',
  alias: "widget.financialDataPanel",
  params: {},
  layout: "fit",
  items: [],
  save: function (refreash) {
    var $this = this;

    Common.CommonManager.showMask(this.title + "数据保存中，请稍候...");

    var buildIndex = this.down("panel").buildIndex;
    var editableConfig = this.down("panel").editableConfig;
    var saveArray = {};

    for (var i = 0; i < editableConfig.length; i++) {
      var editId = "edit_input_" + buildIndex + "_" + editableConfig[i];
      var hideId = "hide_input_" + buildIndex + "_" + editableConfig[i];

      if (document.getElementById(editId).value == 'true') {
        saveArray[editableConfig[i]] = document.getElementById(hideId).value
      }
    }

    if (Object.getOwnPropertyNames(saveArray).length == 0) {
      Common.CommonManager.hideMask();
      return;
    }
    var saveParams = {
      saveDataArray: Ext.encode(saveArray)
    };
    Ext.apply(saveParams, this.params);
    Ext.Ajax.request({
      url: contextPath + '/pkuih/budget/financial',
      params: saveParams,
      timeout: 30000000,
      method: 'POST',
      callback: function (options, success, response) {
        Common.CommonManager.hideMask();
        var result = Ext.JSON.decode(response.responseText);
        if (result.success) {
          if (refreash) {
            $this.load();
          }
        } else {
          Common.Msg.Error(result.message);
        }
      }
    });
  },
  load: function () {
    var $this = this;
    Common.CommonManager.showMask("加载数据中，请稍候...");
    var params = this.params;
    params.sheetIndex = this.sheetIndex;
    Ext.Ajax.request({
      url: contextPath + '/pkuih/budget/financial',
      params: params,
      timeout: 30000000,
      method: 'GET',
      callback: function (options, success, response) {
        Common.CommonManager.hideMask();

        var result = Ext.JSON.decode(response.responseText);
        if (result.success) {
          $this.removeAll();
          $this.add(Common.panel.HtmlPanelBuilder.createPanel(result.data, $this.getWidth(), $this.getHeight()));
        } else {
          Common.Msg.Error(result.message);
        }
      }
    });
  }
});