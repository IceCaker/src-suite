Ext.define("Regulate.view.panel.MainPanel", {
  extend: "Ext.panel.Panel",
  alias: "widget.regulateMainPanel",
  layout: 'border',
  border: false,
  bodyStyle: {
    background: "#FFFFFF"
  },
  bodyPadding: 5,
  items: []
});
