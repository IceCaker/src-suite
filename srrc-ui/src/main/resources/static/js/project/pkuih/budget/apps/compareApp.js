Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');
Ext.Loader.setPath('Pkuih.Common', contextPath + '/static/js/project/pkuih/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.WorkFlowManager',
  "Common.data.ApiReader",
  "Common.combo.ApiCombox",
  "Common.combo.Period",
  "Common.combo.Organization",
  "Common.combo.SetOfBook",
  "Common.combo.ApiDictionaryCombox"
]);

Ext.application({
  name: 'Budget',
  appFolder: contextPath + '/static/js/project/pkuih/budget',
  controllers: ["CompareController"],
  launch: function () {
    Common.CommonManager.init(pageInitParam);
    mainApplication = this;

    var height = window.document.body.clientHeight;
    var width = window.document.body.clientWidth;

    Ext.tip.QuickTipManager.init();
    var initParams = {};

    for (var obj in pageInitParam) {
      initParams[obj] = pageInitParam[obj];
    }
    var panel = Ext.create('Ext.panel.Panel', {
      width: width,
      height: height,
      autoScroll: false,
      layout: 'border',
      border: false,
      bodyStyle: {
        background: "#FFFFFF"
      },
      bodyPadding: 5,
      items: [{
        xtype: "planningSearchToolbar",
        name: "toolbarOne",
        margin: '0 0 5 0',
        hidden: true,
        region: 'north'
      }, {
        xtype: 'toolbar',
        margin: '0 0 5 0',
        items: [{
          id: 'version-compare-tbtext-one',
          xtype: 'tbtext',
          text: '版本一'
        }, '-', {
          id: 'version-compare-tbtext-two',
          xtype: 'tbtext',
          text: '版本二'
        }, '->', {
          xtype: "button",
          id: "version-compare-exportButton",
          text: "导出",
          margin: '4 5 0 0',
          action:'export',
          hidden: false
        }],
        region: 'north'
      }, {
        xtype: 'budgetGroupGrid',
        title: '预算模版',
        border: false,
        collapsible: true,
        split: true,
        scroll: true,
        hideHeaders: true,
        hideCollapseTool: true,
        width: 250,
        region: 'west'
      }, {
        xtype: 'planningTabPanel',
        scroll: true,
        region: 'center'
      }]
    });

    var versions = Ext.create("Common.data.ApiReader").load({
      api: "version",
      joins: [
        {
          type: 'dictionaryOption',
          fields: {
            names: 'versionTypeName'
          },
          conditions: [
            {
              field: 'dictionaryCode',
              condition: 'EQUALS',
              value: "VersionType"
            },
            {
              field: 'code',
              condition: 'EQUALS',
              type: 'Field',
              value: 'type'
            }
          ]
        }
      ],
      conditions: [{
        field: 'id',
        condition: 'IN',
        value: initParams.versionIds
      }]
    });

    if(versionIds.length!=1){
      if(versionIds[0]!=versions[0].id){
        var version = versions[0];
        versions[0] = versions[1];
        versions[1] = version;
      }
    }

    if (initParams.versionIds.length == 1) {
      Ext.getCmp('version-compare-tbtext-one').setText("版本一：当前版本");
      var v = "版本二：" + versions[0].versionTypeName + '(' + moment(versions[0].createTime).format('YYYY-MM-DD HH:mm:ss') + ')';
      Ext.getCmp('version-compare-tbtext-two').setText('<span style="color:blue">' + v + '</span>');
    } else {
      var v1 = "版本一：" + versions[0].versionTypeName + '(' + moment(versions[0].createTime).format('YYYY-MM-DD HH:mm:ss') + ')';
      Ext.getCmp('version-compare-tbtext-one').setText(v1);
      var v2 = "版本二：" + versions[1].versionTypeName + '(' + moment(versions[1].createTime).format('YYYY-MM-DD HH:mm:ss') + ')';
      Ext.getCmp('version-compare-tbtext-two').setText('<span style="color:blue">' + v2 + '</span>');
    }

    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add(panel);
  }
});