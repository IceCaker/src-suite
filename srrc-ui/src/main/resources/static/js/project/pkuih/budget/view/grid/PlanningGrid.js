Ext.define('Budget.view.grid.PlanningGrid', {
  extend: 'Ext.tree.Panel',
  alias: "widget.planningGrid",
  title: "非项目预算",
  rootVisible: false,
  autoScroll: true,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true,
    loadMask: false
  },
  editable: true,
  autoWidth: true,
  defaultNumberColumnWidth: 100,
  isProjectBudget: false,
  dataType: '@@Planning@@',
  isLoading: false,
  store: Ext.create('Ext.data.TreeStore', {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'code', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'showName', type: 'string'},
      {name: 'budgetType', type: 'string'},
      {name: 'isUpdate', type: 'boolean'},
      {name: 'lastYearAmount', type: 'double'},
      {name: 'lastYearAchieved', type: 'double'},
      {name: 'yearAmount', type: 'double'},
      {name: 'm01', type: 'double'},
      {name: 'm02', type: 'double'},
      {name: 'm03', type: 'double'},
      {name: 'm04', type: 'double'},
      {name: 'm05', type: 'double'},
      {name: 'm06', type: 'double'},
      {name: 'm07', type: 'double'},
      {name: 'm08', type: 'double'},
      {name: 'm09', type: 'double'},
      {name: 'm10', type: 'double'},
      {name: 'm11', type: 'double'},
      {name: 'm12', type: 'double'},
      {name: 'q1', type: 'double'},
      {name: 'q2', type: 'double'},
      {name: 'q3', type: 'double'},
      {name: 'q4', type: 'double'},
      {name: 'note', type: 'string'},
      {name: 'sortCode', type: 'string'},
      {name: 'leaf', type: 'boolean'}
    ],
    root: {
      expanded: true,
      children: []
    }
  }),
  columns: [],
  plugins: [],
  loadGrid: function() {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }

    this.columns = [{
      text: "预算项",
      xtype: 'treecolumn',
      dataIndex: 'alias',
      align: 'left',
      minWidth: 240,
      flex: 1,
      menuDisabled: true,
      sortable: false,
      renderer: function(value, meta, record) {
        var str = value;
        if ($this.isProjectBudget && record.data.leaf) {
          str = "<a onclick=\"Budget.view.grid.PlanningGrid.addProjectTab("
            + record.data.id + ",'" + value + "')\">" + value + "</a>";
        }
        return str;
      }
    }];

    if (false && !$this.isProjectBudget) {
      this.columns[this.columns.length] = {
        text: "去年预算",
        dataIndex: 'lastYearAmount',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        locked: true,
        renderer: function(value, meta, record) {
          return Ext.util.Format.number(value, '0,0.00');
        }
      };

      this.columns[this.columns.length] = {
        text: "去年完成",
        dataIndex: 'lastYearAchieved',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        locked: true,
        renderer: function(value, meta, record) {
          return Ext.util.Format.number(value, '0,0.00');
        }
      };
    }

    this.columns[this.columns.length] = {
      text: "预算金额",
      dataIndex: 'yearAmount',
      width: $this.defaultNumberColumnWidth,
      hidden: false,
      sortable: false,
      align: 'right',
      locked: true,
      renderer: function(value, meta, record) {
        if ($this.editable && Common.CommonManager.editor.canSave
          && record.data.leaf && (budgetGroupViewType == ""
          || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "BudgetMonthlyAllocation")) {
          meta.tdCls = 'user-online';
        } else {
          if (value == null || value == '') {
            return '<center>-</center>';
          }
        }
        return Ext.util.Format.number(value, '0,0.00');
      },
      editor: {
        xtype: 'numberfield',
        allowBlank: false,
        selectOnFocus: true,
        hideTrigger: true,
        allowDecimals: true,
        decimalPrecision: 15,
        keyNavEnabled: false,
        mouseWheelEnabled: false,
        fieldStyle: 'text-align: right'
      }
    };

    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      this.columns[this.columns.length] = {
        text: i + "月",
        dataIndex: 'm' + monthStr,
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        renderer: function(value, meta, record) {
          if ($this.editable && Common.CommonManager.editor.canSave
            && record.data.leaf && budgetGroupViewType != "BudgetSummary"
            && budgetGroupViewType != "HumanResourcesSummary") {
            meta.tdCls = 'user-online';
          } else {
            if (value == null || value == '') {
              return '<center>-</center>';
            }
          }
          return Ext.util.Format.number(value, '0,0.00');
        },
        editor: {
          xtype: 'numberfield',
          allowBlank: false,
          selectOnFocus: true,
          hideTrigger: true,
          allowDecimals: true,
          decimalPrecision: 16,
          keyNavEnabled: false,
          mouseWheelEnabled: false,
          fieldStyle: 'text-align: right'
        }
      };
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "HumanResourcesSummary" || budgetGroupViewType == "HumanResourcesMedical"
      || budgetGroupViewType == "HumanResourcesNursing" || budgetGroupViewType == "HumanResourcesTechnology"
      || budgetGroupViewType == "HumanResourcesFunction") {
      this.columns[this.columns.length] = {
        text: "一季度",
        dataIndex: 'q1',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        renderer: function(value, meta, record) {
          var amountSum;
          if ((record.data.m01 != null && record.data.m01 != '')
            || (record.data.m02 != null && record.data.m02 != '')
            || (record.data.m03 != null && record.data.m03 != '')) {
            amountSum = 0.00;
            if (record.data.m01 != '') {
              amountSum += record.data.m01;
            }
            if (record.data.m02 != '') {
              amountSum += record.data.m02;
            }
            if (record.data.m03 != '') {
              amountSum += record.data.m03;
            }
          } else {
            if (value == null || value == '') {
              return '<center>-</center>';
            }
          }
          return Ext.util.Format.number(amountSum, '0,0.00');
        }
      };

      this.columns[this.columns.length] = {
        text: "二季度",
        dataIndex: 'q2',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        renderer: function(value, meta, record) {
          var amountSum;
          if ((record.data.m04 != null && record.data.m04 != '')
            || (record.data.m05 != null && record.data.m05 != '')
            || (record.data.m06 != null && record.data.m06 != '')) {
            amountSum = 0.00;
            if (record.data.m04 != '') {
              amountSum += record.data.m04;
            }
            if (record.data.m05 != '') {
              amountSum += record.data.m05;
            }
            if (record.data.m06 != '') {
              amountSum += record.data.m06;
            }
          } else {
            if (value == null || value == '') {
              return '<center>-</center>';
            }
          }
          return Ext.util.Format.number(amountSum, '0,0.00');
        }
      };

      this.columns[this.columns.length] = {
        text: "三季度",
        dataIndex: 'q3',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        renderer: function(value, meta, record) {
          var amountSum;
          if ((record.data.m07 != null && record.data.m07 != '')
            || (record.data.m08 != null && record.data.m08 != '')
            || (record.data.m09 != null && record.data.m09 != '')) {
            amountSum = 0.00;
            if (record.data.m07 != '') {
              amountSum += record.data.m07;
            }
            if (record.data.m08 != '') {
              amountSum += record.data.m08;
            }
            if (record.data.m09 != '') {
              amountSum += record.data.m09;
            }
          } else {
            if (value == null || value == '') {
              return '<center>-</center>';
            }
          }
          return Ext.util.Format.number(amountSum, '0,0.00');
        }
      };

      this.columns[this.columns.length] = {
        text: "四季度",
        dataIndex: 'q4',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        renderer: function(value, meta, record) {
          var amountSum;
          if ((record.data.m10 != null && record.data.m10 != '')
            || (record.data.m11 != null && record.data.m11 != '')
            || (record.data.m12 != null && record.data.m12 != '')) {
            amountSum = 0.00;
            if (record.data.m10 != '') {
              amountSum += record.data.m10;
            }
            if (record.data.m11 != '') {
              amountSum += record.data.m11;
            }
            if (record.data.m12 != '') {
              amountSum += record.data.m12;
            }
          } else {
            if (value == null || value == '') {
              return '<center>-</center>';
            }
          }
          return Ext.util.Format.number(amountSum, '0,0.00');
        }
      };
    }

    if (false) {
      this.columns[this.columns.length] = {
        text: "备注",
        dataIndex: 'note',
        width: 200,
        renderer: function(value, meta, record) {
          if ($this.editable && Common.CommonManager.editor.canSave) {
            meta.tdCls = 'user-online';
          }
          return "<font title='" + value + "'>" + value + "</font>";
        },
        sortable: false,
        align: 'left'
      };
    }

    this.reconfigure(this.store, this.columns);
  },
  initComponent: function() {
    var $this = this;

    var cellEditor = Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1,
      listeners: {
        beforeedit: function(editor, e) {
          var params = Common.CommonManager.getParams();
          var budgetGroupViewType = params.budgetGroupViewType;
          if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
            budgetGroupViewType = "";
          }

          if (!($this.editable && Common.CommonManager.editor.canSave && e.record.data.leaf)) {
            return false;
          }
          if (e.field == "yearAmount" && (budgetGroupViewType == "BudgetSummary"
            || budgetGroupViewType == "MonthlySummary" || budgetGroupViewType == "BudgetMonthlySummary"
            || budgetGroupViewType == "HumanResourcesSummary" || budgetGroupViewType == "HumanResourcesMedical"
            || budgetGroupViewType == "HumanResourcesNursing" || budgetGroupViewType == "HumanResourcesTechnology"
            || budgetGroupViewType == "HumanResourcesFunction")) {
            return false;
          }
          if (budgetGroupViewType == "BudgetSummary" || budgetGroupViewType == "HumanResourcesSummary") {
            return false;
          }
        },
        afteredit: function(editor, e) {
          //未修改值时不做如何处理
          if (e.originalValue == e.value) {
            return false;
          }

          var params = Common.CommonManager.getParams();
          var budgetGroupViewType = params.budgetGroupViewType;
          if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
            budgetGroupViewType = "";
          }

          if ((budgetGroupViewType == "BudgetSummary" || budgetGroupViewType == "MonthlySummary"
            || budgetGroupViewType == "BudgetMonthlySummary" || budgetGroupViewType == "HumanResourcesSummary"
            || budgetGroupViewType == "HumanResourcesMedical" || budgetGroupViewType == "HumanResourcesNursing"
            || budgetGroupViewType == "HumanResourcesTechnology" || budgetGroupViewType == "HumanResourcesFunction")
            && e.field.substring(0, 1) == "m") {
            var amountSum;
            if (e.record.data.m01 != '' || e.record.data.m02 != '' || e.record.data.m03 != ''
              || e.record.data.m04 != '' || e.record.data.m05 != '' || e.record.data.m06 != ''
              || e.record.data.m07 != '' || e.record.data.m08 != '' || e.record.data.m09 != ''
              || e.record.data.m10 != '' || e.record.data.m11 != '' || e.record.data.m12 != '') {
              amountSum = 0.00;
              if (e.record.data.m01 != '') {
                amountSum += e.record.data.m01;
              }
              if (e.record.data.m02 != '') {
                amountSum += e.record.data.m02;
              }
              if (e.record.data.m03 != '') {
                amountSum += e.record.data.m03;
              }
              if (e.record.data.m04 != '') {
                amountSum += e.record.data.m04;
              }
              if (e.record.data.m05 != '') {
                amountSum += e.record.data.m05;
              }
              if (e.record.data.m06 != '') {
                amountSum += e.record.data.m06;
              }
              if (e.record.data.m07 != '') {
                amountSum += e.record.data.m07;
              }
              if (e.record.data.m08 != '') {
                amountSum += e.record.data.m08;
              }
              if (e.record.data.m09 != '') {
                amountSum += e.record.data.m09;
              }
              if (e.record.data.m10 != '') {
                amountSum += e.record.data.m10;
              }
              if (e.record.data.m11 != '') {
                amountSum += e.record.data.m11;
              }
              if (e.record.data.m12 != '') {
                amountSum += e.record.data.m12;
              }
            }
            e.record.set('yearAmount', amountSum);
          }
        }
      }
    });
    $this.plugins = [cellEditor];

    //加入自定义事件
    this.addEvents({
      "itemNameClick": true,
      "amountClick": true
    });
    this.callParent(arguments);
  },
  loadData: function() {
    var $this = this;
    if ($this.isLoading) {
      return false;
    } else {
      $this.isLoading = true;
    }

    Common.CommonManager.showMask("数据加载中，请稍候...");

    var dataType = $this.dataType;
    var params = Common.CommonManager.getParams();

    //加载Grid
    $this.loadGrid();

    var dynamicQuery = {
      queryType: "read",
      type: "budgetItemGroupItem",
      fields: {
        domainId: "domainId",
        alias: "alias",
        sortCode: "sortCode",
        budgetItemGroupId: "budgetItemGroupId"
      },
      tree: true,
      joins: [
        {
          type: "BudgetItem",
          fields: {
            id: "id",
            code: "code",
            name: "name",
            budgetType: "budgetType",
            leaf: "leaf",
            parentId: "parentId"
          },
          conditions: [
            {
              field: "id",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }
      ],
      associations: [],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: params.budgetGroupId
        }
      ]
    };

    //查询年度数据
    dynamicQuery.associations.push({
      type: "budgetItemGroupItem",
      fields: {
        alias: "alias",
        sortCode: "sortCode",
        budgetItemGroupId: "budgetItemGroupId",
        budgetItemId: "budgetItemId"
      },
      references: {
        budgetItemId: "id"
      },
      joins: [
        {
          type: "annualCostPlanningData",
          fields: {
            amount: "yearAmount"
          },
          conditions: [
            {
              field: "setOfBooksId",
              condition: "EQUALS",
              type: "Integer",
              value: params.setOfBooksId
            },
            {
              field: "period",
              condition: "EQUALS",
              type: "String",
              value: params.period
            },
            {
              field: "organizationId",
              condition: "EQUALS",
              type: "Integer",
              value: params.organizationId
            },
            {
              field: "dataType",
              condition: "STARTS_WITH",
              type: "String",
              value: dataType
            },
            {
              field: "budgetItemGroupId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemGroupId"
            },
            {
              field: "budgetObjectType",
              condition: "EQUALS",
              type: "String",
              value: "BudgetItem"
            },
            {
              field: "budgetObjectId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }
      ],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: params.budgetGroupId
        }
      ]
    });

    //查询月度数据
    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      dynamicQuery.associations[0].joins.push({
        type: "monthlyCostPlanningData",
        fields: {
          amount: 'm' + monthStr
        },
        conditions: [
          {
            field: "setOfBooksId",
            condition: "EQUALS",
            type: "Integer",
            value: params.setOfBooksId
          },
          {
            field: "period",
            condition: "EQUALS",
            type: "String",
            value: params.period.substring(0, 4) + monthStr
          },
          {
            field: "organizationId",
            condition: "EQUALS",
            type: "Integer",
            value: params.organizationId
          },
          {
            field: "dataType",
            condition: "STARTS_WITH",
            type: "String",
            value: dataType
          },
          {
            field: "budgetItemGroupId",
            condition: "EQUALS",
            type: "Field",
            value: "budgetItemGroupId"
          },
          {
            field: "budgetObjectType",
            condition: "EQUALS",
            type: "String",
            value: "BudgetItem"
          },
          {
            field: "budgetObjectId",
            condition: "EQUALS",
            type: "Field",
            value: "budgetItemId"
          }
        ]
      });
    }

    //加载数据
    Ext.Ajax.request({
      url: contextPath + '/api/2.0.0/cql',
      method: 'POST',
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      params: Ext.encode(dynamicQuery),
      paramsAsJson: true,
      success: function(response, operation) {
        var responseJson = Ext.JSON.decode(response.responseText);
        $this.setRootNode({
          text: '根节点',
          expanded: true,
          children: responseJson.children
        });

        $this.isLoading = false;
        Common.CommonManager.hideMask();
      }
    });
  },
  handlePlanningData: function() {
    var $this = this;
    var dataType = $this.dataType;
    var params = Common.CommonManager.getParams();
    var year = params.period.substring(0, 4);
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }

    var result = {message: ''};
    var annualEditData = [];
    var monthlyEditData = [];

    var modifiedRecords = $this.getStore().getModifiedRecords();
    var budgetItemIds = [];
    for (var i = 0; i < modifiedRecords.length; i++) {
      budgetItemIds[i] = modifiedRecords[i].data.id;
    }

    if (budgetItemIds.length == 0) {
      result.message = "未修改数据！<br/>";
      return result;
    }

    var annualData = Ext.create("Common.data.ApiReader").load({
      api: 'annualCostPlanningData',
      conditions: [
        {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        },
        {
          field: 'period',
          condition: 'EQUALS',
          type: 'String',
          value: params.period
        },
        {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        },
        {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: dataType
        }, {
          field: 'budgetItemGroupId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.budgetGroupId
        },
        {
          field: "budgetObjectType",
          condition: "EQUALS",
          type: "String",
          value: "BudgetItem"
        },
        {
          field: "budgetObjectId",
          condition: "IN",
          value: budgetItemIds
        }
      ]
    });

    var monthlyData = Ext.create("Common.data.ApiReader").load({
      api: 'monthlyCostPlanningData',
      conditions: [
        {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        },
        {
          field: 'period',
          condition: 'STARTS_WITH',
          type: 'String',
          value: year
        },
        {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        },
        {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: dataType
        }, {
          field: 'budgetItemGroupId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.budgetGroupId
        },
        {
          field: "budgetObjectType",
          condition: "EQUALS",
          type: "String",
          value: "BudgetItem"
        },
        {
          field: "budgetObjectId",
          condition: "IN",
          value: budgetItemIds
        }
      ]
    });

    if (dataType.split('@@')[2] == null || dataType.split('@@')[2] == "") {
      if (params.budgetType == "Fund") {
        dataType += "Fund@@";
      } else if (params.budgetType == "Expenses") {
        dataType += "Expenses@@";
      } else {
        dataType += "Cost@@";
      }
    }

    for (var i = 0; i < modifiedRecords.length; i++) {
      var modifiedRecord = modifiedRecords[i];
      var isAddYear = true, isAddM01 = true, isAddM02 = true, isAddM03 = true, isAddM04 = true,
        isAddM05 = true, isAddM06 = true, isAddM07 = true, isAddM08 = true,
        isAddM09 = true, isAddM10 = true, isAddM11 = true, isAddM12 = true;

      //处理年度数据
      var yearAmount = 0.00;
      if (modifiedRecord.data.yearAmount != null && modifiedRecord.data.yearAmount != ''
        && modifiedRecord.data.yearAmount != 0) {
        yearAmount = modifiedRecord.data.yearAmount;
        for (var j = 0; j < annualData.length; j++) {
          if (annualData[j].budgetObjectId == modifiedRecord.data.id) {
            annualData[j].amount = yearAmount;
            annualEditData[annualEditData.length] = annualData[j];
            annualData[j].isUpdate = true;
            isAddYear = false;
            break;
          }
        }

        if (isAddYear) {
          annualEditData[annualEditData.length] = {
            domainId: params.domainId,
            setOfBooksId: params.setOfBooksId,
            period: params.period,
            organizationId: params.organizationId,
            dataType: dataType,
            status: 'Draft',
            budgetObjectType: 'BudgetItem',
            budgetObjectId: modifiedRecord.data.id,
            budgetItemGroupId: params.budgetGroupId,
            amount: yearAmount
          };
        }
      }

      //处理月度数据
      var totalAmount = 0.00;
      for (var j = 0; j < monthlyData.length; j++) {
        if (monthlyData[j].budgetObjectId == modifiedRecord.data.id) {
          if (monthlyData[j].period == year + '01' && modifiedRecord.data.m01 != null
            && modifiedRecord.data.m01 != '' && modifiedRecord.data.m01 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m01;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM01 = false;
          }
          if (monthlyData[j].period == year + '02' && modifiedRecord.data.m02 != null
            && modifiedRecord.data.m02 != '' && modifiedRecord.data.m02 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m02;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM02 = false;
          }
          if (monthlyData[j].period == year + '03' && modifiedRecord.data.m03 != null
            && modifiedRecord.data.m03 != '' && modifiedRecord.data.m03 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m03;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM03 = false;
          }
          if (monthlyData[j].period == year + '04' && modifiedRecord.data.m04 != null
            && modifiedRecord.data.m04 != '' && modifiedRecord.data.m04 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m04;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM04 = false;
          }
          if (monthlyData[j].period == year + '05' && modifiedRecord.data.m05 != null
            && modifiedRecord.data.m05 != '' && modifiedRecord.data.m05 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m05;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM05 = false;
          }
          if (monthlyData[j].period == year + '06' && modifiedRecord.data.m06 != null
            && modifiedRecord.data.m06 != '' && modifiedRecord.data.m06 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m06;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM06 = false;
          }
          if (monthlyData[j].period == year + '07' && modifiedRecord.data.m07 != null
            && modifiedRecord.data.m07 != '' && modifiedRecord.data.m07 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m07;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM07 = false;
          }
          if (monthlyData[j].period == year + '08' && modifiedRecord.data.m08 != null
            && modifiedRecord.data.m08 != '' && modifiedRecord.data.m08 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m08;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM08 = false;
          }
          if (monthlyData[j].period == year + '09' && modifiedRecord.data.m09 != null
            && modifiedRecord.data.m09 != '' && modifiedRecord.data.m09 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m09;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM09 = false;
          }
          if (monthlyData[j].period == year + '10' && modifiedRecord.data.m10 != null
            && modifiedRecord.data.m10 != '' && modifiedRecord.data.m10 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m10;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM10 = false;
          }
          if (monthlyData[j].period == year + '11' && modifiedRecord.data.m11 != null
            && modifiedRecord.data.m11 != '' && modifiedRecord.data.m11 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m11;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM11 = false;
          }
          if (monthlyData[j].period == year + '12' && modifiedRecord.data.m12 != null
            && modifiedRecord.data.m12 != '' && modifiedRecord.data.m12 != 0) {
            monthlyData[j].amount = modifiedRecord.data.m12;
            monthlyEditData[monthlyEditData.length] = monthlyData[j];
            monthlyData[j].isUpdate = true;
            isAddM12 = false;
          }
        }
      }

      if (isAddM01 && modifiedRecord.data.m01 != null
        && modifiedRecord.data.m01 != '' && modifiedRecord.data.m01 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '01',
          organizationId: params.organizationId,
          dataType: dataType,
          status: 'Draft',
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m01
        };
      }
      if (isAddM02 && modifiedRecord.data.m02 != null
        && modifiedRecord.data.m02 != '' && modifiedRecord.data.m02 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '02',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m02
        };
      }
      if (isAddM03 && modifiedRecord.data.m03 != null
        && modifiedRecord.data.m03 != '' && modifiedRecord.data.m03 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '03',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m03
        };
      }
      if (isAddM04 && modifiedRecord.data.m04 != null
        && modifiedRecord.data.m04 != '' && modifiedRecord.data.m04 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '04',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m04
        };
      }
      if (isAddM05 && modifiedRecord.data.m05 != null
        && modifiedRecord.data.m05 != '' && modifiedRecord.data.m05 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '05',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m05
        };
      }
      if (isAddM06 && modifiedRecord.data.m06 != null
        && modifiedRecord.data.m06 != '' && modifiedRecord.data.m06 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '06',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m06
        };
      }
      if (isAddM07 && modifiedRecord.data.m07 != null
        && modifiedRecord.data.m07 != '' && modifiedRecord.data.m07 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '07',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m07
        };
      }
      if (isAddM08 && modifiedRecord.data.m08 != null
        && modifiedRecord.data.m08 != '' && modifiedRecord.data.m08 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '08',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m08
        };
      }
      if (isAddM09 && modifiedRecord.data.m09 != null
        && modifiedRecord.data.m09 != '' && modifiedRecord.data.m09 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '09',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m09
        };
      }
      if (isAddM10 && modifiedRecord.data.m10 != null
        && modifiedRecord.data.m10 != '' && modifiedRecord.data.m10 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '10',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m10
        };
      }
      if (isAddM11 && modifiedRecord.data.m11 != null
        && modifiedRecord.data.m11 != '' && modifiedRecord.data.m11 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '11',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m11
        };
      }
      if (isAddM12 && modifiedRecord.data.m12 != null
        && modifiedRecord.data.m12 != '' && modifiedRecord.data.m12 != 0) {
        monthlyEditData[monthlyEditData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: year + '12',
          status: 'Draft',
          organizationId: params.organizationId,
          dataType: dataType,
          budgetObjectType: 'BudgetItem',
          budgetObjectId: modifiedRecord.data.id,
          budgetItemGroupId: params.budgetGroupId,
          amount: modifiedRecord.data.m12
        };
      }

      //汇总月度合计
      if (modifiedRecord.data.m01 != null && modifiedRecord.data.m01 != '') {
        totalAmount += modifiedRecord.data.m01;
      }
      if (modifiedRecord.data.m02 != null && modifiedRecord.data.m02 != '') {
        totalAmount += modifiedRecord.data.m02;
      }
      if (modifiedRecord.data.m03 != null && modifiedRecord.data.m03 != '') {
        totalAmount += modifiedRecord.data.m03;
      }
      if (modifiedRecord.data.m04 != null && modifiedRecord.data.m04 != '') {
        totalAmount += modifiedRecord.data.m04;
      }
      if (modifiedRecord.data.m05 != null && modifiedRecord.data.m05 != '') {
        totalAmount += modifiedRecord.data.m05;
      }
      if (modifiedRecord.data.m06 != null && modifiedRecord.data.m06 != '') {
        totalAmount += modifiedRecord.data.m06;
      }
      if (modifiedRecord.data.m07 != null && modifiedRecord.data.m07 != '') {
        totalAmount += modifiedRecord.data.m07;
      }
      if (modifiedRecord.data.m08 != null && modifiedRecord.data.m08 != '') {
        totalAmount += modifiedRecord.data.m08;
      }
      if (modifiedRecord.data.m09 != null && modifiedRecord.data.m09 != '') {
        totalAmount += modifiedRecord.data.m09;
      }
      if (modifiedRecord.data.m10 != null && modifiedRecord.data.m10 != '') {
        totalAmount += modifiedRecord.data.m10;
      }
      if (modifiedRecord.data.m11 != null && modifiedRecord.data.m11 != '') {
        totalAmount += modifiedRecord.data.m11;
      }
      if (modifiedRecord.data.m12 != null && modifiedRecord.data.m12 != '') {
        totalAmount += modifiedRecord.data.m12;
      }

      if (parseFloat(totalAmount).toFixed(2) != parseFloat(yearAmount).toFixed(2) && budgetGroupViewType.indexOf('MonthlySummary') == -1
        && budgetGroupViewType.indexOf('HumanResources') == -1) {
        var tempMessage = "【" + modifiedRecord.data.alias + "】预算金额 " + Ext.util.Format.number(yearAmount, '0,0.00');
        tempMessage += " 不等于月度合计 " + Ext.util.Format.number(totalAmount, '0,0.00') + "；<br/>";
        result.message += tempMessage;
      }
    }

    result.annualEditData = annualEditData;
    result.monthlyEditData = monthlyEditData;

    //处理删除数据
    var deleteAnnualData = [];
    var deleteMonthlyData = [];

    for (var i = 0; i < annualData.length; i++) {
      if (annualData[i].isUpdate == undefined || !annualData[i].isUpdate) {
        deleteAnnualData[deleteAnnualData.length] = annualData[i];
      }
    }

    for (var i = 0; i < monthlyData.length; i++) {
      if (monthlyData[i].isUpdate == undefined || !monthlyData[i].isUpdate) {
        deleteMonthlyData[deleteMonthlyData.length] = monthlyData[i];
      }
    }

    result.deleteAnnualData = deleteAnnualData;
    result.deleteMonthlyData = deleteMonthlyData;

    return result;
  },
  statics: {
    addProjectTab: function(budgetItemId, budgetItemName) {
      Ext.ComponentQuery.query('planningGrid')[0].fireEvent("itemNameClick", budgetItemId, budgetItemName);
    },
    view: function(budgetItemId) {
      Ext.ComponentQuery.query('planningGrid')[0].fireEvent("amountClick", budgetItemId);
    }
  }
});