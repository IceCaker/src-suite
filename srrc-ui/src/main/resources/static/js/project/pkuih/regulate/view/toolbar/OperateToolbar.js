Ext.define("Regulate.view.toolbar.OperateToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.regulateOperateToolbar",
  items: [
    {
      text: "新建",
      action: "create",
      id: this.id + "-create"
    },
    {
      text: "提交",
      action: "submit",
      id: this.id + "-submit"
    }
  ]
});
