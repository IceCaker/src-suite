Ext.define("Setting.view.benchmark.MainPanel", {
  extend: "Ext.panel.Panel",
  alias: "widget.benchmarkMainPanel",
  layout: 'border',
  border: false,
  bodyStyle: {
    background: "#FFFFFF"
  },
  bodyPadding: 5,
  items: []
});
