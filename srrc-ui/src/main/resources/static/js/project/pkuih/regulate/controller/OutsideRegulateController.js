Ext.define('Regulate.controller.OutsideRegulateController', {
  extend: 'Ext.app.Controller',
  views: ['panel.MainPanel', 'toolbar.SearchToolbar', 'toolbar.OperateToolbar',
    'grid.RegulateGrid', 'panel.RegulateBasePanel', 'panel.RegulateFormPanel',
    'grid.RegulateItemGrid', 'window.RegulateDetailWindow', 'toolbar.RegulateDetailToolbar',
    'grid.RegulateProjectGrid', 'grid.RegulateNotProjectGrid', 'combo.BudgetItem',
    'window.RegulateSelectWindow', 'toolbar.RegulateSelectToolbar', 'grid.RegulateSelectGrid',
    'window.RegulateCreateWindow'],
  refs: [
    {ref: "regulateSearchToolbar", selector: 'regulateSearchToolbar'},
    {ref: "regulateMainPanel", selector: 'regulateMainPanel'},
    {ref: "regulateBasePanel", selector: 'regulateBasePanel'},
    {ref: "regulateGrid", selector: 'regulateGrid'}
  ],
  init: function () {
    var $this = this;
    $this.control({
      'regulateMainPanel': {
        'render': function () {
          $this.addPanelFunc();
        }
      },
      'regulateOperateToolbar button[action=create]': {
        click: function () {
          $this.create(true)
        }
      },
      'regulateBasePanel button[action=cancel]': {
        click: function () {
          $this.create(false)
        }
      },
      'regulateOperateToolbar button[action=submit]': {click: this.submit}
    });
    Common.CommonManager.getParams = this.getParams;
    Common.CommonManager.loadGrid = this.loadGrid;
  },
  addPanelFunc: function () {
    var $this = this;

    $this.getRegulateMainPanel().add({
      id: 'regulate-search-toolbar',
      xtype: 'regulateSearchToolbar',
      region: 'north'
    });

    $this.getRegulateMainPanel().add({
      xtype: 'regulateGrid',
      itemId: 'regulateGrid',
      region: 'center',
      regulateType: "outside",
    });

    $this.getRegulateMainPanel().add({
      xtype: 'regulateBasePanel',
      region: 'center',
      hidden: true,
      regulateType: "outside",
    });
  },
  create: function (isShow) {
    if (isShow) {
      this.getRegulateSearchToolbar().hide();
      this.getRegulateGrid().hide();
      this.getRegulateBasePanel().show();
    } else {
      this.getRegulateSearchToolbar().show();
      this.getRegulateGrid().show();
      this.getRegulateBasePanel().hide();
    }
  },
  submit: function () {
    alert('submit');
  },
  getParams: function () {
    var params = {};
    for (var str in pageInitParam) {
      params[str] = pageInitParam[str];
    }

    var queryButton = Ext.getCmp('regulate-search-queryButton');
    if (queryButton != null && queryButton != undefined) {
      for (var str in queryButton.getParams()) {
        params[str] = queryButton.getParams()[str];
      }
    }
    return params;
  }
});