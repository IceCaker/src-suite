Ext.require([
  "Common.text.CommonAmount"
]);
Ext.define('Pkuih.Common.view.ProjectFormPanel', {
  extend: 'Ext.form.Panel',
  alias: "widget.projectFormPanel",
  layout: "column",
  border: false,
  autoWidth: true,
  defaults: {
    columnWidth: 3.3 / 10,
    margin: "5 2 5 2"
  },
  items: [{
    xtype: "hiddenfield",
    name: "id"
  }, {
    id: "project-textfield-projectCode",
    xtype: "textfield",
    name: "code",
    fieldLabel: "项目编码",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    emptyText: '系统自动生成',
    readOnly: true
  }, {
    id: "project-textfield-projectName",
    xtype: "textfield",
    name: "name",
    fieldLabel: "项目名称",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    columnWidth: 6.6 / 10,
    allowBlank: false,
    blankText: "请输入项目名称",
    editable: true,
    fieldStyle: 'background-color:#FFFACD;'
  }],
  getProjectId: function() {
    return this.up("projectInfoWindow").projectId;
  },
  getVersionId: function() {
    return this.up("projectInfoWindow").versionId;
  },
  loadProject: function () {
    var $this = this;
    var projectId = $this.getProjectId();
    var versionId = $this.getVersionId();

    if (projectId <= 0) {
      return false;
    }

    var dynamicQuery = {
      api: versionId > 0 ? 'projectVersion' : 'project',
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        value: projectId
      }]
    };

    if (versionId > 0) {
      dynamicQuery.conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });
    }

    var projects = Ext.create("Common.data.ApiReader").load(dynamicQuery);

    if(projects != null && projects[0] != null) {
      $this.getForm().setValues(projects[0]);
    }
  },
  saveProject: function() {
    var $this = this;
    var projectId = $this.getProjectId();

    var isSuccess = false;
    var isUpdate = false;
    if (projectId > 0) {
      isUpdate = true;
    }

    if ($this.getForm().isValid()) {
      var project = $this.getFormValues();
      var params = this.up("projectInfoWindow").params;

      //组装其它项目信息
      project.domainId = params.domainId;
      project.setOfBooksId = params.setOfBooksId;
      project.period = params.period;
      project.managerId = userId;
      project.projectCategoryId = params.projectCategoryId;
      project.organizationId = params.organizationId;
      project.status = 'Draft';
      //项目开始结束时间
      if (project.period != undefined && project.period != null && project.period != ""
        && project.period.length >= 4 && (project.startTime == undefined || project.startTime == ''
        || project.endTime == undefined || project.endTime == '')) {
        project.startTime = project.period.substring(0, 4) + "-01-01T00:00:00.000+0800";
        project.endTime = project.period.substring(0, 4) + "-12-31T23:59:59.000+0800";
      }

      if (projectId > 0) {
        project.id = projectId;
      } else {
        delete project.id;
      }

      var callback = function (result) {
        var projectData = result[0].data;
        $this.getForm().setValues(projectData);

        if (!isUpdate) {
          $this.up("projectInfoWindow").projectId = projectData.id;
          $this.up("projectInfoWindow").resourceId = projectData.resourceId;
        }
        isSuccess = true;
      };

      Ext.create("Common.data.ApiReader")
        .createOrUpdate('project', [project], callback, false);
    } else {
      Common.Msg.Alert("请选择或输入必填项！");
    }

    return isSuccess;
  },
  getFormValues: function() {
    var values = this.getForm().getValues();
    Common.text.CommonAmount.getValuesFromForm(values, this);

    //项目开始结束时间
    if (values.period != undefined && values.period != null && values.period != "" && values.period.length >= 4) {
      values.startTime = values.period.substring(0, 4) + "-01-01T00:00:00.000+0800";
      values.endTime = values.period.substring(0, 4) + "-12-31T23:59:59.000+0800";
    }

    return values;
  },
  setEditedFieldStyle: function(form, style) {
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item['editable']) != 'undefined' && item.editable && typeof(item['setFieldStyle']) != 'undefined') {
        item.setFieldStyle(style);
      }
      }
    },
    setReadOnly: function(form) {
      for (var i = 0; i < form.items.items.length; i++) {
        var item = form.items.items[i];
        if (typeof(item['editable']) != 'undefined' && item.editable) {
          item.setReadOnly(true);
        }
      }
    },
    setEditable: function(form) {
      for (var i = 0; i < form.items.items.length; i++) {
        var item = form.items.items[i];
        if (typeof(item['editable']) != 'undefined' && item.editable) {
          item.setReadOnly(false);
      }
    }
  }
})
;