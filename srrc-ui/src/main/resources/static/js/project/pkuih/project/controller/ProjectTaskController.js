Ext.define('Project.controller.ProjectTaskController', {
  extend: 'Ext.app.Controller',
  views: [
    "task.ProjectTaskGrid"
  ],
  refs: [ ],
  init: function () {
    var $this = this;
    this.control({

    });
    // 由于输入备注时需要回车键，暂时去掉这个监听
    //this.addEvent();
  },
  getParams: function(){
    return this.getProjectFlexibleToolbar().getValues();
  }
});
