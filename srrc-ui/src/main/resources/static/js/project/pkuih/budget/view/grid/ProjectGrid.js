Ext.define('Budget.view.grid.ProjectGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.projectGrid',
  title: "关联项目",
  autoScroll: true,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  autoWidth: true,
  viewConfig: {
    loadMask: false,
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  isProjectBudget: true,
  isProjectManage: true,
  budgetItemId: -1,
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'projectId', type: 'string'},
      {name: 'projectCode', type: 'string'},
      {name: 'projectName', type: 'string'},
      {name: 'projectItemId', type: 'int'},
      {name: 'projectItemCode', type: 'string'},
      {name: 'projectItemName', type: 'string'},
      {name: 'expenseItemId', type: 'string'},
      {name: 'expenseItemName', type: 'string'},
      {name: 'organizationId', type: 'int'},
      {name: 'organizationName', type: 'string'},
      {name: 'amount', type: 'double'}
    ],
    api: "relatedProjectItem",
    sorts: [{
      field: 'projectCode',
      direction: 'DESC'
    }],
    pageSize: 50,
    listeners: {
      beforeload: function(store, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      load: function(store) {
        Common.CommonManager.hideMask();
      }
    },
    groupField: "projectCode"
  }),
  features: [{
    ftype: 'groupingsummary',
    groupHeaderTpl: "<a onclick=\"Budget.view.grid.ProjectGrid.edit('{[values.rows[0].data[\"projectId\"]]}')\">{[values.rows[0].data['projectName']]}[{[values.rows[0].data['projectCode']]}]</a>",
    hideGroupedHeader: true,
    enableGroupingMenu: false
  }],
  listeners: {
    groupclick: function(view, node, group, e, eOpts) {
      view.features[0].expand(group);
    }
  },
  columns: [
    {
      text: '费用行编号', dataIndex: 'projectItemCode', align: 'left', width: 160, sortable: false,
      summaryRenderer: function(value, summaryData, dataIndex) {
        return '合计：';
      }
    },
    {text: '费用行名称', dataIndex: 'projectItemName', align: 'left', width: 140, sortable: false, flex: 1},
    {text: '费用项名称', dataIndex: 'expenseItemName', align: 'left', width: 150, sortable: false},
    {text: '承担部门', dataIndex: 'organizationName', align: 'left', width: 140, sortable: false},
    {
      text: '费用项预算', oldText: null, dataIndex: 'amount', align: 'right', width: 120,
      summaryType: 'sum',
      renderer: function(value, meta, record) {
        if (value != null && value != '') {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '-';
        }
      }
    }
  ],
  initComponent: function() {
    this.bbar = Ext.create('Ext.PagingToolbar', {
      store: this.store,
      displayInfo: true,
      displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
      emptyMsg: '没有数据'
    });
    this.addEvents({'nameClickEdit': true});
    this.callParent(arguments);
  },
  loadData: function() {
    var params = Common.CommonManager.getParams();

    //组装数据过滤条件
    this.getStore().conditions = [{
      field: 'domainId',
      condition: 'EQUALS',
      type: 'String',
      value: '#domainId#'
    }, {
      field: 'setOfBooksId',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#setOfBooksId#'
    }, {
      field: 'period',
      condition: 'EQUALS',
      type: 'String',
      value: '#period#'
    }, {
      field: 'budgetObjectId',
      enabled: '"#budgetItemId#" != ""',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#budgetItemId#'
    }, {
      field: 'budgetItemGroupId',
      enabled: '"#budgetGroupId#" != ""',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#budgetGroupId#'
    }];

    if (params.organizationLeaf) {
      this.getStore().api = "relatedProjectItem";

      //组装表连接
      this.getStore().joins = [{
        type: 'project',
        fields: {
          code: 'projectCode',
          name: 'projectName'
        },
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectId'
        }]
      }, {
        type: 'projectItem',
        fields: {
          code: 'projectItemCode',
          description: 'projectItemName',
          expenseItemId: 'expenseItemId'
        },
        joins: [{
          type: 'expenseItem',
          fields: {
            name: 'expenseItemName'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'expenseItemId'
            }
          ]
        }],
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectItemId'
        }]
      }];

      this.getStore().joins.push({
        type: 'organization',
        fields: {
          name: 'organizationName',
          sortCode: 'organizationSortCode'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      });

      this.getStore().conditions.push({
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: '#organizationId#'
      });
    } else {
      this.getStore().api = "relatedProjectItemVersion";

      //组装表连接
      this.getStore().joins = [{
        type: 'projectVersion',
        fields: {
          code: 'projectCode',
          name: 'projectName'
        },
        conditions: [{
          field: 'versionId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'versionId'
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectId'
        }]
      }, {
        type: 'projectItemVersion',
        fields: {
          code: 'projectItemCode',
          description: 'projectItemName',
          expenseItemId: 'expenseItemId'
        },
        joins: [{
          type: 'expenseItem',
          fields: {
            name: 'expenseItemName'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'expenseItemId'
            }
          ]
        }],
        conditions: [{
          field: 'versionId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'versionId'
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectItemId'
        }]
      }];

      this.getStore().joins.push({
        type: 'organization',
        fields: {
          name: 'organizationName',
          sortCode: 'organizationSortCode'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      });

      this.getStore().joins.push({
        type: 'budgetDataStatus',
        fields: {
          versionId: 'statusVersionId'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'setOfBooksId'
        }, {
          field: 'period',
          condition: 'EQUALS',
          type: 'Field',
          value: 'period'
        }, {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'organizationId'
        }]
      });

      this.getStore().conditions.push({
        field: 'organizationSortCode',
        condition: 'STARTS_WITH',
        value: '#organizationSortCode#'
      });

      this.getStore().conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        type: 'Field',
        value: 'statusVersionId'
      });
    }

    var budgetItemId = '';
    if (this.budgetItemId > 0) {
      budgetItemId = this.budgetItemId;
    }

    var budgetGroupId = '';
    if (params.budgetGroupId > 0
      && (params.budgetGroupViewType != undefined && params.budgetGroupViewType != "BudgetSummary")) {
      budgetGroupId = params.budgetGroupId;
    }

    this.getStore().loadData({
      params: {
        domainId: params.domainId,
        setOfBooksId: params.setOfBooksId,
        period: params.period,
        organizationId: params.organizationId,
        organizationSortCode: params.organizationSortCode,
        budgetItemId: budgetItemId,
        budgetGroupId: budgetGroupId
      }
    });
  },
  statics: {
    edit: function(projectId) {
      Ext.ComponentQuery.query('projectGrid')[0].fireEvent('nameClickEdit', projectId);
    }
  }
});