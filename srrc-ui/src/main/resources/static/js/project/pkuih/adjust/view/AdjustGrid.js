Ext.define('Adjust.view.AdjustGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.adjustGrid',
  viewConfig: {
    loadMask: false,
    forceFit: true
  },
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'id', type: 'string'},
      {name: 'organizationName', type: 'string'},
      {name: 'organizationId', type: 'string'},
      {name: 'budgetItemId', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'budgetAmount', type: 'double'},
      {name: 'adjustAmount', type: 'double'},
      {name: 'adjustAfterAmount', type: 'double'}
    ]
  }),
  plugins: [
    Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1
    })
  ],
  selModel: Ext.create('Ext.selection.CheckboxModel', {checkOnly: true}),
  columns: [],
  subscription: {
    observers: ['adjust-operate-query', 'adjust-operate-save', 'adjust-operate-export'],
    operate: {
      onNext: function (params) {
        var $this = this;
        Common.CommonManager.showSpecialMask(function() {
          $this.loadData(params);
        },"加载中，请稍候...");
      }
    }
  },
  loadGrid: function (params) {
    var organizations = Ext.create("Common.data.ApiReader").load({
      api: "organization",
      conditions: [
        {
          field: 'domainId',
          condition: 'EQUALS',
          value: domainId
        }, {
          field: 'id',
          condition: 'IN',
          value: params.organizationId
        }
      ],
      sorts: [{
        field: 'sortCode',
        direction: 'ASC'
      }]
    });
    var fields = [
      {name: 'id', type: 'string'},
      {name: 'organizationName', type: 'string'},
      {name: 'organizationId', type: 'string'},
      {name: 'budgetItemId', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'alias', type: 'string'}
    ];
    for (var i = 0; i < organizations.length; i++) {
      fields.push({name: 'budgetAmount' + organizations[i].id, type: 'double'});
      fields.push({name: 'adjustAmount' + organizations[i].id, type: 'double'});
      fields.push({name: 'adjustAfterAmount' + organizations[i].id, type: 'double'});
    }
    this.store.model.setFields(fields);

    this.columns = [];
    this.columns[this.columns.length] = {
      text: '预算项',
      dataIndex: 'alias',
      align: 'left',
      flex: 1,
      minWidth: 200,
      locked: true,
      menuDisabled: true,
      sortable: false
    };
    for (var i = 0; i < organizations.length; i++) {
      var organization = organizations[i];

      this.columns[this.columns.length] = {
        text: organization.name,
        menuDisabled: true,
        sortable: false,
        columns: [{
          text: '预算金额',
          dataIndex: 'budgetAmount' + organization.id,
          align: 'right',
          flex: 1,
          width: 120,
          menuDisabled: true,
          sortable: false,
          renderer: function (value, meta, record) {
            if (value != null) {
              return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
                + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
            } else {
              return '0.00';
            }
          }
        }, {
          text: '调整值',
          dataIndex: 'adjustAmount' + organization.id,
          align: 'right',
          width: 120,
          menuDisabled: true,
          sortable: false,
          renderer: function (value, meta, record) {
            if (value != null) {
              return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
                + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
            } else {
              return '0.00';
            }
          }
        }, {
          text: '调整后预算',
          oldText: null,
          dataIndex: 'adjustAfterAmount' + organization.id,
          align: 'right',
          width: 120,
          menuDisabled: true,
          sortable: false,
          renderer: function (value, meta, record) {
            if (value != null) {
              return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
                + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
            } else {
              return '0.00';
            }
          }
        }]
      };
    }
    this.reconfigure(this.store, this.columns);
  },
  loadData: function (params) {
    var $this = this;

    $this.loadGrid(params);

    var dynamicQuery = this.getDynamicQuery(params);

    var budgetAdjustData = [];
    Ext.Ajax.request({
      url: contextPath + '/api/2.0.0/cql',
      method: 'POST',
      async: false,
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      params: Ext.encode(dynamicQuery),
      paramsAsJson: true,
      success: function (response, operation) {
        var responseJson = Ext.JSON.decode(response.responseText);
        budgetAdjustData = responseJson.data;
      }
    });

    for (var i = 0; i < budgetAdjustData.length; i++) {
      for (var j = 0; j < params.organizationId.length; j++) {
        var id = params.organizationId[j];
        var organizationAdjustValue = {};
        var organizationAdjustAfterValue = {};
        var organizationBudgetValue = budgetAdjustData[i]['budgetAmount' + id];

        if (params.adjustWay == 'Amount') {
          organizationAdjustValue = params.adjustValue;
        } else if (params.adjustWay == 'Proportion') {
          organizationAdjustValue = organizationBudgetValue * params.adjustValue / 100;
        } else {
          organizationAdjustValue = 0.00;
        }
        organizationAdjustAfterValue = organizationBudgetValue + organizationAdjustValue;

        if (organizationAdjustAfterValue < 0) {
          organizationAdjustValue = 0 - organizationBudgetValue;
          organizationAdjustAfterValue = 0;
        }
        budgetAdjustData[i]['adjustAmount' + id] = organizationAdjustValue;
        budgetAdjustData[i]['adjustAfterAmount' + id] = organizationAdjustAfterValue;
      }
    }

    $this.getStore().removeAll();
    $this.getStore().add(budgetAdjustData);
    $this.getStore().commitChanges();

  },
  getDynamicQuery: function (params) {

    var dataType = '@@SummaryPlanning@@';
    var budgetItemGroup = Ext.create("Common.data.ApiReader").load({
      api: "resourceExtInstance",
      joins: [
        {
          type: "budgetItemGroup",
          fields: {
            id: "budgetItemGroupId"
          },
          conditions: [
            {
              field: "code",
              condition: "EQUALS",
              type: "Field",
              value: "instanceValue"
            }
          ]
        }
      ],
      conditions: [
        {
          field: "domainId",
          condition: "EQUALS",
          type: "String",
          value: domainId
        }, {
          field: "resourceExtOptionId",
          condition: "EQUALS",
          type: "String",
          value: "default-planning-calculate-itemGroup-code"
        }, {
          field: "resourceExtSchemaId",
          condition: "EQUALS",
          type: "String",
          value: "budget-feature-setting-configuration"
        }
      ]
    });
    var budgetItemGroupId = budgetItemGroup[0].budgetItemGroupId;

    var dynamicQuery = {
      queryType: "read",
      type: "budgetItemGroupItem",
      fields: {
        domainId: "domainId",
        alias: "alias",
        sortCode: "sortCode",
        budgetItemId: "budgetItemId",
        budgetItemGroupId: "budgetItemGroupId"
      },
      joins: [
        {
          type: "BudgetItem",
          fields: {
            id: "id",
            code: "code",
            name: "name",
            budgetType: "budgetType",
            leaf: "leaf",
            parentId: "parentId"
          },
          conditions: [
            {
              field: "id",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }
      ],
      associations: [],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: budgetItemGroupId
        }, {
          field: "id",
          condition: "IN",
          type: "Integer",
          value: params.budgetItemId
        }
      ]
    };

    for (var i = 0; i < params.organizationId.length; i++) {
      dynamicQuery.associations.push({
        type: "budgetItemGroupItem",
        fields: {
          alias: "alias",
          sortCode: "sortCode",
          budgetItemGroupId: "budgetItemGroupId",
          budgetItemId: "budgetItemId"
        },
        references: {
          budgetItemId: "budgetItemId"
        },
        joins: [{
          type: "annualCostPlanningData",
          fields: {
            amount: 'budgetAmount' + params.organizationId[i]
          },
          conditions: [
            {
              field: "setOfBooksId",
              condition: "EQUALS",
              type: "Integer",
              value: params.setOfBooksId
            },
            {
              field: "period",
              condition: "EQUALS",
              type: "String",
              value: params.period
            },
            {
              field: "organizationId",
              condition: "IN",
              type: "Integer",
              value: params.organizationId[i]
            },
            {
              field: "dataType",
              condition: "STARTS_WITH",
              type: "String",
              value: dataType
            },
            {
              field: "budgetObjectType",
              condition: "EQUALS",
              type: "String",
              value: "BudgetItem"
            },
            {
              field: "budgetItemGroupId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemGroupId"
            },
            {
              field: "budgetObjectId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }],
        conditions: [
          {
            field: "budgetItemGroupId",
            condition: "EQUALS",
            type: "Integer",
            value: budgetItemGroupId
          }, {
            field: "budgetItemId",
            condition: "IN",
            type: "Integer",
            value: params.budgetItemId
          }
        ]
      });
    }
    return dynamicQuery;
  }
});
