Ext.define("Budget.view.grid.HumanResourcesMedicalGrid", {
  extend: "Budget.view.grid.PlanningGrid",
  alias: "widget.humanResourcesMedicalGrid",
  title: "医疗人员",
  dataType: "@@HumanResourcesMedical@@Cost@@"
});