Ext.define('Divide.view.PlanningGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.planningGrid',
  viewConfig: {
    loadMask: false,
    forceFit: true
  },
  enableColumnMove: false,
  params: {},
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'budgetItemId', type: 'double'},
      {name: 'budgetItemCode', type: 'string'},
      {name: 'budgetItemName', type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'budgetType', type: 'string'},
      {name: 'dataType', type: 'string'},
      {name: 'sortCode', type: 'string'},
      {name: 'leaf', type: 'boolean'},
      {name: 'amount', type: 'double'}
    ],
    pageSize: 1000000,
    api: 'budgetItemGroupItem',
    joins: [
      {
        type: 'budgetItem',
        fields: {
          name: 'budgetItemName',
          code: 'budgetItemCode',
          parentId: 'parentId',
          leaf: 'leaf'
        },
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'budgetItemId'
        }]
      }, {
        type: 'annualCostPlanningData',
        fields: {
          dataType: "dataType",
          amount: "amount"
        },
        conditions: [{
          field: 'budgetItemGroupId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'budgetItemGroupId'
        }, {
          field: 'budgetObjectId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'budgetItemId'
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: '#setOfBooksId#'
        }, {
          field: 'period',
          condition: 'EQUALS',
          value: '#period#'
        }, {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Integer',
          value: '#organizationId#'
        }, {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: '@@Planning@@'
        }]
      }, {
        type: 'budgetItemGroup',
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'budgetItemGroupId'
        }],
        joins: [{
          type: 'resourceExtInstance',
          fields: {
            instanceValue: 'viewType'
          },
          conditions: [{
            field: 'resourceExtOptionId',
            condition: 'EQUALS',
            type: 'String',
            value: 'view-type'
          }, {
            field: 'resourceExtSchemaId',
            condition: 'EQUALS',
            type: 'String',
            value: 'budget-item-group-pkuih-configuration'
          }, {
            field: 'resourceId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'resourceId'
          }]
        }]
      }
    ],
    conditions: [{
      field: 'viewType',
      condition: 'EQUALS',
      value: 'BudgetSummary'
    }, {
      field: 'domainId',
      condition: 'EQUALS',
      value: '#domainId#'
    }, {
      field: 'amount',
      condition: 'NOT_NULL'
    }],
    listeners: {
      beforeload: function (store, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      load: function (store) {
        var deleteRecords = [];
        var params = Common.CommonManager.getParams();

        var humanResourcesDatas = [];
        if (params.organizationCode != undefined && params.organizationCode == '301') {
          var dynamicQuery = {
            api: 'budgetItemGroupItem',
            fields: {
              budgetItemId: 'budgetItemId',
              alias: 'alias',
              sortCode: 'sortCode'
            },
            joins: [{
              type: 'budgetItemGroup',
              fields: {
                code: 'budgetItemGroupCode'
              },
              conditions: [{
                field: 'domainId',
                condition: 'EQUALS',
                type: 'Field',
                value: 'domainId'
              }, {
                field: 'id',
                condition: 'EQUALS',
                type: 'Field',
                value: 'budgetItemGroupId'
              }]
            }],
            conditions: [{
              field: 'domainId',
              condition: 'EQUALS',
              type: 'String',
              value: params.domainId
            }, {
              field: 'budgetItemGroupCode',
              condition: 'EQUALS',
              type: 'String',
              value: 'R00'
            }]
          };
          humanResourcesDatas = Ext.create("Common.data.ApiReader").load(dynamicQuery);
        }

        store.each(function (record) {
          if (record != null && record.data != null) {
            var isLeaf = true;
            var isDelete = false;
            var sortCode = "@@" + record.data.sortCode;
            var budgetItemId = record.data.budgetItemId;
            var budgetItemCode = record.data.budgetItemCode;
            if (budgetItemCode != "YS1001" && budgetItemCode != "YS1005") {
              if (budgetItemCode == "YS100501" || budgetItemCode == "YS100502" || budgetItemCode == "YS100101"
                || budgetItemCode == "YS100102" || budgetItemCode == "YS100103") {
                deleteRecords.push(record);
              } else {
                store.each(function(tempRecord) {
                  var trempSortCode = "@@" + tempRecord.data.sortCode;
                  if (trempSortCode.length > sortCode.length
                    && trempSortCode.indexOf(sortCode) > -1) {
                    isLeaf = false;
                  }
                });

                if (!isLeaf) {
                  deleteRecords.push(record);
                }else {
                  if(humanResourcesDatas.length > 0) {
                    for (var i = 0; i < humanResourcesDatas.length; i++) {
                      if(humanResourcesDatas[i] != null && humanResourcesDatas[i].budgetItemId != undefined
                        && humanResourcesDatas[i].budgetItemId == budgetItemId) {
                        isDelete = true;
                      }
                    }

                    if (isDelete) {
                      deleteRecords.push(record);
                    }
                  }
                }
              }
            }
          }
        });

        for (var i = 0; i < deleteRecords.length; i++) {
          store.remove(deleteRecords[i]);
        }

        Common.CommonManager.hideMask();
      }
    }
  }),
  selModel: Ext.create('Ext.selection.CheckboxModel', {checkOnly: true}),
  columns: [{
    text: '预算项',
    dataIndex: 'alias',
    align: 'left',
    flex: 1,
    sortable: false
  }, {
    text: '预算金额',
    oldText: null,
    dataIndex: 'amount',
    align: 'right',
    width: 300,
    sortable: false,
    renderer: function (value, meta, record) {
      if (value != null) {
        return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
          + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
      } else {
        return '0.00';
      }
    }
  }, {
    text: '预算类型',
    dataIndex: 'budgetType',
    align: 'right',
    width: 300,
    sortable: false
  }],
  initComponent: function () {
    var $this = this;
    this.callParent(arguments);

    $this.getStore().on('load', function (store) {
      var budgetTypeData = Ext.create("Common.data.ApiReader").load({
        api: 'dictionaryOption',
        fields: {
          code: 'code',
          names: 'names'
        },
        conditions: [
          {
            field: 'domainId',
            condition: 'EQUALS',
            value: 'Default'
          }, {
            field: 'dictionaryCode',
            condition: 'EQUALS',
            value: 'BudgetType'
          }
        ]
      });

      store.each(function (record) {
        for (var j = 0; j < budgetTypeData.length; j++) {
          if (record.data.dataType != null && record.data.dataType != '' &&
            record.data.dataType.split('@@')[2] == budgetTypeData[j].code) {
            record.set("budgetType", budgetTypeData[j].names);
            record.commit();
          }
        }
      });
    });
  },
  loadData: function () {
    var params = Common.CommonManager.getParams();
    this.getStore().loadData({
      params: {
        domainId: params.domainId,
        setOfBooksId: params.setOfBooksId,
        period: params.period,
        organizationId: params.organizationId,
        organizationSortCode: params.organizationSortCode
      }
    });
  },
  divide: function () {
    var $this = this;
    var params = Common.CommonManager.getParams();

    var budgetTypes = Ext.getCmp("divide-search-budgetType").getChecked();
    var selectModel = this.getSelectionModel().getSelection();

    if (budgetTypes == null || budgetTypes == "" || budgetTypes == undefined) {
      Common.Msg.Alert("请选择预算类型！");
      return false;
    }

    if (selectModel == null || selectModel == "" || selectModel == undefined) {
      Common.Msg.Alert("请选择要划分的目标！");
      return false;
    }

    var selectData = [];
    var budgetType = budgetTypes[0].name;

    var budgetItemIds = [];
    for (var i = 0; i < selectModel.length; i++) {
      budgetItemIds[i] = selectModel[i].data.budgetItemId;
    }

    var budgetItems = Ext.create("Common.data.ApiReader").load({
      api: 'budgetItem',
      condition: [{
        field: 'domainId',
        condition: 'EQUALS',
        value: 'Default'
      }]
    });

    for (var i = 0; i < budgetItemIds.length; i++) {
      for (var j = 0; j < budgetItems.length; j++) {
        if (budgetItemIds[i] == budgetItems[j].parentId) {
          budgetItemIds[budgetItemIds.length] = budgetItems[j].id
        }
      }
    }

    var saveModel = [
      "annualCostPlanningData", "annualCostPlanningDataVersion",
      "monthlyCostPlanningData", "monthlyCostPlanningDataVersion"
    ];

    var callback = function () {
      $this.loadData();
    };

    for (var i = 0; i < saveModel.length; i++) {
      var api = saveModel[i];
      var divideData = Ext.create("Common.data.ApiReader").load({
        api: api,
        conditions: [
          {
            field: 'domainId',
            condition: 'EQUALS',
            value: params.domainId
          }, {
            field: 'setOfBooksId',
            condition: 'EQUALS',
            type: 'Integer',
            value: params.setOfBooksId
          }, {
            field: 'period',
            condition: 'STARTS_WITH',
            value: params.period.substr(0,4)
          }, {
            field: 'organizationId',
            condition: 'EQUALS',
            type: 'Integer',
            value: params.organizationId
          }, {
            field: 'dataType',
            condition: 'STARTS_WITH',
            type: 'String',
            value: '@@Planning@@'
          }, {
            field: 'budgetObjectId',
            condition: 'IN',
            value: budgetItemIds
          }
        ]
      });

      for (var d = 0; d < divideData.length; d++) {
        var dataTypes = divideData[d].dataType.split('@@');
        dataTypes[2] = budgetType;
        divideData[d].dataType = dataTypes.join('@@');
      }

      if (i == saveModel.length - 1) {
        Ext.create("Common.data.ApiReader")
          .createOrUpdate(api, divideData, callback(), false);
      } else {
        Ext.create("Common.data.ApiReader")
          .createOrUpdate(api, divideData, null, false);
      }
    }
  }
});

