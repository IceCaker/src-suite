Ext.define('Budget.view.compare.PlanningSummaryGrid', {
  extend: 'Budget.view.compare.PlanningGrid',
  alias: "widget.planningSummaryGrid",
  title: "预算汇总",
  dataType: '@@SummaryPlanning@@'
});