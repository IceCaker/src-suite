Ext.define("Budget.view.grid.HumanResourcesSummaryGrid", {
  extend: "Budget.view.grid.PlanningGrid",
  alias: "widget.humanResourcesSummaryGrid",
  title: "人力汇总",
  dataType: "@@HumanResourcesSummary@@Cost@@"
});