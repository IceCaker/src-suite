Ext.define("Divide.view.SearchToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.divideSearchToolbar",
  margin: '0 0 5 0',
  items: [{
    id: "divide-search-setOfBook",
    xtype: "setOfBookCombo",
    hidden: true,
    domainId: domainId,
    loadAfterRender: true,
    observable: true
  }, {
    id: "divide-search-period",
    xtype: "periodCombo",
    emptyText: '请选择',
    observable: true
  }, {
    id: "divide-search-organization",
    xtype: "apiCombox",
    name: "organizationId",
    alias: 'widget.orgCombo',
    labelWidth: 35,
    width: 130,
    fieldLabel: '组织',
    name: 'organizationId',
    selectType: "combo",
    loadAfterRender: true,
    autoSelect: true,
    readCookie: true,
    api: 'organization',
    fields: ["id", "code", "name", "sortCode"],
    joins: [{
      type: "dictionaryOption",
      fields: {
        code: "optionCode",
        names: "optionName"
      },
      conditions: [{
        field: "dictionaryCode",
        condition: "EQUALS",
        type: 'String',
        value: "OrganizationRelationBudgetType"
      }, {
        field: 'code',
        condition: 'EQUALS',
        type: 'Field',
        value: "code"
      }]
    }],
    conditions: [
      {field: 'optionName', condition: "CONTAINS", value: "Public"},
      {field: 'domainId', condition: 'EQUALS', value: domainId},
      {field: 'leaf', condition: 'EQUALS', value: 1}
    ],
    getPublishParam: function() {
      var organizationValue = this.getValue();
      var object = this.getStore().getById(organizationValue);
      if (object == null || typeof(object["data"]) == 'undefined' || object.data == null) {
        return {
          organizationId: "",
          organizationCode: "",
          organizationName: "",
          organizationSortCode: ""
        }
      }
      return {
        organizationId: object.data.id,
        organizationCode: object.data.code,
        organizationName: object.data.name,
        organizationSortCode: object.data.sortCode
      }
    },
    width: 180,
    queryMode: 'local',
    valueField: 'id'
  }, {
    id: 'divide-search-queryButton', text: '查询', xtype: "queryButton", observable: true,
    paramsProperties: ["setOfBooksId", "period", "organizationId"],
    params: {},
    pushParam: function(param) {
      Ext.apply(this.params, param);
    },
    isLoadAll: function() {
      for (var i = 0; i < this.paramsProperties.length; i++) {
        if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
          return false;
        }
      }
      return true;
    },
    getParams: function() {
      return this.params;
    },
    subscription: {
      observers: ["divide-search-setOfBook", "divide-search-period", "divide-search-organization"],
      operate: {
        onNext: function(param) {
          this.pushParam(param);
          if (this.isLoadAll()) {
            this.publish(this.getParams());
          }
        }
      }
    },
    handler: function() {
      this.onNext({});
    }
  }]
});
