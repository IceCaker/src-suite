Ext.define('Pkuih.Common.view.TravelPanel', {
  extend: 'Ext.panel.Panel',
  alias: "widget.travelPanel",
  // autoScroll: true,
  height: 320,
  readOnly: true,
  mark: '',
  isEdit: '',
  formulas: [],
  projectTravelArray: [],
  projectCategoryCode: '',
  tableLeftCode: [],
  tableLeftName: [],
  tableHeadsCode: [],
  tableHeadsName: [],
  employeeLevelCode: [],
  travelExpensesCode: [],
  businessCityClassCode: [],
  businessCityClassName: [],
  initComponent: function () {
    var $this = this;
    this.callParent(arguments);
  },
  inita: function () {
    // this.initColumnCodeAndName();
    this.createHtml();
  },
  createHtml: function (rowNum) {
    var $this = this;
    var tableHeadsName = this.tableHeadsName;

    var level = this.tableLeftName;
    var rowNum = level.length + 1;

    if ($this.mark == 'ForeignTrain' || $this.mark == 'ForeignTravel') {
      $this.setHeight(215);
      var tableHeight = $this.getHeight() - 39;
    } else {
      var tableHeight = $this.getHeight() - 39;
    }

    var html = '<table id = "planningTable", width = "100%" height = ' + tableHeight
      + '  border="1" cellspacing="0" align="center"'
      + ' style="border-collapse:collapse;border-color:silver;">';

    for (var row = 1; row <= rowNum; row++) {
      for (var column = 1; column <= tableHeadsName.length; column++) {
        if (column == 1) {
          html += "<tr>"
        }
        if (row == 1) {
          html += "<td height='30px', align='center' style='background-color:#f5f5f5;'>" + tableHeadsName[column - 1] + "</td>";
        } else {
          if (column == 1) {
            html += "<td>" + level[row - 2] + "</td>"
          } else if (column == 2 && row == 2) {
            html += "<td rowspan='7' align='right' id =" + $this.tableHeadsCode[column - 1] + "></td>"
          } else if (column == 3) {
            html += "<td id =" + $this.tableLeftCode[row - 2] + '-' + $this.tableHeadsCode[column - 1]
              + " name='peopleNum'  align='right'></td>"
          } else if (column == 4 && row == 2) {
            html += "<td id=" + $this.tableHeadsCode[column - 1] + " align='center', rowspan='7'></td>"
          } else if (column >= 5 && column <= 9) {
            html += ("<td align='right' id =" + $this.tableLeftCode[row - 2] + '-' + $this.tableHeadsCode[column - 1]
            + " name='amount' ></td>")
          }
        }
        if (column == tableHeadsName.length) {
          html += "</tr>"
        }
      }
    }
    $this.body.update(html);
    return html;
  },
  afterShow: function (isEdit) {
    var $this = this;
    $this.isEdit = isEdit;
    this.dynamicAddColumns();

    document.getElementById("Days").innerHTML =
      "<input type='text' value ='' " +
      "style='border-style:none; font-size:16px;padding:5px;text-align:right;width:50px;height:150px;'></input>";
    if (isEdit) {
      document.getElementById("Days").style.backgroundColor = "#fffacd";
      document.getElementById("Days").firstChild.style.backgroundColor = "#fffacd";
    } else {
      document.getElementById("Days").firstChild.readOnly = true;
    }

    document.getElementById("Days").onchange = function () {
      $this.verifyInputIsNum(this);
      $this.changeAmount();
      $this.dynamicSummaryYearPlan();
    };

    var peopleNums = document.getElementsByName("peopleNum");
    for (var i = 0; i < peopleNums.length; i++) {
      peopleNums[i].innerHTML = "<input type='text' value ='' " +
        "style='border-style:none; font-size:16px;padding:5px;text-align:right;width:50px;'></input>";
      if (peopleNums.length - 1 != i && isEdit) {
        peopleNums[i].style.backgroundColor = "#fffacd";
        peopleNums[i].firstChild.style.backgroundColor = "#fffacd";
      } else {
        peopleNums[i].firstChild.readOnly = true;
      }
      peopleNums[i].onchange = function () {
        $this.verifyInputIsNum(this);
        $this.changeAmount();
        $this.summaryPeople();
        $this.dynamicSummaryYearPlan();
      }
    }

    var amounts = document.getElementsByName("amount");
    for (var i = 0; i < amounts.length; i++) {
      amounts[i].innerHTML = "<input type='text' value ='' " +
        " style='border-style:none; font-size:16px;padding:5px;text-align:right;width:90px;'></input>";
      amounts[i].onchange = function () {
        $this.verifyInputIsNum(this);
        $this.summaryAmount();
        $this.dynamicSummaryYearPlan();
      };
      if (amounts[i].id.indexOf('totalAmount-') == -1 && isEdit) {
        amounts[i].style.backgroundColor = "#fffacd";
        amounts[i].firstChild.style.backgroundColor = "#fffacd";
      } else {
        amounts[i].firstChild.readOnly = true;
      }
    }

    if ($this.businessCityClassName.length != 0 || this.businessCityClassCode.length != 0) {
      var data = [];
      var cityClassName = $this.businessCityClassName;
      var cityClassCode = $this.businessCityClassCode;
      for (var i = 0; i < cityClassName.length; i++) {
        var name = cityClassName[i];
        var code = cityClassCode[i];
        data.push({name: name, value: code})
      }
      var comboBox = Ext.create('Ext.form.ComboBox', {
        id: 'location',
        inputType: 'text',
        autoSelect: true,
        forceSelection: true,
        editable: false,
        readOnly: !isEdit,
        displayField: 'name',
        valueField: 'value',
        // value: 'firstCities',
        store: Ext.create('Ext.data.Store', {
          fields: ['name', 'value'],
          data: data
        }),
        renderTo: Ext.get('BusinessCity'),
        listeners: {
          change: function (combobox, newValue, oldValue) {
            $this.changeAmount();
            $this.dynamicSummaryYearPlan();
          }
        }
      });
    }
    else {
      document.getElementById('BusinessCity').innerHTML = "<input type='text' value ='' " +
        "style='border-style:none; font-size:16px;padding:5px;text-align:center;" +
        "width:80px;height:150px;'></input>";
      if (isEdit) {
        document.getElementById('BusinessCity').style.backgroundColor = "#fffacd";
        document.getElementById('BusinessCity').firstChild.style.backgroundColor = "#fffacd";
      } else {
        document.getElementById('BusinessCity').firstChild.readOnly = true;
      }
    }
    this.callParent(arguments);
  },
  verifyInputIsNum: function (component) {
    var value = component.firstChild.value;
    if (isNaN(parseFloat(value))) {
      component.firstChild.value = '';
    } else if (component.id.indexOf('Num') != -1 || component.id.indexOf('Days') != -1) {
      component.firstChild.value = Math.floor(parseFloat(value) * 100) / 100;
    } else {
      component.firstChild.value = parseFloat(value).toFixed(2);
    }
  },
  changeAmount: function () {
    var $this = this;
    var map = {};
    var location = '';
    var formulas = $this.formulas;
    if (this.formulas.length == 0) {
      return;
    }
    //获取标准
    for (var i = 0; i < formulas.length; i++) {
      var key = formulas[i].levelCode + '-' + formulas[i].expensesCode + '-' + formulas[i].classCode;
      map[key] = formulas[i].benchmark;
    }
    var heads = $this.travelExpensesCode;
    var levels = $this.employeeLevelCode;
    var location = Ext.getCmp('location') != undefined ? Ext.getCmp('location').value : '';

    for (var i = 0; i < levels.length; i++) {
      var totalAmount = 0;
      for (var j = 0; j < heads.length; j++) {
        var level = levels[i];
        var head = heads[j];
        var key = (location == '' ? level + '-' + head : level + '-' + head + '-' + location);
        var benchmark = map[key];
        var peopleNum = document.getElementById(level + "-Num").firstChild.value;
        var amount = document.getElementById(level + '-' + head).firstChild;
        var dayNum = document.getElementById('Days').firstChild.value;
        if (benchmark != undefined) {
          amount.value = (benchmark * dayNum * peopleNum).toFixed(2);
          amount.readOnly = true;
          amount.style.backgroundColor = '#ffffff';
          amount.parentNode.style.backgroundColor = '#ffffff';
        } else {
          amount.readOnly = false;
          if (i != levels.length - 1) {
            amount.style.backgroundColor = '#fffacd';
            amount.parentNode.style.backgroundColor = '#fffacd';
          }
        }
        if (!$this.isEdit) {
          amount.readOnly = true;
          amount.style.backgroundColor = '#ffffff';
          amount.parentNode.style.backgroundColor = '#ffffff';
        }
      }
    }
    $this.summaryAmount();
  },
  summaryPeople: function () {
    var peopleNums = document.getElementsByName("peopleNum");
    var totalPeopleNum = 0;
    for (var j = 0; j < peopleNums.length - 1; j++) {
      if (peopleNums[j].firstChild.value != '') {
        totalPeopleNum += parseFloat(peopleNums[j].firstChild.value);
      }
    }
    peopleNums[peopleNums.length - 1].firstChild.value = Math.floor(totalPeopleNum * 100) / 100;
  },
  summaryAmount: function () {
    var levels = [];
    Ext.apply(levels, this.employeeLevelCode);
    levels.splice(levels.length - 1, 1);
    var heads = this.travelExpensesCode;
    for (var i = 0; i < heads.length; i++) {
      var totalAmount = 0;
      var head = heads[i];
      for (var j = 0; j < levels.length; j++) {
        var level = levels[j];
        if (document.getElementById(level + '-' + head).firstChild.value != '') {
          var value = document.getElementById(level + '-' + head).firstChild.value;
          totalAmount += parseFloat(value);
        }
      }
      document.getElementById('totalAmount' + '-' + head).firstChild.value = totalAmount.toFixed(2);
      document.getElementById('totalAmount' + '-' + head).firstChild.readOnly = true;
    }
  },
  dynamicSummaryYearPlan: function () {
    var $this = this;
    var travelGrid = Ext.ComponentQuery.query("travelGrid")[0].items.items[0].dataSource.data;
    var index = -1;
    if ($this.mark == 'Recruit' || $this.mark == 'ForeignTrain' ||
      $this.mark == 'DeptTrain' || $this.mark == 'DeptMeeting') {
      var otherExpenses = document.getElementById('OtherExpenses').firstChild.value;
      travelGrid.items[++index].data.yearAmount = $this.formatAmount(otherExpenses);
      travelGrid.items[index].commit();
    }
    var longTraffic = document.getElementById('totalAmount-LongTraffic').firstChild.value;
    var cityTraffic = document.getElementById('totalAmount-CityTraffic').firstChild.value;
    var hotelExpense = document.getElementById('totalAmount-HotelExpense').firstChild.value;
    var meals = document.getElementById('totalAmount-Meals').firstChild.value;
    var other = document.getElementById('totalAmount-Other').firstChild.value;

    travelGrid.items[++index].data.yearAmount = $this.formatAmount(longTraffic, cityTraffic);
    travelGrid.items[index].commit();
    travelGrid.items[++index].data.yearAmount = $this.formatAmount(hotelExpense);
    travelGrid.items[index].commit();
    travelGrid.items[++index].data.yearAmount = $this.formatAmount(meals, other);
    travelGrid.items[index].commit();
  },
  formatAmount: function (amount1, amount2) {
    if (amount2 == undefined) {
      return amount1 == '' ? '' : parseFloat(amount1).toFixed(2);
    }
    amount1 = (amount1 == '' ? '' : parseFloat(amount1));
    amount2 = (amount2 == '' ? '' : parseFloat(amount2));
    return (amount1 + amount2) == '' ? '' : parseFloat(amount1 + amount2).toFixed(2);
  },

  dynamicAddColumns: function () {
    var $this = this;
    var innerText = '';
    if ($this.mark == 'Recruit') {
      innerText = '招聘费';
    } else if ($this.mark == 'ForeignTrain') {
      innerText = '国外培训费';
    } else if ($this.mark == 'DeptTrain') {
      innerText = '部门培训费';
    } else if ($this.mark == 'DeptMeeting') {
      innerText = '部门会议费';
    }
    if (innerText != '') {
      var rowTitle = document.getElementById('planningTable').rows[0].insertCell(4);
      rowTitle.innerText = innerText;
      rowTitle.align = 'center';
      rowTitle.width = '100px';
      rowTitle.style = 'background-color:#f5f5f5';
      var newCell = document.getElementById('planningTable').rows[1].insertCell(4);
      newCell.rowSpan = $this.tableLeftName.length;
      newCell.id = 'OtherExpenses';
      newCell.align = 'right';
      newCell.addEventListener("change", function () {
        $this.verifyInputIsNum(this)
      });
      newCell.innerHTML = "<input type='text' value ='' " +
        " style='border-style:none; font-size:16px;padding:5px;text-align:right;height:150px;width:90px;'></input>";
      newCell.onchange = function () {
        $this.dynamicSummaryYearPlan();
      };
      if ($this.isEdit) {
        newCell.style.backgroundColor = "#fffacd";
        newCell.firstChild.style.backgroundColor = "#fffacd";
      } else {
        newCell.firstChild.readOnly = true;
      }
    }
  },
  loadTravelData: function () {
    var $this = this;
    var projectId = $this.up("projectInfoWindow").projectId;
    var versionId = $this.up("projectInfoWindow").versionId;

    if (projectId < 0) {
      return false;
    }

    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      },
      {
        field: 'projectId',
        condition: 'EQUALS',
        value: projectId
      }
    ];

    if (versionId > 0) {
      conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });
    }

    var travelDataArray = Ext.create("Common.data.ApiReader").load({
      api: versionId > 0 ? 'projectTravelVersion' : 'projectTravel',
      conditions: conditions
    });

    //先将非金额放入单元格中，再将金额放入表中，防止计算错误
    var travelAmountArray = [];
    var travelOtherArray = [];
    for (var i = 0; i < travelDataArray.length; i++) {
      var travelData = travelDataArray[i];
      //忽略excel中导入的合计行
      if (travelData.levelCode == 'Stat') {
        continue;
      }
      if (travelData.objectType == 'Days' ||
        travelData.objectType == 'Num' ||
        travelData.objectType == 'BusinessCity') {
        travelOtherArray.push(travelData);
      } else {
        travelAmountArray.push(travelData);
      }
    }
    for (var i = 0; i < travelOtherArray.length; i++) {
      var id = '';
      var travelData = travelOtherArray[i];
      if (travelData.objectType == 'BusinessCity' && $this.businessCityClassName.length != 0) {
        Ext.getCmp('location').setValue(travelData.objectValue);
        continue;
      }
      if (travelData.objectType == 'BusinessCity' && $this.businessCityClassName.length == 0) {
        document.getElementById('BusinessCity').firstChild.value = travelData.objectValue;
        continue;
      }
      if (travelData.levelCode == null || travelData.levelCode == '') {
        id = travelData.objectType
      } else {
        id = travelData.levelCode + '-' + travelData.objectType;
      }
      var value = '';
      if (travelData.objectValue == null || travelData.objectValue == '') {
        value = travelData.objectAmount;
      } else {
        value = travelData.objectValue
      }
      document.getElementById(id).firstChild.value = value;
    }
    for (var i = 0; i < travelAmountArray.length; i++) {
      var id = '';
      var travelData = travelAmountArray[i];
      if (travelData.levelCode == null || travelData.levelCode == '') {
        id = travelData.objectType
      } else {
        id = travelData.levelCode + '-' + travelData.objectType;
      }
      var value = travelData.objectAmount;
      document.getElementById(id).firstChild.value = value.toFixed(2);
    }

    for (var i = 0; i < travelDataArray.length; i++) {
      travelDataArray[i] = {data: travelDataArray[i]}
    }
    $this.projectTravelArray = travelDataArray;
    $this.summaryPeople();
    $this.summaryAmount();
    $this.dynamicSummaryYearPlan();
  },
  saveTravelData: function () {
    var $this = this;
    var projectId = $this.up("projectInfoWindow").projectId;
    if (projectId == -1) {
      return;
    }
    var infoDataArray = new Array();

    var dayNum = document.getElementById('Days').firstChild.value;
    if (dayNum != '') {
      $this.addTravelDataToArray(projectId, null, 'Days', null, dayNum, infoDataArray);
    }
    if ($this.businessCityClassName.length != 0 || this.businessCityClassCode.length != 0) {
      var city = Ext.getCmp('location').value;
      if (city != null && city != '') {
        $this.addTravelDataToArray(projectId, null, 'BusinessCity', city, null, infoDataArray);
      }
    } else {
      var city = document.getElementById('BusinessCity').firstChild.value;
      if (city != null && city.value != '') {
        $this.addTravelDataToArray(projectId, null, 'BusinessCity', city, null, infoDataArray);
      }
    }

    if ($this.mark == 'Recruit' || $this.mark == 'ForeignTrain' ||
      $this.mark == 'DeptTrain' || $this.mark == 'DeptMeeting') {
      var otherExpenses = document.getElementById('OtherExpenses').firstChild.value;
      if (otherExpenses != '') {
        $this.addTravelDataToArray(projectId, null, 'OtherExpenses', null, otherExpenses, infoDataArray);
      }
    }

    var levels = [];
    Ext.apply(levels, this.employeeLevelCode);
    levels.splice(levels.length - 1, 1);
    var heads = this.travelExpensesCode;
    for (var i = 0; i < levels.length; i++) {
      var peopleNum = document.getElementById(levels[i] + "-Num").firstChild.value;
      if (peopleNum != '') {
        $this.addTravelDataToArray(projectId, levels[i], 'Num', null, peopleNum, infoDataArray);
      }
      for (var j = 0; j < heads.length; j++) {

        var level = levels[i];
        var head = heads[j];

        var amount = document.getElementById(level + '-' + head).firstChild;
        if (amount.value != 0 && amount.value != '') {
          $this.addTravelDataToArray(projectId, level, head, null, amount.value, infoDataArray);
        }
      }
    }
    var callback = function (result) {
      $this.projectTravelArray = result;
    };

    if (this.projectTravelArray.length != 0) {
      Ext.create("Common.data.ApiReader")
        .directDelete_('projectTravel', this.projectTravelArray);
    }
    Ext.create("Common.data.ApiReader")
      .createOrUpdate('projectTravel', infoDataArray, callback, false);
  },
  addTravelDataToArray: function (projectId, levelCode, objectType, objectValue, objectAmount, newTravelArray) {
    var projectTravel = new Object();
    projectTravel.domainId = domainId;
    projectTravel.projectId = projectId;
    if (levelCode != undefined) {
      projectTravel.levelCode = levelCode;
    }
    projectTravel.objectType = objectType;
    if (objectValue != undefined) {
      projectTravel.objectValue = objectValue;
    } else if (objectAmount != undefined) {
      projectTravel.objectAmount = objectAmount;
    }
    newTravelArray.push(projectTravel);
  }
});