Ext.define('Project.view.grid.ProjectFileGrid', {
  extend: "Ext.grid.Panel",
  alias: "widget.projectFileGrid",
  minHeight: 150,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  border: false,
  readOnly: true,
  store: Ext.create('Ext.data.Store', {
    fields: [
      {name: "id", type: "long"},
      {name: "enabled", type: "boolean"},
      {name: "deleted", type: "boolean"},
      {name: "deletable", type: "boolean"},
      {name: "inherent", type: "boolean"},
      {name: "domainId", type: "long"},
      {name: "domainName", type: "long"},
      {name: "domainCode", type: "long"},
      {name: "creatorId", type: "long"},
      {name: "creatorName", type: "string"},
      {name: "createTime", type: "long"},
      {name: "lastModifierId", type: "long"},
      {name: "lastModifierName", type: "string"},
      {name: "lastModifiedTime", type: "long"},
      {name: "code", type: "string"},
      {name: "name", type: "string"},
      {name: "description", type: "string"},
      {name: "createTime", type: "long"},
      {name: "documentSize", type: "long"}
    ],
    proxy: {
      type: 'ajax',
      url: contextPath + '/foundation/service/document',
      method: "GET",
      reader: {
        type: 'json',
        root: 'data'
      }
    }
  }),
  columns: [
    {
      text: "文件名",
      dataIndex: 'name',
      flex: 1,
      style: 'text-align:center;',
      align: 'left',
      minWidth: 240,
      menuDisabled: true,
      sortable: false,
      renderer: function(v, c, r) {
        return '<a href="#" onclick="Project.view.grid.ProjectFileGrid.download('
          + r.data.id + ');"><font title="' + v + '">' + v + '</font></a>';
      }
    },
    {
      text: "文件大小",
      dataIndex: 'documentSize',
      style: 'text-align:center;',
      align: 'left',
      width: 120,
      menuDisabled: true,
      sortable: false,
      renderer: sizeRenderer
    },
    {
      text: "上传人",
      dataIndex: 'creatorName',
      style: 'text-align:center;',
      align: 'center',
      width: 100,
      menuDisabled: true,
      sortable: false
    },
    {
      text: "描述", dataIndex: 'description',
      style: 'text-align:center;',
      align: 'left',
      width: 160,
      menuDisabled: true,
      sortable: false,
      renderer: function(v, c, r) {
        return '<font title="' + v + '">' + v + '</font>';
      }
    },
    {
      text: "上传时间", dataIndex: 'createTime',
      style: 'text-align:center;',
      align: 'center',
      width: 160,
      menuDisabled: true,
      sortable: false, renderer: timestampRender
    },
    {
      text: "操作",
      style: 'text-align:center;',
      align: 'center',
      width: 80,
      menuDisabled: true,
      sortable: false,
      renderer: function(value, meta, record) {
        var operateStr = '';

        operateStr = operateStr + '&nbsp;&nbsp;<a onclick="Project.view.grid.ProjectFileGrid.remove('
          + record.data.id + ')">删除</a>';

        return operateStr;
      }
    }
  ],
  dockedItems: [
    {
      xtype: 'toolbar',
      style: 'background-color: white;',
      padding: 6,
      border: false,
      items: [
        {id: 'project-file-grid-upload', xtype: 'button', text: "上传附件", action: "uploadFile"},
        {id: 'project-file-grid-delete', xtype: 'button', text: "删除附件", action: "deleteFile", hidden: true}
      ]
    }
  ],
  initComponent: function() {
    this.callParent(arguments);
  },
  setResourceId: function(resourceId) {
    this.resourceId = resourceId;
  },
  loadData: function() {
    if (this.resourceId != undefined && this.resourceId != null) {
      this.getStore().load({
        params: {
          resourceId: this.resourceId
        }
      });
    }
  },
  loadGrid: function(resourceId) {
    var readOnly = this.readOnly;
    if (readOnly) {
      this.down("toolbar").hide();
    } else {
      this.down("toolbar").show();
    }
    if (resourceId != null) {
      this.resourceId = resourceId;
    }
    this.loadData();
  },
  statics: {
    download: function(documentId) {
      var url = contextPath + '/foundation/service/document/download';

      var params = {documentId: documentId};
      var body = Ext.getBody();

      var frame = body.createChild({
        tag: 'iframe',
        cls: 'x-hidden',
        id: 'downloader-iframe',
        name: 'downloader-iframe'
      });

      var form = body.createChild({
        tag: 'form',
        cls: 'x-hidden',
        id: 'downloader-form',
        method: this.method,
        action: url,
        target: 'downloader-iframe'
      });

      form.dom.innerHTML = '';
      for (var key in params) {
        var param = document.createElement("input");
        param.type = "hidden";
        param.name = key;
        param.value = params[key];

        form.dom.appendChild(param);
      }

      form.dom.submit();
    },
    remove: function(documentId) {
      Common.CommonManager.showMask("正在删除文件...");

      Ext.Ajax.request({
        url: contextPath + '/foundation/service/document',
        method: 'DELETE',
        params: {ids: documentId},
        success: function(response) {
          Common.CommonManager.hideMask();
          Ext.ComponentQuery.query('projectFileGrid')[0].loadData();
        }
      });
    }
  }
});