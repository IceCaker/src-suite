Ext.define("Setting.view.benchmark.SearchToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.benchmarkSearchToolbar",
  margin: '0 0 5 0',
  items: [
    {
      id: "main-search-scene",
      xtype: "apiDictionaryCombox",
      name: "sceneCode",
      fieldLabel: "场景",
      initParams: {
        domainId: domainId,
        dictionaryCode: "BudgetScene"
      },
      width: 150,
      includeAllItem: true,
      loadAfterRender: true,
      observable: true
    },
    {
      id: "main-search-expenses",
      xtype: "apiDictionaryCombox",
      name: "expensesCode",
      fieldLabel: "费用",
      initParams: {
        domainId: domainId,
        dictionaryCode: ""
      },
      width: 150,
      includeAllItem: true,
      loadAfterRender: true,
      observable: true,
      subscription: {
        observers: ['main-search-scene'],
        operate: {
          onNext: function(param) {
            var dictionaryCode = "";
            var tempStr = param.sceneCode.split('@@')[0];
            if(tempStr != null && tempStr != '' && tempStr != undefined){
              dictionaryCode = tempStr;
            }
            this.initParams = {
              domainId: domainId,
              dictionaryCode: dictionaryCode
            };
            this.loadData();
          }
        }
      },
      handler: function() {
        this.onNext({});
      }
    },
    {
      id: "main-search-level",
      xtype: "apiDictionaryCombox",
      name: "levelCode",
      fieldLabel: "级别",
      initParams: {
        domainId: domainId,
        dictionaryCode: ""
      },
      width: 150,
      includeAllItem: true,
      loadAfterRender: true,
      observable: true,
      subscription: {
        observers: ['main-search-scene'],
        operate: {
          onNext: function(param) {
            var dictionaryCode = "";
            var tempStr = param.sceneCode.split('@@')[1];
            if(tempStr != null && tempStr != '' && tempStr != undefined){
              dictionaryCode = tempStr;
            }
            this.initParams = {
              domainId: domainId,
              dictionaryCode: dictionaryCode
            };
            this.loadData();
          }
        }
      },
      handler: function() {
        this.onNext({});
      }
    },
    {
      id: "main-search-class",
      xtype: "apiDictionaryCombox",
      name: "classCode",
      fieldLabel: "级次",
      initParams: {
        domainId: domainId,
        dictionaryCode: ""
      },
      width: 150,
      includeAllItem: true,
      loadAfterRender: true,
      observable: true,
      subscription: {
        observers: ['main-search-scene'],
        operate: {
          onNext: function(param) {
            var dictionaryCode = "";
            var tempStr = param.sceneCode.split('@@')[2];
            if(tempStr != null && tempStr != '' && tempStr != undefined){
              dictionaryCode = tempStr;
            }
            this.initParams = {
              domainId: domainId,
              dictionaryCode: dictionaryCode
            };
            this.loadData();
          }
        }
      },
      handler: function() {
        this.onNext({});
      }
    },
    {
      id: 'main-search-query', text: '查询', xtype: "queryButton", observable: true,
      paramsProperties: ["sceneCode", "expensesCode", "levelCode", "classCode"],
      params: {},
      pushParam: function (param) {
        Ext.apply(this.params, param);
      },
      isLoadAll: function () {
        for (var i = 0; i < this.paramsProperties.length; i++) {
          if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
            return false;
          }
        }
        return true;
      },
      getParams: function () {
        return this.params;
      },
      subscription: {
        observers: ["main-search-scene", "main-search-expenses", "main-search-level", "main-search-class"],
        operate: {
          onNext: function (param) {
            this.pushParam(param);
            if (this.isLoadAll()) {
              this.publish(this.getParams());
            }
          }
        }
      },
      handler: function () {
        this.onNext({});
      }
    },
    {
      id: 'main-search-save', text: '保存', xtype: "button", observable: true ,
        handler: function () {
          this.publish({isSave: true});
       }
    },
    {
      id: 'main-search-delete', text: '删除', xtype: "button", observable: true ,
      handler: function () {
        this.publish({isDelete: true});
      }
    }
  ]
});

