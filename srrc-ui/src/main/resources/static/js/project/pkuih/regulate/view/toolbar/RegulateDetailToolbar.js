Ext.define("Regulate.view.toolbar.RegulateDetailToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.regulateDetailToolbar",
  items: [],
  regulateType: "",
  initComponent: function () {
    var items = [];

    if (this.regulateType == 'inside') {
      items[items.length] = {
        xtype: "orgCombo",
        name: "organizationId",
        displayField: 'showName',
        fieldLabel: "调整部门",
        labelSeparator: '',
        labelWidth: 65,
        allowBlank: false,
        width: 260,
        initParams: {
          domainId: domainId
        }
      };
      items[items.length] = ' ';
    }

    items[items.length] = {
      id: "regulate-expense-budgetItem",
      xtype: "regulateBudgetItemCombo",
      labelWidth: 65,
      allowBlank: false,
      labelSeparator: ''
    };
    if (this.regulateType != 'inside') {
      items[items.length] = {
        id: 'regulate-detail-createButton', text: '创建项目', xtype: "button",
        paramsProperties: ["budgetItemId"],
        params: {},
        pushParam: function (param) {
          Ext.apply(this.params, param);
        },
        isLoadAll: function () {
          if (this.params.budgetItemId == undefined || this.params.budgetItemId == '') {
            Common.Msg.Alert("请选择支出科目！");
            return false;
          }
          if (!this.isCanCreateProject(this.params.budgetItemId)) {
            Common.Msg.Alert("该科目科目不能创建项目，请选择其他科目！");
            return false;
          }
          for (var i = 0; i < this.paramsProperties.length; i++) {
            if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
              return false;
            }
          }
          return true;
        },
        getParams: function () {
          return this.params;
        },
        subscription: {
          observers: ["regulate-expense-budgetItem"],
          operate: {
            onNext: function (param) {
              this.pushParam(param);
            }
          }
        },
        handler: function () {
          if (this.isLoadAll()) {
            Ext.widget('regulateCreateWindow').showAndRevertFunc();
          }
        },
        isCanCreateProject: function (budgetItemId) {
          if (budgetItemId == undefined || budgetItemId == '') {
            return false;
          }
          var projectItems = Ext.create("Common.data.ApiReader").load({
            api: 'projectCategoryItem',
            joins: [
              {
                type: 'expenseItem',
                fields: {
                  budgetItemId: 'budgetItemId'
                },
                conditions: [
                  {
                    field: 'id',
                    condition: 'EQUALS',
                    type: 'Field',
                    value: 'expenseItemId'
                  }
                ]
              }
            ],
            conditions: [{
              field: 'budgetItemId',
              condition: 'EQUALS',
              type: 'Integer',
              value: budgetItemId
            }]
          });
          if (projectItems.length == 0) {
            return false;
          }
          return true;
        }
      };
    }

    this.items = items;
    this.callParent(arguments);
  },
  afterRender: function () {
    Ext.getCmp("regulate-expense-budgetItem").getStore().on("load",
      function ($this, records, successful, eOpts) {
        Ext.getCmp("regulate-expense-budgetItem").setValue(null);
      }
    );

    Ext.getCmp("regulate-expense-budgetItem").getStore().on("load",
      function ($this, records, successful, eOpts) {
        var deleteRecords = [];
        for (var i = 0; i < $this.data.items.length; i++) {
          var code = $this.data.items[i].data.code;
          var sortCode = $this.data.items[i].data.sortCode;
          if (code != "YS1001" && code != "YS1005" && code != "YS1201" && code != "YS1202") {
            if (code == "YS120102" || code == "YS120101" || code == "YS120103" || code == "YS120201"
              || code == "YS120202" || code == "YS120203" || code == "YS100501" || code == "YS100502"
              || code == "YS100101" || code == "YS100102" || code == "YS100103") {
              deleteRecords.push($this.data.items[i]);
            } else {
              if (!$this.data.items[i].data.leaf) {
                for (var j = 0; j < $this.data.items.length; j++) {
                  if (sortCode == $this.data.items[j].data.sortCode) {
                    continue;
                  } else {
                    if ($this.data.items[j].data.sortCode.length > sortCode.length
                      && $this.data.items[j].data.sortCode.indexOf(sortCode) == 0) {
                      deleteRecords.push($this.data.items[i]);
                      break;
                    }
                  }
                }
              }
            }
          }
        }

        for (var i = 0; i < deleteRecords.length; i++) {
          $this.remove(deleteRecords[i]);
        }

        Ext.getCmp("regulate-expense-budgetItem").setValue(null);
      }
    );
    this.callParent(arguments);
  }
});
