Ext.define('Project.controller.ProjectTaskInfoController', {
  extend: 'Ext.app.Controller',
  views: [
    "panel.ProjectInfoPanel", "panel.ProjectBasePanel", "panel.ProjectFormPanel", "grid.ProjectFileGrid",
    "panel.ProjectProcessPanel", "panel.ProjectBillPanel", "panel.ProjectVersionPanel",
    "combo.ProjectCreateOrganization", "combo.ProjectDutyOrganization",
    "combo.ExpenseItem", "window.FileUploadWindow"
  ],
  refs: [
    { ref: "projectTaskGrid", selector: 'projectTaskGrid'},
    { ref: "projectInfoPanel", select: 'projectInfoPanel'},
    { ref: "projectFileGrid", select: 'projectFileGrid' }
  ],
  init: function () {
    var $this = this;
    this.control({
      'projectInfoPanel': {tabchange: this.tabchange},
      'projectFileGrid button[action=uploadFile]': {click: function(){$this.uploadFile();}},
      'projectFileGrid button[action=deleteFile]': {click: function(){$this.deleteFile();}},
      'projectVersionPanel': { showVersion: $this.showVersion},
      'projectTaskGrid': { nameClick: this.nameClick }
    });
    // 由于输入备注时需要回车键，暂时去掉次监听
    //this.addEvent();
  },
  showPrintView: function(){
    var projectId = this.projectInfoPanel.projectId;
    openBrowserWindow({
      isFull: true,
      url: contextPath + "/bproject/print/" + projectId
    });
  },
  show: function(id, isReadOnly){

  },
  showVersion: function(projectId, versionId){
    var $this = this;

  },
  nameClick: function(id){
    this.show(id, true);
  },
  tabchange: function(tabPanel, newCard, oldCard, eOpts){
    newCard.loadData();
  },
  uploadFile: function(){
    var $this = this;
    Ext.widget('fileUploadWindow').showAndRevertFunc(
        Ext.ComponentQuery.query('projectFileGrid')[0].resourceId,
        function(){
          Ext.ComponentQuery.query('projectFileGrid')[0].loadData();
        }
    );
  },
  deleteFile: function(){
    var $this = this;
    var ids = [];

    var selectedDocuments = Ext.ComponentQuery.query('projectFileGrid')[0].getSelectionModel().getSelection();

    if (selectedDocuments.length == 0) {
      Common.Msg.Error("请选择要删除的文件");
      return;
    }

    for (var i = 0; i < selectedDocuments.length; i++) {
      ids.push(selectedDocuments[i].data.id);
    }

    var fn = function() {
      Common.CommonManager.showMask("正在删除文件...");

      Ext.Ajax.request({
        url: contextPath + '/foundation/service/document',
        method: 'DELETE',
        params: { ids: ids.join() },
        success: function(response) {
          Ext.ComponentQuery.query('projectFileGrid')[0].loadData();
          Common.CommonManager.hideMask();
        },
        failure: function(response) {
          Common.CommonManager.hideMask();
          var result = Ext.decode(response);
          Common.Msg.Error(result.message);
        }
      });
    };
    Common.Msg.Confirm("请确认是否进行文件删除?", "附件管理", fn);
  },
  getParams: function(){
    return {

    };
  }
});
