Ext.require([
  "Pkuih.Common.view.ProjectFormPanel",
  "Pkuih.Common.view.ProjectItemGrid",
  "Pkuih.Common.view.TravelPanel",
  "Pkuih.Common.view.TravelGrid",
  "Pkuih.Common.view.PhoneTrafficPanel",
  "Pkuih.Common.view.PhoneTrafficGrid"
]);
Ext.define("Pkuih.Common.view.ProjectInfoWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.projectInfoWindow",
  title: '项目信息',
  modal: true,
  shadow: false,
  plain: true,
  resizable: false,
  width: 1000,
  height: 500,
  maximizable: true,
  closeAction: "destroy",
  bodyStyle: 'overflow-y: auto; overflow-x: hidden;',
  items: [],
  mark: '',
  projectId: -1,
  versionId: -1,
  isUpdate: false,
  budgetSceneCode: '',
  projectItemArray: [],
  expenseItems: [],
  params: {},
  buttons: [{
    text: "保存",
    action: "save",
    handler: function () {
      this.up("window").saveProject();
    }
  }, {
    text: "取消",
    action: "close",
    handler: function () {
      this.up("window").close();
    }
  }],
  listeners: {
    maximize: function ($this, eOpts) {
      if ($this.down("projectItemGrid") != undefined && $this.down("projectItemGrid") != undefined != null) {
        $this.down("projectItemGrid").setHeight($this.height - 130);
      }

      this.doComponentLayout();
    },
    restore: function ($this, eOpts) {
      if ($this.down("projectItemGrid") != undefined && $this.down("projectItemGrid") != undefined != null) {
        $this.down("projectItemGrid").setHeight(320);
      }

      this.doComponentLayout();
    },
    close: function ($this, eOpts) {
      if ($this.isUpdate && $this.isCommon) {
        $this.saveProject();
        return false;
      }
    }
  },
  initComponent: function () {
    this.callParent(arguments);
  },
  loadProjectGrid: function () {

  },
  showAndRevertFunc: function (params, isEdit, func) {
    var $this = this;

    //获取项目版本ID
    var versionId = -1;
    if (params.versionId != undefined && params.versionId != '' && params.versionId > 0) {
      versionId = params.versionId;
    }

    //获取项目类型
    if (params.projectCategoryCode == undefined || params.projectCategoryCode == '') {
      if (params.projectId <= 0 && params.budgetGroupCode != '') {
        var projectCategorys = $this.getDictionaryOption('BudgetGroupRelationProjectCategory', params.budgetGroupCode);
        if (projectCategorys != null && projectCategorys.length > 0 && projectCategorys[0] != undefined
          && projectCategorys[0].optionName != undefined && projectCategorys[0].optionName != '') {
          params.projectCategoryCode = projectCategorys[0].optionName;
        } else {
          Common.Msg.Error('当前预算模版未配置与项目类型的关联关系！');
          return false;
        }
      } else if (params.projectId > 0) {
        var dynamicQuery = {
          api: versionId > 0 ? 'projectVersion' : 'project',
          joins: [
            {
              type: 'projectCategory',
              fields: {
                code: 'projectCategoryCode'
              },
              conditions: [
                {
                  field: 'id',
                  condition: 'EQUALS',
                  type: 'Field',
                  value: 'projectCategoryId'
                }
              ]
            }
          ],
          conditions: [{
            field: 'id',
            condition: 'EQUALS',
            value: params.projectId
          }]
        };

        if(versionId > 0) {
          dynamicQuery.conditions.push({
            field: 'versionId',
            condition: 'EQUALS',
            value: versionId
          });
        }

        var projects = Ext.create("Common.data.ApiReader").load(dynamicQuery);

        if (projects != null && projects[0] != null && projects[0].projectCategoryCode != undefined) {
          params.projectCategoryCode = projects[0].projectCategoryCode;
        } else {
          Common.Msg.Error('未获取到对应项目类型！');
        }
      } else {
        Common.Msg.Error('未获取到对应预算模版！');
        return false;
      }
    }

    //设置项目数据类型
    var dataType = "@@Project@@";
    if (params.budgetType != undefined && params.budgetType != null && params.budgetType == "Fund") {
      dataType += "Fund@@";
    } else if (params.budgetType != undefined && params.budgetType != null && params.budgetType == "Expenses") {
      dataType += "Expenses@@";
    } else {
      dataType += "Cost@@";
    }
    params.dataType = dataType;

    $this.params = params;
    $this.projectId = params.projectId;
    $this.versionId = versionId;

    //获取项目类型对应的费用项
    var flag = $this.getExpenseItems($this.params.projectCategoryCode);
    if (!flag) {
      return false;
    }

    //获取项目类型对应的表单
    $this.getProjectFrom($this.params.projectCategoryCode);

    if (isEdit && $this.isPhoneTraffic()) {
      var conditions = [{
        field: "domainId",
        condition: "EQUALS",
        type: "String",
        value: params.domainId
      }, {
        field: "setOfBooksId",
        condition: "EQUALS",
        type: "Integer",
        value: params.setOfBooksId
      }, {
        field: "period",
        condition: "EQUALS",
        type: "String",
        value: params.period
      }, {
        field: "organizationId",
        condition: "EQUALS",
        type: "Integer",
        value: params.organizationId
      }, {
        field: "dataType",
        condition: "STARTS_WITH",
        type: "String",
        value: "@@Project@@"
      }, {
        field: "budgetObjectType",
        condition: "EQUALS",
        type: "String",
        value: "BudgetItem"
      }, {
        field: 'budgetItemGroupId',
        condition: 'EQUALS',
        value: params.budgetGroupId
      }];

      if (params.projectId > 0) {
        conditions.push({
          field: 'projectId',
          condition: 'NOT_EQUALS',
          value: params.projectId
        });
      }

      var projects = Ext.create("Common.data.ApiReader").load({
        api: 'relatedProjectItem',
        conditions: conditions
      });

      if (projects.length > 0) {
        Common.Msg.Alert("当前模板只能创建一个项目！");
        return false;
      }
    }

    if (func != null) {
      this.loadProjectGrid = func;
    }
    this.show();

    var items = [];
    items.push({
      xtype: "projectFormPanel",
      border: true,
      margin: "5 5 5 5",
      readOnly: !isEdit
    });

    if ($this.isCommon()) {
      $this.setHeight(450);
      items.push({
        xtype: "projectItemGrid",
        border: true,
        height: 320,
        margin: "5 5 5 5",
        readOnly: !isEdit
      });
    }

    if ($this.isPhoneTraffic()) {
      if ($this.mark == 'FixedTraffic') {
        $this.setHeight(385);
      }
      items = this.insertPhoneTrafficGrid(items, isEdit);

      $this.add(items);

      var projectName = "移动电话";
      if ($this.mark == 'FixedTraffic') {
        projectName = "定额交通";
      }
      Ext.getCmp("project-textfield-projectName").setValue(projectName);

      this.down("phoneTrafficGrid").loadGrid();
      this.down("phoneTrafficPanel").loadGrid();
    }

    if ($this.isTravel()) {
      items.push($this.insertTravelItems($this.mark, isEdit));

      items.push({
        xtype: "travelGrid",
        margin: "0 5 5 5",
        readOnly: !isEdit,
        mark: $this.mark
      });

      $this.add(items);

      this.down('travelPanel').inita();
      this.down('travelPanel').afterShow(isEdit);
      this.down("travelGrid").loadView();
    }

    if ($this.items.length == 0) {
      $this.add(items);
    }

    var projectFormPanel = $this.down('projectFormPanel');
    if (isEdit) {
      //显示按钮
      if (this.down("button[action=save]") != null) {
        this.down("button[action=save]").setVisible(true);
      }
      if (this.down("button[action=close]") != null) {
        this.down("button[action=close]").setText('取消');
      }

      if ($this.isPhoneTraffic()) {
        //设置Form为只读状态
        projectFormPanel.setEditedFieldStyle(projectFormPanel, 'background-color: white;');
        projectFormPanel.setReadOnly(projectFormPanel);
      } else {
        //设置Form为编辑状态
        projectFormPanel.setEditedFieldStyle(projectFormPanel, 'background-color: #FFFACD;');
        projectFormPanel.setEditable(projectFormPanel);
      }
    } else {
      //隐藏按钮
      if (this.down("button[action=save]") != null) {
        this.down("button[action=save]").setVisible(false);
      }
      if (this.down("button[action=close]") != null) {
        this.down("button[action=close]").setText('关闭');
      }

      //设置Form为只读状态
      projectFormPanel.setEditedFieldStyle(projectFormPanel, 'background-color: white;');
      projectFormPanel.setReadOnly(projectFormPanel);
    }
    //重新布局
    this.down("projectFormPanel").doComponentLayout();

    $this.loadData();
  },
  getExpenseItems: function (projectCategoryCode) {
    var $this = this;

    var projectCategoryItems = Ext.create("Common.data.ApiReader").load({
      api: 'projectCategoryItem',
      conditions: [
        {
          field: 'domainId',
          condition: 'EQUALS',
          value: domainId
        },
        {
          field: 'categoryCode',
          condition: 'EQUALS',
          value: projectCategoryCode
        }
      ],
      joins: [
        {
          type: 'projectCategory',
          fields: {
            id: 'categoryId',
            code: 'categoryCode'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'projectCategoryId'
            }
          ]
        },
        {
          type: 'expenseItem',
          fields: {
            name: 'expenseItemName'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'expenseItemId'
            }
          ]
        }
      ]
    });

    if (projectCategoryItems != null && projectCategoryItems.length > 0) {
      var expenseItems = [];

      for (var i = 0; i < projectCategoryItems.length; i++) {
        if (projectCategoryItems[i].expenseItemId != null && projectCategoryItems[i].expenseItemId != ''
          && projectCategoryItems[i].expenseItemName != null && projectCategoryItems[i].expenseItemName != '') {
          expenseItems[expenseItems.length] = {
            expenseItemId: projectCategoryItems[i].expenseItemId,
            expenseItemName: projectCategoryItems[i].expenseItemName
          }
        }
      }

      $this.expenseItems = expenseItems;
      $this.params.projectCategoryId = projectCategoryItems[0].projectCategoryId;
      return true;
    } else {
      Common.Msg.Error('当前项目类型未配置与费用项的关联关系！');
      return false;
    }
  },
  getProjectFrom: function (projectCategoryCode) {
    var $this = this;

    var projectFroms = $this.getDictionaryOption('ProjectCategoryRelationProjectForm', projectCategoryCode);
    if (projectFroms != null && projectFroms.length > 0 && projectFroms[0] != undefined
      && projectFroms[0].optionName != undefined && projectFroms[0].optionName != '') {
      $this.mark = projectFroms[0].optionName;
    }
  },
  getDictionaryOption: function (dictionaryCode, optionCode) {
    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      },
      {
        field: 'code',
        condition: 'EQUALS',
        value: dictionaryCode
      }
    ];

    if (optionCode != undefined && optionCode != '') {
      conditions.push({
        field: 'optionCode',
        condition: 'EQUALS',
        value: optionCode
      })
    }

    var dictionaryOptions = Ext.create("Common.data.ApiReader").load({
      api: 'dictionary',
      conditions: conditions,
      sorts: [
        {
          'field': 'optionId',
          'direction': 'ASC'
        }
      ],
      joins: [
        {
          type: 'dictionaryOption',
          fields: {
            id: 'optionId',
            names: 'optionName',
            code: 'optionCode'
          },
          conditions: [
            {
              field: 'dictionaryId',
              condition: 'EQUALS',
              type: 'Field',
              value: 'id'
            }
          ]
        }]
    });

    return dictionaryOptions;
  },
  loadData: function (id) {
    var $this = this;

    $this.down('projectFormPanel').loadProject();

    if ($this.isCommon()) {
      $this.down('projectItemGrid').loadGrid($this.projectId, $this.versionId);
    } else if ($this.isTravel()) {
      $this.loadProjectItemData();
      $this.down('travelPanel').loadTravelData();
      $this.down('travelGrid').loadTravelMonthPlanData();
      $this.down('travelGrid').loadTravelYearPlanData();
    } else if ($this.isPhoneTraffic()) {
      $this.loadProjectItemData();
      $this.down('phoneTrafficGrid').loadPhoneTrafficDetails();
      $this.down('phoneTrafficGrid').loadPhoneTrafficMonthPlan();
      $this.down('phoneTrafficGrid').loadPhoneTrafficYearPlan();
    }
  },
  insertPhoneTrafficGrid: function (items, isEdit) {
    var data = this.getDictionaryOption('ProjectCategoryRelationBudgetScene', this.params.projectCategoryCode);
    var formulas = this.getBudgetBenchmark(data[0].optionName);
    var array = data[0].optionName.split('@@');
    var MobilePhoneFeeLevel = this.getDictionaryOption(array[1]);
    var level = [];
    for (var i = 0; i < MobilePhoneFeeLevel.length; i++) {
      var code = MobilePhoneFeeLevel[i].optionCode;
      var name = MobilePhoneFeeLevel[i].optionName;
      level.push({code: code, name: name});
    }

    items.push({
      xtype: 'phoneTrafficGrid',
      margin: "5 5 5 5",
      readOnly: !isEdit,
      formulas: formulas,
      level: level
    });

    items.push({
      xtype: 'phoneTrafficPanel',
      margin: "0 5 5 5",
      readOnly: !isEdit,
      level: level
    });

    return items;
  },
  insertTravelItems: function (mark, isEdit) {
    var $this = this;
    var projectCategoryCode = $this.params.projectCategoryCode;
    var data = this.getDictionaryOption('ProjectCategoryRelationBudgetScene', projectCategoryCode);
    var array = data[0].optionName.split('@@');
    var travelExpenses = this.getDictionaryOption(array[0]);
    var employeeLevel = this.getDictionaryOption(array[1]);
    var businessCityClass = array.length == 3 ? this.getDictionaryOption(array[2]) : undefined;

    var travelExpensesCode = new Array();
    var travelExpensesName = new Array();
    $this.getCodeAndName(travelExpenses, travelExpensesCode, travelExpensesName);

    var tableHeadsCode = new Array('level', 'Days', 'Num', 'BusinessCity');
    var tableHeadsName = new Array('级别', '天数', '人数', '出差地级次');
    tableHeadsCode = tableHeadsCode.concat(travelExpensesCode);
    tableHeadsName = tableHeadsName.concat(travelExpensesName);


    var employeeLevelCode = new Array();
    var employeeLevelName = new Array();
    $this.getCodeAndName(employeeLevel, employeeLevelCode, employeeLevelName);

    employeeLevelCode = employeeLevelCode.concat('totalAmount');
    employeeLevelName = employeeLevelName.concat('小计');

    var businessCityClassCode = new Array();
    var businessCityClassName = new Array();
    if (businessCityClass != undefined) {
      $this.getCodeAndName(businessCityClass, businessCityClassCode, businessCityClassName);
    }
    var formulas = this.getBudgetBenchmark(data[0].optionName);
    var items = {
      xtype: "travelPanel",
      margin: "5 5 5 5",
      readOnly: !isEdit,
      dockedItems: $this.addDockedItems(isEdit),
      projectCategoryCode: projectCategoryCode,
      mark: mark,
      formulas: formulas,
      tableHeadsCode: tableHeadsCode,
      tableHeadsName: tableHeadsName,
      tableLeftCode: employeeLevelCode,
      tableLeftName: employeeLevelName,
      travelExpensesCode: travelExpensesCode,
      employeeLevelCode: employeeLevelCode,
      businessCityClassCode: businessCityClassCode,
      businessCityClassName: businessCityClassName
    };
    return items;
  },
  addDockedItems: function (isEdit) {
    var dockedItems = {
      xtype: 'toolbar',
      style: 'backgrounGd-color: white;',
      border: true,
      items: [{
        id: 'project-textfield-projectItemName',
        xtype: "textfield",
        name: "name",
        fieldLabel: "费用行名称",
        labelSeparator: '',
        labelStyle: 'font-weight:bold;',
        labelAlign: 'right',
        labelWidth: 75,
        grow: true,
        growMin: 550,
        allowBlank: false,
        blankText: "请输入项目名称",
        readOnly: !isEdit,
        fieldStyle: isEdit ? 'background-color:#FFFACD;' : 'background-color: white;'
      }]
    };
    return dockedItems;
  },
  saveProject: function () {
    var $this = this;
    var params = this.params;
    var mark = $this.mark;

    if ($this.isTravel() && $this.down("travelGrid") != null
      && !$this.down("travelGrid").verifyTotalMonthWithYearAmount()) {
      return false;
    }

    if (params.budgetGroupId > 0) {
      var projectName = Ext.getCmp("project-textfield-projectName").getValue();
      var conditions = [{
        field: "domainId",
        condition: "EQUALS",
        type: "String",
        value: params.domainId
      }, {
        field: "setOfBooksId",
        condition: "EQUALS",
        type: "Integer",
        value: params.setOfBooksId
      }, {
        field: "period",
        condition: "EQUALS",
        type: "String",
        value: params.period
      }, {
        field: "organizationId",
        condition: "EQUALS",
        type: "Integer",
        value: params.organizationId
      }, {
        field: "dataType",
        condition: "STARTS_WITH",
        type: "String",
        value: "@@Project@@"
      }, {
        field: "budgetObjectType",
        condition: "EQUALS",
        type: "String",
        value: "BudgetItem"
      }, {
        field: 'budgetItemGroupId',
        condition: 'EQUALS',
        value: params.budgetGroupId
      }, {
        field: 'projectName',
        condition: 'EQUALS',
        value: projectName
      }];

      if (this.projectId > 0) {
        conditions.push({
          field: 'projectId',
          condition: 'NOT_EQUALS',
          value: this.projectId
        })
      }

      var projects = Ext.create("Common.data.ApiReader").load({
        api: 'relatedProjectItem',
        joins: [
          {
            type: 'project',
            fields: {
              name: 'projectName'
            },
            conditions: [{
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'projectId'
            }]
          }
        ],
        conditions: conditions
      });

      if (projects.length > 0) {
        Common.Msg.Alert("当前模板此项目名称已存在，请修改项目名称！");
        return false;
      }
    }

    var isSuccess = $this.down("projectFormPanel").saveProject();
    if (!isSuccess) {
      return false;
    }

    if ($this.isTravel()) {
      var projectItemName = Ext.getCmp("project-textfield-projectItemName").getValue();
      if(!$this.saveTravelProjectItem(projectItemName)){
        return false;
      }
      $this.down("travelPanel").saveTravelData();

      $this.down("travelGrid").saveTravelMonthPlanData();
      $this.down("travelGrid").saveTravelYearPlanData();
    } else if ($this.isPhoneTraffic()) {
      var expenseItemName = $this.expenseItems[0].expenseItemName;
      if(!$this.savePhoneTrafficProjectItem(expenseItemName)){
        return false;
      }
      $this.down('phoneTrafficGrid').savePhoneTrafficDetails();
      $this.down('phoneTrafficGrid').savePhoneTrafficMonthPlanData();
      $this.down('phoneTrafficGrid').savePhoneTrafficYearPlanData();
    }

    var projects = Ext.create("Common.data.ApiReader").load({
      api: 'project',
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        value: this.projectId
      }]
    });

    var projectItems = Ext.create("Common.data.ApiReader").load({
      api: 'projectItem',
      joins: [
        {
          type: 'expenseItem',
          fields: {
            budgetItemId: 'budgetItemId'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'expenseItemId'
            }
          ]
        }
      ],
      conditions: [{
        field: 'projectId',
        condition: 'EQUALS',
        value: this.projectId
      }]
    });


    //处理项目和费用行状态
    if (projects != null && projects.length > 0 && projects[0] != null) {
      if (projects[0].status == undefined || projects[0].status == ''
        || projects[0].status == 'Draft' || projects[0].status == 'PreAdjusting') {
        projects[0].status = 'PreApproved';
      }
    }

    if (projectItems != null && projectItems.length > 0) {
      for (var i = 0; i < projectItems.length; i++) {
        if (projectItems[i] != null && (projectItems[i].status == undefined || projectItems[i].status == ''
          || projectItems[i].status == 'Draft' || projectItems[i].status == 'PreAdjusting')) {
          projectItems[i].status = 'PreApproved';
        }
      }
    }

    Ext.create("Common.data.ApiReader").createOrUpdate('project', projects, null, false);
    Ext.create("Common.data.ApiReader").createOrUpdate('projectItem', projectItems, null, false);

    //TODO 保存项目版本

    //保存关联数据
    var relatedProjectItems = [];
    if (projectItems != null && projectItems.length > 0) {
      for (var i = 0; i < projectItems.length; i++) {
        if (projectItems[i] != null) {
          var relatedProjectItem = {
            projectVersionId: -1,
            projectId: projectItems[i].projectId,
            projectItemId: projectItems[i].id,
            domainId: $this.params.domainId,
            setOfBooksId: $this.params.setOfBooksId,
            period: projectItems[i].period,
            organizationId: projectItems[i].organizationId,
            dataType: $this.params.dataType,
            budgetItemGroupId: $this.params.budgetGroupId,
            budgetObjectType: 'BudgetItem',
            budgetObjectId: projectItems[i].budgetItemId,
            amount: projectItems[i].amount,
            status: 'Draft'
          };
          delete relatedProjectItem.id;
          relatedProjectItems.push(relatedProjectItem)
        }
      }
    }

    //获取项目原有关联关系
    var oldRelatedProjectItems = Ext.create("Common.data.ApiReader").load({
      api: 'relatedProjectItem',
      conditions: [{
        field: 'projectId',
        condition: 'EQUALS',
        value: this.projectId
      }]
    });

    var deleteRelatedProjectItems = [];
    if (oldRelatedProjectItems != null && oldRelatedProjectItems.length > 0) {
      for (var i = 0; i < oldRelatedProjectItems.length; i++) {
        var isDelete = true;
        for (var j = 0; j < relatedProjectItems.length; j++) {
          if (!relatedProjectItems[j].isUpdate
            && relatedProjectItems[j].domainId == oldRelatedProjectItems[i].domainId
            && relatedProjectItems[j].setOfBooksId == oldRelatedProjectItems[i].setOfBooksId
            && relatedProjectItems[j].period == oldRelatedProjectItems[i].period
            && relatedProjectItems[j].projectId == oldRelatedProjectItems[i].projectId
            && relatedProjectItems[j].projectItemId == oldRelatedProjectItems[i].projectItemId
            && relatedProjectItems[j].organizationId == oldRelatedProjectItems[i].organizationId) {
            relatedProjectItems[j].id = oldRelatedProjectItems[i].id;
            relatedProjectItems[j].dataType = oldRelatedProjectItems[i].dataType;
            relatedProjectItems[j].status = oldRelatedProjectItems[i].status;
            relatedProjectItems[j].isUpdate = true;
            isDelete = false;
            break;
          }
        }
        if (isDelete) {
          deleteRelatedProjectItems[deleteRelatedProjectItems.length] = oldRelatedProjectItems[i];
        }
      }
    }

    //删除项目关联关系
    if (deleteRelatedProjectItems.length > 0) {
      Ext.create("Common.data.ApiReader").directDelete_('relatedProjectItem', deleteRelatedProjectItems, null);
    }

    //保存和更新项目关联关系
    if (relatedProjectItems.length > 0) {
      Ext.create("Common.data.ApiReader").createOrUpdate('relatedProjectItem', relatedProjectItems, null, false);
    }

    $this.isUpdate = false;

    this.loadProjectGrid();
    this.close();
  },
  loadProjectItemData: function () {
    var $this = this;
    if ($this.projectId == -1) {
      return;
    }
    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      },
      {
        field: 'projectId',
        condition: 'EQUALS',
        value: $this.projectId
      }
    ];
    var projectItem = Ext.create("Common.data.ApiReader").load({
      api: 'projectItem',
      fields: {
        id: 'projectItemId',
        description: 'description'
      },
      conditions: conditions
    });
    if ($this.down('travelPanel') != undefined && projectItem != null
      && projectItem[0] != null && projectItem[0].description != undefined) {
      Ext.getCmp("project-textfield-projectItemName").setValue(projectItem[0].description);
    }
    for (var i = 0; i < projectItem.length; i++) {
      projectItem[i] = {data: projectItem[i]}
    }
    $this.projectItemArray = projectItem;
    if ($this.down('travelPanel') != undefined) {
      var records = $this.down("travelGrid").getStore().getRange();
      for (var i = 0; i < projectItem.length; i++) {
        for (var j = 0; j < records.length; j++) {
          if (projectItem[i].data.expenseItemId == records[j].data.expenseItemId) {
            records[j].data.id = projectItem[i].data.id;
            records[j].commit()
          }
        }
      }
    } else if ($this.down('phoneTrafficPanel') != undefined) {
      var records = $this.down("phoneTrafficPanel").getStore().getRange();
      records[records.length - 1].data.id = projectItem[0].data.id;
      records[records.length - 1].commit();
    }

  },
  saveTravelProjectItem: function (name) {
    var $this = this;
    var params = $this.params;
    var projectId = $this.projectId;
    if (projectId <= 0) {
      return false;
    }

    if (name != undefined && name != '') {
      var projectItem = {
        domainId: params.domainId,
        projectId: projectId,
        description: name,
        period: params.period,
        organizationId: params.organizationId,
        status: 'Draft'
      };

      //费用行开始结束时间
      if (projectItem.period != undefined && projectItem.period != null && projectItem.period != ""
        && projectItem.period.length >= 4 && (projectItem.startTime == undefined || projectItem.startTime == ''
        || projectItem.endTime == undefined || projectItem.endTime == '')) {
        projectItem.startTime = projectItem.period.substring(0, 4) + "-01-01T00:00:00.000+0800";
        projectItem.endTime = projectItem.period.substring(0, 4) + "-12-31T23:59:59.000+0800";
      }

      var projectItems = [];
      var deleteProjectItems = [];
      var expenseItems = $this.expenseItems;
      var travelRecords = $this.down("travelGrid").getStore().getRange();
      for (var i = 0; i < travelRecords.length; i++) {
        var newProjectItem = new Object();
        Ext.apply(newProjectItem, projectItem);
        if (travelRecords[i].data.id != undefined && travelRecords[i].data.id > 0) {
          newProjectItem.id = travelRecords[i].data.id;
        }
        newProjectItem.expenseItemId = travelRecords[i].data.expenseItemId;
        newProjectItem.amount = travelRecords[i].data.yearAmount == '' ? 0 : travelRecords[i].data.yearAmount;
        if(newProjectItem.amount != null && newProjectItem.amount != 0.00) {
          projectItems.push(newProjectItem);
        }else {
          if(newProjectItem.id != undefined && newProjectItem.id != null && newProjectItem.id != '') {
            deleteProjectItems.push(newProjectItem);
          }
        }
      }

      if(projectItems.length == 0) {
        Common.Msg.Alert("请输入差旅预算！");
        return false;
      }

      var callback = function (result) {
        $this.projectItemArray = result;
        for (var i = 0; i < result.length; i++) {
          for (var j = 0; j < travelRecords.length; j++) {
            if (result[i].data.expenseItemId == travelRecords[j].data.expenseItemId) {
              travelRecords[j].data.id = result[i].data.id;
              travelRecords[j].commit();
            }
          }
        }
      };

      Ext.create("Common.data.ApiReader").createOrUpdate('projectItem', projectItems, callback, false);

      if(deleteProjectItems.length > 0) {
        Ext.create("Common.data.ApiReader").directDelete_('projectItem', deleteProjectItems, null);
      }
    } else {
      Common.Msg.Alert("请选择或输入必填项！");
      return false;
    }

    return true;
  },
  savePhoneTrafficProjectItem: function (name) {
    var $this = this;
    var params = $this.params;
    var projectId = $this.projectId;
    if (projectId <= 0) {
      return false;
    }

    if (name != undefined && name != '') {
      var projectItem = {
        domainId: params.domainId,
        projectId: projectId,
        description: name,
        period: params.period,
        organizationId: params.organizationId,
        status: 'Draft'
      };

      //费用行开始结束时间
      if (projectItem.period != undefined && projectItem.period != null && projectItem.period != ""
        && projectItem.period.length >= 4 && (projectItem.startTime == undefined || projectItem.startTime == ''
        || projectItem.endTime == undefined || projectItem.endTime == '')) {
        projectItem.startTime = projectItem.period.substring(0, 4) + "-01-01T00:00:00.000+0800";
        projectItem.endTime = projectItem.period.substring(0, 4) + "-12-31T23:59:59.000+0800";
      }

      var projectItems = [];
      var expenseItems = $this.expenseItems;
      var phoneTrafficRecords = $this.down("phoneTrafficPanel").getStore().getRange();
      var phoneTrafficRecord = phoneTrafficRecords[phoneTrafficRecords.length - 1].data;

      if (phoneTrafficRecord.id != undefined && phoneTrafficRecord.id > 0) {
        projectItem.id = phoneTrafficRecord.id;
      }
      if (phoneTrafficRecord.expenseItemId != undefined && phoneTrafficRecord.expenseItemId > 0) {
        projectItem.expenseItemId = phoneTrafficRecord.expenseItemId;
      } else {
        var expenseItems = $this.expenseItems;
        if (expenseItems[0] != null && expenseItems[0].expenseItemId != undefined && expenseItems[0].expenseItemId > 0) {
          projectItem.expenseItemId = expenseItems[0].expenseItemId;
        }
      }

      projectItem.amount = phoneTrafficRecord.yearAmount;
      if(projectItem.amount != null && projectItem.amount != '') {
        projectItems.push(projectItem);
      }else {
        Common.Msg.Alert("请输入" + name + "人数！");
        return false;
      }

      var callback = function (result) {
        $this.projectItemArray = result;
        var records = $this.down("phoneTrafficPanel").getStore().getRange();
        records[records.length - 1].data.id = result[0].data.id;
        records[records.length - 1].commit();
      };

      Ext.create("Common.data.ApiReader").createOrUpdate('projectItem', projectItems, callback, false);
    } else {
      Common.Msg.Alert("请选择或输入必填项！");
      return false;
    }

    return true;
  },
  getBudgetBenchmark: function (budgetSceneCode) {
    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      },
      {
        field: 'sceneCode',
        condition: 'EQUALS',
        value: budgetSceneCode/*'TravelExpenses@@DomesticTravelLevel@@BusinessCityClass'*/
      }
    ];
    var data = Ext.create("Common.data.ApiReader").load({
      api: 'budgetBenchmark',
      conditions: conditions
    });
    return data;
  },
  getCodeAndName: function (array, code, name) {
    for (var i = 0; i < array.length; i++) {
      code.push(array[i].optionCode);
      name.push(array[i].optionName);
    }
  },
  isTravel: function () {
    var mark = this.mark;
    if (mark == 'Recruit' || mark == 'ForeignTravel' ||
      mark == 'ForeignTrain' || mark == 'DomesticTravel' ||
      mark == 'DeptTrain' || mark == 'DeptMeeting') {
      return true;
    }
  },
  isCommon: function () {
    var mark = this.mark;
    if (mark == 'Common' || mark == 'Entertain' || mark == 'UnifiedTrain' || mark == 'CommonTime') {
      return true;
    }
  },
  isPhoneTraffic: function () {
    var mark = this.mark;
    if (mark == 'MobilePhone' || mark == 'FixedTraffic') {
      return true;
    }
  }
});
