Ext.define("Project.view.toolbar.SearchToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.projectSearchToolbar",
  margin: '0 0 5 0',
  items: [{
    id: 'project-search-setOfBooks',
    xtype: 'setOfBookCombo',
    hidden: true,
    domainId: domainId,
    loadAfterRender: true,
    observable: true
  }, {
    id: 'project-search-period',
    xtype: 'periodCombo',
    observable: true
  }, {
    id: 'project-search-organizationId',
    xtype: 'orgCombo',
    displayField: 'showName',
    labelWidth: 35,
    width: 200,
    initParams: {
      domainId: domainId
    },
    loadAfterRender: true,
    observable: true
  }, {
    id: 'project-search-projectAllCategory',
    xtype: 'apiCombox',
    alias: 'widget.projectAllClassCombo',
    width: 180,
    labelWidth: 65,
    selectType: "combo",
    loadAfterRender: true,
    includeAllItem:true,
    readCookie: true,
    fieldLabel: '项目类型',
    name: 'projectCategoryId',
    api: 'ProjectCategory',
    fields: ['id', 'code', 'name'],
    sorts: [{
      field: 'sortCode',
      direction: 'ASC'
    }],
    queryMode: 'local',
    displayField: 'name',
    observable: true
  }, {
    id: 'project-search-projectSearch',
    xtype: 'textfield',
    alias: "widget.projectSearch",
    width: 180,
    name: "projectSearch",
    labelWidth: 35,
    fieldLabel: '',
    emptyText: '项目编号/项目名称',
    observable: true
  }, {
    id: 'project-search-queryButton', text: '查询', xtype: "button", observable: true,
    paramsProperties: ["setOfBooksId", "period", "organizationId", "projectCategoryId", "projectSearch"],
    params: {},
    pushParam: function (param) {
      Ext.apply(this.params, param);
    },
    isLoadAll: function () {
      for (var i = 0; i < this.paramsProperties.length; i++) {
        if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
          return false;
        }
      }
      return true;
    },
    getParams: function () {
      return this.params;
    },
    subscription: {
      observers: [
        'project-search-setOfBooks',
        'project-search-period',
        'project-search-organizationId',
        'project-search-projectAllCategory',
        'project-search-projectSearch'
      ],
      operate: {
        onNext: function (param) {
          this.pushParam(param);
          if (this.isLoadAll()) {
            this.publish(this.getParams());
          }
        }
      }
    },
    handler: function () {
      this.onNext({});
    }
  }]
});

