Ext.define('Budget.view.panel.PlanningTabComparePanel', {
  extend: 'Ext.tab.Panel',
  alias: "widget.planningTabComparePanel",
  processPage: false,
  padding: [0, 0, 0, 0],
  items: []
});