Ext.define("Budget.view.toolbar.SearchToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.planningSearchToolbar",
  margin: '0 0 5 0',
  items: [{
    id: 'planning-search-setOfBooks',
    xtype: 'setOfBookCombo',
    hidden: true,
    domainId: domainId,
    loadAfterRender: true,
    observable: true
  }, {
    id: 'planning-search-period',
    xtype: 'periodCombo',
    observable: true
  }, {
    id: 'planning-search-organizationId',
    xtype: 'orgCombo',
    displayField: 'showName',
    labelWidth: 35,
    width: 200,
    initParams: {
      domainId: domainId
    },
    loadAfterRender: true,
    observable: true
  }, {
    id: "planning-search-status",
    xtype: "apiDictionaryCombox",
    name: "status",
    fieldLabel: "状态",
    initParams: {
      domainId: domainId,
      dictionaryCode: "BudgetStatus"
    },
    labelWidth: 35,
    width: 140,
    includeAllItem: false,
    loadAfterRender: true,
    readOnly: true,
    observable: true
  }, {
    id: 'planning-search-queryButton', text: '查询', xtype: "button", hidden: true, observable: true,
    paramsProperties: ["setOfBooksId", "period", "organizationId", "organizationCode", "budgetType"],
    params: {},
    pushParam: function(param) {
      if(param.organizationCode != undefined && param.organizationCode != '') {
        this.setBudgetTypeValue(param.organizationCode);
      }
      Ext.apply(this.params, param);
    },
    setBudgetTypeValue: function(organizationCode) {
      var data = Ext.create("Common.data.ApiReader").load({
        api: 'dictionaryOption',
        conditions: [
          {
            field: 'domainId',
            condition: 'EQUALS',
            value: domainId
          },
          {
            field: 'dictionaryCode',
            condition: 'EQUALS',
            value: 'OrganizationRelationBudgetType'
          }, {
            field: 'code',
            condition: 'EQUALS',
            value: organizationCode
          }
        ]
      });

      if(data.length > 0 && data[0] != undefined && data[0].name != undefined && data[0].name != '') {
        this.params.budgetType = data[0].name;
      }else {
        this.params.budgetType = '';
      }
    },
    isLoadAll: function() {
      for (var i = 0; i < this.paramsProperties.length; i++) {
        if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
          return false;
        }
      }
      return true;
    },
    getParams: function() {
      return this.params;
    },
    subscription: {
      observers: [
        'planning-search-setOfBooks',
        'planning-search-period',
        'planning-search-organizationId'
      ],
      operate: {
        onNext: function(param) {
          this.pushParam(param);
          if (this.isLoadAll()) {
            this.publish(this.getParams());
          }
        }
      }
    },
    handler: function() {
      this.onNext({});
    }
  }],
  initComponent: function () {
    if(pageInitParam.isProcessPage != undefined && pageInitParam.isProcessPage) {
      this.items[2].type = "global";
    }
    this.callParent(arguments);
  }
});

