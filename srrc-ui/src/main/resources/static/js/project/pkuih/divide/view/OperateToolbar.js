Ext.define("Divide.view.OperateToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.divideOperateToolbar",
  margin: '-5 0 5 0',
  style: 'border-top: 0px;',
  items: [{
    id: 'divide-search-budgetType',
    xtype: "budgetType",
    observable: true
  }, {
    id: 'divide-operate-save',
    text: '划分',
    xtype: "button",
    action: 'divide',
    observable: true
  }]
});