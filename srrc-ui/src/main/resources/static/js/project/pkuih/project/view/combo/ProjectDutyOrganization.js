Ext.define('Project.view.combo.ProjectDutyOrganization', {
  extend: 'Ext.form.ComboBox',
  alias: 'widget.projectDutyOrganization',
  width: 200,
  labelWidth: 65,
  fieldLabel: '主管部门',
  emptyText: '请选择',
  typeAhead: false,
  editable: false,
  name: 'dutyOrgId',
  store: Ext.create('Ext.data.Store', {
    fields: ['id', 'showName', 'sortCode'],
    proxy: {
      type: 'ajax',
      url: contextPath + '/api/2.0.0/cql',
      actionMethods: {
        read: 'POST'
      },
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      paramsAsJson: true,
      reader: {
        type: 'json',
        root: 'data'
      }
    },
    listeners: {
      beforeload: function(store) {
        var dynamicQuery = {
          queryType: 'read',
          type: 'organization',
          sorts: [{
            field: 'sortCode',
            direction: 'ASC'
          }],
          page: 1,
          size: 1000000
        };
        store.proxy.extraParams = dynamicQuery;
      }
    },
    autoLoad: true
  }),
  queryMode: 'local',
  displayField: 'showName',
  valueField: 'id',
  readCookie: true,
  initComponent: function() {
    var $this = this;

    this.store.on('load', function(store) {
      var dutyOrgId = $this.value;

      var isSelect = false;
      for (var i = 0; i < store.getTotalCount(); i++) {
        if (store.getAt(i).data.id == dutyOrgId) {
          $this.select(store.getAt(i));
          isSelect = true;
          break;
        }
      }

      if (!isSelect) {
        $this.select(store.getAt(0));
      }
    });

    this.callParent(arguments);
  }
});