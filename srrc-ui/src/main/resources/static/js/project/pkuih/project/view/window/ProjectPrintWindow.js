Ext.define("Project.view.window.ProjectPrintWindow", {
  extend: 'Ext.window.Window',
  alias: 'widget.projectPrintWindow',
  title: '打印视图',
  layout: 'fit',
  modal: true,
  shadow: false,
  plain: true,
  resizable: false,
  width: 400,
  height: 200,
  closeAction: "hide",
  items: [],
  buttons: [
    {
      text: '确定',
      action: "showPrintView"
    },
    {
      text: '关闭',
      handler: function () {
        this.up("window").hide();
      }
    }
  ],
  getOptionValue: function () {
    var optionValue = "";
    var checkValue = this.down("checkboxgroup").getChecked();
    for (var i = 0; i < checkValue.length; i++) {
      if (optionValue != "") {
        optionValue += ",";
      }
      optionValue += checkValue[i].inputValue;
    }
    return optionValue;
  },
  initComponent: function () {
    this.items = [{
      xtype: 'checkboxgroup',
      columns: 3,
      items: [
        {boxLabel: '基本信息', inputValue: 'projectBasePanel', margin: '10 10 10 10', checked: true},
        {boxLabel: '占用明细', inputValue: 'projectBillPanel', margin: '10 10 10 10', checked: false},
        {boxLabel: '审批信息', inputValue: 'projectProcessPanel', margin: '10 10 10 10', checked: false},
        {boxLabel: '版本信息', inputValue: 'projectVersionPanel', margin: '10 10 10 10', checked: false}
      ]
    }];
    this.callParent(arguments);
  }
});