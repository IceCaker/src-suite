Ext.define('AssetsBudget.view.AssetsBudgetTabPanel', {
  extend: 'Ext.tab.Panel',
  alias: "widget.assetsBudgetTabPanel",
  processPage: false,
  padding: [0, 0, 0, 0],
  loadingPageTime: false,
  items: [],
  subscription: {
    observers: ['report-search-queryButton'],
    operate: {
      onNext: function(params) {
        params.domainId = domainId;
        this.loadTabs(params);
      }
    }
  },
  loadTabs: function(params) {
    this.loadingPageTime = true;
    this.removeAll();
    var tabs = [];
    Ext.Ajax.request({
      url: contextPath + '/pkuih/assets/budget/tab',
      params: params,
      async: false,
      timeout: 30000000,
      method: 'POST',
      callback: function(options, success, response) {
        tabs = Ext.JSON.decode(response.responseText);
      }
    });
    if (tabs.length > 0) {
      //显示和隐藏操作按钮
      //Ext.getCmp('report-search-getButton').setVisible(true);
      Ext.getCmp('report-search-exportButton').setVisible(true);
      Ext.getCmp('report-search-importButton').setVisible(true);

      //验证是否可以导入
      if (this.verifyOrganizationStatus(params)) {
        Ext.getCmp('report-search-importButton').setDisabled(false);
      }else {
        Ext.getCmp('report-search-importButton').setDisabled(true);
      }

      var activeTab = null;
      for (var i = 0; i < tabs.length; i++) {
          if (tabs[i] != null && tabs[i] != '') {
              var panel = Ext.widget("assetsBudgetDataPanel", {
                  title: tabs[i],
                  sheetIndex: i,
                  params: params
              });
              if (activeTab == null) {
                  activeTab = panel;
              }
              this.add(panel);
          }
      }
      this.loadingPageTime = false;
      if (activeTab != null) {
        this.setActiveTab(activeTab);
        this.loadData(activeTab);
      }
    } else {
      //显示和隐藏操作按钮
      Ext.getCmp('report-search-getButton').setVisible(false);
      Ext.getCmp('report-search-exportButton').setVisible(false);
      Ext.getCmp('report-search-importButton').setVisible(false);
    }
  },
  loadData: function(panel) {
    var d = new Ext.util.DelayedTask(function() {
      panel.load();
    });
    d.delay(50);
  },
  endsWith: function(mainStr, str) {
    if (str == null || str == "" || mainStr.length == 0 || str.length > mainStr.length) {
      return false;
    } else if (mainStr.substring(mainStr.length - str.length) == str) {
      return true;
    } else {
      return false;
    }
  },
  verifyOrganizationStatus: function (params) {
    var $this = this;

    var organizations = Ext.create("Common.data.ApiReader").load({
      api: "organization",
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        value: params.domainId
      }, {
        field: 'parentId',
        condition: 'EQUALS',
        value: 0
      }]
    });

    if (organizations != null && organizations[0] != null
      && organizations[0].id != undefined && organizations[0].id != '') {
      var subOrganizationStatuses = Ext.create("Common.data.ApiReader").load({
        api: 'organization',
        fields: {
          id: 'id',
          name: 'name',
          parentId: 'parentId'
        },
        joins: [{
          type: 'budgetDataStatus',
          fields: {
            period: 'period',
            dataType: 'dataType',
            statusCode: 'statusCode',
            setOfBooksId: 'setOfBooksId',
            businessType: 'businessType'
          },
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'domainId'
          }, {
            field: 'organizationId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'id'
          }, {
            field: 'setOfBooksId',
            condition: 'EQUALS',
            type: 'Integer',
            value: params.setOfBooksId
          }, {
            field: 'period',
            condition: 'EQUALS',
            type: 'String',
            value: params.period
          }, {
            field: 'businessType',
            condition: 'STARTS_WITH',
            type: 'String',
            value: 'yearPlanning'
          }, {
            field: 'dataType',
            condition: 'STARTS_WITH',
            type: 'String',
            value: '@@Planning@@'
          }]
        }],
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          conditions: [{
            operator: 'OR',
            field: 'id',
            condition: 'EQUALS',
            type: 'Integer',
            value: organizations[0].id
          }]
        }]
      });

      for (var i = 0; i < subOrganizationStatuses.length; i++) {
        var subOrganizationStatus = subOrganizationStatuses[i];
        if (subOrganizationStatus != null) {
          var statusCode = 'Draft';
          if (subOrganizationStatus.statusCode != undefined && subOrganizationStatus.statusCode != '') {
            statusCode = subOrganizationStatus.statusCode;
          }
          if (subOrganizationStatus.id == organizations[0].id
            && (statusCode == 'Approving' || statusCode == 'Approved' || statusCode == 'Submitted'
            || statusCode == 'FirstTrailApproved' || statusCode == 'ReexamineApproved'
            || statusCode == 'FinalJudgmentApproved')) {
            return false;
          }
        }
      }
    }

    return true;
  },
  listeners: {
    beforetabchange: function(tabPanel, newCard, oldCard, eOpts) {
      if (tabPanel.loadingPageTime) {
        return;
      }
    },
    tabchange: function(tabPanel, newCard, oldCard, eOpts) {
      if (tabPanel.loadingPageTime) {
        return;
      }
      newCard.load();
    }
  }
});