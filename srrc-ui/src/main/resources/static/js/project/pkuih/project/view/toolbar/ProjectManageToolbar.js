Ext.require([
  'Common.button.Export'
]);
Ext.define("Project.view.toolbar.ProjectManageToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.manageToolbar",
  readOnly: false,
  isShowWindow: false,
  items: [],
  loadOperate: function (setting) {
    if (this.down("button[action=create]") != null) this.down("button[action=create]").setDisabled(!setting.create);
    if (this.down("button[action=copy]") != null) this.down("button[action=copy]").setDisabled(!setting.create);
    if (this.down("button[action=import]") != null) this.down("button[action=import]").setDisabled(!setting.create);
  },
  initComponent: function () {
    var items = [];
    var setting = {cancel: false, ways: ""};
    var isCreate = (pageInitParam.fromStatus.indexOf("Draft") >= 0);

    if (isCreate) {
      items[items.length] = {text: "新建", action: "create", hidden: this.readOnly, id: this.id + "-create"};
      items[items.length] = {text: "复制", action: "copy", hidden: this.readOnly, id: this.id + "-copy"};
      items[items.length] = {text: "删除", action: "delete", hidden: this.readOnly, id: this.id + "-delete"};
      items[items.length] = {text: "预立项", action: "confirm", hidden: this.readOnly, id: this.id + "-confirm"};
      items[items.length] = {text: "立项", action: "commit", hidden: true, id: this.id + "-commit"};
    }

    if (false && isCreate) {
      items[items.length] = {text: "导入", action: "import", hidden: this.readOnly, id: this.id + "-import"};

      items[items.length] = {
        xtype: "exportButton", hidden: this.readOnly,
        actionUrl: '', id: this.id + "-export"
      };
    }

    this.items = items;
    this.callParent(arguments);
  }
});