Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');
Ext.Loader.setPath('Pkuih.Common', contextPath + '/static/js/project/pkuih/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.WorkFlowManager',
  "Common.data.ApiReader",
  "Common.combo.ApiCombox",
  "Common.combo.Period",
  "Common.combo.Organization",
  "Common.combo.SetOfBook",
  "Common.combo.ApiDictionaryCombox",
  "Pkuih.Common.view.ProjectInfoWindow",
  "Pkuih.Common.view.FileUploadWindow",
  'Common.workflow.MainInfoData'
]);

Ext.application({
  name: 'Budget',
  appFolder: contextPath + '/static/js/project/pkuih/budget',
  controllers: ["PlanningController", "ProjectController"],
  launch: function() {
    Common.CommonManager.init(pageInitParam);

    Ext.tip.QuickTipManager.init();
    var initParams = {}

    for (var obj in pageInitParam) {
      initParams[obj] = pageInitParam[obj];
    }

    var panel = Ext.create("Ext.panel.Panel", {
      layout: 'border',
      border: false,
      bodyStyle: {
        background: "#FFFFFF"
      },
      height: 500,
      margin: '0 0 5 0',
      items: [{
        xtype: "planningSearchToolbar",
        name: "toolbarOne",
        margin: '0 0 5 0',
        region: 'north'
      }, {
        xtype: 'budgetGroupGrid',
        title: '预算模版',
        collapsible: true,
        split: true,
        scroll: true,
        hideHeaders: true,
        hideCollapseTool: true,
        width: 200,
        region: 'west'
      }, {
        xtype: "operateToolbar",
        name: "toolbarTwo",
        margin: '0 0 0 0',
        weight: -100,
        region: 'north'
      }, {
        xtype: 'planningTabPanel',
        scroll: true,
        processPage: true,
        tabBar: {
          items: [{
            xtype: 'component',
            flex: 1
          }, {
            xtype: "button",
            text: "附件",
            margin: '4 5 0 0',
            action: "fileUpload",
            hidden: true
          }]
        },
        region: 'center'
      }]
    });
    Common.workflow.MainInfoData.items.push(panel);

    var loadParams = {};
    for (var str in pageInitParam) {
      loadParams[str] = pageInitParam[str];
    }

    if (loadParams.setOfBooksId != undefined && loadParams.setOfBooksId != null
      && loadParams.setOfBooksId > 0 && Ext.getCmp('planning-search-setOfBooks') != undefined) {
      Ext.getCmp('planning-search-setOfBooks').setValue(loadParams.setOfBooksId);
    }
    if (loadParams.period != undefined && loadParams.period != null
      && loadParams.period != '' && Ext.getCmp('planning-search-period') != undefined) {
      Ext.getCmp('planning-search-period').setValue(loadParams.period);
    }
    if (loadParams.organizationId != undefined && loadParams.organizationId != null
      && loadParams.organizationId > 0 && Ext.getCmp('planning-search-organizationId') != undefined) {
      Ext.getCmp('planning-search-organizationId').setValue(loadParams.organizationId);
    }

    Ext.application('WorkFlow.DefaultApplication');
  }
});