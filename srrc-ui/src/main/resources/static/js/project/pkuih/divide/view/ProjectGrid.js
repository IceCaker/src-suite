Ext.define('Divide.view.ProjectGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.projectGrid',
  viewConfig: {
    loadMask: false,
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  enableColumnMove: false,
  params: {},
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'projectCode', type: 'string'},
      {name: 'projectName', type: 'string'},
      {name: 'projectId', type: 'string'},
      {name: 'projectCode', type: 'string'},
      {name: 'projectName', type: 'string'},
      {name: 'projectItemId', type: 'int'},
      {name: 'projectItemCode', type: 'string'},
      {name: 'projectItemName', type: 'string'},
      {name: 'expenseItemId', type: 'string'},
      {name: 'expenseItemName', type: 'string'},
      {name: 'organizationId', type: 'int'},
      {name: 'organizationName', type: 'string'},
      {name: 'expenseType', type: 'string'},
      {name: 'dataType', type: 'string'},
      {name: 'amount', type: 'double'}
    ],
    api: 'relatedProjectItem',
    pageSize: 50,
    joins: [{
      type: 'project',
      fields: {
        code: 'projectCode',
        name: 'projectName'
      },
      conditions: [
        {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectId'
        }
      ]
    }, {
      type: 'projectItem',
      fields: {
        code: 'projectItemCode',
        description: 'projectItemName',
        expenseItemId: 'expenseItemId'
      },
      joins: [{
        type: 'expenseItem',
        fields: {
          name: 'expenseItemName'
        },
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'expenseItemId'
        }]
      }],
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        type: 'Field',
        value: 'projectItemId'
      }]
    }, {
      type: 'organization',
      fields: {
        name: 'organizationName',
        sortCode: 'organizationSortCode'
      },
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        type: 'Field',
        value: 'organizationId'
      }]
    }, {
      type: 'budgetItemGroupItem',
      fields: {
        budgetItemGroupId: 'humanResourcesGroupId'
      },
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'Field',
        value: 'domainId'
      }, {
        field: 'budgetItemId',
        condition: 'EQUALS',
        type: 'Field',
        value: 'budgetObjectId'
      }, {
        field: 'budgetItemGroupId',
        condition: 'EQUALS',
        type: 'Integer',
        value: '#humanResourcesGroupId#'
      }]
    }],
    conditions: [{
      field: 'domainId',
      condition: 'EQUALS',
      value: '#domainId#'
    }, {
      field: 'setOfBooksId',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#setOfBooksId#'
    }, {
      field: 'period',
      condition: 'EQUALS',
      value: '#period#'
    }, {
      field: 'organizationId',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#organizationId#'
    }, {
      field: 'humanResourcesGroupId',
      enabled: "'#humanResourcesGroupId#' != '-1'",
      condition: 'NULL'
    }],
    listeners: {
      beforeload: function (store, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      load: function (store) {
        Common.CommonManager.hideMask();
      }
    },
    groupField: "projectCode"
  }),
  features: [{
    ftype: 'groupingsummary',
    groupHeaderTpl: "{[values.rows[0].data['projectName']]}[{[values.rows[0].data['projectCode']]}]",
    hideGroupedHeader: true,
    enableGroupingMenu: false
  }],
  selModel: Ext.create('Ext.selection.CheckboxModel', {checkOnly: true}),
  columns: [
    {
      text: '费用行编号', dataIndex: 'projectItemCode', align: 'left', width: 160, sortable: false,
      summaryRenderer: function (value, summaryData, dataIndex) {
        return '合计：';
      }
    },
    {text: '费用行名称', dataIndex: 'projectItemName', align: 'right', width: 160, sortable: false, flex: 1},
    {text: '费用项名称', dataIndex: 'expenseItemName', align: 'right', width: 200, sortable: false},
    {text: '承担部门', dataIndex: 'organizationName', align: 'right', width: 160, sortable: false},
    {
      text: '预算金额', oldText: null, dataIndex: 'amount', align: 'right', width: 160, sortable: false,
      summaryType: 'sum',
      renderer: function (value, meta, record) {
        if (value != null && value != '') {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '-';
        }
      }
    },
    {text: '预算类型', dataIndex: 'expenseType', align: 'right', width: 160, sortable: false}
  ],
  initComponent: function () {
    var $this = this;
    this.bbar = Ext.create('Ext.PagingToolbar', {
      store: this.store,
      displayInfo: true,
      displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
      emptyMsg: '没有数据'
    });
    this.callParent(arguments);

    $this.getStore().on('load', function (store) {
      var params = Common.CommonManager.getParams();
      var budgetTypes = Ext.create("Common.data.ApiReader").load({
        api: 'dictionaryOption',
        fields: {
          code: 'code',
          names: 'names'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          value: params.domainId
        }, {
          field: 'dictionaryCode',
          condition: 'EQUALS',
          value: 'BudgetType'
        }]
      });

      store.each(function (record) {
        for (var j = 0; j < budgetTypes.length; j++) {
          if (record.data.dataType.split('@@')[2] == budgetTypes[j].code) {
            record.set("expenseType", budgetTypes[j].names);
            record.commit();
          }
        }
      });
    });
  },
  loadData: function () {
    var params = Common.CommonManager.getParams();
    var humanResourcesGroupId = "-1";
    if (params.organizationCode != undefined && params.organizationCode == '301') {
      var humanResourcesGroups = Ext.create("Common.data.ApiReader").load({
        api: "budgetItemGroup",
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          field: 'code',
          condition: 'EQUALS',
          type: 'String',
          value: 'R00'
        }]
      });

      if (humanResourcesGroups != null && humanResourcesGroups[0] != null
        && humanResourcesGroups[0].id != null && humanResourcesGroups[0].id != '') {
        humanResourcesGroupId = humanResourcesGroups[0].id;
      }
    }

    this.getStore().loadData({
      params: {
        domainId: params.domainId,
        setOfBooksId: params.setOfBooksId,
        period: params.period,
        organizationId: params.organizationId,
        organizationSortCode: params.organizationSortCode,
        humanResourcesGroupId: humanResourcesGroupId
      }
    });
  },
  divide: function () {
    var $this = this;
    var params = Common.CommonManager.getParams();

    var budgetTypes = Ext.getCmp("divide-search-budgetType").getChecked();
    var selectModel = this.getSelectionModel().getSelection();

    if (budgetTypes == null || budgetTypes == "" || budgetTypes == undefined) {
      Common.Msg.Alert("请选择预算类型！");
      return false;
    }
    if (selectModel == null || selectModel == "" || selectModel == undefined) {
      Common.Msg.Alert("请选择要划分的目标！");
      return false;
    }

    var divideData = [];
    var budgetType = budgetTypes[0].name;
    var projectItemIds = [];
    for (var a = 0; a < selectModel.length; a++) {
      var dataTypes = selectModel[a].data.dataType.split('@@');
      dataTypes[2] = budgetType;
      selectModel[a].data.dataType = dataTypes.join('@@');
      divideData[a] = selectModel[a].data;
      projectItemIds[a] = selectModel[a].data.projectItemId;
    }

    var saveModel = [
      "annualCostPlanningData", "annualCostPlanningDataVersion", "relatedProjectItemVersion",
      "monthlyCostPlanningData", "monthlyCostPlanningDataVersion"
    ];

    for (var i = 0; i < saveModel.length; i++) {
      var api = saveModel[i];
      var data = Ext.create("Common.data.ApiReader").load({
        api: api,
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          value: params.domainId
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        }, {
          field: 'period',
          condition: 'STARTS_WITH',
          value: params.period.substr(0,4)
        }, {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        }, {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: '@@Project@@'
        }, {
          field: 'budgetObjectId',
          condition: 'IN',
          value: projectItemIds
        }]
      });

      for (var o = 0; o < data.length; o++) {
        for (var j = 0; j < divideData.length; j++) {
          if (data[o].budgetObjectId == divideData[j].projectItemId) {
            data[o].dataType = divideData[j].dataType;
            break;
          }
        }
      }

      Ext.create("Common.data.ApiReader")
        .createOrUpdate(api, data, null, false);
    }

    var callback = function () {
      $this.loadData();
    };

    Ext.create("Common.data.ApiReader")
      .createOrUpdate('relatedProjectItem', divideData, callback, false);
  }
});