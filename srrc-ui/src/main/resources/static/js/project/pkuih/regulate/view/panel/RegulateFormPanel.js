Ext.require([
  "Common.text.CommonAmount"
]);
Ext.define('Regulate.view.panel.RegulateFormPanel', {
  extend: 'Ext.form.Panel',
  alias: "widget.regulateFormPanel",
  layout: "column",
  border: false,
  autoWidth: true,
  editable: true,
  regulateType: '',
  defaults: {
    columnWidth: 3.3 / 10,
    margin: "5 2 5 2",
    isInsideRegulateType: false
  },
  items: [],
  initComponent: function () {
    var items = [];

    items[items.length] = {
      xtype: "hiddenfield",
      name: "id"
    };
    items[items.length] = {
      xtype: "setOfBookCombo",
      hidden: true,
      domainId: domainId,
      loadAfterRender: true
    };
    items[items.length] = {
      xtype: "textfield",
      name: "code",
      fieldLabel: "申请编码",
      labelSeparator: ' ',
      labelStyle: 'font-weight:bold;',
      labelAlign: 'right',
      labelWidth: 80,
      columnWidth: 3.3 / 10,
      emptyText: '系统自动生成',
      readOnly: true,
      hidden: this.isInsideRegulateType,
    };
    items[items.length] = {
      xtype: "orgCombo",
      name: "organizationId",
      displayField: 'showName',
      fieldLabel: "申请部门",
      labelSeparator: '',
      labelStyle: 'font-weight:bold;',
      labelAlign: 'right',
      labelWidth: 80,
      fieldStyle: 'background-color:#FFFACD',
      allowBlank: false,
      columnWidth: 3.3 / 10,
      initParams: {
        domainId: domainId
      },
      blankText: "请选择主管部门",
      editable: true,
      readOnly: true
    };
    items[items.length] = {
      xtype: "periodCombo",
      name: "period",
      fieldLabel: "期间",
      labelSeparator: '',
      labelStyle: 'font-weight:bold;',
      labelAlign: 'right',
      labelWidth: 80,
      fieldStyle: 'background-color:#FFFACD',
      columnWidth: 3.3 / 10,
      allowBlank: false,
      blankText: "请选择所属年份"
    };

    if (this.regulateType == 'inside') {
      items[items.length] = {
        id: 'regulate-budgetItem-combo',
        xtype: "regulateBudgetItemCombo",
        fieldLabel: '调增科目',
        margin: "5 0 0 0",
        labelSeparator: '',
        labelStyle: 'font-weight:bold;',
        labelAlign: 'right',
        labelWidth: 80,
        fieldStyle: 'background-color:#FFFACD',
        columnWidth: 3.3 / 10,
        allowBlank: false,
        observable: true
      };
      items[items.length] = {
        id: 'regulate-project-combo',
        xtype: "textfield",
        name: "projectName",
        fieldLabel: "项目名称",
        labelSeparator: '',
        labelStyle: 'font-weight:bold;',
        labelAlign: 'right',
        labelWidth: 80,
        columnWidth: 6 / 10
      };
      items[items.length] = {
        id: 'regulate-project-selectButton',
        xtype: "button",
        name: "projectName",
        text: "选择",
        columnWidth: 0.4 / 10,
        paramsProperties: ["budgetItemId"],
        params: {},
        pushParam: function (param) {
          Ext.apply(this.params, param);
        },
        isLoadAll: function () {
          if (this.params.budgetItemId == undefined || this.params.budgetItemId == '') {
            Common.Msg.Alert("请选择支出科目！");
            return false;
          }
          if (!this.isCanCreateProject(this.params.budgetItemId)) {
            Common.Msg.Alert("该科目科目不能创建项目，请选择其他科目！");
            return false;
          }
          for (var i = 0; i < this.paramsProperties.length; i++) {
            if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
              return false;
            }
          }
          return true;
        },
        getParams: function () {
          return this.params;
        },
        subscription: {
          observers: ["regulate-budgetItem-combo"],
          operate: {
            onNext: function (param) {
              this.pushParam(param);
              if (!this.isCanCreateProject(param.budgetItemId)) {
                Ext.getCmp("regulate-project-selectButton").setDisabled(true);
                Ext.getCmp("regulate-project-combo").setReadOnly(true);
              } else {
                Ext.getCmp("regulate-project-selectButton").setDisabled(false);
                Ext.getCmp("regulate-project-combo").setReadOnly(false);
              }
            }
          }
        },
        handler: function () {
          if (this.isLoadAll()) {
            Ext.widget('regulateSelectWindow').showAndRevertFunc();
          }
        },
        isCanCreateProject: function (budgetItemId) {
          if (budgetItemId == undefined || budgetItemId == '') {
            return false;
          }
          var projectItems = Ext.create("Common.data.ApiReader").load({
            api: 'projectCategoryItem',
            joins: [
              {
                type: 'expenseItem',
                fields: {
                  budgetItemId: 'budgetItemId'
                },
                conditions: [
                  {
                    field: 'id',
                    condition: 'EQUALS',
                    type: 'Field',
                    value: 'expenseItemId'
                  }
                ]
              }
            ],
            conditions: [{
              field: 'budgetItemId',
              condition: 'EQUALS',
              type: 'Integer',
              value: budgetItemId
            }]
          });
          if (projectItems.length == 0) {
            return false;
          }
          return true;
        }
      };
      items[items.length] = {
        id: 'regulate-form-budgetAmount',
        xtype: "commonAmount",
        name: "budgetAmount",
        fieldLabel: "调整前金额",
        labelSeparator: '',
        labelStyle: 'font-weight:bold;',
        labelAlign: 'right',
        labelWidth: 80,
        fieldStyle: 'background-color:#FFFACD',
        columnWidth: 3.3 / 10,
        allowBlank: false,
        listeners: {
          blur: function (component) {
            Common.text.CommonAmount.blur(component);
            var budgetAmount = component.getValue();
            var adjustAmount = Ext.getCmp('regulate-form-adjustAmount').getValue();
            if (budgetAmount != '' && adjustAmount != '') {
              Ext.getCmp('regulate-form-afterAmount').setValue(budgetAmount + adjustAmount);
            }
          },
          focus: function (component) {
            Common.text.CommonAmount.focus(component);
          }
        }
      };
      items[items.length] = {
        id: 'regulate-form-adjustAmount',
        xtype: "commonAmount",
        name: "adjustAmount",
        fieldLabel: "调整值",
        labelSeparator: '',
        labelStyle: 'font-weight:bold;',
        labelAlign: 'right',
        labelWidth: 80,
        fieldStyle: 'background-color:#FFFACD',
        columnWidth: 3.3 / 10,
        allowBlank: false,
        listeners: {
          blur: function (component) {
            Common.text.CommonAmount.blur(component);
            var adjustAmount = component.getValue();
            var budgetAmount = Ext.getCmp('regulate-form-budgetAmount').getValue();
            if (budgetAmount != '' && adjustAmount != '') {
              Ext.getCmp('regulate-form-afterAmount').setValue(budgetAmount + adjustAmount);
            }
          },
          focus: function (component) {
            Common.text.CommonAmount.focus(component);
          }
        }
      };
      items[items.length] = {
        id: 'regulate-form-afterAmount',
        xtype: "commonAmount",
        name: "adjustAfterAmount",
        fieldLabel: "调整后金额",
        labelSeparator: '',
        labelStyle: 'font-weight:bold;',
        labelAlign: 'right',
        labelWidth: 80,
        fieldStyle: 'background-color:#FFFACD',
        columnWidth: 3.3 / 10,
        allowBlank: false,
        readOnly: true
      };
    }
    this.items = items;
    this.callParent(arguments);
  },
  afterRender: function () {
    if (this.regulateType == 'inside') {
      Ext.getCmp("regulate-budgetItem-combo").getStore().on("load",
        function ($this, records, successful, eOpts) {
          Ext.getCmp("regulate-budgetItem-combo").setValue(null);
        }
      );

      Ext.getCmp("regulate-budgetItem-combo").getStore().on("load",
        function ($this, records, successful, eOpts) {
          var deleteRecords = [];
          for (var i = 0; i < $this.data.items.length; i++) {
            var code = $this.data.items[i].data.code;
            var sortCode = $this.data.items[i].data.sortCode;
            if (code != "YS1001" && code != "YS1005" && code != "YS1201" && code != "YS1202") {
              if (code == "YS120102" || code == "YS120101" || code == "YS120103" || code == "YS120201"
                || code == "YS120202" || code == "YS120203" || code == "YS100501" || code == "YS100502"
                || code == "YS100101" || code == "YS100102" || code == "YS100103") {
                deleteRecords.push($this.data.items[i]);
              } else {
                if (!$this.data.items[i].data.leaf) {
                  for (var j = 0; j < $this.data.items.length; j++) {
                    if (sortCode == $this.data.items[j].data.sortCode) {
                      continue;
                    } else {
                      if ($this.data.items[j].data.sortCode.length > sortCode.length
                        && $this.data.items[j].data.sortCode.indexOf(sortCode) == 0) {
                        deleteRecords.push($this.data.items[i]);
                        break;
                      }
                    }
                  }
                }
              }
            }
          }

          for (var i = 0; i < deleteRecords.length; i++) {
            $this.remove(deleteRecords[i]);
          }

          Ext.getCmp("regulate-budgetItem-combo").setValue(null);
        }
      );
    }
    this.callParent(arguments);
  }
});