Ext.define('Adjust.controller.AdjustTaskController', {
  extend: 'Ext.app.Controller',
  views: ['AdjustTaskGrid'],
  refs: [
    {ref: "adjustTaskGrid", selector: 'adjustTaskGrid'}
  ],
  init: function () {
    var $this = this;
  },
  onLaunch:function () {
    this.getAdjustTaskGrid().loadData();
  },
});