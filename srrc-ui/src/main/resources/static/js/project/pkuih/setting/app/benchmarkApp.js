Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.button.Query',
  "Common.combo.ApiCombox",
  "Common.combo.ApiDictionaryCombox"
]);

Ext.application({
  name: 'Setting',
  appFolder: contextPath + '/static/js/project/pkuih/setting',
  controllers: ["BenchmarkController"],
  launch: function () {
    Ext.tip.QuickTipManager.init();

    mainApplication = this;

    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add({
      xtype: 'benchmarkMainPanel'
    });
  }
});