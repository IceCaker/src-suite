Ext.define('Pkuih.Common.view.PhoneTrafficPanel', {
  extend: 'Ext.grid.Panel',
  alias: "widget.phoneTrafficPanel",
  editable: true,
  level: [],
  defaultColumnWidth: 80,
  columns: [],
  plugins: [],
  loadGrid: function () {
    var $this = this;
    var data = new Array();
    var level = $this.level;
    for (var i = 0; i < level.length; i++) {
      data.push({'expenseName': level[i].name, 'expenseCode': level[i].code})
    }
    data.push({'expenseCode': 'monthTotal', 'expenseName': '合计'});
    var store = Ext.create('Ext.data.Store', {
      fields: [
        {name: 'id', type: 'long'},
        {name: 'expenseItemId', type: 'long'},
        {name: 'expenseCode', type: 'string'},
        {name: 'expenseName', type: 'string'},
        {name: 'yearAmount', type: 'double'},
        {name: 'M01', type: 'double'},
        {name: 'M02', type: 'double'},
        {name: 'M03', type: 'double'},
        {name: 'M04', type: 'double'},
        {name: 'M05', type: 'double'},
        {name: 'M06', type: 'double'},
        {name: 'M07', type: 'double'},
        {name: 'M08', type: 'double'},
        {name: 'M09', type: 'double'},
        {name: 'M10', type: 'double'},
        {name: 'M11', type: 'double'},
        {name: 'M12', type: 'double'}
      ],
      data: data
    });

    this.columns[this.columns.length] = {
      text: "id",
      dataIndex: 'id',
      width: 100,
      hidden: true,
      align: 'left',
      locked: true
    };
    this.columns[this.columns.length] = {
      text: "费用编码",
      dataIndex: 'expenseCode',
      width: 100,
      hidden: true,
      align: 'left',
      locked: true
    };

    this.columns[this.columns.length] = {
      text: "费用名称",
      dataIndex: 'expenseName',
      width: 240,
      align: 'left',
      locked: true
    };

    this.columns[this.columns.length] = {
      text: "预算金额",
      dataIndex: 'yearAmount',
      width: 90,
      hidden: false,
      sortable: false,
      align: 'right',
      locked: true,
      renderer: function (value, meta, record) {
        var totalAmount = 0;
        for (var i = 1; i <= 12; i++) {
          var monthStr = i < 10 ? "0" + i : i;
          var month = 'M' + monthStr;
          totalAmount += (parseInt(record.data[month] == '' ? 0 : record.data[month]))
        }
        if (totalAmount == 0) {
          return '';
        }
        record.data.yearAmount = totalAmount;
        return Ext.util.Format.number(totalAmount, '0,0.00');
      }
    };

    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      this.columns[this.columns.length] = {
        text: i + "月",
        dataIndex: 'M' + monthStr,
        width: $this.defaultColumnWidth,
        sortable: false,
        align: 'right',
        summaryType: 'sum',
        renderer: function (value, meta, record) {
          return Ext.util.Format.number(value, '0,0.00');
        }
      };
    }
    this.reconfigure(store, this.columns);
  },
  initComponent: function () {
    var $this = this;

    var cellEditor = Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1
    });
    $this.plugins = [cellEditor];

    this.callParent(arguments);
  }
});