Ext.define('Project.view.panel.ProjectVersionPanel', {
  extend: 'Ext.grid.Panel',
  alias: "widget.projectVersionPanel",
  title: '版本信息',
  showOperate: true,
  columns: [
    {text: "ID", dataIndex: "id", width: 60, align: "center"},
    {
      text: "类型", dataIndex: "versionTypeName", minWidth: 160, align: "left", flex: 1
    },
    {
      text: "创建时间", dataIndex: "createTime", width: 150, align: "center",
      renderer: function (value, meta, record) {
        return Common.CommonFunc.rendererFunc.numberToTime(value);
      }
    },
    {
      text: "项目预算", dataIndex: "amount", width: 110, align: "right",
      renderer: function (value) {
        return Ext.util.Format.number(value, '0,000.00');
      }
    },
    {
      text: "备注", dataIndex: "versionNote", width: 180, align: "left",
      renderer: function (value, meta, record) {
        if (value != null && value != "") return value;
        else if (record.data.status == "PreApproved") {
          if (record.data.fromStatus != record.data.toStatus) {
            return record.data.toStatusName + "审批通过";
          } else {
            return record.data.toStatusName + "调整审批通过";
          }
        } else if (record.data.type == "abort") {
          if (record.data.fromStatus != record.data.toStatus) {
            return record.data.toStatusName + "拒绝";
          } else {
            return record.data.toStatusName + "调整拒绝";
          }
        } else if (record.data.type == "cancelAdjust") {
          if (record.data.fromStatus != record.data.toStatus) {
            return record.data.toStatusName + "撤销回" + record.data.fromStatusName;
          } else {
            return record.data.toStatusName + "调整撤销";
          }
        }
      }
    },
    {
      text: "操作", dataIndex: "id", width: 90, align: "center", operate: 1,
      renderer: function (value, meta, record) {
        return '<a onclick="Project.view.panel.ProjectVersionPanel.showVersion('
          + record.data.id + ',' + record.data.versionId + ')">查看</a>' + '&nbsp' + '&nbsp' +
          '<a onclick="Project.view.panel.ProjectVersionPanel.deleteProjectVersion('
          + record.data.id + ',' + record.data.versionId + ',\'' + this.id + '\')">删除</a>'
      }
    }
  ],
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'id', type: 'long'},
      {name: 'amount', type: 'double'},
      {name: 'creatorId', type: 'long'},
      {name: 'creatorName', type: 'string'},
      {name: 'createTime', type: 'long'},
      {name: 'fromStatus', type: 'string'},
      {name: 'fromStatusName', type: 'string'},
      {name: 'toStatus', type: 'string'},
      {name: 'toStatusName', type: 'string'},
      {name: 'versionTypeName', type: 'string'},
      {name: 'status', type: 'string'},
      {name: 'versionNote', type: 'string'},
      {name: 'versionId', type: 'Integer'}
    ],
    idProperty: 'id' + 'versionId',
    api: "ProjectVersion",
    joins: [{
      type: 'version',
      fields: {
        type: 'type',
        note: 'versionNote',
        domainTypes:'domainTypes'
      },
      joins: [{
        type: 'dictionaryOption',
        fields: {
          names: 'versionTypeName'
        },
        conditions: [
          {
            field: 'dictionaryCode',
            condition: 'EQUALS',
            value: 'VersionType'
          },
          {
            field: 'domainId',
            condition: 'EQUALS',
            value: '#domainId#'
          },
          {
            field: 'code',
            condition: 'EQUALS',
            type: 'Field',
            value: 'type'
          }
        ]
      }],
      conditions: [
        {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'versionId'
        },
        {
          field: 'domainId',
          condition: 'EQUALS',
          value: '#domainId#'
        }
      ]
    }],
    pageSize: 25,
    proxy: {
      type: 'ajax',
      url: contextPath + '/api/2.0.0/cql',
      actionMethods: {
        read: 'POST'
      },
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      paramsAsJson: true,
      reader: {
        type: 'json',
        root: 'data',
        totalProperty: 'total',
        idProperty: "projectId" + "versionId"
      }
    },
    sorts: [{
      field: 'createTime',
      direction: 'DESC'
    }],
    listeners: {
      beforeload: function (store, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      load: function (store) {
        Common.CommonManager.hideMask();
      }
    }
  }),
  initComponent: function () {
    this.bbar = Ext.create('Ext.PagingToolbar', {
      store: this.store,
      displayInfo: true,
      displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
      emptyMsg: '没有数据'
    });
    this.addEvents({"showVersion": true});
    this.callParent(arguments);
  },
  getProjectId: function () {
    return this.up("projectInfoPanel").projectId;
  },
  isReadOnly: function () {
    return this.up("projectInfoPanel").readOnly;
  },
  loadData: function () {
    if (this.showOperate) {
      this.down("gridcolumn[operate=1]").show();
    } else {
      this.down("gridcolumn[operate=1]").hide();
    }
    var params = Common.CommonManager.getParams();
    var projectId = this.getProjectId();
    this.projectId = projectId;
    this.getStore().conditions = [{
      field: 'domainId',
      condition: 'EQUALS',
      type: 'String',
      value: domainId
    }, {
      field: 'domainTypes',
      condition: 'EQUALS',
      type: 'String',
      value: 'project'
    }, {
      field: 'id',
      condition: 'EQUALS',
      type: 'Integer',
      value: projectId
    }];
    this.getStore().loadData({
      params: {
        domainId: domainId,
        params: params
      }
    });
  },
  statics: {
    showVersion: function (projectId, versionId) {
      Ext.ComponentQuery.query('projectVersionPanel')[0].fireEvent("showVersion", projectId, versionId);
    },
    deleteProjectVersion: function (projectId, versionId, gridId) {
      var grid = Ext.getCmp(gridId);
      var versionData = [];
      var data = {id:versionId};
      versionData.push(data);
        var callback = function () {
          grid.loadData();
        };
        Ext.create("Common.data.ApiReader").directDelete_('version', versionData, callback
        );
    }
  }
});