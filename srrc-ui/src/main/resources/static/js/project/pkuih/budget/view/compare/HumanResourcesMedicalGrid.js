Ext.define("Budget.view.compare.HumanResourcesMedicalGrid", {
  extend: "Budget.view.compare.PlanningGrid",
  alias: "widget.humanResourcesMedicalGrid",
  title: "医疗人员",
  dataType: "@@HumanResourcesMedical@@Cost@@"
});