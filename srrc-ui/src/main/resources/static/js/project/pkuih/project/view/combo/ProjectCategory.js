Ext.define('Project.view.combo.ProjectCategory', {
  extend: 'Ext.form.ComboBox',
  alias: 'widget.prjClassCombo',
  width: 180,
  labelWidth: 65,
  fieldLabel: '项目类型',
  emptyText: '请选择',
  name: 'projectCategoryId',
  store: Ext.create('Ext.data.Store', {
    fields: ['id', 'code', 'name', 'formViewId'],
    proxy: {
      type: 'ajax',
      url: contextPath + '/api/2.0.0/cql',
      actionMethods: {
        read: 'POST'
      },
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      paramsAsJson: true,
      reader: {
        type: 'json',
        root: 'data'
      }
    },
    autoLoad: true,
    listeners: {
      beforeload: function(store) {
        var dynamicQuery = {
          queryType: 'read',
          type: 'ProjectCategory',
          page: 1,
          size: 1000000
        };
        store.proxy.extraParams = dynamicQuery;
      }
    }
  }),
  queryMode: 'local',
  displayField: 'name',
  valueField: 'id',
  initComponent: function() {
    var $this = this;

    this.store.on('load', function(store) {
      var projectCategoryId = $this.value;

      var isSelect = false;
      for (var i = 0; i < store.getTotalCount(); i++) {
        if (store.getAt(i).data.id == projectCategoryId) {
          $this.select(store.getAt(i));
          isSelect = true;
          break;
        }
      }

      if (!isSelect) {
        $this.select(store.getAt(0));
      }
    });

    this.callParent(arguments);
  }
});