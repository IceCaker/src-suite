Ext.define('Divide.view.BudgetType', {
  extend: 'Ext.form.CheckboxGroup',
  alias: 'widget.budgetType',
  fieldLabel: '预算类型',
  name: 'budgetType',
  valueField: 'code',
  items: [],
  labelWidth: 65,
  allowBlank: true,
  displayField: 'names',
  apiReader: Ext.create("Common.data.ApiReader"),
  api: 'dictionaryOption',
  fields: ['id', 'code', 'names'],
  conditions: [{
    field: 'domainId',
    condition: 'EQUALS',
    value: domainId
  }, {
    field: 'dictionaryCode',
    condition: 'EQUALS',
    value: 'BudgetType'
  }],
  initComponent: function() {
    var $this = this;
    this.loadData();
    this.callParent(arguments);
  },
  loadData: function() {
    var $this = this;

    var budgetTypeData = this.apiReader.load({
      api: this.api,
      joins: this.joins,
      conditions: this.conditions
    });

    var widthSum = 0;
    for (var i = 0; i < budgetTypeData.length; i++) {
      this.items[i] = {
        boxLabel: budgetTypeData[i].names, name: budgetTypeData[i].code, inputValue: budgetTypeData[i].id,
        width: 35 + (budgetTypeData[i].names.length) * 14,
        margin: '0 0 0 -10',
        listeners: {
          'change': function(item, newValue, oldValue, eOpts) {
            if (item.checked) {
              for (var j = 0; j < $this.items.items.length; j++) {
                if (item.id != $this.items.items[j].id) {
                  $this.items.items[j].setValue(false);
                }
              }
            }
          }
        }
      }
      widthSum += this.items[i].width;
    }
    this.width = widthSum + 30;
  },
  getPublishParam: function() {
    return {
      budgetType: this.getValue()
    }
  }
});
