Ext.require([
  "Common.text.CommonAmount"
]);
Ext.define('Pkuih.Common.view.ProjectItemForm', {
  alias: "widget.projectItemForm",
  extend: 'Ext.form.Panel',
  layout: "column",
  border: false,
  width: 780,
  defaults: {
    columnWidth: 3.3 / 10,
    margin: "5 2 5 2"
  },
  items: [{
    xtype: "hiddenfield",
    name: "id"
  }, {
    id: 'project-item-period',
    xtype: "periodCombo",
    name: "period",
    fieldLabel: "所属年份",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    allowBlank: false,
    blankText: "请选择所属年份",
    readOnly: true
  }, {
    xtype: "textfield",
    name: "description",
    fieldLabel: "费用行名称",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    columnWidth: 6.6 / 10,
    allowBlank: false,
    blankText: "请输入费用行名称",
    editable: true
  }, {
    id: 'project-item-organizationId',
    xtype: 'orgCombo',
    name: "organizationId",
    initParams: {
      domainId: domainId
    },
    readCookie: false,
    loadAfterRender: true,
    fieldLabel: "承担部门",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    allowBlank: false,
    blankText: "请选择承担部门",
    readOnly: true
  }, {
    id: 'project-item-expenseItemId',
    xtype: 'apiCombox',
    name: 'expenseItemId',
    api: 'expenseItem',
    includeAllItem: false,
    loadAfterRender: false,
    fieldLabel: "费用项",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    allowBlank: false,
    blankText: "请选择费用项",
    editable: true,
    observable: true
  }, {
    id: 'project-item-classCode',
    xtype: 'apiCombox',
    name: 'classCode',
    valueField: 'code',
    api: 'dictionaryOption',
    includeAllItem: false,
    loadAfterRender: false,
    fieldLabel: "级次",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    blankText: "请选择级次",
    editable: true,
    hidden: true,
    observable: true
  }, {
    id: 'project-item-cityName',
    xtype: "textfield",
    name: 'cityName',
    fieldLabel: "地点",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    hidden: true,
    editable: true
  }, {
    id: 'project-item-days',
    xtype: "numberfield",
    name: "days",
    fieldLabel: "天数",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    selectOnFocus: true,
    hideTrigger: true,
    allowDecimals: true,
    decimalPrecision: 2,
    keyNavEnabled: false,
    mouseWheelEnabled: false,
    hidden: true,
    editable: true
  }, {
    id: 'project-item-num',
    xtype: "numberfield",
    name: "num",
    fieldLabel: "人数",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    selectOnFocus: true,
    hideTrigger: true,
    allowDecimals: true,
    decimalPrecision: 2,
    keyNavEnabled: false,
    mouseWheelEnabled: false,
    hidden: true,
    editable: true,
    observable: true
  }, {
    id: 'project-item-benchmark',
    xtype: "commonAmount",
    name: 'benchmark',
    fieldLabel: "标准",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    editable: true,
    hidden: true,
    observable: true,
    subscription: {
      observers: ['project-item-expenseItemId', 'project-item-classCode'],
      operate: {
        onNext: function() {
          if (this.up("projectItemPanel") != undefined && this.up("projectItemPanel").formType != undefined
            && this.up("projectItemPanel").formType == 'Entertain' && !this.up("projectItemPanel").readOnly) {
            var expenseItemId = Ext.getCmp('project-item-expenseItemId').getValue();
            var classCode = Ext.getCmp('project-item-classCode').getValue();
            if (expenseItemId != null && expenseItemId != '' && classCode != null && classCode != '') {
              var benchmarkData = Ext.create("Common.data.ApiReader").load({
                api: 'budgetBenchmark',
                conditions: [
                  {
                    field: 'domainId',
                    condition: 'EQUALS',
                    value: domainId
                  },
                  {
                    field: 'expensesCode',
                    condition: 'EQUALS',
                    type: 'String',
                    value: expenseItemId
                  },
                  {
                    field: 'classCode',
                    condition: 'EQUALS',
                    value: classCode
                  },
                  {
                    field: 'sceneCode',
                    condition: 'EQUALS',
                    value: 'EntertainExpenses@@null@@EntertainClass'
                  }
                ]
              });

              if (benchmarkData != null && benchmarkData[0] != null
                && benchmarkData[0].benchmark != undefined && benchmarkData[0].benchmark != '') {
                this.setReadOnly(true);
                this.setFieldStyle('background-color: white;');
                this.setValue(benchmarkData[0].benchmark);
              } else {
                this.setReadOnly(false);
                this.setFieldStyle('background-color: #FFFACD;');
                this.setValue(null);
              }
            } else {
              this.setReadOnly(false);
              this.setFieldStyle('background-color: #FFFACD;');
              this.setValue(null);
            }
            this.publish({benchmark: this.getValue()});
          }
        }
      }
    }
  }, {
    id: 'project-item-amount',
    xtype: "commonAmount",
    name: "amount",
    fieldLabel: "项目预算",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    allowBlank: false,
    labelWidth: 80,
    editable: true,
    observable: true,
    subscription: {
      observers: [
        'project-item-expenseItemId',
        'project-item-classCode',
        'project-item-num',
        'project-item-benchmark'
      ],
      operate: {
        onNext: function(param) {
          if (this.up("projectItemPanel") != undefined && this.up("projectItemPanel").formType != undefined
            && this.up("projectItemPanel").formType == 'Entertain') {
            var num = Ext.getCmp('project-item-num').getValue();
            var benchmark = Ext.getCmp('project-item-benchmark').getValue();
            if (num != null && num != null && benchmark != null && benchmark != '') {
              this.setValue(num * benchmark);
            }
          }
        }
      }
    }
  }, {
    id: 'project-item-startTime',
    xtype: "datefield",
    name: 'startTime',
    fieldLabel: "开始时间",
    format: 'Y-m-d',
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    hidden: true,
    editable: true,
    listeners: {
      focus: {
        fn: function() {
          if (Ext.getCmp("project-item-endTime") != undefined) {
            var endTime = Ext.getCmp("project-item-endTime").getValue();
            if (endTime != null && endTime != "") {
              Ext.getCmp("project-item-startTime").setMaxValue(endTime);
            }
          }
        },
        scope: this
      }
    }
  }, {
    id: 'project-item-endTime',
    xtype: "datefield",
    name: 'endTime',
    fieldLabel: "结束时间",
    format: 'Y-m-d',
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    hidden: true,
    editable: true,
    listeners: {
      focus: {
        fn: function() {
          if (Ext.getCmp("project-item-startTime") != undefined) {
            var startTime = Ext.getCmp("project-item-startTime").getValue();
            if (startTime != null && startTime != "") {
              Ext.getCmp("project-item-endTime").setMinValue(startTime);
            }
          }
        },
        scope: this
      }
    }
  }, {
    xtype: "textfield",
    name: "completePercent",
    fieldLabel: "执行进度",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    hidden: true,
    readOnly: true
  }, {
    xtype: "textareafield",
    name: "note",
    fieldLabel: "备注",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    columnWidth: 9.9 / 10,
    height: 75,
    hidden: true,
    editable: true
  }]
});
