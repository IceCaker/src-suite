Ext.define('AssetsBudget.view.AssetsBudgetDataPanel', {
  extend: 'Ext.panel.Panel',
  alias: "widget.assetsBudgetDataPanel",
  params: {},
  layout: "fit",
  items: [],
  load: function () {
    var $this = this;
    Common.CommonManager.showMask("加载数据中，请稍候...");
    var params = this.params;
    params.sheetIndex = this.sheetIndex;
    Ext.Ajax.request({
      url: contextPath + '/pkuih/assets/budget',
      params: params,
      timeout: 30000000,
      method: 'GET',
      callback: function (options, success, response) {
        Common.CommonManager.hideMask();

        var result = Ext.JSON.decode(response.responseText);
        if (result.success) {
          $this.removeAll();
          $this.add(Common.panel.HtmlPanelBuilder.createPanel(result.data, $this.getWidth(), $this.getHeight()));
        } else {
          Common.Msg.Error(result.message);
        }
      }
    });
  }
});