Ext.define('Pkuih.Common.view.PhoneTrafficGrid', {
  extend: 'Ext.grid.Panel',
  alias: "widget.phoneTrafficGrid",
  readOnly: true,
  level: [],
  formulas: [],
  yearPlanArray: [],
  monthPlanArray: [],
  phoneTrafficDetails: [],
  defaultColumnWidth: 80,
  columns: [],
  plugins: [],
  loadGrid: function () {
    var $this = this;
    var data = new Array();
    var level = $this.level;
    var formulas = $this.formulas;
    for (var i = 0; i < level.length; i++) {
      var column = {'expenseName': level[i].name, 'expenseCode': level[i].code}
      for (var j = 0; j < formulas.length; j++) {
        if (formulas[j].levelCode == level[i].code) {
          column.benchmark = formulas[j].benchmark
          if (column.benchmark != undefined && column.benchmark != null) {
            column.isExistBenchmark = true;
          }
        }
      }
      data.push(column)
    }
    data.push({'expenseCode': 'monthTotal', 'expenseName': '合计', 'benchmark': '-', 'isExistBenchmark': true});

    var store = Ext.create('Ext.data.Store', {
      fields: [
        {name: 'expenseName', type: 'string'},
        {name: 'expenseCode', type: 'string'},
        {name: 'benchmark', type: 'string'},
        {name: 'isExistBenchmark', type: 'string'},
        {name: 'totalNum', type: 'int'},
        {name: 'M01', type: 'double'},
        {name: 'M02', type: 'double'},
        {name: 'M03', type: 'double'},
        {name: 'M04', type: 'double'},
        {name: 'M05', type: 'double'},
        {name: 'M06', type: 'double'},
        {name: 'M07', type: 'double'},
        {name: 'M08', type: 'double'},
        {name: 'M09', type: 'double'},
        {name: 'M10', type: 'double'},
        {name: 'M11', type: 'double'},
        {name: 'M12', type: 'double'}
      ],
      data: data
    });

    this.columns[this.columns.length] = {
      text: "费用编码",
      dataIndex: 'expenseCode',
      width: 100,
      hidden: true,
      align: 'left',
      locked: true
    };
    this.columns[this.columns.length] = {
      text: "费用名称",
      dataIndex: 'expenseName',
      width: 240,
      align: 'left',
      locked: true
    };

    this.columns[this.columns.length] = {
      text: "标准",
      dataIndex: 'benchmark',
      width: $this.defaultColumnWidth,
      hidden: false,
      sortable: false,
      align: 'center',
      locked: true,
      renderer: function (value, meta, record) {
        if (!$this.readOnly && !record.data.isExistBenchmark) {
          meta.tdCls = 'user-online';
        }
        if (record.data.expenseName == '合计') {
          return '-';
        } else {
          return Ext.util.Format.number(value, '0,0.00');
        }
      },
      editor: {
        xtype: 'numberfield',
        allowBlank: false,
        selectOnFocus: true,
        hideTrigger: true,
        allowDecimals: true,
        decimalPrecision: 2,
        keyNavEnabled: false,
        mouseWheelEnabled: false,
        fieldStyle: 'text-align: right'
      }
    };

    this.columns[this.columns.length] = {
      text: "人数",
      dataIndex: 'totalNum',
      width: 90,
      hidden: false,
      sortable: false,
      align: 'right',
      locked: true,
      renderer: function (value, meta, record) {
        var totalNum = 0;
        for (var i = 1; i <= 12; i++) {
          var monthStr = i < 10 ? "0" + i : i;
          var month = 'M' + monthStr;
          totalNum += (parseFloat(record.data[month] == '' ? 0 : record.data[month]))
        }
        if (totalNum == 0) {
          return '';
        }
        record.data.totalNum = totalNum.toFixed(2);
        return Ext.util.Format.number(totalNum, '0,0.00');
      }
    };

    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      this.columns[this.columns.length] = {
        text: i + "月",
        dataIndex: 'M' + monthStr,
        width: $this.defaultColumnWidth,
        sortable: false,
        align: 'right',
        summaryType: 'sum',
        locked: true,
        renderer: function (value, meta, record) {
          if (!$this.readOnly) {
            meta.tdCls = 'user-online';
          }
          return Ext.util.Format.number(value, '0,0.00');
        },
        summaryRenderer: function (value, summaryData, dataIndex) {
          return value.toFixed(2);
        },
        editor: {
          xtype: 'numberfield',
          allowBlank: false,
          selectOnFocus: true,
          hideTrigger: true,
          allowDecimals: true,
          decimalPrecision: 2,
          keyNavEnabled: false,
          mouseWheelEnabled: false,
          fieldStyle: 'text-align: right'
        }
      };
    }

    this.reconfigure(store, this.columns);
  },
  initComponent: function () {
    var $this = this;

    var cellEditor = Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1,
      listeners: {
        beforeedit: function (editor, e) {
          if ($this.readOnly) {
            return false;
          }

          if (e.field == 'benchmark' && e.record.data.isExistBenchmark) {
            return false;
          }
          if ((e.rowIdx + 1) == e.store.getRange().length) {
            return false;
          }
        },
        afteredit: function (editor, e) {
          var amountRecord = Ext.ComponentQuery.query("phoneTrafficPanel")[0].items.items[0].dataSource.data.items;
          //修改标准时，计算金额 = 人数 * 标准
          if (e.field == 'benchmark') {
            for (var i = 1; i <= 12; i++) {
              var monthStr = i < 10 ? "0" + i : i;
              var month = 'M' + monthStr;
              if (e.record.data[month] != '') {
                amountRecord[e.rowIdx].data[month] = e.record.data[month] * e.record.data.benchmark;
                amountRecord[e.rowIdx].commit();
              }
            }
          }
          //汇总月份人数合计
          if (e.field != 'benchmark') {
            var range = e.store.getRange();
            var totalNum = 0;
            for (var i = 0; i < (range.length - 1); i++) {
              var cellNum = range[i].data[e.field];
              totalNum += (cellNum == '' ? 0 : Math.floor(parseFloat(cellNum) * 100) / 100)
            }
            range[range.length - 1].data[e.field] = totalNum.toFixed(2);
            range[range.length - 1].commit();
          }

          //计算金额 = 人数 * 标准
          var amountRecord = Ext.ComponentQuery.query("phoneTrafficPanel")[0].items.items[0].dataSource.data.items;
          var field = e.field;
          amountRecord[e.rowIdx].data[field] = e.value * e.record.data.benchmark;
          amountRecord[e.rowIdx].commit();

          //汇总月份金额合计
          var totalAmount = 0;
          for (var i = 0; i < amountRecord.length - 1; i++) {
            var cellAmount = amountRecord[i].data[e.field];
            totalAmount += (cellAmount == '' ? 0 : parseFloat(cellAmount))
          }
          amountRecord[amountRecord.length - 1].data[e.field] = totalAmount;
          amountRecord[amountRecord.length - 1].commit();
        }
      }
    });
    $this.plugins = [cellEditor];
    this.callParent(arguments);
  },
  loadPhoneTrafficDetails: function () {
    var $this = this;
    var projectId = $this.up("projectInfoWindow").projectId;
    var versionId = $this.up("projectInfoWindow").versionId;

    if (projectId < 0) {
      return false;
    }

    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      },
      {
        field: 'projectId',
        condition: 'EQUALS',
        value: projectId
      }
    ];

    if (versionId > 0) {
      conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });
    }

    var phoneTrafficArray = Ext.create("Common.data.ApiReader").load({
      api:  versionId > 0 ? 'projectTravelVersion' : 'projectTravel',
      conditions: conditions
    });

    var numRowArray = $this.items.items[0].dataSource.data.items;
    for (var i = 0; i < numRowArray.length; i++) {
      for (var j = 0; j < phoneTrafficArray.length; j++) {
        var rowData = numRowArray[i];
        var findData = phoneTrafficArray[j];

        if (findData.objectType == 'Num'
          && findData.levelCode == rowData.data.expenseCode) {
          var month = findData.objectValue.substring(4, 6);
          rowData.data['M' + month] = findData.objectAmount;
        } else if (findData.objectType == 'Benchmark' && findData.levelCode == rowData.data.expenseCode) {
          rowData.data.benchmark = findData.objectAmount;
        }
      }

      numRowArray[i].commit();
    }

    for (var i = 1; i <= 12; i++) {
      var totalNum = 0;
      for (var j = 0; j < numRowArray.length; j++) {
        var monthStr = i < 10 ? "0" + i : i;
        var month = 'M' + monthStr;
        if (j < (numRowArray.length - 1)) {
          var num = numRowArray[j].data[month];
          totalNum += (num == '' ? 0 : Math.floor(parseFloat(num) * 100) / 100);
        } else {
          numRowArray[numRowArray.length - 1].data[month] = (totalNum == 0 ? '' : Math.floor(totalNum * 100) / 100);
          numRowArray[numRowArray.length - 1].commit();
        }
      }
    }

    var amountRowArray = Ext.ComponentQuery.query("phoneTrafficPanel")[0].getStore().getRange();
    for (var i = 0; i < amountRowArray.length; i++) {
      for (var j = 0; j < phoneTrafficArray.length; j++) {
        var rowData = amountRowArray[i];
        var findData = phoneTrafficArray[j];

        if (findData.objectType == 'Amount'
          && findData.levelCode == rowData.data.expenseCode
          && findData.objectValue != null) {
          var month = findData.objectValue.substring(4, 6);
          rowData.data['M' + month] = findData.objectAmount;
        }
      }
      amountRowArray[i].commit();
    }
    for (var i = 0; i < phoneTrafficArray.length; i++) {
      phoneTrafficArray[i] = {data: phoneTrafficArray[i]}
    }
    $this.phoneTrafficDetails = phoneTrafficArray;
  },
  loadPhoneTrafficMonthPlan: function () {
    var $this = this;
    var projectId = $this.up("projectInfoWindow").projectId;
    var versionId = $this.up("projectInfoWindow").versionId;

    if (projectId < 0) {
      return false;
    }

    var conditions = [{
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
    }, {
        field: 'projectId',
        condition: 'EQUALS',
        value: $this.up("projectInfoWindow").projectId
      }
    ];

    var joins = [
      {
        type: 'projectItem',
        fields: {
          id: 'projectItemId',
          projectId: 'projectId',
          expenseItemId: 'expenseItemId'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'budgetObjectId'
          }
        ]
      }
    ];

    if (versionId > 0) {
      conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });

      joins = [
        {
          type: 'projectItemVersion',
          fields: {
            id: 'projectItemId',
            projectId: 'projectId',
            expenseItemId: 'expenseItemId'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'budgetObjectId'
            }, {
              field: 'versionId',
              condition: 'EQUALS',
              type: 'Field',
              value: 'versionId'
            }
          ]
        }
      ];
    }

    var monthPlanArray = Ext.create("Common.data.ApiReader").load({
      api: versionId > 0 ? 'monthlyCostPlanningDataVersion' : 'monthlyCostPlanningData',
      conditions: conditions,
      joins: joins
    });

    var amountRowArray = Ext.ComponentQuery.query("phoneTrafficPanel")[0].getStore().getRange();
    var amountRow = amountRowArray[amountRowArray.length - 1].data;

    for (var i = 0; i < monthPlanArray.length; i++) {
      var monthPlan = monthPlanArray[i];
      if (monthPlan.budgetObjectType == 'ProjectItem') {
        var month = monthPlan.period.substr(4, 5);
        amountRow['M' + month] = monthPlan.amount
      }
    }
    amountRowArray[amountRowArray.length - 1].commit();
    for (var i = 0; i < monthPlanArray.length; i++) {
      monthPlanArray[i] = {data: monthPlanArray[i]}
    }
    $this.monthPlanArray = monthPlanArray;
  },
  loadPhoneTrafficYearPlan: function () {
    var $this = this;
    var projectId = $this.up("projectInfoWindow").projectId;
    var versionId = $this.up("projectInfoWindow").versionId;

    if (projectId < 0) {
      return false;
    }

    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      },
      {
        field: 'projectId',
        condition: 'EQUALS',
        value: $this.up("projectInfoWindow").projectId
      }
    ];

    var joins = [
      {
        type: 'projectItem',
        fields: {
          id: 'projectItemId',
          projectId: 'projectId',
          expenseItemId: 'expenseItemId'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'budgetObjectId'
          }
        ]
      }
    ];

    if (versionId > 0) {
      conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });

      joins = [
        {
          type: 'projectItemVersion',
          fields: {
            id: 'projectItemId',
            projectId: 'projectId',
            expenseItemId: 'expenseItemId'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'budgetObjectId'
            }, {
              field: 'versionId',
              condition: 'EQUALS',
              type: 'Field',
              value: 'versionId'
            }
          ]
        }
      ];
    }

    var yearPlanArray = Ext.create("Common.data.ApiReader").load({
      api: versionId > 0 ? 'annualCostPlanningDataVersion' : 'annualCostPlanningData',
      conditions: conditions,
      joins: joins
    });

    var amountRowArray = Ext.ComponentQuery.query("phoneTrafficPanel")[0].getStore().getRange();
    var amountRow = amountRowArray[amountRowArray.length - 1].data;

    for (var i = 0; i < yearPlanArray.length; i++) {
      var yearPlan = yearPlanArray[i];
      if (yearPlan.budgetObjectType == 'ProjectItem') {
        amountRow.yearAmount = yearPlan.amount
      }
    }
    amountRowArray[amountRowArray.length - 1].commit()
    for (var i = 0; i < yearPlanArray.length; i++) {
      yearPlanArray[i] = {data: yearPlanArray[i]}
    }
    $this.yearPlanArray = yearPlanArray;
  },
  savePhoneTrafficDetails: function () {
    var $this = this;
    var projectId = $this.up("projectInfoWindow").projectId;
    if (projectId == -1) {
      return;
    }
    var infoDataArray = new Array();
    var model = $this.getStore().getRange();
    var row = model.length - 1;
    for (var i = 0; i < row; i++) {
      for (var j = 1; j <= 12; j++) {
        var gridData = model[i].getData();
        var month = j < 10 ? '0' + j : j;
        var monthStr = j < 10 ? 'M0' + j : 'M' + j;
        if (gridData[monthStr] != '') {
          var nums = new Object();
          nums.levelCode = gridData.expenseCode;
          nums.objectType = 'Num';
          nums.objectValue = this.up('projectInfoWindow').params.period.substring(0, 4).concat(month);
          nums.objectAmount = gridData[monthStr];
          nums.domainId = domainId;
          nums.projectId = $this.up("projectInfoWindow").projectId;
          infoDataArray.push(nums);
        }
      }
      if (gridData.benchmark != '') {
        var benchmarks = new Object();
        benchmarks.levelCode = gridData.expenseCode;
        benchmarks.objectType = 'Benchmark';
        benchmarks.objectAmount = gridData.benchmark;
        benchmarks.domainId = domainId;
        benchmarks.projectId = $this.up("projectInfoWindow").projectId;
        infoDataArray.push(benchmarks);
      }
    }

    var model = Ext.ComponentQuery.query("phoneTrafficPanel")[0].getStore().getRange();
    for (var i = 0; i < row; i++) {
      for (var j = 1; j <= 12; j++) {
        var gridData = model[i].getData();
        var month = j < 10 ? '0' + j : j;
        var monthStr = j < 10 ? 'M0' + j : 'M' + j;
        if (gridData[monthStr] != '') {
          var amount = new Object();
          amount.levelCode = gridData.expenseCode;
          amount.objectType = 'Amount';
          amount.objectValue = this.up('projectInfoWindow').params.period.substring(0, 4).concat(month);
          amount.objectAmount = gridData[monthStr];
          amount.domainId = domainId;
          amount.projectId = $this.up("projectInfoWindow").projectId;
          infoDataArray.push(amount);
        }
      }
    }

    var callback = function (result) {
      $this.phoneTrafficDetails = result;
    };
    if ($this.phoneTrafficDetails.length != 0) {
      Ext.create("Common.data.ApiReader")
        .directDelete_('projectTravel', $this.phoneTrafficDetails);
    }
    Ext.create("Common.data.ApiReader")
      .createOrUpdate('projectTravel', infoDataArray, callback, false);
  },
  savePhoneTrafficMonthPlanData: function () {
    var $this = this;
    var monthPlanArray = [];
    var projectItems = $this.up('projectInfoWindow').projectItemArray;

    var model = Ext.ComponentQuery.query("phoneTrafficPanel")[0].getStore().getRange();
    var record = model[model.length - 1].getData();
    for (var i = 0; i < projectItems.length; i++) {
      for (var j = 1; j <= 12; j++) {
        var month = j < 10 ? '0' + j : j;
        var monthStr = j < 10 ? 'M0' + j : 'M' + j;
        if (record[monthStr] != '' && record[monthStr] != '0') {
          var monthPlan = new Object();
          monthPlan.amount = record[monthStr];
          monthPlan.dataType = this.up('projectInfoWindow').params.dataType;
          monthPlan.status = 'Draft';
          monthPlan.budgetItemGroupId = -1;
          monthPlan.budgetObjectType = 'ProjectItem';
          monthPlan.budgetObjectId = projectItems[i].data.id;
          monthPlan.budgetItemGroupId = this.up('projectInfoWindow').params.budgetGroupId;
          monthPlan.domainId = domainId;
          monthPlan.setOfBooksId = this.up('projectInfoWindow').params.setOfBooksId;
          monthPlan.period = this.up('projectInfoWindow').params.period.substring(0, 4).concat(month);
          monthPlan.organizationId = Ext.ComponentQuery.query("planningSearchToolbar")[0].down("orgCombo").getValue();
          monthPlanArray.push(monthPlan);
        }
      }
    }
    var callback = function (result) {
      $this.monthPlanArray = result;
    };
    if ($this.monthPlanArray.length != 0) {
      Ext.create("Common.data.ApiReader")
        .directDelete_('monthlyCostPlanningData', $this.monthPlanArray);
    }
    Ext.create("Common.data.ApiReader")
      .createOrUpdate('monthlyCostPlanningData', monthPlanArray, callback, false);
  },
  savePhoneTrafficYearPlanData: function () {
    var $this = this;
    var yearPlanArray = [];
    var projectItems = $this.up('projectInfoWindow').projectItemArray;

    var model = Ext.ComponentQuery.query("phoneTrafficPanel")[0].getStore().getRange();
    var record = model[model.length - 1].getData();
    for (var i = 0; i < projectItems.length; i++) {
      var yearTotal = 0;
      if (record.yearAmount != '') {
        var yearPlan = new Object();
        yearPlan.amount = record.yearAmount;
        yearPlan.dataType = this.up('projectInfoWindow').params.dataType;
        yearPlan.status = 'Draft';
        yearPlan.budgetItemGroupId = -1;
        yearPlan.budgetObjectType = 'ProjectItem';
        yearPlan.budgetObjectId = projectItems[i].data.id;
        yearPlan.budgetItemGroupId = this.up('projectInfoWindow').params.budgetGroupId;
        yearPlan.domainId = domainId;
        yearPlan.setOfBooksId = this.up('projectInfoWindow').params.setOfBooksId;
        yearPlan.period = this.up('projectInfoWindow').params.period.substring(0, 4).concat('00');
        yearPlan.organizationId = Ext.ComponentQuery.query("planningSearchToolbar")[0].down("orgCombo").getValue();
        yearPlanArray.push(yearPlan);
      }
    }
    var callback = function (result) {
      $this.yearPlanArray = result;
    };
    if ($this.yearPlanArray.length != 0) {
      Ext.create("Common.data.ApiReader")
        .directDelete_('annualCostPlanningData', $this.yearPlanArray);
    }
    Ext.create("Common.data.ApiReader")
      .createOrUpdate('annualCostPlanningData', yearPlanArray, callback, false);
  }
});