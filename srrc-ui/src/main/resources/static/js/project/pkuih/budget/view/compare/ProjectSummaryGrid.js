Ext.define('Budget.view.compare.ProjectSummaryGrid', {
  extend: 'Budget.view.compare.PlanningGrid',
  alias: "widget.projectSummaryGrid",
  title: "项目预算",
  isProjectBudget: true,
  dataType: '@@ProjectPlanning@@'
});