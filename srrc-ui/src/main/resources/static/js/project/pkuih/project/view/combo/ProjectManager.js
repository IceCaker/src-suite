Ext.define('Project.view.combo.ProjectManager', {
  extend: "Ext.form.ComboBox",
  alias: "widget.projectManagerCombo",
  width: 180,
  labelWidth: 65,
  fieldLabel: '负责人',
  emptyText: "请选择",
  name: "projectManagerId",
  store: Ext.create('Ext.data.Store', {
    fields: ['id', 'username', 'name'],
    proxy: {
      type: 'ajax',
      url: contextPath + '/foundation/service/user',
      method: "GET",
      reader: {
        type: 'json',
        root: 'data'
      }
    },
    autoLoad: true,
    listeners: {
      beforeload: function(store) {
        store.proxy.extraParams = {
          includeChildren: true
        };
      }
    }
  }),
  queryMode: 'local',
  displayField: 'name',
  valueField: 'id',
  initComponent: function() {
    var $this = this;

    this.store.on("load", function (store) {
      var projectManagerId = $this.value;

      var isSelect = false;
      for (var i = 0; i < store.getTotalCount(); i++) {
        if (store.getAt(i).data.id == projectManagerId) {
          $this.select(store.getAt(i));
          isSelect = true;
          break;
        }
      }

      if(!isSelect){
        $this.select(store.getAt(0));
      }
    });

    this.callParent(arguments);
  }
});