Ext.define("Regulate.view.window.RegulateDetailWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.regulateDetailWindow",
  // title: '',
  modal: true,
  shadow: false,
  plain: true,
  resizable: false,
  width: 800,
  height: 380,
  closeAction: "destroy",
  maximizable: false,
  regulateType: "",
  items: [],
  buttons: [
    {
      text: "确定",
      action: "save",
      handler: function () {
        this.up("window").saveRegulateDetail();
      }
    },
    {
      text: "取消",
      action: "close",
      handler: function () {
        this.up("window").close();
      }
    }
  ],
  initComponent: function () {
    var $this = this;

    this.callParent(arguments);
  },
  loadGrid: function () {
  },
  showAndRevertFunc: function (regulateType) {
    this.regulateType = regulateType;
    this.show();

    var items = [];
    items[items.length] = {
      xtype: "regulateDetailToolbar",
      border: true,
      margin: "5 5 5 5",
      regulateType: regulateType,
    };
    items[items.length] = {
      xtype: 'tabpanel',
      border: true,
      margin: "5 5 5 5",
      minHeight: 250,
      items: [{
        xtype: "regulateNotProjectGrid"
      }, {
        xtype: "regulateProjectGrid"
      }]
    };
    this.add(items);
  },
  saveRegulateDetail: function () {
    var $this = this;
    var budgetRegulateDetail = {};
    var form = Ext.ComponentQuery.query('regulateFormPanel')[0].getValues();

    budgetRegulateDetail.organizationId = form.organizationId;
    budgetRegulateDetail.setOfBooksId = form.setOfBooksId;
    budgetRegulateDetail.period = form.period;
    budgetRegulateDetail.status = "Draft";
    budgetRegulateDetail.dataType = "BudgetExternal";


    if (this.regulateType == 'inside') {
      budgetRegulateDetail.dataType = "BudgetInternal";
    }

    var callback = function (result) {
      $this.close();
    };

    Ext.create("Common.data.ApiReader")
      .createOrUpdate('budgetRegulateDetail', [budgetRegulateDetail], callback, false);
  }
});
