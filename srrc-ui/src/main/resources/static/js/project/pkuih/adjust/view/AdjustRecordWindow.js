Ext.define("Adjust.view.AdjustRecordWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.adjustRecordWindow",
  title: "调整记录",
  plain: true,
  width: 900,
  height: 400,
  layout: 'fit',
  closeAction: "destroy",
  autoShow: true,
  modal: true,
  params: [],
  queryParams: {},
  items: [{
    xtype: "grid",
    simpleSelect: false,
    multiSelect: true,
    viewConfig: {
      loadMask: false,
      forceFit: true
    },
    params: {},
    store: Ext.create('Common.data.ApiDirectGridStore', {
      fields: [
        {name: 'id', type: 'long'},
        {name: 'period', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'adjustWay', type: 'string'},
        {name: 'adjustValue', type: 'string'},
        {name: 'createTime', type: 'string'}
      ],
      api: "budgetAdjust",
      sorts: [{
        field: 'createTime',
        direction: 'DESC'
      }],
      pageSize: 20,
      // listeners: {
      //   beforeload: function (store, operation, eOpts) {
      //     var grid = Ext.ComponentQuery.query('adjustRecordWindow')[0].down('grid');
      //     Common.CommonManager.showMask("数据加载中，请稍候...", null, grid);
      //   },
      //   load: function (store) {
      //     Common.CommonManager.hideMask();
      //   }
      // },
    }),
    columns: [
      {
        text: '期间',
        dataIndex: 'period',
        align: 'center',
        hidden: true,
        width: 70,
        renderer: function (value, meta, record) {
          var str = value.substr(0, 4);
          return str + '年';
        },
        sortable: false
      },
      {
        text: '组织',
        dataIndex: 'description',
        align: 'left',
        flex: 1,
        minWidth: 150,
        renderer: function (value, meta, record) {
          var str = '';
          if (value != '') {
            var str = value.split("@@")[1];

          }
          return '<span data-qtip="' + str + '">' + str + '</span>';
        },
        sortable: false
      },
      {
        text: '预算项',
        dataIndex: 'description',
        align: 'left',
        width: 200,
        renderer: function (value, meta, record) {
          var str = '';
          if (value != '') {
            var str = value.split("@@")[2];

          }
          return '<span data-qtip="' + str + '">' + str + '</span>';
        },
        sortable: false
      },
      {
        text: '调整方式',
        dataIndex: 'adjustWay',
        align: 'center',
        width: 90,
        sortable: false,
        renderer: function (value, meta, record) {
          if (value == 'Amount') {
            return '按金额调整';
          } else if (value == 'Proportion') {
            return '按比例调整';
          }
        }
      },
      {
        text: '调整值',
        dataIndex: 'adjustValue',
        align: 'right',
        width: 150,
        sortable: false,
        renderer: function (value, meta, record) {
          var str = value;
          if (record.data.adjustWay == 'Proportion') {
            str = value * 100 + '%';
          }
          return '<span data-qtip="' + str + '">' + str + '</span>';
        }
      }, {
        text: '操作时间',
        dataIndex: 'createTime',
        align: 'center',
        width: 150,
        sortable: false,
        renderer: timestampRender
      },
      {
        text: '操作',
        align: 'center',
        width: 60,
        sortable: false,
        renderer: function (value, meta, record) {
          var pageUrl = contextPath + '/' + "bbudget/service/adjust/task/ui?adjustId="
            + record.data.id;
          return "<a href=\"#\" onclick=\"openBrowserWindow({isFull:true,url:'" + pageUrl + "'});\">查看</a>";
        }
      }
    ]
  }],
  loadGrid: function () {
    var $this = this;
    var tbar = [{
      id: 'adjust-record-query-description',
      xtype: 'textfield',
      width: 200,
      emptyText: '组织/预算项模糊查询'
    }, {
      id: 'adjust-record-query-button',
      xtype: 'button',
      text: '查询',
      handler: function () {
        var description = Ext.getCmp("adjust-record-query-description").value;
        if (description != undefined) {
          $this.queryParams.description = description;
          $this.loadData();
        }
      }
    }, {
      id: 'adjust-record-reset-button',
      xtype: 'button',
      text: '清空',
      handler: function () {
        Ext.getCmp("adjust-record-query-description").setValue('');
        $this.queryParams.description = '';
        $this.loadData();
      }
    }];
    $this.down('grid').addDocked({
      xtype: 'toolbar',
      dock: 'top',
      items: tbar
    });

    var bbar = new Ext.PagingToolbar({
      store: $this.down('grid').store,
      displayInfo: true,
      width: 795,
      displayMsg: '第{0}-{1}条,共{2}条'
    });
    $this.addDocked({
      xtype: 'toolbar',
      ui: 'footer',
      dock: 'bottom',
      items: [
        bbar,
        '->',
        {
          xtype: 'button',
          text: "关闭",
          width: '75px',
          handler: function () {
            this.up("window").close();
          }
        }
      ]
    })
  },
  showAndRevertFunc: function (func) {
    var $this = this;

    if (func != null) {
      $this.loadGrid = func;
    }

    $this.loadGrid();
    $this.loadData();
    $this.show();
  },
  loadData: function () {
    var $this = this;
    var params = Common.CommonManager.getParams();

    $this.down('grid').getStore().conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: '#domainId#'
      },
      {
        field: 'setOfBooksId',
        condition: 'EQUALS',
        type: 'Long',
        value: '#setOfBooksId#'
      },
      {
        field: 'period',
        condition: 'EQUALS',
        type: 'String',
        value: '#period#'
      }
    ];

    var description = $this.queryParams.description;
    if (description != undefined && description != '') {
      $this.down('grid').getStore().conditions.push(
        {
          field: 'description',
          condition: 'CONTAINS',
          type: 'String',
          value: '#description#'
        }
      );
    }
    $this.down('grid').getStore().loadData({
      params: {
        domainId: params.domainId,
        setOfBooksId: params.setOfBooksId,
        period: params.period,
        description: description,
      }
    });
  }
});