Ext.define('Project.view.grid.ProjectGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.projectGrid',
  autoScroll: true,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  autoWidth: true,
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  tbar: {
    xtype: 'manageToolbar',
    id: 'project-apply-grid-tool'
  },
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'code', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'resourceId', type: 'string'},
      {name: 'status', type: 'string'},
      {name: 'statusName', type: 'string'},
      {name: 'nextStatus', type: 'string'},
      {name: 'nextStatusName', type: 'string'},
      {name: 'organizationId', type: 'int'},
      {name: 'organizationName', type: 'string'},
      {name: 'amount', type: 'double'},
      {name: 'creatorName', type: 'string'},
      {name: 'managerName', type: 'string'},
      {name: 'processId', type: 'int'},
      {name: 'isSave', type: 'boolean'},
      {name: 'isCancel', type: 'boolean'},
      {name: 'isAdjust', type: 'boolean'},
      {name: 'deletable', type: 'boolean'}
    ],
    api: "project",
    joins: [
      {
        type: 'organization',
        fields: {
          name: 'organizationName',
          sortCode: 'organizationSortCode'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      },
      {
        type: 'user',
        fields: {
          name: 'managerName'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'managerId'
          }
        ]
      }
    ],
    sorts: [
      {
        field: 'createTime',
        direction: 'DESC'
      },
      {
        field: 'name',
        direction: 'ASC'
      }
    ],
    listeners: {
      beforeload: function (store, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      load: function (store) {
        Common.CommonManager.hideMask();
      }
    }
  }),
  selModel: Ext.create('Ext.selection.CheckboxModel', {mode: 'SIMPLE', checkOnly: true}),
  columns: [
    {text: '项目编码', dataIndex: 'code', align: 'left', width: 160, sortable: false},
    {
      text: '项目名称', dataIndex: 'name', align: 'left', minWidth: 140, flex: 1,
      renderer: function (value, meta, record) {
        return '<a onclick="Project.view.grid.ProjectGrid.edit('
          + record.data.id + ',\'' + record.data.resourceId + '\')"><font title="' + value + '">' + value + '</font></a>';
      }
    },
    {text: '主管部门', dataIndex: 'organizationName', align: 'left', width: 140, sortable: false},
    {
      text: '项目金额', oldText: null, dataIndex: 'amount', align: 'right', width: 130,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    },
    {text: '负责人', dataIndex: 'managerName', align: 'center', width: 100, sortable: false},
    {
      text: '状态', dataIndex: 'statusName', align: 'center', width: 130, sortable: false,
      renderer: function (value, meta, record) {
        var statusName = '草稿';
        if (value != null && value != '') {
          statusName = value;
        }
        if (record.data.processId > 0) {
          return '<a onclick = \'Project.view.grid.ProjectGrid.showProcessWindow('
            + record.data.processId + ')\'>' + statusName + '</a>';
        } else {
          return statusName;
        }
      }
    },
    {
      text: '操作', align: 'center', width: 80, sortable: false,
      renderer: function (value, meta, record) {
        var operateStr = '';

        if (record.data.isSave) {
          operateStr = operateStr + '&nbsp;&nbsp;<a onclick="Project.view.grid.ProjectGrid.edit('
            + record.data.id + ',\'' + record.data.resourceId + '\')">修改</a>';
        }

        if (record.data.isCancel) {
          operateStr = operateStr + '&nbsp;&nbsp;<a onclick="Project.view.grid.ProjectGrid.cancel('
            + record.data.id + ')">撤销</a>';
        }

        operateStr = operateStr + '&nbsp;&nbsp;<a onclick="Project.view.grid.ProjectGrid.remove('
          + record.data.id + ')">删除</a>';

        return operateStr;
      }
    }
  ],
  initComponent: function () {
    this.bbar = Ext.create('Ext.PagingToolbar', {
      store: this.store,
      displayInfo: true,
      displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
      emptyMsg: '没有数据'
    });
    this.addEvents({'nameClickEdit': true});
    this.addEvents({'nameClickDelete': true});
    this.addEvents({'nameClickCancel': true});
    this.callParent(arguments);
  },
  loadConversionFactor: function () {
    var column = this.down('gridcolumn[dataIndex=amount]');
    if (column.oldText == null) {
      column.oldText = column.text;
    }
    column.setText(column.oldText + '(' + Common.CommonManager.conversionFactorName + ')');
  },
  loadGrid: function (params) {
    Common.CommonManager.showMask();
    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: domainId
      },
      {
        field: 'startTime',
        condition: 'LESS_OR_EQUALS',
        type: 'Date',
        value: params.period.substring(0, 4) + "-12-31T23:59:59.000+0800"
      },
      {
        field: 'endTime',
        condition: 'GREATER_OR_EQUALS',
        type: 'Date',
        value: params.period.substring(0, 4) + "-01-01T00:00:00.000+0800"
      },
      {
        field: 'organizationSortCode',
        condition: 'STARTS_WITH',
        value: params.organizationSortCode
      },
      {
        conditions: [
          {
            operator: 'OR',
            field: 'name',
            condition: 'CONTAINS',
            value: params.projectSearch
          },
          {
            operator: 'OR',
            field: 'code',
            condition: 'CONTAINS',
            value: params.projectSearch
          }
        ]
      }
    ];
    if (params.projectCategoryId > 0) {
      conditions.push({
        field: 'projectCategoryId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.projectCategoryId
      });
    }
    this.getStore().conditions = conditions;
    this.getStore().loadData({
      params: {
        domainId: domainId,
        params:params
      }
    });
    Common.CommonManager.hideMask();
  },
  subscription: {
    observers: ['project-search-queryButton'],
    operate: {
      onNext: function (params) {
        this.loadGrid(params);
      }
    }
  },
  statics: {
    edit: function (id, resourceId) {
      Ext.ComponentQuery.query('projectGrid')[0].fireEvent('nameClickEdit', id, resourceId);
    },
    cancel: function (id) {
      Ext.ComponentQuery.query('projectGrid')[0].fireEvent('nameClickCancel', id);
    },
    remove: function (id) {
      Ext.ComponentQuery.query('projectGrid')[0].fireEvent('nameClickDelete', id);
    },
    showProcessWindow: function (processId) {
      openBrowserWindow({
        isFull: true,
        url: ''
      });
    }
  }
});