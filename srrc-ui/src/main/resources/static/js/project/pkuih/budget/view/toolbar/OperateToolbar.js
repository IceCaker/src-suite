Ext.require([
  'Common.ToolbarUtil',
  'Common.button.Import',
  'Common.button.Export',
  'Common.button.Calculate',
  'Common.button.VersionMge',
  'Common.button.VersionSave'
]);
Ext.define("Budget.view.toolbar.OperateToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.operateToolbar",
  isTreeEnabled: true,
  isAdjust: false,
  isProcessEnable: false,
  readOnly: false,
  saveVersionUrl: '',
  versionListUrl: '',
  isCompareto: false,
  importUrl: contextPath + '/pkuih/budget/planning/import',
  exportUrl: contextPath + '/pkuih/budget/planning/export',
  calculateUrl: contextPath + '/pkuih/budget/planning/calculate',
  items: [],
  initComponent: function () {
    var items = [];

    items[items.length] = {text: "查询", action: "query", hidden: true};
    items[items.length] = {text: "保存", action: "save", hidden: true};

    items[items.length] = {text: "创建项目", action: "create", hidden: true};

    items[items.length] = {xtype: "calculateButton", actionUrl: this.calculateUrl, hidden: true};

    items[items.length] = {text: "验证划分", action: "verifyDivide", hidden: true};

    items[items.length] = {text: "提交", action: "commit", hidden: true};

    items[items.length] = {text: "终审", action: "archive", hidden: true};

    items[items.length] = {text: "退回调整", action: "returnAdjust", hidden: true};

    if (false && this.isAdjust) {
      items[items.length] = {text: "调整", action: "adjust", hidden: true};
      items[items.length] = {text: "撤销调整", action: "cancelAdjust", hidden: true};
    }

    if (false) {
      items[items.length] = {text: "获取项目预算", action: "pullProject", hidden: true};
    }

    items[items.length] = {xtype: "exportButton", actionUrl: this.exportUrl, hidden: true};
    items[items.length] = {xtype: "importButton", actionUrl: this.importUrl, hidden: true};

    var dataType = '@@SummaryPlanning@@Cost@@&@@SummaryPlanning@@Expenses@@';
    dataType += '&@@Planning@@Cost@@&@@Planning@@Expenses@@&@@Planning@@Public@@';
    dataType += '&@@ProjectPlanning@@Cost@@&@@ProjectPlanning@@Expenses@@';
    dataType += '&@@Project@@Cost@@&@@Project@@Expenses@@&@@Project@@Public@@';
    dataType += '&@@Project@@';
    dataType += '&@@HumanResourcesSummary@@Cost@@&@@HumanResourcesMedical@@Cost@@&@@HumanResourcesNursing@@Cost@@&@@HumanResourcesTechnology@@Cost@@&@@HumanResourcesFunction@@Expenses@@';

    items[items.length] = {
      id: "operate-version-save",
      xtype: "versionSaveButton",
      params: {
        domainId: "",
        setOfBooksId: -1,
        period: "",
        organizationId: -1,
        includeSubOrganizations: true,
        domainTypes: "annualCostPlanningData,monthlyCostPlanningData,relatedProjectItem,project",
        dataType: dataType
      },
      hidden: true,
      subscription: {
        observers: ['planning-search-queryButton'],
        operate: {
          onNext: function () {
            var params = Common.CommonManager.getParams();
            this.params.domainId = params.domainId;
            this.params.setOfBooksId = params.setOfBooksId;
            this.params.period = params.period;
            this.params.organizationId = params.organizationId;
            this.params.organizationCode = params.organizationCode;
            this.params.organizationSortCode = params.organizationSortCode;
          }
        }
      }
    };
    items[items.length] = {
      id: "operate-version-manager",
      xtype: "versionManageButton",
      showViewLink: true,
      showUseVersion: false,
      showCompareButton: true,
      queryParams: {
        domainId: "",
        setOfBooksId: -1,
        period: "",
        note: '',
        organizationId: -1,
        userId: userId,
        domainTypes: "annualCostPlanningData,monthlyCostPlanningData,relatedProjectItem,project"
      },
      versionSaveParams: {
        domainId: "",
        setOfBooksId: -1,
        period: "",
        organizationId: -1,
        includeSubOrganizations: true,
        domainTypes: "annualCostPlanningData,monthlyCostPlanningData,relatedProjectItem,project",
        dataType: dataType
      },
      showVersion: function (versionId) {
        var params = Common.CommonManager.getParams();
        var versionSetOfBooksId = params.setOfBooksId;
        var versionPeriod = params.period;
        var versionOrganizationId = params.organizationId;
        var url = contextPath +"/pkuih/budget/version/ui?versionId=" + versionId
          + "&setOfBooksId="+versionSetOfBooksId+"&period="+versionPeriod+"&organizationId="+versionOrganizationId;
        openBrowserWindow({isFull: true, url: url});
      },
      afterUseVersion: function () {
        Common.CommonManager.loadGrid();
      },
      hidden: true,
      subscription: {
        observers: ['planning-search-queryButton'],
        operate: {
          onNext: function () {
            var params = Common.CommonManager.getParams();
            this.queryParams.domainId = params.domainId;
            this.queryParams.setOfBooksId = params.setOfBooksId;
            this.queryParams.period = params.period;
            this.queryParams.organizationId = params.organizationId;
            this.queryParams.organizationCode = params.organizationCode;
            this.queryParams.organizationSortCode = params.organizationSortCode;

            this.versionSaveParams.domainId = params.domainId;
            this.versionSaveParams.setOfBooksId = params.setOfBooksId;
            this.versionSaveParams.period = params.period;
            this.versionSaveParams.organizationId = params.organizationId;
            this.versionSaveParams.organizationCode = params.organizationCode;
            this.versionSaveParams.organizationSortCode = params.organizationSortCode;
          }
        }
      }
    };

    items[items.length] = {text: "编制情况", action: "planningSituation", hidden: true};

    items[items.length] = '->';

    this.items = items;
    this.callParent(arguments);
  }
});
