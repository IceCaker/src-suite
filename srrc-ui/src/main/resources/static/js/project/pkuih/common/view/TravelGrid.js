Ext.define('Pkuih.Common.view.TravelGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.travelGrid',
  rowLines: true,
  useArrows: true,
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true,
    loadMask: false
  },
  defaultNumberColumnWidth: 100,
  columns: [],
  plugins: [],
  readOnly: true,
  mark: '',
  monthPlanArray: [],
  yearPlanArray: [],
  initComponent: function () {
    var cellEditor = Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1,
      listeners: {}
    });
    this.plugins = [cellEditor];
    this.callParent(arguments);
  },
  loadView: function () {
    var $this = this;
    var data = [];
    var expenseItems = $this.up('projectInfoWindow').expenseItems;
    var projectItems = $this.up('projectInfoWindow').projectItemArray;
    if ($this.mark == 'Recruit') {
      data.push({'items': '招聘费', 'expenseItemName': '招聘'});
    } else if ($this.mark == 'ForeignTrain') {
      data.push({'items': '国外培训费', 'expenseItemName': '国外培训'});
    } else if ($this.mark == 'DeptTrain') {
      data.push({'items': '部门培训费', 'expenseItemName': '部门培训'});
    } else if ($this.mark == 'DeptMeeting') {
      data.push({'items': '部门会议费', 'expenseItemName': '部门会议'});
    }
    data.push(
      {'items': '交通费', 'expenseItemName': '交通'},
      {'items': '住宿费', 'expenseItemName': '住宿'},
      {'items': '其他', 'expenseItemName': '其他'}
    );
    for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < expenseItems.length; j++) {
        var expenseItem = expenseItems[j];
        var projectItem = data[i];
        if (expenseItem.expenseItemName.indexOf(projectItem.expenseItemName) > -1) {
          projectItem.expenseItemId = expenseItem.expenseItemId
        }
      }
    }

    var store = Ext.create('Ext.data.Store', {
      fields: [
        {name: 'id', type: 'long'},
        {name: 'expenseItemId', type: 'long'},
        {name: 'expenseItemName', type: 'string'},
        {name: 'items', type: 'string'},
        {name: 'yearAmount', type: 'double'},
        {name: 'M01', type: 'double'},
        {name: 'M02', type: 'double'},
        {name: 'M03', type: 'double'},
        {name: 'M04', type: 'double'},
        {name: 'M05', type: 'double'},
        {name: 'M06', type: 'double'},
        {name: 'M07', type: 'double'},
        {name: 'M08', type: 'double'},
        {name: 'M09', type: 'double'},
        {name: 'M10', type: 'double'},
        {name: 'M11', type: 'double'},
        {name: 'M12', type: 'double'}
      ],
      data: data
    });

    this.columns[this.columns.length] = {
      text: "id",
      dataIndex: 'id',
      width: $this.defaultNumberColumnWidth,
      hidden: true,
      align: 'center'
    };
    this.columns[this.columns.length] = {
      text: "expenseId",
      dataIndex: 'expenseItemId',
      width: $this.defaultNumberColumnWidth,
      hidden: true,
      align: 'center'
    };
    this.columns[this.columns.length] = {
      text: "费用项",
      dataIndex: 'items',
      width: $this.defaultNumberColumnWidth,
      hidden: false,
      sortable: false,
      align: 'left',
      locked: true
    };
    this.columns[this.columns.length] = {
      text: "预算金额",
      dataIndex: 'yearAmount',
      width: $this.defaultNumberColumnWidth,
      sortable: false,
      align: 'center',
      locked: true,
      renderer: function (value, meta, record) {
        return Ext.util.Format.number(value, '0,0.00');
      }
    };

    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      this.columns[this.columns.length] = {
        text: i + "月",
        dataIndex: 'M' + monthStr,
        width: $this.defaultNumberColumnWidth,
        sortable: false,
        align: 'right',
        renderer: function (value, meta, record) {
          if (!$this.readOnly) {
            meta.tdCls = 'user-online';
          }
          return Ext.util.Format.number(value, '0,0.00');
        },
        editor: {
          xtype: 'numberfield',
          allowBlank: false,
          selectOnFocus: true,
          hideTrigger: true,
          allowDecimals: true,
          decimalPrecision: 2,
          keyNavEnabled: false,
          mouseWheelEnabled: false,
          fieldStyle: 'text-align: right'
        }
      };
    }

    this.reconfigure(store, this.columns);
  },
  initComponent: function () {
    var $this = this;

    var cellEditor = Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1,
      listeners: {
        beforeedit: function (editor, e) {
          if($this.readOnly) {
            return false;
          }
        }
      }
    });
    $this.plugins = [cellEditor];
    this.callParent(arguments);
  },
  loadTravelMonthPlanData: function () {
    var $this = this;
    var expenseItemId = [];
    var projectId = $this.up("projectInfoWindow").projectId;
    var versionId = $this.up("projectInfoWindow").versionId;

    if (projectId < 0) {
      return false;
    }

    var model = $this.getStore().getRange();
    for (var i = 0; i < model.length; i++) {
      expenseItemId[i] = model[i].getData().expenseItemId;
    }

    var conditions = [
      {
        field: 'budgetObjectType',
        condition: 'EQUALS',
        value: 'ProjectItem'
      },
      {
        field: 'projectId',
        condition: 'EQUALS',
        value: $this.up("projectInfoWindow").projectId
      }
    ];

    var joins = [
      {
        type: 'projectItem',
        fields: {
          id: 'projectItemId',
          projectId: 'projectId',
          expenseItemId: 'expenseItemId'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'budgetObjectId'
          }
        ]
      }
    ];

    if (versionId > 0) {
      conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });

      joins = [
        {
          type: 'projectItemVersion',
          fields: {
            id: 'projectItemId',
            projectId: 'projectId',
            expenseItemId: 'expenseItemId'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'budgetObjectId'
            }, {
              field: 'versionId',
              condition: 'EQUALS',
              type: 'Field',
              value: 'versionId'
            }
          ]
        }
      ];
    }

    var monthPlanArray = Ext.create("Common.data.ApiReader").load({
      api: versionId > 0 ? 'monthlyCostPlanningDataVersion' : 'monthlyCostPlanningData',
      conditions: conditions,
      joins: joins
    });
    var pageDataArray = $this.items.items[0].dataSource.data.items;

    for (var i = 0; i < pageDataArray.length; i++) {
      for (var j = 0; j < monthPlanArray.length; j++) {
        var rowData = pageDataArray[i];
        var findData = monthPlanArray[j];
        if (rowData.data.expenseItemId == findData.expenseItemId) {
          var month = findData.period.substr(4, 5);
          rowData.data['M' + month] = findData.amount;
        }
      }
      pageDataArray[i].commit();
    }
    for (var i = 0; i < monthPlanArray.length; i++) {
      monthPlanArray[i] = {data: monthPlanArray[i]}
    }
    $this.monthPlanArray = monthPlanArray;
  },
  loadTravelYearPlanData: function () {
    var $this = this;
    var expenseItemId = [];
    var projectId = $this.up("projectInfoWindow").projectId;
    var versionId = $this.up("projectInfoWindow").versionId;

    if (projectId < 0) {
      return false;
    }

    var model = $this.getStore().getRange();
    for (var i = 0; i < model.length; i++) {
      expenseItemId[i] = model[i].getData().expenseItemId;
    }

    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      },
      {
        field: 'budgetObjectType',
        condition: 'EQUALS',
        value: 'ProjectItem'
      },
      {
        field: 'projectId',
        condition: 'EQUALS',
        value: $this.up("projectInfoWindow").projectId
      }
    ];

    if (versionId > 0) {
      conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });

      joins = [
        {
          type: 'projectItemVersion',
          fields: {
            id: 'projectItemId',
            projectId: 'projectId',
            expenseItemId: 'expenseItemId'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'budgetObjectId'
            }, {
              field: 'versionId',
              condition: 'EQUALS',
              type: 'Field',
              value: 'versionId'
            }
          ]
        }
      ];
    }

    var yearPlanArray = Ext.create("Common.data.ApiReader").load({
      api: versionId > 0 ? 'annualCostPlanningDataVersion' : 'annualCostPlanningData',
      conditions: conditions,
      joins: joins
    });
    var pageDataArray = $this.items.items[0].dataSource.data.items;

    for (var i = 0; i < pageDataArray.length; i++) {
      for (var j = 0; j < yearPlanArray.length; j++) {
        var rowData = pageDataArray[i];
        var findData = yearPlanArray[j];
        if (rowData.data.expenseItemId == findData.expenseItemId) {
          var month = findData.period.substr(4, 5);
          rowData.data.yearAmount = findData.amount;
        }
      }
      pageDataArray[i].commit();
    }
    for (var i = 0; i < yearPlanArray.length; i++) {
      yearPlanArray[i] = {data: yearPlanArray[i]}
    }
    $this.yearPlanArray = yearPlanArray;
  },
  verifyTotalMonthWithYearAmount: function () {
    var $this = this;
    var message = '';
    var model = $this.getStore().getRange();
    for (var i = 0; i < model.length; i++) {
      var monthTotalAmount = 0;
      var record = model[i].data;
      for (var j = 1; j <= 12; j++) {
        var monthStr = j < 10 ? 'M0' + j : 'M' + j;
        if (record[monthStr] != '' && record[monthStr] != '0') {
          monthTotalAmount += record[monthStr]
        }
      }
      if (!(parseFloat(monthTotalAmount).toFixed(2) == parseFloat(record.yearAmount).toFixed(2)
        || (record.yearAmount == '' && parseFloat(monthTotalAmount).toFixed(2) == 0.00))) {
        message += Ext.String.format("【{0}】预算金额 {1} 不等于月度合计 {2}；<br/>",
          record.items,
          Ext.util.Format.number(record.yearAmount, '0,0.00'),
          Ext.util.Format.number(monthTotalAmount, '0,0.00'));
      }
    }
    if (message != '') {
      Common.Msg.Alert(message);
      return false;
    }
    return true;
  },
  saveTravelMonthPlanData: function () {
    var $this = this;
    var monthPlanArray = [];
    var expenseIdAndProjectItemId = $this.relationExpenseIdAndProjectItemId();

    var model = $this.getStore().getRange()
    for (var i = 0; i < model.length; i++) {
      var data = model[i].getData();
      for (var j = 1; j <= 12; j++) {
        var month = j < 10 ? '0' + j : j;
        var monthStr = j < 10 ? 'M0' + j : 'M' + j;
        if (data[monthStr] != '') {
          var monthPlan = new Object();
          monthPlan.amount = data[monthStr];
          monthPlan.dataType = this.up('projectInfoWindow').params.dataType;
          monthPlan.status = 'Draft';
          monthPlan.budgetItemGroupId = -1;
          monthPlan.budgetObjectType = 'ProjectItem';
          monthPlan.budgetObjectId = expenseIdAndProjectItemId[data.expenseItemId];
          monthPlan.budgetItemGroupId = this.up('projectInfoWindow').params.budgetGroupId;
          monthPlan.domainId = domainId;
          monthPlan.setOfBooksId = this.up('projectInfoWindow').params.setOfBooksId;
          monthPlan.period = this.up('projectInfoWindow').params.period.substring(0, 4).concat(month);
          monthPlan.organizationId = Ext.ComponentQuery.query("planningSearchToolbar")[0].down("orgCombo").getValue();
          monthPlanArray.push(monthPlan);
        }
      }
    }
    var callback = function (result) {
      $this.monthPlanArray = result;
    };
    if ($this.monthPlanArray.length != 0) {
      Ext.create("Common.data.ApiReader")
        .directDelete_('monthlyCostPlanningData', $this.monthPlanArray);
    }
    Ext.create("Common.data.ApiReader")
      .createOrUpdate('monthlyCostPlanningData', monthPlanArray, callback, false);
  }
  ,
  saveTravelYearPlanData: function () {
    var $this = this;
    var yearPlanArray = [];
    var expenseIdAndProjectItemId = $this.relationExpenseIdAndProjectItemId();
    var model = $this.getStore().getRange();
    for (var i = 0; i < model.length; i++) {
      var data = model[i].getData();
      if (data.yearAmount != '') {
        var yearPlan = new Object();
        yearPlan.amount = data.yearAmount;
        yearPlan.dataType = this.up('projectInfoWindow').params.dataType;
        yearPlan.status = 'Draft';
        yearPlan.budgetItemGroupId = -1;
        yearPlan.budgetObjectType = 'ProjectItem';
        yearPlan.budgetObjectId = expenseIdAndProjectItemId[data.expenseItemId];
        yearPlan.budgetItemGroupId = this.up('projectInfoWindow').params.budgetGroupId;
        yearPlan.domainId = domainId;
        yearPlan.setOfBooksId = this.up('projectInfoWindow').params.setOfBooksId;
        yearPlan.period = this.up('projectInfoWindow').params.period.substring(0, 4).concat('00');
        yearPlan.organizationId = Ext.ComponentQuery.query("planningSearchToolbar")[0].down("orgCombo").getValue();
        yearPlanArray.push(yearPlan);
      }
    }
    var callback = function (result) {
      $this.yearPlanArray = result;
    };
    if ($this.yearPlanArray.length != 0) {
      Ext.create("Common.data.ApiReader")
        .directDelete_('annualCostPlanningData', $this.yearPlanArray);
    }
    Ext.create("Common.data.ApiReader")
      .createOrUpdate('annualCostPlanningData', yearPlanArray, callback, false);
  },
  relationExpenseIdAndProjectItemId: function () {
    var $this = this;
    var expenseIdAndProjectItemIds = new Object();
    var projectItems = $this.up('projectInfoWindow').projectItemArray;
    for (var i = 0; i < projectItems.length; i++) {
      var projectItem = projectItems[i];
      expenseIdAndProjectItemIds[projectItem.data.expenseItemId] = projectItem.data.id
    }
    return expenseIdAndProjectItemIds;
  }
});