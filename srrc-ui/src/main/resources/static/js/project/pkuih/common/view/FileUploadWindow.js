Ext.require([
  "Pkuih.Common.view.FileUploadGrid",
  "Pkuih.Common.view.FileWindow"
]);
Ext.define("Pkuih.Common.view.FileUploadWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.fileUploadWindow",
  title: "附件",
  modal: true,
  shadow: false,
  plain: true,
  resizable: false,
  width: 1000,
  height: 400,
  closeAction: "destroy",
  bodyStyle: 'overflow-y: auto; overflow-x: hidden;',
  items: [{
    xtype: "fileUploadGrid",
    border: true,
    height: 345,
    margin: "5 5 5 5"
  }],
  loadData: function() {

  },
  showAndRevertFunc: function(resourceId, isEdit, func) {
    var $this = this;
    if (func != null) {
      $this.loadData = func;
    }
    if (isEdit) {
      $this.down("fileUploadGrid").readOnly = false;
    }
    $this.down("fileUploadGrid").loadGrid(resourceId);
    $this.show();
  }
});