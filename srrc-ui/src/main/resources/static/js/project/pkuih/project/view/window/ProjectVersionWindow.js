Ext.define("Project.view.window.ProjectVersionWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.projectVersionWindow",
  title: "项目详情",
  modal: true,
  shadow: false,
  plain: true,
  resizable: false,
  width: 1200,
  height: 600,
  closeAction: "hide",
  items: [],
  versionId: 0,
  initComponent: function () {
    this.callParent(arguments);
  },
  showAndRevertFunc: function (projectId, versionId) {
    var items =[];

    items.push({
      xtype: 'projectFormPanel',
      title: '项目信息',
      border: true,
      margin: "9 5 5 5",
      height: 270,
      readyOnly: true
    });

    items.push({
      xtype: 'projectItemGrid',
      title: '费用项信息',
      border: true,
      margin: "5 5 5 5",
      height: 260,
      readyOnly: true
    });

    this.add(items);

    this.down('projectFormPanel').loadProjectVersion(projectId, versionId);
    this.down('projectItemGrid').loadGrid(projectId, versionId);
    this.show();
  }
});
