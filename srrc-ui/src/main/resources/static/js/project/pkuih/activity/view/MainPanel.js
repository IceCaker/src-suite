Ext.define("Activity.view.MainPanel", {
    extend: "Ext.panel.Panel",
    alias: "widget.activityMainPanel",
    layout: 'border',
    border: false,
    bodyStyle: {
        background: "#FFFFFF"
    },
    bodyPadding: 5,
    items: []
});
