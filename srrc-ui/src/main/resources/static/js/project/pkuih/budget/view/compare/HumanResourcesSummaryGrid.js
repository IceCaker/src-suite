Ext.define("Budget.view.compare.HumanResourcesSummaryGrid", {
  extend: "Budget.view.compare.PlanningGrid",
  alias: "widget.humanResourcesSummaryGrid",
  title: "人力汇总",
  dataType: "@@HumanResourcesSummary@@Cost@@"
});