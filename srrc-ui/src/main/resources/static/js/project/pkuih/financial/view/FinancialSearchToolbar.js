Ext.require([
  'Common.ToolbarUtil',
  'Common.button.Import',
  'Common.button.Export'
]);
Ext.define("Financial.view.FinancialSearchToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.financialSearchToolbar",
  importUrl: contextPath + '/pkuih/budget/financial/import',
  exportUrl: contextPath + '/pkuih/budget/financial/export',
  margin: '0 0 5 0',
  items: [
    {
      id: 'report-search-setOfBooks',
      xtype: 'setOfBookCombo',
      hidden: true,
      domainId: domainId,
      loadAfterRender: true,
      observable: true
    }, {
      id: 'report-search-period',
      xtype: 'periodCombo',
      observable: true
    }, {
      id: 'report-search-organizationId',
      xtype: 'orgCombo',
      displayField: 'name',
      labelWidth: 35,
      width: 200,
      type: "global",
      readCookie: false,
      initParams: {
        domainId: domainId,
        organizationLevel: 'Company'
      },
      conditions: [
        {field: 'domainId', condition: 'EQUALS', value: '#domainId#'},
        {field: 'organizationLevel', condition: 'EQUALS', value: '#organizationLevel#'},
        {field: 'enabled', condition: 'EQUALS', value: true},
        {field: 'userId', condition: 'EQUALS', type: "Long", value: '#userId#'}
      ],
      loadAfterRender: true,
      observable: true
    }, {
      id: 'report-search-queryButton', text: '查询', observable: true,
      paramsProperties: ["setOfBooksId", "period", "organizationId"],
      params: {},
      pushParam: function(param) {
        Ext.apply(this.params, param);
      },
      isLoadAll: function() {
        for (var i = 0; i < this.paramsProperties.length; i++) {
          if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
            return false;
          }
        }
        return true;
      },
      getParams: function() {
        if (window.location.pathname.indexOf("summary") > 0) {
          this.params.businessType = "financialSummary";
        } else {
          this.params.businessType = "financialStatements";
        }
        return this.params;
      },
      subscription: {
        observers: ['report-search-setOfBooks', 'report-search-period', 'report-search-organizationId'],
        operate: {
          onNext: function(param) {
            this.pushParam(param);
            if (this.isLoadAll()) {
              this.publish(this.getParams());
            }
          }
        }
      },
      handler: function() {
        this.publish(this.getParams());
      }
    }, {
      id: 'report-search-saveButton', text: '保存', observable: true,
      hidden: true,
      handler: function() {
        if(this.up("panel").down("tabpanel").getActiveTab() != null) {
          this.up("panel").down("tabpanel").getActiveTab().save(true);
        }
      }
    }, {
      id: 'report-search-getButton', text: '获取预算编制数据', observable: true,
      hidden: true,
      handler: function() {
        var $this = this;
        Common.CommonManager.showMask($this.text + "处理中，请稍候...");

        var params = Ext.getCmp("report-search-queryButton").getParams();
        Ext.Ajax.request({
          url: contextPath + '/pkuih/budget/financial/getPlanningSummary',
          params: params,
          timeout: 30000000,
          method: 'POST',
          callback: function(options, success, response) {
            Common.CommonManager.hideMask();

            var result = Ext.JSON.decode(response.responseText);
            if (result.success) {
              if ($this.up("panel").down("tabpanel").getActiveTab() != null) {
                $this.up("panel").down("tabpanel").getActiveTab().save(true);
              }
            } else {
              Common.Msg.Error(result.message);
            }
          }
        });
      }
    }, {
      id: 'report-search-exportButton',
      xtype: 'exportButton',
      hidden: true,
      actionUrl: contextPath + '/pkuih/budget/financial/export',
      getParams: function() {
        return Ext.getCmp("report-search-queryButton").getParams();
      }
    }, {
      id: 'report-search-importButton',
      xtype: 'importButton',
      hidden: true,
      actionUrl: contextPath + '/pkuih/budget/financial/import',
      getParams: function() {
        return Ext.getCmp("report-search-queryButton").getParams();
      },
      successAction: function(from, action) {
        var result = Ext.JSON.decode(action.response.responseText);
        if (!result.success) {
          Common.Msg.Error(result.message);
        }
        if (result.success && result.message != null) {
          Ext.Msg.show({
            title: "警告提示",
            modal: true,
            msg: result.message + "</br>已跳过警告信息项",
            buttons: Ext.Msg.OK,
            width: 300
          });
        }
        this.impWindow.hide();
        if(this.up("panel").down("tabpanel").getActiveTab() != null) {
          this.up("panel").down("tabpanel").getActiveTab().load();
        }
      }
    }
  ]
});

