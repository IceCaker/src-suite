Ext.define('Setting.view.benchmark.DataGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.benchmarkDataGrid',
  simpleSelect: false,
  multiSelect: true,
  viewConfig: {
    loadMask: false,
    forceFit: true
  },
  params: {},
  store: Ext.create('Common.data.ApiDirectGridStore', {
    api: 'budgetBenchmark',
    fields: [
      {name: 'id', type: 'string'},
      {name: 'sceneCode', type: 'string'},
      {name: 'sceneName', type: 'string'},
      {name: 'expensesCode', type: 'string'},
      {name: 'expensesName', type: 'string'},
      {name: 'levelCode', type: 'string'},
      {name: 'levelName', type: 'string'},
      {name: 'classCode', type: 'string'},
      {name: 'className', type: 'string'},
      {name: 'benchmark', type: 'double'}
    ]
  }),
  plugins: [
    Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1,
      autoCancel: false,
      listeners: {
        beforeedit: function(editor, e) {
          var rows = e.grid.getSelectionModel().getSelection();
          setTimeout(function() {
            e.grid.getSelectionModel().select(rows);
          }, 0);
        }
      }
    })
  ],
  selModel: Ext.create('Ext.selection.CheckboxModel', {checkOnly: true}),
  columns: [{
    text: '场景',
    dataIndex: 'sceneName',
    align: 'left',
    flex: 1,
    minWidth: 150,
    sortable: false
  }, {
    text: '费用',
    dataIndex: 'expensesName',
    align: 'left',
    width: 150,
    sortable: false,
    renderer: function (value, meta, record) {
      if (value == '') {
        value = '<center>-</center>';
      }
      return value;
    }
  }, {
    text: '级别',
    dataIndex: 'levelName',
    align: 'left',
    width: 250,
    sortable: false,
    renderer: function (value, meta, record) {
      if (value == '') {
        value = '<center>-</center>';
      }
      return value;
    }
  }, {
    text: '级次',
    dataIndex: 'className',
    align: 'left',
    width: 150,
    sortable: false,
    renderer: function (value, meta, record) {
      if (value == '') {
        value = '<center>-</center>';
      }
      return value;
    }
  }, {
    text: '标准',
    dataIndex: 'benchmark',
    align: 'right',
    width: 200,
    sortable: false,
    renderer: function (value, meta, record) {
      meta.tdCls = 'user-online';
      return Ext.util.Format.number(value, '0,0.00');
    },
    editor: {
      xtype: 'numberfield',
      allowBlank: false,
      selectOnFocus: true,
      hideTrigger: true,
      allowDecimals: true,
      decimalPrecision: 2,
      keyNavEnabled: false,
      mouseWheelEnabled: false,
      fieldStyle: 'text-align: right'
    }
  }],

  subscription: {
    observers: ['main-search-query', 'main-search-save', 'main-search-delete'],
    operate: {
      onNext: function (params) {
        var $this = this;
        this.params = params;
        if (params.isSave) {
          Common.CommonManager.showSpecialMask(function() {
            $this.save();
          },"预算标准保存中，请稍候...");
        }
        if (params.isDelete) {
          $this.deleteFunc();
        } else {
          Common.CommonManager.showSpecialMask(function() {
            $this.loadData()
          },"加载中，请稍候...");
        }
      }
    }
  },
  loadData: function() {
    var data = [];
    var params = Ext.getCmp("main-search-query").getParams();
    var sceneData = this.queryDictionaryData('BudgetScene', params.sceneCode);
    for (var i = 0; i < sceneData.length; i++) {
      var sceneCode = sceneData[i].optionCode;
      var sceneArray = sceneCode.split('@@');
      var expensesDictionaryCode = sceneArray[0];
      var levelDictionaryCode = sceneArray[1];
      var classDictionaryCode = sceneArray[2];

      var expensesData = [];
      var levelData = [];
      var classData = [];
      if (expensesDictionaryCode != null && expensesDictionaryCode != undefined && expensesDictionaryCode != 'null') {
        var expensesData = this.queryDictionaryData(expensesDictionaryCode, params.expensesCode);
      }
      if (levelDictionaryCode != null && levelDictionaryCode != undefined && levelDictionaryCode != 'null') {
        var levelData = this.queryDictionaryData(levelDictionaryCode, params.levelCode);
      }
      if (classDictionaryCode != null && classDictionaryCode != undefined && classDictionaryCode != 'null') {
        var classData = this.queryDictionaryData(classDictionaryCode, params.classCode);
      }

      if (expensesData.length > 0) {
        for (var j = 0; j < expensesData.length; j++) {
          if (levelData.length > 0) {
            for (var m = 0; m < levelData.length; m++) {
              if (classData.length > 0) {
                for (var n = 0; n < classData.length; n++) {
                  data[data.length] = {
                    sceneCode: sceneData[i].optionCode,
                    sceneName: sceneData[i].optionName,
                    expensesCode: expensesData[j].optionCode,
                    expensesName: expensesData[j].optionName,
                    levelCode: levelData[m].optionCode,
                    levelName: levelData[m].optionName,
                    classCode: classData[n].optionCode,
                    className: classData[n].optionName
                  };
                }
              } else {
                data[data.length] = {
                  sceneCode: sceneData[i].optionCode,
                  sceneName: sceneData[i].optionName,
                  expensesCode: expensesData[j].optionCode,
                  expensesName: expensesData[j].optionName,
                  levelCode: levelData[m].optionCode,
                  levelName: levelData[m].optionName,
                  classCode: '',
                  className: ''
                };
              }
            }
          } else {
            if (classData.length > 0) {
              for (var n = 0; n < classData.length; n++) {
                data[data.length] = {
                  sceneCode: sceneData[i].optionCode,
                  sceneName: sceneData[i].optionName,
                  expensesCode: expensesData[j].optionCode,
                  expensesName: expensesData[j].optionName,
                  levelCode: '',
                  levelName: '',
                  classCode: classData[n].optionCode,
                  className: classData[n].optionName
                };
              }
            } else {
              data[data.length] = {
                sceneCode: sceneData[i].optionCode,
                sceneName: sceneData[i].optionName,
                expensesCode: expensesData[j].optionCode,
                expensesName: expensesData[j].optionName,
                levelCode: '',
                levelName: '',
                classCode: '',
                className: ''
              };
            }
          }
        }
      } else {
        if (levelData.length > 0) {
          for (var m = 0; m < levelData.length; m++) {
            if (classData.length > 0) {
              for (var n = 0; n < classData.length; n++) {
                data[data.length] = {
                  sceneCode: sceneData[i].optionCode,
                  sceneName: sceneData[i].optionName,
                  expensesCode: '',
                  expensesName: '',
                  levelCode: levelData[m].optionCode,
                  levelName: levelData[m].optionName,
                  classCode: classData[n].optionCode,
                  className: classData[n].optionName
                };
              }
            } else {
              data[data.length] = {
                sceneCode: sceneData[i].optionCode,
                sceneName: sceneData[i].optionName,
                expensesCode: '',
                expensesName: '',
                levelCode: levelData[m].optionCode,
                levelName: levelData[m].optionName,
                classCode: '',
                className: ''
              };
            }
          }
        } else {
          if (classData.length > 0) {
            for (var n = 0; n < classData.length; n++) {
              data[data.length] = {
                sceneCode: sceneData[i].optionCode,
                sceneName: sceneData[i].optionName,
                expensesCode: '',
                expensesName: '',
                levelCode: '',
                levelName: '',
                classCode: classData[n].optionCode,
                className: classData[n].optionName
              };
            }
          } else {
            data[data.length] = {
              sceneCode: sceneData[i].optionCode,
              sceneName: sceneData[i].optionName,
              expensesCode: '',
              expensesName: '',
              levelCode: '',
              levelName: '',
              classCode: '',
              className: ''
            };
          }
        }
      }
    }

    var benchmarkData = this.queryBenchmark(params);
    for (var i = 0; i < benchmarkData.length; i++) {
      for (var j = 0; j < data.length; j++) {
        if ((benchmarkData[i].sceneCode == data[j].sceneCode || (benchmarkData[i].sceneCode == null && data[j].sceneCode == ''))
          && (benchmarkData[i].expensesCode == data[j].expensesCode || (benchmarkData[i].expensesCode == null && data[j].expensesCode == ''))
          && (benchmarkData[i].levelCode == data[j].levelCode || (benchmarkData[i].levelCode == null && data[j].levelCode == ''))
          && (benchmarkData[i].classCode == data [j].classCode || (benchmarkData[i].classCode == null && data[j].classCode == ''))) {
          data[j].id = benchmarkData[i].id;
          data[j].benchmark = benchmarkData[i].benchmark;
          break;
        }
      }
    }
    this.getStore().removeAll();
    this.getStore().add(data);
    this.getStore().commitChanges();
  },
  queryDictionaryData: function (dictionaryCode, optionCode) {
    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      }, {
        field: 'code',
        condition: 'EQUALS',
        value: dictionaryCode
      }
    ];

    if (optionCode != null && optionCode != '' && optionCode != undefined) {
      conditions.push({
        field: 'optionCode',
        condition: 'EQUALS',
        value: optionCode
      })
    }

    var dictionaryData = Ext.create("Common.data.ApiReader").load({
      api: 'dictionary',
      joins: [{
        type: 'dictionaryOption',
        fields: {
          code: 'optionCode',
          names: 'optionName'
        },
        conditions: [{
          field: 'dictionaryId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'id'
        }]
      }],
      conditions: conditions
    });

    return dictionaryData;
  },
  queryBenchmark: function (params) {
    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        value: domainId
      }
    ];

    if (params.sceneCode != undefined && params.sceneCode != '') {
      conditions.push({
        field: 'sceneCode',
        condition: 'EQUALS',
        value: params.sceneCode
      })
    }
    if (params.expensesCode != undefined && params.expensesCode != '') {
      conditions.push({
        field: 'expensesCode',
        condition: 'EQUALS',
        value: params.expensesCode
      })
    }
    if (params.levelCode != undefined && params.levelCode != '') {
      conditions.push({
        field: 'levelCode',
        condition: 'EQUALS',
        value: params.levelCode
      })
    }
    if (params.classCode != undefined && params.classCode != '') {
      conditions.push({
        field: 'classCode',
        condition: 'EQUALS',
        value: params.classCode
      })
    }

    var benchmarkData = Ext.create("Common.data.ApiReader").load({
      api: 'budgetBenchmark',
      conditions: conditions
    });
    return benchmarkData;
  },
  save: function () {
    var $this = this;
    var modifyData = this.getStore().getModifiedRecords();
    var benchmarkData = [];
    for (var i = 0; i < modifyData.length; i++) {
      if (modifyData[i].data.id == '') {
        delete modifyData[i].data.id;
      }
      benchmarkData[benchmarkData.length] = modifyData[i].data;
    }

    var callback = function () {
      $this.loadData();
      Common.Msg.Alert("保存预算标准成功！");
    }

    Ext.create("Common.data.ApiReader")
      .createOrUpdate('budgetBenchmark', benchmarkData, callback, false);
  },
  deleteFunc: function () {
    var $this = this;
    var selectedBenchmarks = $this.getSelectionModel().getSelection();

    if (selectedBenchmarks.length == 0) {
      Common.Msg.Alert("请选择删除的预算标准！");
      return false;
    }

    var callback = function() {
      $this.loadData();
      Common.Msg.Alert("删除预算标准成功！");
    }

    Ext.create("Common.data.ApiReader").deleteFunc('budgetBenchmark', selectedBenchmarks, callback);
  }
});