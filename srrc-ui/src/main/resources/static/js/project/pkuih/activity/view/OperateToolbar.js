Ext.require([
    'Common.ToolbarUtil',
    'Common.button.VersionMge'
]);
Ext.define("Activity.view.OperateToolbar", {
    extend: "Ext.toolbar.Toolbar",
    alias: "widget.activityOperateToolbar",
    margin: '-5 0 5 0',
    style: 'border-top: 0px;',
    items: [{
        id: 'activity-operate-add', text: '新增', xtype: "button", action: 'add', observable: true
    }, {
        id: 'activity-operate-save', text: '提交', xtype: "button", action: 'commit', observable: true
    }, {
        id: 'activity-operate-delete', text: '删除', xtype: "button", action: 'delete', observable: true
    },  {
        id: 'activity-operate-close', text: '关闭', xtype: "button", action: 'close', observable: true
    },{
        id: 'activity-operate-export', text: '导出', xtype: "button", action: 'export', disabled: true, observable: true,
    }]
});
