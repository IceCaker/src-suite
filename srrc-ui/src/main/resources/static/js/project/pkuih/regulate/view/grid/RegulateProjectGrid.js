Ext.define('Regulate.view.grid.RegulateProjectGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.regulateProjectGrid',
  title: '项目预算',
  minHeight: 150,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  border: false,
  readOnly: true,
  selModel: Ext.create('Ext.selection.CheckboxModel',
    {mode: 'SIMPLE', checkOnly: true}
  ),
  columns: [{
    text: '项目名称',
    dataIndex: 'projectName',
    flex: 1,
    style: 'text-align:center;',
    align: 'left',
    minWidth: 120,
    menuDisabled: true,
    sortable: false
  }, {
    text: '费用项',
    dataIndex: 'expenseItem',
    style: 'text-align:center;',
    align: 'left',
    width: 150,
    menuDisabled: true,
    sortable: false
  }, {
    text: '费用项预算', dataIndex: 'expenseItemAmount', align: 'left', width: 150, menuDisabled: true, sortable: false,
    renderer: function (value, meta, record) {
      if (value != null) {
        return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
          + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
      } else {
        return '0.00';
      }
    }
  },
    {
      text: '使用预算', dataIndex: 'usedAmount', align: 'left', width: 150, menuDisabled: true, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    },
    {
      text: '剩余预算', align: 'notUseAmount', align: 'left', width: 150, menuDisabled: true, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    }],
  initComponent: function () {
    this.callParent(arguments);
  },
  loadData: function () {
  },
  loadGrid: function (projectId, versionId) {
  },
  showProjectItemWindow: function () {
  },
  statics: {
    edit: function (id, gridId) {
    },
    remove: function (id, gridId) {
    }
  }
});