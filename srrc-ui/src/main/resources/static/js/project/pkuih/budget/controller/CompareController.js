Ext.define('Budget.controller.CompareController', {
  extend: 'Ext.app.Controller',
  views: ["toolbar.SearchToolbar", 'grid.BudgetGroupGrid', "panel.PlanningTabPanel",
    "compare.PlanningSummaryGrid", "compare.PlanningGrid", "compare.ProjectSummaryGrid",
    "compare.HumanResourcesSummaryGrid", "compare.HumanResourcesMedicalGrid", "compare.HumanResourcesNursingGrid",
    "compare.HumanResourcesTechnologyGrid", "compare.HumanResourcesFunctionGrid"
  ],
  refs: [
    {ref: 'budgetGroupGrid', selector: 'budgetGroupGrid'},
    {ref: 'planningTabPanel', selector: 'planningTabPanel'},
    {ref: 'projectSummaryGrid', selector: 'projectSummaryGrid'}
  ],
  businessTypeName: "预算编制",
  defaultTimeout: 600000,
  isTabchangeHandle: true,
  init: function () {
    var $this = this;
    this.control({
      'budgetGroupGrid': {
        select: $this.selectBudgetGroupRow
      },
      'planningTabPanel': {tabchange: $this.tabchange},
      'button[action=export]': {click: $this.exportData},
    });

    Common.CommonManager.getParams = $this.getParams;
    Common.CommonManager.loadStatus = $this.loadStatus;
    Common.CommonManager.loadGrid = function (isCalculate) {
      $this.loadGrid(isCalculate);
    };
    Common.CommonManager.editor.isShowPlanningForLevelOrganizationFlag = true;
  },
  selectBudgetGroupRow: function (grid, record, index, eOpts) {
    this.query(true);
  },
  getMainGrid: function () {
    if (this.getPlanningTabPanel() != undefined) {
      return this.getPlanningTabPanel().getActiveTab();
    } else {
      return null;
    }
  },
  loadStatus: function (func) {
    if (func != null) {
      func();
    }
  },
  exportData: function () {
    var $this = this;
    var params = $this.getParams();
    downloadFile(contextPath + '/pkuih/budget/versionCompare/export', {
      'versionIds':params.versionIds.toString(),
      'setOfBooksId':params.setOfBooksId,
      'period':params.period,
      'organizationId':params.organizationId,
      'budgetGroupId':params.budgetGroupId
    });
  },
  tabchange: function (tabPanel, newCard, oldCard, eOpts) {
    var $this = this;
    if ($this.isTabchangeHandle) {
      var params = $this.getParams();
      var budgetGroupViewType = params.budgetGroupViewType;
      if (budgetGroupViewType == undefined || budgetGroupViewType == null) {
        budgetGroupViewType == "";
      }
      tabPanel.getActiveTab().loadData();
    }
  },
  loadGrid: function (isCalculate) {
    //修改计算状态
    if (isCalculate) {
      this.updateStatus(null, true);
    } else {
      this.updateStatus(null, false);
    }

    this.query(false);
  },
  query: function (isLoadGrids) {
    if (isLoadGrids) {
      this.loadGrids(this.getPlanningTabPanel());
    } else {
      var panel = this.getMainGrid();
      if (panel != null) {
        panel.loadData();
      }
    }
  },
  loadGrids: function (tabPanel) {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var organizationSortCode = params.organizationSortCode;
    var organizationLeaf = params.organizationLeaf;
    var budgetGroupViewType = params.budgetGroupViewType;

    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }

    var activePanelXtype = null;
    var activePanel = null;
    if (tabPanel.getActiveTab() != null) {
      activePanelXtype = tabPanel.getActiveTab().xtype;
    }

    var items = [];
    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary") {
      $this.createNewPanel(items, "预算汇总", "Budget.view.compare.PlanningSummaryGrid", true, false);
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "MonthlySummary"
      || budgetGroupViewType == "BudgetMonthlyAllocation" || budgetGroupViewType == "BudgetMonthlySummary") {
      $this.createNewPanel(items, "非项目预算", "Budget.view.compare.PlanningGrid", true, true);
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "MonthlySummary"
      || budgetGroupViewType == "ProjectMonthlyAllocation" || budgetGroupViewType == "ProjectMonthlySummary") {
      $this.createNewPanel(items, "项目预算", "Budget.view.compare.ProjectSummaryGrid", true, false);
    }

    if (budgetGroupViewType == "HumanResourcesSummary") {
      $this.createNewPanel(items, "人力汇总", "Budget.view.compare.HumanResourcesSummaryGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesMedical") {
      $this.createNewPanel(items, "医疗人员", "Budget.view.compare.HumanResourcesMedicalGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesNursing") {
      $this.createNewPanel(items, "护理人员", "Budget.view.compare.HumanResourcesNursingGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesTechnology") {
      $this.createNewPanel(items, "医技人员", "Budget.view.compare.HumanResourcesTechnologyGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesFunction") {
      $this.createNewPanel(items, "职能人员", "Budget.view.compare.HumanResourcesFunctionGrid", true, true);
    }

    $this.isTabchangeHandle = false;
    tabPanel.removeAll();
    $this.isTabchangeHandle = true;

    for (var i = 0; i < items.length; i++) {
      tabPanel.add(items[i]);
      if (items[i].xtype == activePanelXtype) {
        activePanel = items[i];
      }
    }

    if (activePanel != null) {
      tabPanel.setActiveTab(activePanel);
    } else {
      tabPanel.setActiveTab(items[0]);
    }
  },
  createNewPanel: function (items, title, type, show, editable, closable) {
    if (show) {
      var initConfig = {border: false, editable: editable, title: title, closable: closable};
      var item = Ext.create(type, initConfig);
      if (items != null) {
        items.push(item);
      }
      return item;
    }
  },
  getParams: function () {
    return Budget.controller.CompareController.getParams();
  },
  statics: {
    getParams: function () {
      var params = {};
      for (var str in pageInitParam) {
        params[str] = pageInitParam[str];
      }

      //获取预算模版相关信息
      var budgetGroupId = -1;
      var budgetGroupCode = '';
      var budgetGroupViewType = '';
      var budgetGroup = Ext.ComponentQuery.query('budgetGroupGrid')[0];
      if (budgetGroup != null && budgetGroup != undefined) {
        var budgetGroupRecord = budgetGroup.getSelectionModel().getLastSelected();
        if (budgetGroupRecord != null && budgetGroupRecord != undefined) {
          budgetGroupId = budgetGroupRecord.data.id;
          budgetGroupCode = budgetGroupRecord.data.code;
          budgetGroupViewType = budgetGroupRecord.data.viewType;
        }
      }
      params.budgetGroupId = budgetGroupId;
      params.budgetGroupCode = budgetGroupCode;
      params.budgetGroupViewType = budgetGroupViewType;

      return params;
    }
  }
});