Ext.define('Regulate.view.panel.RegulateBasePanel', {
  extend: 'Ext.panel.Panel',
  alias: "widget.regulateBasePanel",
  bodyStyle: 'overflow-y: auto; overflow-x: hidden',
  values: {},
  items: [],
  buttonAlign: "left",
  regulateType: "",
  buttons: [
    {
      xtype: 'button',
      text: '保存',
      action: "save",
      handler: function () {
        this.up("regulateBasePanel").saveRegulate();
      }
    },
    {
      xtype: 'button',
      text: '提交',
      action: "submit",
      handler: function () {
        alert("提交");
      }
    },
    {
      xtype: 'button',
      text: '取消',
      action: "cancel"
    }
  ],
  initComponent: function () {
    var $this = this;

    this.title = this.regulateType == 'inside' ? '北京大学国际医院预算内支出申请明细表' : '预算外（或超预算）支出申请明细表',

      this.items.push({
        xtype: "panel",
        border: true,
        margin: "5 5 5 5",
        items: [{xtype: "regulateFormPanel", regulateType: this.regulateType}]
      });

    this.items.push({
      xtype: "panel",
      border: true,
      margin: "5 5 5 5",
      items: [{xtype: "regulateItemGrid", regulateType: this.regulateType}]
    });

    this.items.push({
      xtype: "panel",
      title: this.regulateType == 'inside' ? '预算调整原因' : '超预算原因',
      border: true,
      margin: "5 5 5 5",
      layout: {
        type: 'fit'
      },
      items: [{
        xtype: "textarea",
        fieldStyle: 'background-color:#FFFACD',
        width: '100%',
        minHeight: 100,
        emptyText: this.regulateType == 'inside' ? '说明调整原因' : '说明超预算原因，请尽量详细'
      }]
    });


    this.callParent(arguments);
  },
  loadRegulate: function () {
  },
  saveRegulate: function () {
    var budgetRegulate = {};
    var budgetRegulateDetails = [];
    var budgetRegulateId = -1;
    var form = Ext.ComponentQuery.query('regulateFormPanel')[0].getValues();

    budgetRegulate.organizationId = form.organizationId;
    budgetRegulate.setOfBooksId = form.setOfBooksId;
    budgetRegulate.period = form.period;
    budgetRegulate.description = this.down('textarea').getValue();
    budgetRegulate.status = "Draft";
    budgetRegulate.dataType = "BudgetExternal";

    if (this.regulateType == 'inside') {
      budgetRegulate.dataType = "BudgetInternal";
      budgetRegulate.budgetAmount = form.budgetAmount;
      budgetRegulate.adjustAmount = form.adjustAmount;
      budgetRegulate.budgetItemId = form.budgetItemId;
    }

    var callback = function (result) {
      budgetRegulateId = result[0].data.id;
    };

    Ext.create("Common.data.ApiReader")
      .createOrUpdate('budgetRegulate', [budgetRegulate], callback, false);

    var record = Ext.ComponentQuery.query('regulateItemGrid')[0].getStore().getRange();
    for (var i = 0; i < record.length; i++) {
      budgetRegulateDetails[i] = {};
      budgetRegulateDetails[i].id = record[i].raw.id;
      budgetRegulateDetails[i].budgetRegulateId = budgetRegulateId;
    }

    var callback = function (result) {
      var params = Common.CommonManager.getParams();
      Ext.ComponentQuery.query('regulateSearchToolbar')[0].show();
      Ext.ComponentQuery.query('regulateGrid')[0].show();
      Ext.ComponentQuery.query('regulateBasePanel')[0].hide();
      Ext.ComponentQuery.query('regulateGrid')[0].loadGrid(params);
    };

    Ext.create("Common.data.ApiReader")
      .createOrUpdate('budgetRegulateDetail', budgetRegulateDetails, callback, false);
  }
});