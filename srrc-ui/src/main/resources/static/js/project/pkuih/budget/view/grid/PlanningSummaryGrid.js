Ext.define('Budget.view.grid.PlanningSummaryGrid', {
  extend: 'Budget.view.grid.PlanningGrid',
  alias: "widget.planningSummaryGrid",
  title: "预算汇总",
  dataType: '@@SummaryPlanning@@'
});