Ext.define("Adjust.view.SearchToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.adjustSearchToolbar",
  margin: '0 0 5 0',
  items: [{
    id: "adjust-search-setOfBook",
    xtype: "setOfBookCombo",
    hidden: true,
    domainId: domainId,
    loadAfterRender: true,
    observable: true
  }, {
    id: "adjust-search-period",
    xtype: "periodCombo",
    width: 120,
    emptyText: '请选择',
    observable: true
  }, {
    id: "adjust-search-multiOrganization",
    xtype: "orgCombo",
    name: "organizationId",
    emptyText: '请选择',
    width: 200,
    initParams: {
      domainId: domainId,
      leaf: 1
    },
    multiSelect: true,
    selectType: "window",
    windowSelectValue: true,
    loadAfterRender: true,
    observable: true
  }, {
    id: "adjust-search-multiBudgetItem",
    xtype: "budgetItemCombo",
    name: "budgetItemId",
    emptyText: '请选择',
    fieldLabel: '预算项',
    labelWidth: 50,
    width: 260,
    editable: false,
    queryMode: 'local',
    displayField: 'alias',
    valueField: 'budgetItemId',
    multiSelect: true,
    selectType: "window",
    windowSelectValue: true,
    api: "budgetItemGroupItem",
    fields:["budgetItemId", "code","sortCode", "name", "alias", "leaf"],
    joins: [{
      type: 'budgetItem',
      fields: {
        name: 'name',
        code: 'code',
        leaf: 'leaf'
      },
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        type: 'Field',
        value: 'budgetItemId'
      }]
    }, {
      type: 'budgetItemGroup',
      joins: [{
        type: 'resourceExtInstance',
        fields: {
          instanceValue: 'budgetItemGroupCode'
        },
        conditions: [{
          field: 'instanceValue',
          condition: 'EQUALS',
          type: 'Field',
          value: 'code'
        }, {
          field: 'resourceExtOptionId',
          condition: 'EQUALS',
          type: 'String',
          value: 'default-planning-calculate-itemGroup-code'
        }, {
          field: 'resourceExtSchemaId',
          condition: 'EQUALS',
          type: 'String',
          value: 'budget-feature-setting-configuration'
        }]
      }],
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        type: 'Field',
        value: 'budgetItemGroupId'
      }]
    }],
    conditions: [
      {field: 'domainId', condition: 'EQUALS', value: '#domainId#'},
      {field: 'budgetItemGroupCode', condition: 'NOT_NULL'}
    ],
    initParams: {
      domainId: domainId
    },
    loadAfterRender: true,
    observable: true
  }]
});
