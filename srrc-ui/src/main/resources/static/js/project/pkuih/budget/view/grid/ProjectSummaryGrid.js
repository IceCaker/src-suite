Ext.define('Budget.view.grid.ProjectSummaryGrid', {
  extend: 'Budget.view.grid.PlanningGrid',
  alias: "widget.projectSummaryGrid",
  title: "项目预算",
  isProjectBudget: true,
  dataType: '@@ProjectPlanning@@'
});