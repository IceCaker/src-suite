Ext.require([
  'Common.ToolbarUtil',
  'Common.button.Import',
  'Common.button.Export',
  'Common.button.VersionMge',
  'Common.button.VersionSave'
]);
Ext.define('Project.view.panel.ProjectInfoPanel', {
  extend: "Ext.tab.Panel",
  alias: "widget.projectInfoPanel",
  domainId: '',
  projectId: -1,
  resourceId: '',
  readOnly: false,
  autoScroll: false,
  autoWidth: true,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  autoHeight: true,
  height: 600,
  tabBar: {
    items: [{
      xtype: 'component',
      flex: 1
    }, {
      id: 'operate-version-preliminaryProject',
      xtype: 'button',
      action: "confirm",
      text: '预立项',
      margin: '0 5 0 0 '
    }, {
      id: 'operate-version-projectApproval',
      xtype: 'button',
      action: 'commit',
      text: '立项',
      hidden: true,
      margin: '0 5 0 0'
    }, {
      id: 'operate-project-version-save',
      xtype: 'versionSaveButton',
      margin: '0 5 0 0',
      params: {
        domainId: 'Default',
        domainTypes: 'project',
        projectId: '1'
      }
    }, {
      id: 'operate-version-print',
      xtype: 'button',
      action: "print",
      text: '打印预览',
      margin: '0 5 0 0 '
    }, {
      xtype: 'button',
      action: "close",
      text: '关闭',
      margin: '0 5 0 0 '
    }]
  },
  items: [{xtype: "projectBasePanel"}],
  showTabs: function (isTravel) {
    //删除原有页签
    var length = this.items.length;
    for (var i = 1; i < length; i++) {
      this.remove(this.items.get(1));
    }

    //增加新页签
    var tabs = [];
    var tabsIndex = 0;

    //差旅页签
    if (isTravel) {

    }

    if (this.projectId > 0) {
      if (false && this.down("projectProcessPanel") == null) {
        tabs[tabsIndex++] = {xtype: "projectProcessPanel"};
      }
      if (false && this.down("projectBillPanel") == null) {
        tabs[tabsIndex++] = {xtype: "projectBillPanel"};
      }
      if (this.down("projectVersionPanel") == null) {
        tabs[tabsIndex++] = {xtype: "projectVersionPanel"};
      }
      Ext.getCmp("operate-version-preliminaryProject").show();
      Ext.getCmp("operate-project-version-save").show();
      Ext.getCmp('operate-version-print').show();
    } else {
      Ext.getCmp("operate-version-preliminaryProject").hide();
      Ext.getCmp("operate-project-version-save").hide();
      Ext.getCmp('operate-version-print').hide();
    }

    this.add(tabs);
    this.setActiveTab(0);
  },
  loadData: function () {
    this.showTabs();
    this.down("projectBasePanel").loadData();
  }
});
