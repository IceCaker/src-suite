Ext.define('Budget.controller.ProjectController', {
  extend: 'Ext.app.Controller',
  views: ["toolbar.OperateToolbar", "grid.ProjectSummaryGrid", "grid.ProjectGrid"],
  refs: [
    {ref: 'operateToolbar', selector: 'operateToolbar'},
    {ref: 'planningTabPanel', selector: 'planningTabPanel'},
    {ref: 'projectSummaryGrid', selector: 'projectSummaryGrid'},
    {ref: 'projectGrid', selector: 'projectGrid'},
    {ref: 'projectFormPanel', selector: 'projectFormPanel'},
    {ref: 'projectInfoWindow', selector: 'projectInfoWindow'},
    {ref: 'projectItemGrid', selector: 'projectItemGrid'}
  ],
  businessTypeName: "项目编制",
  defaultTimeout: 600000,
  init: function () {
    var $this = this;
    this.control({
      'operateToolbar button[action=create]': {click: function(){$this.showProject(-1);}},
      'projectGrid': {
        nameClickEdit: this.showProject
      },
      'projectItemGrid button[action=add]': {
        click: function() {
          $this.addProjectItem();
        }
      }
    });
  },
  showProject: function (projectId) {
    var $this = this;
    var tabPanel = $this.getPlanningTabPanel();
    var params = this.getParams();
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == undefined || budgetGroupViewType == null) {
      budgetGroupViewType == "";
    }
    var canSave = Common.CommonManager.editor.canSave;
    params.projectId = projectId;

    var isEdit = false;
    if(canSave && !tabPanel.processPage && budgetGroupViewType != "BudgetSummary") {
      isEdit = true;
    }

    Ext.widget('projectInfoWindow').showAndRevertFunc(params, isEdit,
      function () {
        mainApplication.getController('PlanningController').loadGrid(true);
      }
    );
  },
  addProjectItem: function() {
    var $this = this;
    var params = this.getParams();
    var projectId = $this.getProjectInfoWindow().projectId;
    var isSaveSuccess = true;

    if (params.budgetGroupId > 0) {
      var projectName = Ext.getCmp("project-textfield-projectName").getValue();
      var conditions = [{
        field: "domainId",
        condition: "EQUALS",
        type: "String",
        value: params.domainId
      }, {
        field: "setOfBooksId",
        condition: "EQUALS",
        type: "Integer",
        value: params.setOfBooksId
      }, {
        field: "period",
        condition: "EQUALS",
        type: "String",
        value: params.period
      }, {
        field: "organizationId",
        condition: "EQUALS",
        type: "Integer",
        value: params.organizationId
      }, {
        field: "dataType",
        condition: "STARTS_WITH",
        type: "String",
        value: "@@Project@@"
      }, {
        field: "budgetObjectType",
        condition: "EQUALS",
        type: "String",
        value: "BudgetItem"
      }, {
        field: 'budgetItemGroupId',
        condition: 'EQUALS',
        value: params.budgetGroupId
      }, {
        field: 'projectName',
        condition: 'EQUALS',
        value: projectName
      }];

      if (projectId > 0) {
        conditions.push({
          field: 'projectId',
          condition: 'NOT_EQUALS',
          value: projectId
        })
      }

      var projects = Ext.create("Common.data.ApiReader").load({
        api: 'relatedProjectItem',
        joins: [
          {
            type: 'project',
            fields: {
              name: 'projectName'
            },
            conditions: [{
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'projectId'
            }]
          }
        ],
        conditions: conditions
      });

      if (projects.length > 0) {
        Common.Msg.Alert("此项目名称已存在，请修改项目名称！");
        return false;
      }
    }

    if (projectId <= 0) {
      isSaveSuccess = $this.getProjectFormPanel().saveProject();
      projectId =  $this.getProjectInfoWindow().projectId;
    }

    if (isSaveSuccess && projectId > 0) {
      this.getProjectItemGrid().loadGrid(projectId);
    }
  },
  getParams: function () {
    return Budget.controller.PlanningController.getParams();
  }
});
