Ext.define('Regulate.view.combo.BudgetItem', {
  extend: 'Common.combo.BudgetItem',
  alias: 'widget.regulateBudgetItemCombo',
  name: "budgetItemId",
  emptyText: '请选择',
  fieldLabel: '支出科目',
  labelWidth: 60,
  width: 260,
  editable: false,
  queryMode: 'local',
  displayField: 'alias',
  valueField: 'budgetItemId',
  multiSelect: false,
  selectType: "combo",
  windowSelectValue: true,
  api: "budgetItemGroupItem",
  fields: ["budgetItemId", "code", "sortCode", "name", "alias", "leaf"],
  joins: [{
    type: 'budgetItem',
    fields: {
      name: 'name',
      code: 'code',
      leaf: 'leaf'
    },
    conditions: [{
      field: 'id',
      condition: 'EQUALS',
      type: 'Field',
      value: 'budgetItemId'
    }]
  }, {
    type: 'budgetItemGroup',
    joins: [{
      type: 'resourceExtInstance',
      fields: {
        instanceValue: 'budgetItemGroupCode'
      },
      conditions: [{
        field: 'instanceValue',
        condition: 'EQUALS',
        type: 'Field',
        value: 'code'
      }, {
        field: 'resourceExtOptionId',
        condition: 'EQUALS',
        type: 'String',
        value: 'default-planning-calculate-itemGroup-code'
      }, {
        field: 'resourceExtSchemaId',
        condition: 'EQUALS',
        type: 'String',
        value: 'budget-feature-setting-configuration'
      }]
    }],
    conditions: [{
      field: 'id',
      condition: 'EQUALS',
      type: 'Field',
      value: 'budgetItemGroupId'
    }]
  }],
  conditions: [
    {field: 'domainId', condition: 'EQUALS', value: '#domainId#'},
    {field: 'budgetItemGroupCode', condition: 'NOT_NULL'}
  ],
  initParams: {
    domainId: domainId
  },
  loadAfterRender: true
});