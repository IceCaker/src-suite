Ext.define("Budget.view.window.PlanningSituationWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.planningSituationWindow",
  title: "编制情况",
  plain: true,
  width: 500,
  height: 300,
  layout: 'fit',
  closeAction: "destroy",
  autoShow: true,
  modal: true,
  params: [],
  items: [{
    xtype: "grid",
    simpleSelect: false,
    multiSelect: true,
    viewConfig: {
      loadMask: false,
      forceFit: true
    },
    params: {},
    store: Ext.create('Ext.data.Store', {
      fields: [
        {name: 'id', type: 'long'},
        {name: 'name', type: 'string'},
        {name: 'organizationLevel', type: 'string'},
        {name: 'statusName', type: 'string'},
        {name: 'handlerName', type: 'string'}
      ],
      data: []
    }),
    columns: [
      {
        text: '组织',
        dataIndex: 'name',
        align: 'left',
        flex: 1,
        minWidth: 150,
        sortable: false
      },
      {
        text: '状态',
        dataIndex: 'statusName',
        align: 'left',
        width: 120,
        sortable: false
      },
      {
        text: '审批人',
        dataIndex: 'handlerName',
        align: 'left',
        width: 120,
        sortable: false
      }
    ]
  }],
  buttons: [{
    text: "关闭",
    handler: function () {
      this.up("window").close();
    }
  }],
  loadGrid: function () {

  },
  showAndRevertFunc: function (func) {
    var $this = this;
    var subOrganizationStatuses = this.loadData();
    for (var i = 0; i < subOrganizationStatuses.length; i++) {
      if (subOrganizationStatuses[i].statusName == undefined
        || subOrganizationStatuses[i].statusName == '') {
        subOrganizationStatuses[i].statusName = '草稿';
      }
      if (subOrganizationStatuses[i].processId != undefined
        && subOrganizationStatuses[i].processId != ''
        && subOrganizationStatuses[i].statusName == '审批中') {
        var task = Ext.create('Common.data.ApiReader').load({
          api: 'taskImpl',
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            value: domainId
          }, {
            field: 'processInstanceId',
            condition: 'IN',
            value: subOrganizationStatuses[i].processId
          }],
          sorts: [{
            field: 'createdOn',
            direction: 'DESC'
          }]
        });
        if (task[0].id == undefined && task[0].id == '') {
          continue;
        }
        var message = Ext.create('Common.data.ApiReader').load({
          api: 'message',
          joins: [{
            type: 'user',
            fields: {
              name: 'userName'
            },
            conditions: [{
              field: 'domainId',
              condition: 'EQUALS',
              type: 'Field',
              value: 'domainId'
            }, {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'receiverId'
            }]
          }],
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            value: domainId
          }, {
            field: 'param',
            condition: 'CONTAINS',
            value: "taskId=" + task[0].id + "\""
          }]
        });
        if (message[0].userName != undefined && message[0].userName != '') {
          subOrganizationStatuses[i].handlerName = message[0].userName;
        }
      }
    }

    $this.down("grid").store.loadData(subOrganizationStatuses);

    if (func != null) {
      this.loadGrid = func;
    }

    $this.show();
  },
  loadData: function () {
    var params = Common.CommonManager.getParams();

    var subOrganizationStatuses = Ext.create('Common.data.ApiReader').load({
      api: 'organization',
      fields: {
        id: 'id',
        name: 'name',
        sortCode: 'sortCode',
        organizationLevel: 'organizationLevel'
      },
      joins: [{
        type: 'budgetDataStatus',
        fields: {
          processId: 'processId'
        },
        joins: [{
          type: 'dictionaryOption',
          fields: {
            names: 'statusName'
          },
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'domainId'
          }, {
            field: 'code',
            condition: 'EQUALS',
            type: 'Field',
            value: 'statusCode'
          }, {
            field: 'dictionaryCode',
            condition: 'EQUALS',
            value: 'BudgetStatus'
          }]
        }],
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'id'
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        }, {
          field: 'period',
          condition: 'EQUALS',
          type: 'String',
          value: params.period
        }, {
          field: 'businessType',
          condition: 'EQUALS',
          type: 'String',
          value: params.businessType
        }, {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: '@@Planning@@'
        }]
      }],
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'Field',
        value: 'domainId'
      }, {
        field: 'sortCode',
        condition: 'STARTS_WITH',
        value: params.organizationSortCode
      }],
      sorts: [{
        field: 'sortCode',
        direction: 'ASC'
      }]
    });

    return subOrganizationStatuses;
  }
});