Ext.define('Project.controller.ProjectPrintController', {
  extend: 'Ext.app.Controller',
  views: [
    "panel.ProjectBasePanel", "grid.ProjectFileGrid",
    "panel.ProjectProcessPanel", "panel.ProjectBillPanel", "panel.ProjectVersionPanel",
    "combo.ProjectCreateOrganization", "combo.ProjectDutyOrganization",
    "combo.ExpenseItem", "toolbar.ProjectManageToolbar","toolbar.SearchToolbar",
    "window.FileUploadWindow", "panel.default.ProjectFormPanel"
  ],
  refs: [
    { ref: "projectGrid", selector: 'projectGrid'},
    { ref: "projectSearchToolbar", selector: 'projectSearchToolbar'},
    { ref: "manageToolbar", select: 'manageToolbar' },
    { ref: "projectInfoPanel", select: 'projectInfoPanel'},
    { ref: "projectFileGrid", select: 'projectFileGrid' }
  ],
  init: function () {
    var $this = this;
    this.control({
      'projectVersionPanel': { showVersion: $this.showVersion}
    });
    // 由于输入备注时需要回车键，暂时去掉次监听
    //this.addEvent();
  },
  showVersion: function(projectId, versionId){
    var $this = this;
  }
});
