Ext.define('Adjust.controller.AdjustController', {
  extend: 'Ext.app.Controller',
  views: ['MainPanel', 'SearchToolbar', 'OperateToolbar', 'AdjustGrid', 'AdjustRecordWindow'],
  refs: [
    {ref: "adjustMainPanel", selector: 'adjustMainPanel'},
    {ref: "adjustOperateToolbar", selector: 'adjustOperateToolbar'},
    {ref: "adjustGrid", selector: 'adjustGrid'}
  ],
  init: function () {
    var $this = this;
    this.control({
      'adjustMainPanel': {
        'render': function () {
          $this.addPanelFunc();
        }
      },
      'adjustOperateToolbar button[action=export]': {click: $this.exportData},
      'adjustOperateToolbar button[action=save]': {click: function () {
        Common.CommonManager.showSpecialMask(function () {
          $this.save();
        },"预算调整中，请稍候...");
      }},
      'adjustOperateToolbar button[action=adjustRecord]': {click: $this.adjustRecord},
      'adjustOperateToolbar button[xtype=versionManageButton]': {click: $this.verifyVersionManage}
    });
    Common.CommonManager.getParams = this.getParams;
    Common.CommonManager.loadGrid = this.loadGrid;
    Common.CommonManager.showDifferent = $this.showDifferent;
  },
  addPanelFunc: function () {
    var $this = this;

    $this.getAdjustMainPanel().add({
      id: 'adjust-search-toolbar',
      xtype: 'adjustSearchToolbar',
      region: 'north'
    });

    Ext.getCmp("adjust-search-multiOrganization").getStore().on("load",
      function ($this, records, successful, eOpts) {
        Ext.getCmp("adjust-search-multiOrganization").setValue(null);
      }
    );

    Ext.getCmp("adjust-search-multiBudgetItem").getStore().on("load",
      function ($this, records, successful, eOpts) {
        Ext.getCmp("adjust-search-multiBudgetItem").setValue(null);
      }
    );

    Ext.getCmp("adjust-search-multiBudgetItem").getStore().on("load",
      function ($this, records, successful, eOpts) {
        var deleteRecords = [];
        for (var i = 0; i < $this.data.items.length; i++) {
          var code = $this.data.items[i].data.code;
          var sortCode = $this.data.items[i].data.sortCode;
          if (code != "YS1001" && code != "YS1005" && code != "YS1201" && code != "YS1202") {
            if (code == "YS120102" || code == "YS120101" || code == "YS120103" || code == "YS120201"
              || code == "YS120202" || code == "YS120203" || code == "YS100501" || code == "YS100502"
              || code == "YS100101" || code == "YS100102" || code == "YS100103") {
              deleteRecords.push($this.data.items[i]);
            } else {
              if (!$this.data.items[i].data.leaf) {
                for (var j = 0; j < $this.data.items.length; j++) {
                  if (sortCode == $this.data.items[j].data.sortCode) {
                    continue;
                  } else {
                    if ($this.data.items[j].data.sortCode.length > sortCode.length
                      && $this.data.items[j].data.sortCode.indexOf(sortCode) == 0) {
                      deleteRecords.push($this.data.items[i]);
                      break;
                    }
                  }
                }
              }
            }
          }
        }

        for (var i = 0; i < deleteRecords.length; i++) {
          $this.remove(deleteRecords[i]);
        }

        Ext.getCmp("adjust-search-multiBudgetItem").setValue(null);
      }
    );


    $this.getAdjustMainPanel().add({
      id: 'adjust-operate-toolbar',
      xtype: 'adjustOperateToolbar',
      region: 'north'
    });

    $this.getAdjustMainPanel().add({
      xtype: 'adjustGrid',
      itemId: 'adjustGrid',
      region: 'center'
    });
  },
  loadGrid: function () {
    var $this = this;
    var params = $this.getParams();
    var isLoadAll = $this.isLoadAll();

    if (isLoadAll) {
      $this.getAdjustGrid().loadData(params);
    }
  },
  exportData: function () {
    var $this = this;
    var params = $this.getParams();
    var isLoadAll = $this.isLoadAll();

    if (isLoadAll) {
      //验证是否允许调整
      //if (!this.verifyOrganizationStatus()) {
      //  return false;
      //}

      var budgetItemGroup = Ext.create("Common.data.ApiReader").load({
        api: "resourceExtInstance",
        joins: [
          {
            type: "budgetItemGroup",
            fields: {
              id: "budgetItemGroupId"
            },
            conditions: [
              {
                field: "code",
                condition: "EQUALS",
                type: "Field",
                value: "instanceValue"
              }
            ]
          }
        ],
        conditions: [
          {
            field: "domainId",
            condition: "EQUALS",
            type: "String",
            value: domainId
          }, {
            field: "resourceExtOptionId",
            condition: "EQUALS",
            type: "String",
            value: "default-planning-calculate-itemGroup-code"
          }, {
            field: "resourceExtSchemaId",
            condition: "EQUALS",
            type: "String",
            value: "budget-feature-setting-configuration"
          }
        ]
      });
      var budgetItemGroupId = budgetItemGroup[0].budgetItemGroupId;

      downloadFile(contextPath + '/pkuih/budget/adjust/export', {
        'period': params.period,
        'domainId': params.domainId,
        'adjustWay': params.adjustWay,
        'dataType': '@@SummaryPlanning@@',
        'adjustValue': params.adjustValue,
        'setOfBooksId': params.setOfBooksId,
        'budgetItemIds': params.budgetItemId,
        'budgetItemGroupId': budgetItemGroupId,
        'organizationIds': params.organizationId
      });
    }
  },
  save: function () {
    var $this = this;
    var params = $this.getParams();
    var isLoadAll = $this.isLoadAll();

    if (isLoadAll) {
      //验证是否允许调整
      if (!this.verifyOrganizationStatus()) {
        return false;
      }

      var budgetAdjustData = [];
      var budgetAdjustId = {};
      var selectedBudgetAdjusts = $this.getAdjustGrid().getSelectionModel().getSelection();

      if (selectedBudgetAdjusts.length == 0) {
        Common.Msg.Alert("请选择需要调整的预算项！");
        return false;
      }

      var budgetAdjust = {};
      budgetAdjust.setOfBooksId = params.setOfBooksId;
      budgetAdjust.period = params.period;
      budgetAdjust.adjustWay = params.adjustWay;
      if (params.adjustWay == 'Amount') {
        budgetAdjust.adjustValue = params.adjustValue;
      } else {
        budgetAdjust.adjustValue = params.adjustValue / 100;
      }
      budgetAdjust.status = 'Draft';

      var organizationText =  Ext.ComponentQuery.query('orgCombo')[0].rawValue;
      var budgetItemText =  Ext.ComponentQuery.query('budgetItemCombo')[0].rawValue;
      budgetAdjust.description = '@@' + organizationText + '@@' + budgetItemText + '@@';

      var adjustCallback = function (result) {
        budgetAdjustId = result[0].data.id;
      };

      Ext.create("Common.data.ApiReader")
        .createOrUpdate('budgetAdjust', [budgetAdjust], adjustCallback, false);

      var budgetAdjustDetailArray = [];
      var organizationIds = params.organizationId;
      for (var i = 0; i < selectedBudgetAdjusts.length; i++) {
        for (var j = 0; j < organizationIds.length; j++) {

          var id = organizationIds[j];
          var budgetAdjustDetail = {};
          budgetAdjustDetail.budgetAdjustId = budgetAdjustId;
          budgetAdjustDetail.setOfBooksId = params.setOfBooksId;
          budgetAdjustDetail.period = params.period;
          budgetAdjustDetail.organizationId = organizationIds[j];
          budgetAdjustDetail.budgetItemId = selectedBudgetAdjusts[i].data.id;
          budgetAdjustDetail.budgetAmount = selectedBudgetAdjusts[i].data['budgetAmount' + organizationIds[j]];
          budgetAdjustDetail.adjustAmount = selectedBudgetAdjusts[i].data['adjustAmount' + id];
          budgetAdjustDetailArray.push(budgetAdjustDetail);
        }
      }

      Ext.create("Common.data.ApiReader")
        .createOrUpdate('budgetAdjustDetail', budgetAdjustDetailArray);

      budgetAdjust.id = budgetAdjustId;
      budgetAdjust.status = 'Adjust';

      var updateCallback = function (result) {
        Common.Msg.Alert("预算调整成功！");
        Ext.getCmp("adjust-operate-adjustWay").setValue('');
        Ext.getCmp("adjust-operate-adjustValue").setValue('');
        $this.loadGrid();
      };
      Ext.create("Common.data.ApiReader")
        .createOrUpdate('budgetAdjust', [budgetAdjust], updateCallback, false);
    }
  },
  verifyOrganizationStatus: function (isShowAlert) {
    var $this = this;
    var params = $this.getParams();
    if (isShowAlert == null) {
      isShowAlert = true;
    }

    var organizations = Ext.create("Common.data.ApiReader").load({
      api: "organization",
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        value: params.domainId
      }, {
        field: 'parentId',
        condition: 'EQUALS',
        value: 0
      }]
    });

    if (organizations != null && organizations[0] != null
      && organizations[0].id != undefined && organizations[0].id != '') {

      //检查系统配置，是否校验下级组织审批通过
      var isVerifySubOrganizationStatus = true;
      var resourceExtInstance = Ext.create("Common.data.ApiReader").load({
        api: "resourceExtInstance",
        conditions: [{
          field: "resourceExtOptionId",
          condition: "EQUALS",
          type: "String",
          value: "enabled-verify-subOrganization-approved"
        }, {
          field: "resourceExtSchemaId",
          condition: "EQUALS",
          type: "String",
          value: "pkuih-configuration"
        }]
      });
      if (resourceExtInstance != null && resourceExtInstance[0] != null
        && resourceExtInstance[0].instanceValue != undefined && resourceExtInstance[0].instanceValue == "false") {
        isVerifySubOrganizationStatus = false;
      }

      //非末级组织，验证下级部门，全部初审通过或已提交
      var subOrganizationStatuses = Ext.create("Common.data.ApiReader").load({
        api: 'organization',
        fields: {
          id: 'id',
          name: 'name',
          parentId: 'parentId'
        },
        joins: [{
          type: 'budgetDataStatus',
          fields: {
            period: 'period',
            dataType: 'dataType',
            statusCode: 'statusCode',
            setOfBooksId: 'setOfBooksId',
            businessType: 'businessType'
          },
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'domainId'
          }, {
            field: 'organizationId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'id'
          }, {
            field: 'setOfBooksId',
            condition: 'EQUALS',
            type: 'Integer',
            value: params.setOfBooksId
          }, {
            field: 'period',
            condition: 'EQUALS',
            type: 'String',
            value: params.period
          }, {
            field: 'businessType',
            condition: 'STARTS_WITH',
            type: 'String',
            value: 'yearPlanning'
          }, {
            field: 'dataType',
            condition: 'STARTS_WITH',
            type: 'String',
            value: '@@Planning@@'
          }]
        }],
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          conditions: [{
            operator: 'OR',
            field: 'id',
            condition: 'EQUALS',
            type: 'Integer',
            value: organizations[0].id
          }, {
            operator: 'OR',
            field: 'parentId',
            condition: 'EQUALS',
            type: 'Integer',
            value: organizations[0].id
          }]
        }]
      });

      var organizationMessage = "";
      var errorMessage = "";
      for (var i = 0; i < subOrganizationStatuses.length; i++) {
        var subOrganizationStatus = subOrganizationStatuses[i];
        if (subOrganizationStatus != null) {
          var statusCode = 'Draft';
          if (subOrganizationStatus.statusCode != undefined && subOrganizationStatus.statusCode != '') {
            statusCode = subOrganizationStatus.statusCode;
          }
          if (subOrganizationStatus.id == organizations[0].id
            && (statusCode == 'Approving' || statusCode == 'Approved' || statusCode == 'Submitted'
            || statusCode == 'FirstTrailApproved' || statusCode == 'ReexamineApproved'
            || statusCode == 'FinalJudgmentApproved')) {
            var statusName = "审批通过";
            if (statusCode == 'Approving') {
              statusName = "审批中";
            } else if (statusCode == 'Submitted') {
              statusName = "已提交";
            } else if (statusCode == 'FirstTrailApproved') {
              statusName = "初审通过";
            } else if (statusCode == 'ReexamineApproved') {
              statusName = "复审通过";
            } else if (statusCode == 'FinalJudgmentApproved') {
              statusName = "终审通过";
            }
            organizationMessage = Ext.String.format("【{0}】的状态为{1}，暂时不能调整！", subOrganizationStatus.name, statusName);
          } else if (isVerifySubOrganizationStatus && subOrganizationStatus.id != organizations[0].id
            && !(statusCode == 'Approved' || statusCode == 'Submitted'
            || statusCode == 'FirstTrailApproved' || statusCode == 'ReexamineApproved'
            || statusCode == 'FinalJudgmentApproved')) {
            if (errorMessage != null && errorMessage != "") {
              errorMessage += Ext.String.format("、【{0}】", subOrganizationStatus.name);
            } else {
              errorMessage += Ext.String.format("【{0}】", subOrganizationStatus.name);
            }
          }
        }
      }

      if (organizationMessage != "") {
        if (isShowAlert) {
          organizationMessage += "<br/>";
          Common.Msg.Alert(organizationMessage);
        }
        return false;
      }

      if (errorMessage != "") {
        if (isShowAlert) {
          errorMessage += "未审批通过，暂时不能调整！<br/>";
          Common.Msg.Alert(errorMessage);
        }
        return false;
      }
    }

    return true;
  },
  adjustRecord: function () {
    var $this = this;
    var params = $this.getParams();
    Ext.widget('adjustRecordWindow').showAndRevertFunc();
  },
  verifyVersionManage: function () {
    var $this = this;
    var isEdit = true;

    //验证是否允许调整
    if (!$this.verifyOrganizationStatus(false)) {
      isEdit = false;
    }

    if (isEdit) {
      $this.getAdjustOperateToolbar().down("button[xtype=versionManageButton]").showUseVersion = true;
    } else {
      $this.getAdjustOperateToolbar().down("button[xtype=versionManageButton]").showUseVersion = false;
    }
  },
  showDifferent: function (ids) {
    var $this = this;
    var params = $this.getParams();
    var versionIds = [];
    var organizations = Ext.create("Common.data.ApiReader").load({
      api: "organization",
      conditions: [
        {
          field: 'domainId',
          condition: 'EQUALS',
          value: domainId
        }, {
          field: 'parentId',
          condition: 'EQUALS',
          value: 0
        }
      ]
    });

    var versionOrganizationId = -1;
    if (organizations != null && organizations[0] != null
      && organizations[0].id != null && organizations[0].id != '') {
      versionOrganizationId = organizations[0].id;
    }
    if (ids.length == 0) {
      Common.Msg.Alert("请至少选择一个版本进行对比！");
      return;
    }

    if (ids.length <= 2) {
      for (var i = 0; i < ids.length; i++) {
        versionIds.push(ids[i].data.id);
      }
      var versionSetOfBooksId = params.setOfBooksId;
      var versionPeriod = params.period;
      var url = contextPath + "/pkuih/budget/compare/ui?versionIds=" + versionIds
        + "&setOfBooksId="+versionSetOfBooksId+"&period="+versionPeriod+"&organizationId="+versionOrganizationId;
      openBrowserWindow({isFull: true, url: url});
    } else {
      Common.Msg.Alert("请选择最多两个版本进行对比，如选一个将与当前版本进行对比！");
    }
  },
  isLoadAll: function () {
    var isLoadAll = false;
    var queryButton = Ext.getCmp('adjust-operate-query');
    if (queryButton != null && queryButton != undefined) {
      isLoadAll = queryButton.isLoadAll();
    }
    return isLoadAll;
  },
  getParams: function () {
    return Adjust.controller.AdjustController.getParams();
  },
  statics: {
    getParams: function () {
      var params = {};
      for (var str in pageInitParam) {
        params[str] = pageInitParam[str];
      }

      var queryButton = Ext.getCmp('adjust-operate-query');
      if (queryButton != null && queryButton != undefined) {
        params.setOfBooksId = queryButton.getParams().setOfBooksId;
        params.period = queryButton.getParams().period;
        params.organizationId = queryButton.getParams().organizationId;
        params.budgetItemId = queryButton.getParams().budgetItemId;
        params.adjustWay = queryButton.getParams().adjustWay;
        params.adjustValue = queryButton.getParams().adjustValue;
      }

      return params;
    }
  }
});