Ext.define('Project.controller.ProjectListController', {
  extend: 'Ext.app.Controller',
  views: [
    "toolbar.ProjectManageToolbar", "grid.ProjectGrid", "toolbar.SearchToolbar", "combo.ProjectManager"
  ],
  refs: [
    {ref: "projectGrid", selector: 'projectGrid'},
    {ref: "projectSearchToolbar", selector: 'projectSearchToolbar'},
    {ref: "manageToolbar", selector: 'manageToolbar'},
    {ref: "projectInfoPanel", selector: 'projectInfoPanel'}
  ],
  init: function () {
    var $this = this;
    this.control({
      'manageToolbar button[action=create]': {
        click: function () {
          $this.showInfoPanel(true, true, -1, '');
        }
      },
      'manageToolbar button[action=delete]': {click: this.deleteFunc},
      'manageToolbar button[action=confirm]': {click: this.confirm},
      'manageToolbar button[action=commit]': {click: this.commit},
      'manageToolbar button[action=copy]': {click: this.copy},
      'projectInfoPanel button[action=close]': {
        click: function () {
          $this.showInfoPanel(false);
        }
      },
      'projectGrid': {
        nameClickEdit: this.nameClickEdit,
        nameClickCancel: this.nameClickCancel,
        nameClickDelete: this.nameClickDelete
      }
    });
    Common.CommonManager.getParams = function () {
      return $this.getParams();
    }
    Common.CommonManager.loadGrid = function () {
      $this.query(true);
    }
  },
  showInfoPanel: function (isShow, isEdit, projectId, resourceId) {
    if (isShow) {
      this.getProjectSearchToolbar().hide();
      this.getProjectGrid().hide();
      this.getProjectInfoPanel().projectId = projectId;
      this.getProjectInfoPanel().resourceId = resourceId;
      if (isEdit) {
        this.getProjectInfoPanel().readOnly = false;
      } else {
        this.getProjectInfoPanel().readOnly = true;
      }
      this.getProjectInfoPanel().show();
      this.getProjectInfoPanel().loadData();
    } else {
      this.getProjectSearchToolbar().show();
      this.getProjectGrid().show();
      this.getProjectInfoPanel().hide();
      this.query(true);
    }
  },

  query: function (isAgain) {
    if (isAgain) {
      this.getProjectGrid().getStore().load();
    } else {
      this.getProjectGrid().getStore().reload();
    }
  },
  deleteFunc: function (btn) {
    var $this = this;
    var selectedProjects = $this.getProjectGrid().getSelectionModel().getSelection();

    if (selectedProjects.length == 0) {
      Common.Msg.Alert("请选择" + btn.text + "的项目！");
      return false;
    }

    var records = [];
    for (var i = 0; i < selectedProjects.length; i++) {
      if (selectedProjects[i].data.deletable) {
        records.push(selectedProjects[i]);
      }
    }

    if (records.length == 0) {
      Common.Msg.Error('选择的项目不能删除！');
      return false;
    }

    var callback = function () {
      $this.query();
    };

    Ext.create("Common.data.ApiReader").deleteFunc('project', records, callback);
  },
  confirm: function (btn) {
    var $this = this;
    var selectModels = $this.getProjectGrid().getSelectionModel().getSelection();
    if(selectModels.length ==0) {
      Common.Msg.Alert("请至少选择一个项目！");
    }else {
      alert("confirm");
    }
  },
  commit: function (btn) {
    var $this = this;

  },
  approve: function (params) {
    var $this = this;

  },
  cancelAdjust: function (btn) {
    var $this = this;

  },
  copy: function (btn) {
    var $this = this;
    var selectModels = $this.getProjectGrid().getSelectionModel().getSelection();
    if(selectModels.length ==0) {
      Common.Msg.Alert("请至少选择一个项目！");
    }else {
      alert("copy");
    }
  },
  nameClickEdit: function (id, resourceId) {
    var isEdit = true;
    var params = this.getParams();
    this.showInfoPanel(true, isEdit, id, resourceId);
    Ext.getCmp('operate-project-version-save').params = {
      domainId: 'Default',
      domainTypes: 'project',
      projectId: id
    };
  },
  nameClickCancel: function (id) {
    var $this = this;

  },
  nameClickDelete: function (id) {
    var $this = this;
    var record = $this.getProjectGrid().getStore().getById(id);

    var callback = function () {
      $this.query();
    };

    Ext.create("Common.data.ApiReader").deleteFunc('project', [record], callback);
  },
  getParams: function () {
    return Ext.getCmp('project-search-queryButton').params
  }
});
