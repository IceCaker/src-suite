Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.button.Query',
  "Common.combo.ApiCombox",
  "Common.combo.CommonCombox",
  "Common.combo.SetOfBook",
  "Common.combo.Period",
  "Common.combo.Organization",
  "Common.combo.BudgetItem"
]);

Ext.application({
  name: 'Adjust',
  appFolder: contextPath + '/static/js/project/pkuih/adjust',
  controllers: ["AdjustTaskController"],
  launch: function () {
    Ext.tip.QuickTipManager.init(pageInitParam);

    var panel = Ext.create('Ext.panel.Panel', {
      autoScroll: false,
      layout: 'border',
      border: false,
      region: 'center',
      bodyStyle: {
        background: "#FFFFFF"
      },
      bodyPadding: 5,
      items: {
        xtype: 'adjustTaskGrid',
        scroll: true,
        region: 'center'
      }
    });
    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add(panel);
  }
});