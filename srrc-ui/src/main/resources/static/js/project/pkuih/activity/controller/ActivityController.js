Ext.define('Activity.controller.ActivityController', {
    extend: 'Ext.app.Controller',
    views: ['MainPanel', 'SearchToolbar', 'OperateToolbar', 'ActivityGrid'],
    refs: [
        {ref: "activityMainPanel", selector: 'activityMainPanel'},
        {ref: "activityOperateToolbar", selector: 'activityOperateToolbar'},
        {ref: "activitySearchToolbar", selector: 'activitySearchToolbar'},
        {ref: "activityGrid", selector: 'activityGrid'}
    ],
    init: function () {
        var $this = this;
        this.control({
            'activityMainPanel': {
                'render': function () {
                    $this.addPanelFunc();
                }
            },
            'activityOperateToolbar button[action=add]': {click: this.addFunc},
            'activityOperateToolbar button[action=commit]': {
                click: function () {
                    Common.CommonManager.showSpecialMask(function () {
                        $this.save();
                    }, "提交中，请稍候...");
                }
            },
            'activityOperateToolbar button[action=delete]': {click: this.deleteFunc},
            'activityOperateToolbar button[action=close]': {},
            'activityOperateToolbar button[action=export]': {click: $this.exportData}
        });
        Common.CommonManager.getParams = this.getParams;
        Common.CommonManager.loadGrid = this.loadGrid;
        Common.CommonManager.showDifferent = $this.showDifferent;
    },
    addPanelFunc: function () {
        var $this = this;

        $this.getActivityMainPanel().add({
            id: 'acivity-search-toolbar',
            xtype: 'activitySearchToolbar',
            region: 'north'
        });

        $this.getActivityMainPanel().add({
            id: 'activity-operate-toolbar',
            xtype: 'activityOperateToolbar',
            region: 'north'
        });

        $this.getActivityMainPanel().add({
            id: 'activityGrid',
            xtype: 'activityGrid',
            region: 'center'
        });
    },
    loadGrid: function () {
        var $this = this;
        var params = $this.getParams();
        this.getActivityGrid().getStore().load();
    },
    exportData: function () {
        var $this = this;
        var params = $this.getParams();
        var isLoadAll = $this.isLoadAll();
    },
    commit: function () {
        var $this = this;
        var params = $this.getParams();
        var isLoadAll = $this.isLoadAll();

        if (isLoadAll) {}
    },
    addFunc: function () {
        if(projectCategoryCode == 'MeetingCost'){
            Common.Msg.Alert("会议费");
            // Ext.widget('adjustRecordWindow').showFunc();
        }else if(projectCategoryCode == 'BuyBooks'){
            Common.Msg.Alert("图书购买");
        }else if(projectCategoryCode == 'ExpertAdvisor'){
            Common.Msg.Alert("专家顾问");
        }else if(projectCategoryCode == 'MediaCost'){
            Common.Msg.Alert("媒体劳务");
        }else if(projectCategoryCode == 'RepairMetering'){
            Common.Msg.Alert("维修计量");
        }else if(projectCategoryCode == 'VehicleMaintenance'){
            Common.Msg.Alert("车辆维修保养");
        }else if(projectCategoryCode == 'ExcessiveCost'){
            Common.Msg.Alert("超标费用");
        }else if(projectCategoryCode == 'DomesticTravel'){
            Common.Msg.Alert("国内差旅");
        }else if(projectCategoryCode == 'InternationalTravel'){
            Common.Msg.Alert("国际差旅");
        }else if(projectCategoryCode == 'MedicalStaffTraining'){
            Common.Msg.Alert("医务人员培训");
        }else if(projectCategoryCode == 'FunctionalTraining'){
            Common.Msg.Alert("职能人员培训");
        };
    },
    deleteFunc : function(btn){
        var $this = this;
        var selectedProjects = $this.getActivityGrid().getSelectionModel().getSelection();

        if (selectedProjects.length == 0) {
            Common.Msg.Alert("请选择" + btn.text + "的项目！");
            return false;
        }

        var records = [];
        for (var i = 0; i < selectedProjects.length; i++) {
            if (selectedProjects[i].data.status != 'Draft' && selectedProjects[i].data.status != '') {
                Common.Msg.Alert("只能删除草稿状态的数据,编号["+selectedProjects[i].data.code+"]的数据状态不是草稿，不能继续，请重新选择!");
                return false;
            }
            records.push(selectedProjects[i]);
        }

        if (records.length == 0) {
            Common.Msg.Error('选择的项目不能删除！');
            return false;
        }

        var callback = function() {
            $this.query();
        }

        Ext.create("Common.data.ApiReader").deleteFunc('project', records, callback);
    },
    isLoadAll: function () {
        var isLoadAll = false;
        var queryButton = Ext.getCmp('activity-operate-query');
        if (queryButton != null && queryButton != undefined) {
            isLoadAll = queryButton.isLoadAll();
        }
        return isLoadAll;
    },
    getParams: function () {
        return Ext.getCmp('project-search-queryButton').params;
    },
    query: function(){
        this.getActivityGrid().getStore().load();
    }
});