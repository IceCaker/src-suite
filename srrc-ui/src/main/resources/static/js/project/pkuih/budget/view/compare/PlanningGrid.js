Ext.define('Budget.view.compare.PlanningGrid', {
  extend: 'Ext.tree.Panel',
  alias: "widget.planningGrid",
  title: "非项目预算",
  rootVisible: false,
  autoScroll: true,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true,
    loadMask: false
  },
  editable: true,
  autoWidth: true,
  defaultNumberColumnWidth: 100,
  isProjectBudget: false,
  dataType: '@@Planning@@',
  isLoading: false,
  store: Ext.create('Ext.data.TreeStore', {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'code', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'showName', type: 'string'},
      {name: 'budgetType', type: 'string'},
      {name: 'isUpdate', type: 'boolean'},
      {name: 'lastYearAmount', type: 'double'},
      {name: 'lastYearAchieved', type: 'double'},
      {name: 'yearAmount', type: 'double'},
      {name: 'compareYearAmount', type: 'double'},
      {name: 'm01', type: 'double'},
      {name: 'c01', type: 'double'},
      {name: 'm02', type: 'double'},
      {name: 'c02', type: 'double'},
      {name: 'm03', type: 'double'},
      {name: 'c03', type: 'double'},
      {name: 'm04', type: 'double'},
      {name: 'c04', type: 'double'},
      {name: 'm05', type: 'double'},
      {name: 'c05', type: 'double'},
      {name: 'm06', type: 'double'},
      {name: 'c06', type: 'double'},
      {name: 'm07', type: 'double'},
      {name: 'c07', type: 'double'},
      {name: 'm08', type: 'double'},
      {name: 'c08', type: 'double'},
      {name: 'm09', type: 'double'},
      {name: 'c09', type: 'double'},
      {name: 'm10', type: 'double'},
      {name: 'c10', type: 'double'},
      {name: 'm11', type: 'double'},
      {name: 'c11', type: 'double'},
      {name: 'm12', type: 'double'},
      {name: 'c12', type: 'double'},
      {name: 'q1', type: 'double'},
      {name: 'cq1', type: 'double'},
      {name: 'q2', type: 'double'},
      {name: 'cq2', type: 'double'},
      {name: 'q3', type: 'double'},
      {name: 'cq3', type: 'double'},
      {name: 'q4', type: 'double'},
      {name: 'cq4', type: 'double'},
      {name: 'note', type: 'string'},
      {name: 'sortCode', type: 'string'},
      {name: 'leaf', type: 'boolean'}
    ],
    root: {
      expanded: true,
      children: []
    }
  }),
  columns: [],
  plugins: [],
  loadGrid: function () {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }
    var thisValue="";
    this.columns = [{
      text: "预算项",
      xtype: 'treecolumn',
      dataIndex: 'alias',
      align: 'left',
      minWidth: 240,
      flex: 1,
      menuDisabled: true,
      sortable: false
    }];

    if (false && !$this.isProjectBudget) {
      this.columns[this.columns.length] = {
        text: "去年预算",
        dataIndex: 'lastYearAmount',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        locked: true,
        renderer: function (value, meta, record) {
          return Ext.util.Format.number(value, '0,0.00');
        }
      };

      this.columns[this.columns.length] = {
        text: "去年完成",
        dataIndex: 'lastYearAchieved',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        locked: true,
        renderer: function (value, meta, record) {
          return Ext.util.Format.number(value, '0,0.00');
        }
      };
    }

    this.columns[this.columns.length] = {
      text: "预算金额",
      columns: [{
        text: "版本一",
        dataIndex: 'yearAmount',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        locked: true,
        renderer: function (value, meta, record) {
          thisValue = value;
          if ($this.editable && Common.CommonManager.editor.canSave
            && record.data.leaf && (budgetGroupViewType == ""
            || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "BudgetMonthlyAllocation")) {
            meta.tdCls = 'user-online';
          } else {
            if (value == null || value == '') {
              return '<center>-</center>';
            }
          }
          return Ext.util.Format.number(value, '0,0.00');
        }
      }, {
        text: '<span style="color:blue;">版本二</span>',
        dataIndex: 'compareYearAmount',
        width: $this.defaultNumberColumnWidth,
        hidden: false,
        sortable: false,
        align: 'right',
        locked: true,
        renderer: function (value, meta, record) {
          if (value == null || value == '') {
            if(thisValue == null || thisValue == '') {
              return '<center>-</center>';
            }
            if (value != thisValue) {
              return '<span style="color:red;">' + Ext.util.Format.number(0.00, '0,0.00') + '</span>';
            }
          } else {
            if (value != thisValue) {
              return '<span style="color:red;">' + Ext.util.Format.number(value, '0,0.00') + '</span>';
            }
          }
          return '<span style="color:blue">' + Ext.util.Format.number(value, '0,0.00') + '</span>';
        }
      }]
    };


    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      this.columns[this.columns.length] = {
        text: i + "月",
        columns: [{
          text: "版本一",
          dataIndex: 'm' + monthStr,
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            thisValue = value;
            if ($this.editable && Common.CommonManager.editor.canSave
              && record.data.leaf && budgetGroupViewType != "BudgetSummary"
              && budgetGroupViewType != "HumanResourcesSummary") {
              meta.tdCls = 'user-online';
            } else {
              if (value == null || value == '') {
                return '<center>-</center>';
              }
            }
            return Ext.util.Format.number(value, '0,0.00');
          }
        }, {
          text: '<span style="color:blue;">版本二</span>',
          dataIndex: 'c' + monthStr,
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            if (value == null || value == '') {
              if (value != thisValue) {
                return '<span style="color:red;">' + Ext.util.Format.number(0.00, '0,0.00') + '</span>';
              }
              return '<center>-</center>';
            } else {
              if (value != thisValue) {
                return '<span style="color:red;">' + Ext.util.Format.number(value, '0,0.00') + '</span>';
              }
            }
            return '<span style="color:blue">' + Ext.util.Format.number(value, '0,0.00') + '</span>';
          }
        }]
      };
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "HumanResourcesSummary" || budgetGroupViewType == "HumanResourcesMedical"
      || budgetGroupViewType == "HumanResourcesNursing" || budgetGroupViewType == "HumanResourcesTechnology"
      || budgetGroupViewType == "HumanResourcesFunction") {
      this.columns[this.columns.length] = {
        text: "一季度",
        columns: [{
          text: "版本一",
          dataIndex: 'q1',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.m01 != null && record.data.m01 != '')
              || (record.data.m02 != null && record.data.m02 != '')
              || (record.data.m03 != null && record.data.m03 != '')) {
              amountSum = 0.00;
              if (record.data.m01 != '') {
                amountSum += record.data.m01;
              }
              if (record.data.m02 != '') {
                amountSum += record.data.m02;
              }
              if (record.data.m03 != '') {
                amountSum += record.data.m03;
              }
            } else {
              thisValue = amountSum;
              if (value == null || value == '') {
                return '<center>-</center>';
              }
            }
            thisValue = amountSum;
            return Ext.util.Format.number(amountSum, '0,0.00');
          }
        }, {
          text: '<span style="color:blue;">版本二</span>',
          dataIndex: 'cq1',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.c01 != null && record.data.c01 != '')
              || (record.data.c02 != null && record.data.c02 != '')
              || (record.data.c03 != null && record.data.c03 != '')) {
              amountSum = 0.00;
              if (record.data.c01 != '') {
                amountSum += record.data.c01;
              }
              if (record.data.c02 != '') {
                amountSum += record.data.c02;
              }
              if (record.data.c03 != '') {
                amountSum += record.data.c03;
              }
            } else {
              if (value == null || value == '') {
                if (amountSum != thisValue && thisValue != null && thisValue != '') {
                  return '<span style="color:red;">' + Ext.util.Format.number(0.00, '0,0.00') + '</span>';
                }
                return '<center>-</center>';
              }
            }
            if (amountSum != thisValue) {
              return '<span style="color:red;">' + Ext.util.Format.number(amountSum, '0,0.00') + '</span>';
            }
            return '<span style="color:blue">' + Ext.util.Format.number(amountSum, '0,0.00') + '</span>';
          }
        }]
      };

      this.columns[this.columns.length] = {
        text: "二季度",
        columns: [{
          text: '版本一',
          dataIndex: 'q2',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.m04 != null && record.data.m04 != '')
              || (record.data.m05 != null && record.data.m05 != '')
              || (record.data.m06 != null && record.data.m06 != '')) {
              amountSum = 0.00;
              if (record.data.m04 != '') {
                amountSum += record.data.m04;
              }
              if (record.data.m05 != '') {
                amountSum += record.data.m05;
              }
              if (record.data.m06 != '') {
                amountSum += record.data.m06;
              }
            } else {
              thisValue = amountSum;
              if (value == null || value == '') {
                return '<center>-</center>';
              }
            }
            thisValue = amountSum;
            return Ext.util.Format.number(amountSum, '0,0.00');
          }
        }, {
          text: '<span style="color:blue;">版本二</span>',
          dataIndex: 'cq2',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.c04 != null && record.data.c04 != '')
              || (record.data.c05 != null && record.data.c05 != '')
              || (record.data.c06 != null && record.data.c06 != '')) {
              amountSum = 0.00;
              if (record.data.c04 != '') {
                amountSum += record.data.c04;
              }
              if (record.data.c05 != '') {
                amountSum += record.data.c05;
              }
              if (record.data.c06 != '') {
                amountSum += record.data.c06;
              }
            } else {
              if (value == null || value == '') {
                if (amountSum != thisValue && thisValue != null && thisValue != '') {
                  return '<span style="color:red;">' + Ext.util.Format.number(0.00, '0,0.00') + '</span>';
                }
                return '<center>-</center>';
              }
            }
            if (amountSum != thisValue) {
              return '<span style="color:red;">' + Ext.util.Format.number(amountSum, '0,0.00') + '</span>';
            }
            return '<span style="color:blue">' + Ext.util.Format.number(amountSum, '0,0.00')+ '</span>';
          }
        }]
      };

      this.columns[this.columns.length] = {
        text: "三季度",
        columns: [{
          text: "版本一",
          dataIndex: 'q3',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.m07 != null && record.data.m07 != '')
              || (record.data.m08 != null && record.data.m08 != '')
              || (record.data.m09 != null && record.data.m09 != '')) {
              amountSum = 0.00;
              if (record.data.m07 != '') {
                amountSum += record.data.m07;
              }
              if (record.data.m08 != '') {
                amountSum += record.data.m08;
              }
              if (record.data.m09 != '') {
                amountSum += record.data.m09;
              }
            } else {
              thisValue = amountSum;
              if (value == null || value == '') {
                return '<center>-</center>';
              }
            }
            thisValue = amountSum;
            return Ext.util.Format.number(amountSum, '0,0.00');
          }
        }, {
          text: '<span style="color:blue;">版本二</span>',
          dataIndex: 'cq3',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.c07 != null && record.data.c07 != '')
              || (record.data.c08 != null && record.data.c08 != '')
              || (record.data.c09 != null && record.data.c09 != '')) {
              amountSum = 0.00;
              if (record.data.c07 != '') {
                amountSum += record.data.c07;
              }
              if (record.data.c08 != '') {
                amountSum += record.data.c08;
              }
              if (record.data.c09 != '') {
                amountSum += record.data.c09;
              }
            } else {
              if (value == null || value == '') {
                if (amountSum != thisValue && thisValue != null && thisValue != '') {
                  return '<span style="color:red;">' + Ext.util.Format.number(0.00, '0,0.00') + '</span>';
                }
                return '<center>-</center>';
              }
            }
            if (amountSum != thisValue) {
              return '<span style="color:red;">' + Ext.util.Format.number(amountSum, '0,0.00') + '</span>';
            }
            return '<span style="color:blue">' + Ext.util.Format.number(amountSum, '0,0.00')+'</span>';
          }
        }]
      };

      this.columns[this.columns.length] = {
        text: "四季度",
        columns: [{
          text: '版本一',
          dataIndex: 'q4',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.m10 != null && record.data.m10 != '')
              || (record.data.m11 != null && record.data.m11 != '')
              || (record.data.m12 != null && record.data.m12 != '')) {
              amountSum = 0.00;
              if (record.data.m10 != '') {
                amountSum += record.data.m10;
              }
              if (record.data.m11 != '') {
                amountSum += record.data.m11;
              }
              if (record.data.m12 != '') {
                amountSum += record.data.m12;
              }
            } else {
              thisValue = amountSum;
              if (value == null || value == '') {
                return '<center>-</center>';
              }
            }
            thisValue = amountSum;
            return Ext.util.Format.number(amountSum, '0,0.00');
          }
        }, {
          text: '<span style="color:blue;">版本二</span>',
          dataIndex: 'cq4',
          width: $this.defaultNumberColumnWidth,
          hidden: false,
          sortable: false,
          align: 'right',
          renderer: function (value, meta, record) {
            var amountSum;
            if ((record.data.c10 != null && record.data.c10 != '')
              || (record.data.c11 != null && record.data.c11 != '')
              || (record.data.c12 != null && record.data.c12 != '')) {
              amountSum = 0.00;
              if (record.data.c10 != '') {
                amountSum += record.data.c10;
              }
              if (record.data.c11 != '') {
                amountSum += record.data.c11;
              }
              if (record.data.c12 != '') {
                amountSum += record.data.c12;
              }
            } else {
              if (value == null || value == '') {
                if (amountSum != thisValue && thisValue != null && thisValue != '') {
                  return '<span style="color:red;">' + Ext.util.Format.number(0.00, '0,0.00') + '</span>';
                }
                return '<center>-</center>';
              }
            }
            if (amountSum != thisValue) {
              return '<span style="color:red;">' + Ext.util.Format.number(amountSum, '0,0.00') + '</span>';
            }
            return '<span style="color:blue">' + Ext.util.Format.number(amountSum, '0,0.00')+ '</span>';
          }
        }]
      };
    }

    if (false) {
      this.columns[this.columns.length] = {
        text: "备注",
        dataIndex: 'note',
        width: 200,
        renderer: function (value, meta, record) {
          if ($this.editable && Common.CommonManager.editor.canSave) {
            meta.tdCls = 'user-online';
          }
          return "<font title='" + value + "'>" + value + "</font>";
        },
        sortable: false,
        align: 'left'
      };
    }

    this.reconfigure(this.store, this.columns);
  },
  initComponent: function () {
    var $this = this;

    var cellEditor = Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1,
      listeners: {
        beforeedit: function (editor, e) {
          var params = Common.CommonManager.getParams();
          var budgetGroupViewType = params.budgetGroupViewType;
          if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
            budgetGroupViewType = "";
          }

          if (!($this.editable && Common.CommonManager.editor.canSave && e.record.data.leaf)) {
            return false;
          }
          if (e.field == "yearAmount" || e.field == "compareYearAmount" && (budgetGroupViewType == "BudgetSummary"
            || budgetGroupViewType == "MonthlySummary" || budgetGroupViewType == "BudgetMonthlySummary"
            || budgetGroupViewType == "HumanResourcesSummary" || budgetGroupViewType == "HumanResourcesMedical"
            || budgetGroupViewType == "HumanResourcesNursing" || budgetGroupViewType == "HumanResourcesTechnology"
            || budgetGroupViewType == "HumanResourcesFunction")) {
            return false;
          }
          if (budgetGroupViewType == "BudgetSummary" || budgetGroupViewType == "HumanResourcesSummary") {
            return false;
          }
        }
      }
    });
    $this.plugins = [cellEditor];

    //加入自定义事件
    this.addEvents({
      "itemNameClick": true,
      "amountClick": true
    });
    this.callParent(arguments);
  },
  loadData: function () {
    var $this = this;
    if ($this.isLoading) {
      return false;
    } else {
      $this.isLoading = true;
    }

    Common.CommonManager.showMask("数据加载中，请稍候...");

    var dataType = $this.dataType;
    var params = Common.CommonManager.getParams();

    //加载Grid
    $this.loadGrid();

    if(params.budgetGroupCode!="Z00") {
      Ext.getCmp("version-compare-exportButton").hide();
    }else {
      Ext.getCmp("version-compare-exportButton").show();
    }

    var dynamicQuery = {
      queryType: "read",
      type: "budgetItemGroupItem",
      fields: {
        domainId: "domainId",
        alias: "alias",
        sortCode: "sortCode",
        budgetItemGroupId: "budgetItemGroupId"
      },
      tree: true,
      joins: [
        {
          type: "BudgetItem",
          fields: {
            id: "id",
            code: "code",
            name: "name",
            budgetType: "budgetType",
            leaf: "leaf",
            parentId: "parentId"
          },
          conditions: [
            {
              field: "id",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }
      ],
      associations: [],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: params.budgetGroupId
        }
      ]
    };

    //当选中一条时，和默认版本对比
    var versionIdCount = params.versionIds.length;
    if (versionIdCount == 1) {
      dynamicQuery.associations.push({
        type: "budgetItemGroupItem",
        fields: {
          alias: "alias",
          sortCode: "sortCode",
          budgetItemGroupId: "budgetItemGroupId",
          budgetItemId: "budgetItemId"
        },
        references: {
          budgetItemId: "id"
        },
        joins: [
          {
            type: "annualCostPlanningData",
            fields: {
              amount: "yearAmount"
            },
            conditions: [
              {
                field: "setOfBooksId",
                condition: "EQUALS",
                type: "Integer",
                value: params.setOfBooksId
              },
              {
                field: "period",
                condition: "EQUALS",
                type: "String",
                value: params.period
              },
              {
                field: "organizationId",
                condition: "EQUALS",
                type: "Integer",
                value: params.organizationId
              },
              {
                field: "dataType",
                condition: "STARTS_WITH",
                type: "String",
                value: dataType
              },
              {
                field: "budgetItemGroupId",
                condition: "EQUALS",
                type: "Field",
                value: "budgetItemGroupId"
              },
              {
                field: "budgetObjectType",
                condition: "EQUALS",
                type: "String",
                value: "BudgetItem"
              },
              {
                field: "budgetObjectId",
                condition: "EQUALS",
                type: "Field",
                value: "budgetItemId"
              }
            ]
          }
        ],
        conditions: [
          {
            field: "budgetItemGroupId",
            condition: "EQUALS",
            type: "Integer",
            value: params.budgetGroupId
          }
        ]
      });
    } else {
      dynamicQuery.associations.push({
        type: "budgetItemGroupItem",
        fields: {
          alias: "alias",
          sortCode: "sortCode",
          budgetItemGroupId: "budgetItemGroupId",
          budgetItemId: "budgetItemId"
        },
        references: {
          budgetItemId: "id"
        },
        joins: [
          {
            type: "annualCostPlanningDataVersion",
            fields: {
              amount: "yearAmount",
              versionId: "versionId"
            },
            conditions: [
              {
                field: "setOfBooksId",
                condition: "EQUALS",
                type: "Integer",
                value: params.setOfBooksId
              },
              {
                field: "period",
                condition: "EQUALS",
                type: "String",
                value: params.period
              },
              {
                field: "organizationId",
                condition: "EQUALS",
                type: "Integer",
                value: params.organizationId
              },
              {
                field: "dataType",
                condition: "STARTS_WITH",
                type: "String",
                value: dataType
              },
              {
                field: "budgetItemGroupId",
                condition: "EQUALS",
                type: "Field",
                value: "budgetItemGroupId"
              },
              {
                field: "budgetObjectType",
                condition: "EQUALS",
                type: "String",
                value: "BudgetItem"
              },
              {
                field: "budgetObjectId",
                condition: "EQUALS",
                type: "Field",
                value: "budgetItemId"
              }, {
                field: "versionId",
                condition: "EQUALS",
                value: params.versionIds[0]
              }
            ]
          }
        ],
        conditions: [
          {
            field: "budgetItemGroupId",
            condition: "EQUALS",
            type: "Integer",
            value: params.budgetGroupId
          }
        ]
      });
    }

    dynamicQuery.associations.push({
      type: "budgetItemGroupItem",
      references: {
        budgetItemId: "id"
      },
      joins: [
        {
          type: "annualCostPlanningDataVersion",
          fields: {
            amount: "compareYearAmount",
            versionId: "versionId"
          },
          conditions: [
            {
              field: "setOfBooksId",
              condition: "EQUALS",
              type: "Integer",
              value: params.setOfBooksId
            },
            {
              field: "period",
              condition: "EQUALS",
              type: "String",
              value: params.period
            },
            {
              field: "organizationId",
              condition: "EQUALS",
              type: "Integer",
              value: params.organizationId
            },
            {
              field: "dataType",
              condition: "STARTS_WITH",
              type: "String",
              value: dataType
            },
            {
              field: "budgetItemGroupId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemGroupId"
            },
            {
              field: "budgetObjectType",
              condition: "EQUALS",
              type: "String",
              value: "BudgetItem"
            },
            {
              field: "budgetObjectId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }, {
              field: "versionId",
              condition: "EQUALS",
              value: params.versionIds[versionIdCount - 1]
            }
          ]
        }
      ],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: params.budgetGroupId
        }
      ]
    });


    //查询月度数据
    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      if (params.versionIds.length == 1) {
        dynamicQuery.associations[0].joins.push({
          type: "monthlyCostPlanningData",
          fields: {
            amount: 'm' + monthStr
          },
          conditions: [
            {
              field: "setOfBooksId",
              condition: "EQUALS",
              type: "Integer",
              value: params.setOfBooksId
            },
            {
              field: "period",
              condition: "EQUALS",
              type: "String",
              value: params.period.substring(0, 4) + monthStr
            },
            {
              field: "organizationId",
              condition: "EQUALS",
              type: "Integer",
              value: params.organizationId
            },
            {
              field: "dataType",
              condition: "STARTS_WITH",
              type: "String",
              value: dataType
            },
            {
              field: "budgetItemGroupId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemGroupId"
            },
            {
              field: "budgetObjectType",
              condition: "EQUALS",
              type: "String",
              value: "BudgetItem"
            },
            {
              field: "budgetObjectId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        });
      } else {
        dynamicQuery.associations[0].joins.push({
          type: "monthlyCostPlanningDataVersion",
          fields: {
            amount: 'm' + monthStr
          },
          conditions: [
            {
              field: "setOfBooksId",
              condition: "EQUALS",
              type: "Integer",
              value: params.setOfBooksId
            },
            {
              field: "period",
              condition: "EQUALS",
              type: "String",
              value: params.period.substring(0, 4) + monthStr
            },
            {
              field: "organizationId",
              condition: "EQUALS",
              type: "Integer",
              value: params.organizationId
            },
            {
              field: "dataType",
              condition: "STARTS_WITH",
              type: "String",
              value: dataType
            },
            {
              field: "budgetItemGroupId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemGroupId"
            },
            {
              field: "budgetObjectType",
              condition: "EQUALS",
              type: "String",
              value: "BudgetItem"
            },
            {
              field: "budgetObjectId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            },
            {
              field: "versionId",
              condition: "EQUALS",
              value: params.versionIds[0]
            }
          ]
        });
      }

      dynamicQuery.associations[1].joins.push({
        type: "monthlyCostPlanningDataVersion",
        fields: {
          amount: 'c' + monthStr
        },
        conditions: [
          {
            field: "setOfBooksId",
            condition: "EQUALS",
            type: "Integer",
            value: params.setOfBooksId
          },
          {
            field: "period",
            condition: "EQUALS",
            type: "String",
            value: params.period.substring(0, 4) + monthStr
          },
          {
            field: "organizationId",
            condition: "EQUALS",
            type: "Integer",
            value: params.organizationId
          },
          {
            field: "dataType",
            condition: "STARTS_WITH",
            type: "String",
            value: dataType
          },
          {
            field: "budgetItemGroupId",
            condition: "EQUALS",
            type: "Field",
            value: "budgetItemGroupId"
          },
          {
            field: "budgetObjectType",
            condition: "EQUALS",
            type: "String",
            value: "BudgetItem"
          },
          {
            field: "budgetObjectId",
            condition: "EQUALS",
            type: "Field",
            value: "budgetItemId"
          },
          {
            field: "versionId",
            condition: "EQUALS",
            value: params.versionIds[versionIdCount - 1]
          }
        ]
      });
    }

    //加载数据
    Ext.Ajax.request({
      url: contextPath + '/api/2.0.0/cql',
      method: 'POST',
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      params: Ext.encode(dynamicQuery),
      paramsAsJson: true,
      success: function (response, operation) {
        var responseJson = Ext.JSON.decode(response.responseText);
        $this.setRootNode({
          text: '根节点',
          expanded: true,
          children: responseJson.children
        });

        $this.isLoading = false;
        Common.CommonManager.hideMask();
      }
    });
  },
  statics: {
    addProjectTab: function (budgetItemId, budgetItemName) {
      Ext.ComponentQuery.query('planningGrid')[0].fireEvent("itemNameClick", budgetItemId, budgetItemName);
    },
    view: function (budgetItemId) {
      Ext.ComponentQuery.query('planningGrid')[0].fireEvent("amountClick", budgetItemId);
    }
  }
});