Ext.define('Budget.view.grid.BudgetGroupGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.budgetGroupGrid',
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  autoWidth: true,
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  loadAfterRender: false,
  store: Ext.create('Common.data.ApiGridStore', {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'code', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'budgetType', type: 'string'},
      {name: 'resourceId', type: 'string'},
      {name: 'viewType', type: 'string'}
    ],
    api: "budgetItemGroup",
    joins: [{
      type: 'budgetItemGroupAssignment',
      fields: {
        businessType: 'assignBusinessType',
        period: 'assignPeriod',
        organizationId: 'assignOrganizationId'
      },
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'Field',
        value: 'domainId'
      }, {
        field: 'budgetItemGroupId',
        condition: 'EQUALS',
        type: 'Field',
        value: 'id'
      }]
    }, {
      type: 'resourceExtInstance',
      fields: {
        instanceValue: 'viewType'
      },
      conditions: [{
        field: 'resourceExtOptionId',
        condition: 'EQUALS',
        type: 'String',
        value: 'view-type'
      }, {
        field: 'resourceExtSchemaId',
        condition: 'EQUALS',
        type: 'String',
        value: 'budget-item-group-pkuih-configuration'
      }, {
        field: 'resourceId',
        condition: 'EQUALS',
        type: 'Field',
        value: 'resourceId'
      }]
    }],
    conditions: [{
      field: 'domainId',
      condition: 'EQUALS',
      value: '#domainId#'
    }, {
      field: 'setOfBooksId',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#setOfBooksId#'
    }, {
      field: 'assignBusinessType',
      condition: 'EQUALS',
      value: '#businessType#'
    }, {
      field: 'assignPeriod',
      condition: 'EQUALS',
      value: '#period#'
    }, {
      field: 'assignOrganizationId',
      condition: 'EQUALS',
      value: '#organizationId#'
    }],
    sorts: [{
      field: 'sortCode',
      direction: 'ASC'
    }],
    page: 1,
    size: 1000000
  }),
  columns: [{
    text: '预算模版', dataIndex: 'name', align: 'left', flex: 1, sortable: false,
    renderer: function(value, meta, record) {
      meta.style = 'white-space: normal; overflow: visible; word-break: break-all;';
      return value;
    }
  }],
  initComponent: function() {
    var $this = this;

    $this.getStore().on('load', function(store) {
      $this.getSelectionModel().deselectAll();
      if (store.getTotalCount() > 0) {
        $this.getSelectionModel().select(store.getAt(0), false);
      }
    });

    if ($this.loadAfterRender) {
      $this.loadData();
    }

    this.callParent(arguments);
  },
  subscription: {
    observers: ['planning-search-queryButton'],
    operate: {
      onNext: function(params) {
        this.loadData();
      }
    }
  },
  loadData: function() {
    var $this = this;
    var params = Common.CommonManager.getParams();

    var callback = function() {
      $this.getStore().load({
        params: {
          businessType: params.businessType,
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: params.period,
          organizationId: params.organizationId
        }
      });
    };

    //加载部门状态
    Common.CommonManager.loadStatus(callback);
  }
});