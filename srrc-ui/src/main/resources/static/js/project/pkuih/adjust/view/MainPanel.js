Ext.define("Adjust.view.MainPanel", {
  extend: "Ext.panel.Panel",
  alias: "widget.adjustMainPanel",
  layout: 'border',
  border: false,
  bodyStyle: {
    background: "#FFFFFF"
  },
  bodyPadding: 5,
  items: []
});
