Ext.define("Activity.view.SearchToolbar", {
    extend: "Ext.toolbar.Toolbar",
    alias: "widget.activitySearchToolbar",
    margin: '0 0 5 0',
    items: [{
        id: "activity-search-period",
        xtype: "periodCombo",
        width: 120,
        emptyText: '请选择',
        observable: true
    }, {
        id: "activity-search-nameCode",
        xtype: 'textfield',
        name: 'nameCode',
        fieldLabel: '名称/编号',
        width: 220,
        labelWidth: 60,
        selectOnFocus: true,
        hideTrigger: true,
        keyNavEnabled: false,
        mouseWheelEnabled: false,
        fieldStyle: 'text-align: right',
        observable: true
    }, {
        id: "activity-search-organization",
        xtype: "orgCombo",
        name: "organizationId",
        emptyText: '请选择',
        width: 200,
        initParams: {
            domainId: domainId,
            leaf: 1
        },
        multiSelect: true,
        selectType: "window",
        windowSelectValue: true,
        loadAfterRender: true,
        observable: true
    }, {
        id: 'activity-search-manager',
        xtype: 'textfield',
        name: 'managerName',
        fieldLabel: '经办人',
        width: 220,
        labelWidth: 50,
        selectOnFocus: true,
        hideTrigger: true,
        keyNavEnabled: false,
        mouseWheelEnabled: false,
        fieldStyle: 'text-align: right',
        observable: true
    },{
        id: "activity-search-status",
        name : 'status',
        xtype: "commonCombo",
        emptyText: '请选择',
        width: 180,
        labelWidth: 40,
        editable: false,
        name: 'status',
        fieldLabel: '状态',
        value : '',
        store: Ext.create('Ext.data.Store', {
            fields: ['code', 'name'],
            data: {
                'data': [
                    {code: '', name: '全部'},
                    {code: 'Draft', name: '草稿'},
                    {code: 'DraftApproval', name: '草稿审批中'},
                    {code: 'Approved', name: '已完成'},
                    {code: 'Adjusting', name: '调整中'},
                    {code: 'AdjustApproval', name: '调整审批中'},
                    {code: 'Closed', name: '已结束'},
                    {code: 'CloseApproval', name: '结束审批中'}
                ]
            },
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            }
        }),
        queryMode: 'local',
        displayField: 'name',
        valueField: 'code',
        observable: true
    },{
        id: 'activity-search-query', text: '查询', xtype: "button",  observable: true,
        paramsProperties: ["period","nameCode", "organizationId", "managerName"],
        params: {},
        pushParam: function (param) {
            Ext.apply(this.params, param);
        },
        isLoadAll: function () {
            for (var i = 0; i < this.paramsProperties.length; i++) {
                if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
                    return false;
                }
            }
            return true;
        },
        getParams: function () {
            return this.params;
        },
        subscription: {
            observers: [
                'activity-search-period',
                'activity-search-nameCode',
                'activity-search-organization',
                'activity-search-manager',
                'activity-search-status'
            ],
            operate: {
                onNext: function (param) {
                    this.pushParam(param);
                    if (this.isLoadAll()) {
                        this.publish(this.getParams());
                    }
                }
            }
        },
        handler: function () {
            this.onNext({});
        }
    }]
});
