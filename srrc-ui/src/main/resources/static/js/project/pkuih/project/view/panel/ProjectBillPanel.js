Ext.define('Project.view.panel.ProjectBillPanel', {
  extend: 'Ext.grid.Panel',
  alias: "widget.projectBillPanel",
  title: '占用明细',
  columns:[
    { text: "申请单号", dataIndex: 'invoiceCode', width: 112, sortable: false, align: 'center' },
    { text: "子单号", dataIndex: 'lineNum', width: 60, sortable: false, align: 'center' },
    { text: "申请部门", dataIndex: 'usingOrganizationName', width: 140, sortable: false, align: 'left' },
    { text: "预算部门", dataIndex: 'budgetOrganizationName', width: 140, sortable: false, align: 'left' },
    { text: "预算指标", dataIndex: 'itemName', width: 140, sortable: false, align: 'left' },
    { text: "关联预算事项", dataIndex: 'projectItemName', width: 140, sortable: false, align: 'left' },
    { text: "金额", dataIndex: 'amount', width: 85, sortable: false, align: 'right',
      renderer: Ext.util.Format.numberRenderer('0,0.00')},
    { text: "申请人", dataIndex: 'creatorName', width: 65, sortable: false, align: 'center'},
    { text: "申请日期", dataIndex: 'createTime', width: 140, sortable: false, align: 'center'}
  ],
  initComponent: function(){
    var $this = this;
    Ext.define('Project.model.ProjectBillModel', {
      extend: 'Ext.data.Model',
      fields: [
        {name: 'projectItemId', type: 'long'},
        {name: 'projectItemCode', type: 'long'},
        {name: 'projectItemName', type: 'long'},
        {name: 'invoiceCode', type: 'string'},
        {name: 'invoiceLineCode', type: 'string'},
        {name: 'lineNum', type: 'string'},
        {name: 'itemName', type: 'string'},
        {name: 'amount', type: 'double'},
        {name: 'creatorName', type: 'string'},
        {name: 'usingOrganizationId', type: 'int'},
        {name: 'usingOrganizationName', type: 'string'},
        {name: 'budgetOrganizationId', type: 'int'},
        {name: 'budgetOrganizationName', type: 'string'},
        {name: 'createTime', type: 'string'}
      ]
    });
    this.store = Ext.create('Ext.data.Store', {
      model: 'Project.model.ProjectBillModel',
      proxy: {
        type: 'ajax',
        method: "GET", url: "",
        reader: {
          type: 'json',
          root: 'data',
          totalProperty: 'totalElements'
        }
      },
      listeners: {
        'beforeload': function (obj, operation, eOpts) {
          obj.getProxy().url = contextPath + '/bproject/query/bill/' + $this.projectId;
        },
        'load': function (store) {
          Common.CommonManager.hideMask();
        }
      }
    });
    this.bbar = Ext.create('Ext.PagingToolbar', {
      store: this.store,
      displayInfo: true,
      displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
      emptyMsg: "没有数据"
    });
    this.callParent(arguments);
  },
  getProjectId: function(){
    return this.up("projectInfoPanel").projectId;
  },
  isReadOnly: function(){
    return this.up("projectInfoPanel").readOnly;
  },
  loadData: function(){
    var projectId = this.getProjectId();
    this.projectId = projectId;

    this.getStore().load();
  }
});