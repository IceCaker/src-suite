Ext.define('Divide.controller.DivideController', {
  extend: 'Ext.app.Controller',
  views: ['MainPanel', 'SearchToolbar', 'BudgetType', 'OperateToolbar', 'PlanningGrid', 'ProjectGrid'],
  refs: [
    {ref: "divideMainPanel", selector: 'divideMainPanel'}
  ],
  init: function() {
    var $this = this;
    this.control({
      ' divideMainPanel': {
        'render': function() {
          $this.addPanelFunc();
        }
      },
      'divideOperateToolbar button[action=divide]': {click: this.divide}
    });
    Common.CommonManager.getParams = this.getParams;
  },
  addPanelFunc: function() {
    var $this = this;

    $this.getDivideMainPanel().add({
      id: 'divide-search-toolbar',
      xtype: 'divideSearchToolbar',
      region: 'north'
    });

    $this.getDivideMainPanel().add({
      id: 'divide-operate-toolbar',
      xtype: 'divideOperateToolbar',
      region: 'north'
    });

    $this.getDivideMainPanel().add({
      id: 'devide-tabPanel',
      xtype: 'tabpanel',
      activeTab: 0,
      items: [{
        title: '非项目预算',
        xtype: 'planningGrid',
        itemId: 'planningGrid'
      }, {
        title: '项目预算',
        xtype: 'projectGrid',
        itemId: 'project'
      }],
      listeners: {
        tabchange: function(tabPanel, newCard, oldCard, eOpts) {
          newCard.loadData();
        }
      },
      subscription: {
        observers: ['divide-search-queryButton'],
        operate: {
          onNext: function(params) {
            Ext.getCmp('devide-tabPanel').getActiveTab().loadData();
          }
        }
      },
      region: 'center'
    });
  },

  divide: function() {
    Common.CommonManager.showSpecialMask(function () {
      Ext.getCmp('devide-tabPanel').getActiveTab().divide();
    },"预算划分中，请稍候...");
  },

  getParams: function() {
    return Divide.controller.DivideController.getParams();
  },
  statics: {
    getParams: function() {
      var params = {};
      for (var str in pageInitParam) {
        params[str] = pageInitParam[str];
      }

      var queryButton = Ext.getCmp('divide-search-queryButton');
      if (queryButton != null && queryButton != undefined) {
        params.setOfBooksId = queryButton.getParams().setOfBooksId;
        params.period = queryButton.getParams().period;
        params.organizationId = queryButton.getParams().organizationId;
        params.organizationCode = queryButton.getParams().organizationCode;
        params.organizationSortCode = queryButton.getParams().organizationSortCode;
        params.organizationLeaf = queryButton.getParams().organizationLeaf;
      }

      return params;
    }
  }
});