Ext.define('Regulate.view.grid.RegulateItemGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.regulateItemGrid',
  minHeight: 230,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  border: false,
  readOnly: true,
  regulateType: '',
  features: [{
    ftype: 'groupingsummary',
    // groupHeaderTpl: "<a onclick=\"Budget.view.grid.ProjectGrid.edit('{[values.rows[0].data[\"projectId\"]]}')\">{[values.rows[0].data['projectName']]}[{[values.rows[0].data['projectCode']]}]</a>",
    hideGroupedHeader: true,
    enableGroupingMenu: false
  }],
  listeners: {
    groupclick: function (view, node, group, e, eOpts) {
      view.features[0].expand(group);
    }
  },
  columns: [
    {
      text: '序号',
      dataIndex: 'number',
      style: 'text-align:center;',
      align: 'center',
      width: 140,
      menuDisabled: true,
      sortable: false,
      summaryRenderer: function (value, summaryData, dataIndex) {
        return '预计调整值合计：';
      }
    },
    {
      text: '支出科目',
      dataIndex: 'budgetItem',
      style: 'text-align:center;',
      align: 'left',
      flex: 1,
      minWidth: 170,
      menuDisabled: true,
      sortable: false
    },
    {
      text: '项目名称',
      dataIndex: 'projectName',
      style: 'text-align:center;',
      align: 'left',
      width: 200,
      menuDisabled: true,
      sortable: false,
      renderer: function (value, meta, record) {
        return '<a href="#" onclick="Pkuih.Common.view.ProjectItemGrid.edit('
          + record.data.id + ',\'' + this.id + '\');"><font title="' + value + '">' + value + '</font></a>';
      }
    },
    {
      text: '调整前预算', dataIndex: 'budgetAmount', align: 'left', width: 200, menuDisabled: true, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    },
    {
      text: '调整值', dataIndex: 'adjustAmount', align: 'left', width: 200, menuDisabled: true, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    },
    {
      text: '调整后预算', align: 'adjustAfterAmount', align: 'left', width: 200, menuDisabled: true, sortable: false,
      summaryType: 'sum',
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    }
  ],
  dockedItems: [{
    xtype: 'toolbar',
    style: 'background-color: white;',
    padding: 6,
    border: true,
    items: [{
      id: 'project-item-grid-add',
      action: 'add',
      xtype: 'button',
      text: '添加',
      handler: function () {
        this.up('grid').showProjectItemWindow();
      }
    }, {
      id: 'project-item-grid-delete',
      action: 'delete',
      xtype: 'button',
      text: '删除'
    }]
  }],
  initComponent: function () {

    this.store = Ext.create('Common.data.ApiDirectGridStore', {
      fields: [
        {name: 'id', type: 'int'},
        {name: 'code', type: 'string'},
        {name: 'budgetItemName', type: 'string'},
        {name: 'projectName', type: 'string'},
        {name: 'budgetItemId', type: 'int'},
        {name: 'budgetAmount', type: 'double'},
        {name: 'adjustAmount', type: 'double'},
      ],
      api: "budgetRegulateDetail",
      joins: [
        {
          type: 'budgetItem',
          fields: {
            name: 'budgetItemName'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'budgetItemId'
            }
          ]
        }
      ],
      sorts: [
        {
          field: 'createTime',
          direction: 'DESC'
        }
      ]
    });
    this.loadData();
    this.callParent(arguments);
  },
  loadData: function () {
    this.getStore().conditions = [
      {
        field: 'budgetRegulateId',
        condition: 'EQUALS',
        type: 'Long',
        value: '#budgetRegulateId#'
      }
    ];
    this.getStore().loadData({
      params: {
        budgetRegulateId: 7
      }
    });
  },
  loadGrid: function (projectId, versionId) {
  },
  showProjectItemWindow: function () {
    Ext.widget('regulateDetailWindow').showAndRevertFunc(this.regulateType);
  }
});