Ext.define('Project.controller.ProjectInfoController', {
  extend: 'Ext.app.Controller',
  views: [
    'panel.ProjectInfoPanel', 'panel.ProjectBasePanel', 'panel.ProjectFormPanel', 'grid.ProjectFileGrid',
    'panel.ProjectProcessPanel', 'panel.ProjectBillPanel', 'panel.ProjectVersionPanel',
    'combo.ProjectCreateOrganization', 'combo.ProjectDutyOrganization', 'combo.ExpenseItem', 'window.ProjectVersionWindow',
    'toolbar.ProjectManageToolbar', 'toolbar.SearchToolbar', 'window.FileUploadWindow', 'window.ProjectPrintWindow'
  ],
  refs: [
    {ref: 'projectGrid', selector: 'projectGrid'},
    {ref: 'projectSearchToolbar', selector: 'projectSearchToolbar'},
    {ref: 'manageToolbar', selector: 'manageToolbar'},
    {ref: 'projectInfoPanel', selector: 'projectInfoPanel'},
    {ref: 'projectFormPanel', selector: 'projectFormPanel'},
    {ref: 'projectBasePanel', selector: 'projectBasePanel'},
    {ref: 'projectVersionWindow', selector: 'projectVersionWindow'},
    {ref: 'projectItemGrid', selector: 'projectItemGrid'},
    {ref: 'projectFileGrid', selector: 'projectFileGrid'}
  ],
  init: function () {
    var $this = this;
    this.control({
      'projectInfoPanel': {tabchange: this.tabchange},
      'projectItemGrid button[action=add]': {
        click: function () {
          $this.addProjectItem();
        }
      },
      'projectFileGrid button[action=uploadFile]': {
        click: function () {
          $this.uploadFile();
        }
      },
      'projectFileGrid button[action=deleteFile]': {
        click: function () {
          $this.deleteFile();
        }
      },
      'projectPrintWindow button[action=showPrintView]': {
        click: function () {
          $this.showPrintView();
        }
      },
      'projectVersionPanel': {showVersion: $this.showVersion}
    })
  },
  tabchange: function (tabPanel, newCard, oldCard, eOpts) {
    var $this = this;
    if (oldCard != null && oldCard.xtype == 'projectBasePanel' && !tabPanel.readOnly) {
      var isSuccess = $this.getProjectBasePanel().saveProject(false);
      if (!isSuccess) {
        tabPanel.setActiveTab(oldCard);
      } else {
        newCard.loadData();
        tabPanel.setActiveTab(newCard);
      }
    } else {
      newCard.loadData();
    }
  },
  addProjectItem: function () {
    var $this = this;
    var projectId = $this.getProjectInfoPanel().projectId;
    var isSaveSuccess = true;
    if (projectId == -1) {
      isSaveSuccess = $this.getProjectBasePanel().saveProject(false);
      projectId = $this.getProjectInfoPanel().projectId;
    }

    if (isSaveSuccess && projectId > 0) {
      this.getProjectItemGrid().loadGrid(projectId);
    }
  },
  uploadFile: function () {
    var $this = this;
    var resourceId = $this.getProjectInfoPanel().resourceId;
    var isSaveSuccess = true;
    if (resourceId == '') {
      isSaveSuccess = $this.getProjectBasePanel().saveProject(false);
      resourceId = $this.getProjectInfoPanel().resourceId;
    }

    if (isSaveSuccess && resourceId != '') {
      Ext.widget('fileUploadWindow').showAndRevertFunc(
        function () {
          return $this.getProjectInfoPanel().resourceId;
        },
        function () {
          $this.getProjectInfoPanel().down('projectFileGrid').loadData();
        }
      )
    }
  },
  deleteFile: function () {
    var $this = this;
    var ids = [];

    var selectedDocuments = Ext.ComponentQuery.query('projectFileGrid')[0].getSelectionModel().getSelection();

    if (selectedDocuments.length == 0) {
      Common.Msg.Error('请选择要删除的文件！');
      return false;
    }

    for (var i = 0; i < selectedDocuments.length; i++) {
      ids.push(selectedDocuments[i].data.id);
    }

    var fn = function () {
      Common.CommonManager.showMask('正在删除文件...');

      Ext.Ajax.request({
        url: contextPath + '/foundation/service/document',
        method: 'DELETE',
        params: {ids: ids.join()},
        success: function (response) {
          Ext.ComponentQuery.query('projectFileGrid')[0].loadData();
          Common.CommonManager.hideMask();
        },
        failure: function (response) {
          Common.CommonManager.hideMask();
          var result = Ext.decode(response);
          Common.Msg.Error(result.message);
        }
      })
    };
    Common.Msg.Confirm('请确认是否进行文件删除?', '附件管理', fn);
  },
  showPrintWindow: function () {
    var $this = this;
    if ($this.projectPrintWindow == null) {
      $this.projectPrintWindow = Ext.widget('projectPrintWindow');
    }
    $this.projectPrintWindow.show();
  },
  showPrintView: function () {
    var projectId = this.projectInfoPanel.projectId;
    var optionValue = this.projectInfoPanel.getOptionValue();
    if (optionValue == '') {
      Common.Msg.Error('请选择要打印的选项！');
      return false;
    }
    openBrowserWindow({
      isFull: true,
      url: contextPath + '/bproject/print/' + projectId + '?optionValue=' + optionValue
    });
    this.projectPrintWindow.close();
  },
  showVersion: function (projectId, versionId) {
    Ext.widget('projectVersionWindow').showAndRevertFunc(projectId, versionId);
  },
  getParams: function () {
    return this.getProjectSearchToolbar().getValues();
  }
});
