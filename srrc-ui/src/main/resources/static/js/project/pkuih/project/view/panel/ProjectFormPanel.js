Ext.require([
  "Common.text.CommonAmount"
]);
Ext.define('Project.view.panel.ProjectFormPanel', {
  extend: 'Ext.form.Panel',
  alias: "widget.projectFormPanel",
  layout: "column",
  border: false,
  autoWidth: true,
  editable: true,
  defaults: {
    columnWidth: 3.3 / 10,
    margin: "5 2 5 2"
  },
  items: [{
    xtype: "hiddenfield",
    name: "id"
  }, {
    xtype: "textfield",
    name: "code",
    fieldLabel: "项目编码",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    emptyText: '系统自动生成',
    readOnly: true
  }, {
    xtype: "textfield",
    name: "name",
    fieldLabel: "<font color=red>*</font> 项目名称",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    columnWidth: 6.6 / 10,
    allowBlank: false,
    blankText: "请输入项目名称",
    editable: true,
    readOnly: true
  }, {
    xtype: "projectDutyOrganization",
    name: "organizationId",
    fieldLabel: "<font color=red>*</font> 主管部门",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    allowBlank: false,
    blankText: "请选择主管部门",
    editable: true,
    readOnly: true
  }, {
    xtype: "periodCombo",
    name: "period",
    fieldLabel: "<font color=red>*</font> 所属年份",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    allowBlank: false,
    blankText: "请选择所属年份",
    editable: true,
    readOnly: true
  }, {
    xtype: 'apiCombox',
    width: 180,
    labelWidth: 65,
    selectType: "combo",
    loadAfterRender: true,
    autoSelect: true,
    readCookie: true,
    fieldLabel: "<font color=red>*</font> 项目类型",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    allowBlank: false,
    blankText: "请选择项目类型",
    editable: true,
    name: 'projectCategoryId',
    api: 'ProjectCategory',
    fields: ['id', 'code', 'name'],
    sorts: [{
      field: 'sortCode',
      direction: 'ASC'
    }],
    queryMode: 'local',
    displayField: 'name',
    observable: true,
    readOnly: true
  }, {
    xtype: "projectManagerCombo",
    name: "managerId",
    fieldLabel: "<font color=red>*</font> 负责人",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    allowBlank: false,
    blankText: "请选择负责人",
    editable: true,
    readOnly: true
  }, {
    xtype: "commonAmount",
    name: "amount",
    fieldLabel: "项目预算",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    columnWidth: 6.6 / 10,
    readOnly: true,
    editable: true
  }, {
    xtype: "textfield",
    name: "completePercent",
    fieldLabel: "执行进度",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    hidden: true,
    value: '0.00%',
    readOnly: true
  }, {
    xtype: "textareafield",
    name: "note",
    fieldLabel: "内容说明",
    labelSeparator: '',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 80,
    columnWidth: 9.9 / 10,
    height: 75,
    emptyText: '备注说明项目事由与内容',
    editable: true,
    readOnly: true
  }],
  dockedItems: [
    {
      xtype: 'toolbar',
      style: 'background-color: white;',
      padding: 6,
      border: false,
      items: [
        {
          id: 'project-save', xtype: 'button', text: "保存", action: "save", handler: function () {
          this.up("projectBasePanel").saveProject();
        }
        },
        {
          id: 'project-reset', xtype: 'button', text: "重置", action: "reset", handler: function () {
          this.up("projectBasePanel").loadProject();
        }
        }
      ]
    }
  ],
  loadProjectVersion: function (projectId, versionId) {
    var $this = this;
    $this.getDockedItems()[0].hide();
    var dynamicQuery = {
      api: 'projectVersion',
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        value: projectId
      }, {
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      }]
    };

    var projects = Ext.create("Common.data.ApiReader").load(dynamicQuery);

    if (projects != null && projects[0] != null) {
      $this.getForm().setValues(projects[0]);
    }
  }
});