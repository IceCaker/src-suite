Ext.define("Regulate.view.toolbar.SearchToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.regulateSearchToolbar",
  margin: '0 0 5 0',
  items: [{
    id: "regulate-search-period",
    xtype: "periodCombo",
    width: 120,
    emptyText: '请选择',
    observable: true
  }, {
    id: 'regulate-search-organizationId',
    xtype: 'orgCombo',
    displayField: 'showName',
    labelWidth: 35,
    width: 200,
    initParams: {
      domainId: domainId
    },
    loadAfterRender: true,
    observable: true
  }, {
    id: 'regulate-search-queryButton', text: '查询', xtype: "button", observable: true,
    paramsProperties: ["period", "organizationId"],
    params: {},
    pushParam: function (param) {
      Ext.apply(this.params, param);
    },
    isLoadAll: function () {
      for (var i = 0; i < this.paramsProperties.length; i++) {
        if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
          return false;
        }
      }
      return true;
    },
    getParams: function () {
      return this.params;
    },
    subscription: {
      observers: [
        'regulate-search-period',
        'regulate-search-organizationId'
      ],
      operate: {
        onNext: function (param) {
          this.pushParam(param);
          if (this.isLoadAll()) {
            this.publish(this.getParams());
          }
        }
      }
    },
    handler: function () {
      this.onNext({});
    }
  }]
});
