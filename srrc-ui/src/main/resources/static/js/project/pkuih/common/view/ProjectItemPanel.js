Ext.define('Pkuih.Common.view.ProjectItemPanel', {
  extend: 'Ext.panel.Panel',
  alias: 'widget.projectItemPanel',
  id: 'project-panel-projectItemPanel',
  projectItemId: -1,
  projectId: -1,
  versionId: -1,
  projectCategoryId: -1,
  formType: 'Common',
  readOnly: true,
  padding: '2 2 2 2',
  items: [],
  loadView: function() {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var setOfBooksId = '';
    var projectCategoryId = '';
    var isMonthlySummary = false;
    if (params.setOfBooksId != undefined && params.setOfBooksId != '' && params.setOfBooksId > 0) {
      setOfBooksId = params.setOfBooksId;
    }
    if (this.projectCategoryId != null && this.projectCategoryId > 0) {
      projectCategoryId = this.projectCategoryId;
    }
    if(params.budgetGroupViewType != undefined && params.budgetGroupViewType != ''
      && params.budgetGroupViewType.indexOf('MonthlySummary') > -1) {
      isMonthlySummary = true;
    }

    var form = Ext.create('Pkuih.Common.view.ProjectItemForm');

    //加载费用项数据
    var expenseItemCombox = Ext.getCmp('project-item-expenseItemId');
    if (expenseItemCombox != undefined) {
      expenseItemCombox.joins = [{
        type: 'projectCategoryItem',
        fields: {
          projectCategoryId: 'projectCategoryId'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'expenseItemId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'id'
        }, {
          field: 'setOfBooksId',
          enabled: "'#setOfBooksId#' != ''",
          condition: 'EQUALS',
          value: '#setOfBooksId#'
        }]
      }];
      expenseItemCombox.conditions = [{
        field: 'domainId',
        condition: 'EQUALS',
        value: '#domainId#'
      }, {
        field: 'projectCategoryId',
        enabled: "'#projectCategoryId#' != ''",
        condition: 'EQUALS',
        value: '#projectCategoryId#'
      }];
      expenseItemCombox.sorts = [{
        field: 'sortCode',
        direction: 'ASC'
      }];
      expenseItemCombox.initParams = {
        domainId: domainId,
        setOfBooksId: setOfBooksId,
        projectCategoryId: projectCategoryId,
        leaf: 1
      };
      expenseItemCombox.loadData();
    }

    //根据formType设置Form属性的隐藏与显示
    if (this.formType == "UnifiedTrain") {
      var daysField = Ext.getCmp('project-item-days');
      if (daysField != undefined) {
        daysField.setVisible(true);
      }
      var numField = Ext.getCmp('project-item-num');
      if (numField != undefined) {
        numField.setVisible(true);
      }
      var cityNameField = Ext.getCmp('project-item-cityName');
      if (cityNameField != undefined) {
        cityNameField.setVisible(true);
      }
    } else if (this.formType == "Entertain") {
      var classCodeCombox = Ext.getCmp('project-item-classCode');
      if (classCodeCombox != undefined) {
        classCodeCombox.conditions = [{
          field: 'domainId',
          condition: 'EQUALS',
          value: "#domainId#"
        }, {
          field: 'dictionaryCode',
          condition: 'EQUALS',
          value: "#dictionaryCode#"
        }];
        classCodeCombox.sorts = [{
          field: 'priority',
          direction: 'ASC'
        }, {
          field: 'id',
          direction: 'ASC'
        }];
        classCodeCombox.initParams = {
          domainId: domainId,
          dictionaryCode: 'EntertainClass'
        };
        classCodeCombox.loadData();
        classCodeCombox.allowBlank = false;
        classCodeCombox.fieldLabel = '招待级次<font color=red>*</font>';
        classCodeCombox.setVisible(true);
      }
      var numField = Ext.getCmp('project-item-num');
      if (numField != undefined) {
        numField.allowBlank = false;
        numField.fieldLabel = '招待人数<font color=red>*</font>';
        numField.setVisible(true);
      }
      var benchmarkField = Ext.getCmp('project-item-benchmark');
      if (benchmarkField != undefined) {
        benchmarkField.allowBlank = false;
        benchmarkField.fieldLabel = '招待标准<font color=red>*</font>';
        benchmarkField.setVisible(true);
      }
      var amountField = Ext.getCmp('project-item-amount');
      if (amountField != undefined) {
        amountField.readOnly = true;
        amountField.editable = false;
      }
    } else if (this.formType == "CommonTime") {
      var startTimeField = Ext.getCmp('project-item-startTime');
      if (startTimeField != undefined) {
        startTimeField.setVisible(true);
      }
      var endTimeField = Ext.getCmp('project-item-endTime');
      if (endTimeField != undefined) {
        endTimeField.setVisible(true);
      }
    }

    if(isMonthlySummary) {
      var amountField = Ext.getCmp('project-item-amount');
      if (amountField != undefined) {
        amountField.readOnly = true;
        amountField.editable = false;
      }
    }

    if (this.readOnly) {
      this.setEditedFieldStyle(form, 'background-color: white;');
      this.setReadOnly(form);
    } else {
      this.setEditedFieldStyle(form, 'background-color: #FFFACD;');
      this.setEditable(form);
    }

    //根据Form内容，设置Window高度
    var height = this.up('window').height;
    var itemCount = 0;
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item['hidden']) != 'undefined' && !item.hidden) {
        itemCount++;
      }
    }
    var itemNum = Math.ceil((itemCount - 5)/3);
    this.up('window').height = height + itemNum*35;

    this.add(form);

    function moneyRenderer(value, metaData, record, rowIndex, colIndex, store) {
      if (!$this.readOnly) {
        metaData.tdCls = 'user-online';
      }
      return Ext.util.Format.number(value, '0,0.00');
    }

    var firstHalfStore = Ext.create('Ext.data.Store', {
      storeId: 'firstHalfStore',
      fields: ['M01', 'M02', 'M03', 'M04', 'M05', 'M06']
    });

    var secondHalfStore = Ext.create('Ext.data.Store', {
      storeId: 'secondHalfStore',
      fields: ['M07', 'M08', 'M09', 'M10', 'M11', 'M12']
    });

    var firstHalfColumns = [];
    var secondHalfColumns = [];
    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      if (i <= 6) {
        firstHalfColumns[firstHalfColumns.length] = {
          text: i + "月",
          dataIndex: 'M' + monthStr,
          style: 'text-align:center;',
          align: 'right',
          width: 130,
          menuDisabled: true,
          sortable: false,
          renderer: moneyRenderer,
          editor: {
            xtype: 'numberfield',
            allowBlank: false,
            selectOnFocus: true,
            hideTrigger: true,
            allowDecimals: true,
            decimalPrecision: 2,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            fieldStyle: 'text-align: right'
          }
        };
      } else {
        secondHalfColumns[secondHalfColumns.length] = {
          text: i + "月",
          dataIndex: 'M' + monthStr,
          style: 'text-align:center;',
          align: 'right',
          width: 130,
          menuDisabled: true,
          sortable: false,
          renderer: moneyRenderer,
          editor: {
            xtype: 'numberfield',
            allowBlank: false,
            selectOnFocus: true,
            hideTrigger: true,
            allowDecimals: true,
            decimalPrecision: 2,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            fieldStyle: 'text-align: right'
          }
        };
      }
    }

    var firstHalfGrid = Ext.create('Ext.grid.Panel', {
      store: firstHalfStore,
      plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
          clicksToEdit: 1,
          listeners: {
            beforeedit: function(editor, e) {
              if ($this.readOnly) {
                return false;
              }
            },
            afteredit: function(editor, e) {
              //未修改值时不做如何处理
              if (e.originalValue == e.value) {
                return false;
              }

              if(isMonthlySummary) {
                var amountSum;
                var secondHalfRecord = secondHalfStore.getRange()[0];
                if (e.record.data.M01 != '' || e.record.data.M02 != '' || e.record.data.M03 != ''
                  || e.record.data.M04 != '' || e.record.data.M05 != '' || e.record.data.M06 != ''
                  || secondHalfRecord.data.M07 != '' || secondHalfRecord.data.M08 != ''
                  || secondHalfRecord.data.M09 != '' || secondHalfRecord.data.M10 != ''
                  || secondHalfRecord.data.M11 != '' || secondHalfRecord.data.M12 != '') {
                  amountSum = 0.00;
                  if (e.record.data.M01 != '') {
                    amountSum += e.record.data.M01;
                  }
                  if (e.record.data.M02 != '') {
                    amountSum += e.record.data.M02;
                  }
                  if (e.record.data.M03 != '') {
                    amountSum += e.record.data.M03;
                  }
                  if (e.record.data.M04 != '') {
                    amountSum += e.record.data.M04;
                  }
                  if (e.record.data.M05 != '') {
                    amountSum += e.record.data.M05;
                  }
                  if (e.record.data.M06 != '') {
                    amountSum += e.record.data.M06;
                  }
                  if (secondHalfRecord.data.M07 != '') {
                    amountSum += secondHalfRecord.data.M07;
                  }
                  if (secondHalfRecord.data.M08 != '') {
                    amountSum += secondHalfRecord.data.M08;
                  }
                  if (secondHalfRecord.data.M09 != '') {
                    amountSum += secondHalfRecord.data.M09;
                  }
                  if (secondHalfRecord.data.M10 != '') {
                    amountSum += secondHalfRecord.data.M10;
                  }
                  if (secondHalfRecord.data.M11 != '') {
                    amountSum += secondHalfRecord.data.M11;
                  }
                  if (secondHalfRecord.data.M12 != '') {
                    amountSum += secondHalfRecord.data.M12;
                  }
                }

                var amountField = Ext.getCmp('project-item-amount');
                if (amountField != undefined) {
                  amountField.setValue(amountSum);
                }
              }
            }
          }
        })
      ],
      columns: firstHalfColumns
    });

    var secondHalfGrid = Ext.create('Ext.grid.Panel', {
      store: secondHalfStore,
      plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
          clicksToEdit: 1,
          listeners: {
            beforeedit: function(editor, e) {
              if ($this.readOnly) {
                return false;
              }
            },
            afteredit: function(editor, e) {
              //未修改值时不做如何处理
              if (e.originalValue == e.value) {
                return false;
              }

              if(isMonthlySummary) {
                var amountSum;
                var firstHalfRecord = firstHalfStore.getRange()[0];
                if (firstHalfRecord.data.M01 != '' || firstHalfRecord.data.M02 != ''
                  || firstHalfRecord.data.M03 != '' || firstHalfRecord.data.M04 != ''
                  || firstHalfRecord.data.M05 != '' || firstHalfRecord.data.M06 != ''
                  || e.record.data.M07 != '' || e.record.data.M08 != '' || e.record.data.M09 != ''
                  || e.record.data.M10 != '' || e.record.data.M11 != '' || e.record.data.M12 != '') {
                  amountSum = 0.00;
                  if (firstHalfRecord.data.M01 != '') {
                    amountSum += firstHalfRecord.data.M01;
                  }
                  if (firstHalfRecord.data.M02 != '') {
                    amountSum += firstHalfRecord.data.M02;
                  }
                  if (firstHalfRecord.data.M03 != '') {
                    amountSum += firstHalfRecord.data.M03;
                  }
                  if (firstHalfRecord.data.M04 != '') {
                    amountSum += firstHalfRecord.data.M04;
                  }
                  if (firstHalfRecord.data.M05 != '') {
                    amountSum += firstHalfRecord.data.M05;
                  }
                  if (firstHalfRecord.data.M06 != '') {
                    amountSum += firstHalfRecord.data.M06;
                  }
                  if (e.record.data.M07 != '') {
                    amountSum += e.record.data.M07;
                  }
                  if (e.record.data.M08 != '') {
                    amountSum += e.record.data.M08;
                  }
                  if (e.record.data.M09 != '') {
                    amountSum += e.record.data.M09;
                  }
                  if (e.record.data.M10 != '') {
                    amountSum += e.record.data.M10;
                  }
                  if (e.record.data.M11 != '') {
                    amountSum += e.record.data.M11;
                  }
                  if (e.record.data.M12 != '') {
                    amountSum += e.record.data.M12;
                  }
                }

                var amountField = Ext.getCmp('project-item-amount');
                if (amountField != undefined) {
                  amountField.setValue(amountSum);
                }
              }
            }
          }
        })
      ],
      columns: secondHalfColumns
    });

    this.add(firstHalfGrid);
    this.add(secondHalfGrid);
  },
  setEditedFieldStyle: function(form, style) {
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item['editable']) != 'undefined' && item.editable && typeof(item['setFieldStyle']) != 'undefined') {
        item.setFieldStyle(style);
      }
    }
  },
  setReadOnly: function(form) {
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item['editable']) != 'undefined' && item.editable) {
        item.setReadOnly(true);
      }
    }
  },
  setEditable: function(form) {
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item['editable']) != 'undefined' && item.editable) {
        item.setReadOnly(false);
      }
    }
  }
});