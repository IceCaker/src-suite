Ext.define('Budget.controller.VersionController', {
  extend: 'Ext.app.Controller',
  views: [ "toolbar.SearchToolbar", 'grid.BudgetGroupGrid', "panel.PlanningTabPanel",
    "version.PlanningSummaryGrid", "version.PlanningGrid", "version.ProjectSummaryGrid", "version.ProjectGrid",
    "version.HumanResourcesSummaryGrid", "version.HumanResourcesMedicalGrid", "version.HumanResourcesNursingGrid",
    "version.HumanResourcesTechnologyGrid", "version.HumanResourcesFunctionGrid"
  ],
  refs: [
    {ref: 'budgetGroupGrid', selector: 'budgetGroupGrid'},
    {ref: 'planningTabPanel', selector: 'planningTabPanel'},
    {ref: 'projectSummaryGrid', selector: 'projectSummaryGrid'},
    {ref: 'projectGrid', selector: 'projectGrid'},
    {ref: 'projectFormPanel', selector: 'projectFormPanel'},
    {ref: 'projectInfoWindow', selector: 'projectInfoWindow'},
    {ref: 'projectItemGrid', selector: 'projectItemGrid'}
  ],
  businessTypeName: "预算编制",
  defaultTimeout: 600000,
  isTabchangeHandle: true,
  init: function () {
    var $this = this;
    this.control({
      'budgetGroupGrid': {
        select: $this.selectBudgetGroupRow
      },
      'planningTabPanel': {tabchange: $this.tabchange},
      'projectGrid': {
        nameClickEdit: this.showProject
      },
      'planningGrid': {
        itemNameClick: this.itemNameClick
      }
    });
    Common.CommonManager.getParams = $this.getParams;
    Common.CommonManager.loadStatus = $this.loadStatus;
    Common.CommonManager.loadGrid = function (isCalculate) {
      $this.loadGrid(isCalculate);
    };
  },
  selectBudgetGroupRow: function (grid, record, index, eOpts) {
    this.query(true);
  },
  getMainGrid: function () {
    if (this.getPlanningTabPanel() != undefined) {
      return this.getPlanningTabPanel().getActiveTab();
    } else {
      return null;
    }
  },
  tabchange: function (tabPanel, newCard, oldCard, eOpts) {
    var $this = this;
    if ($this.isTabchangeHandle) {
      var params = $this.getParams();
      var budgetGroupViewType = params.budgetGroupViewType;
      if (budgetGroupViewType == undefined || budgetGroupViewType == null) {
        budgetGroupViewType == "";
      }
      var canSave = Common.CommonManager.editor.canSave;
      var canCommit = Common.CommonManager.editor.canCommit;
      tabPanel.getActiveTab().loadData();

    }
  },
  loadGrid: function (isCalculate) {
    //修改计算状态
    if (isCalculate) {
      this.updateStatus(null, true);
    } else {
      this.updateStatus(null, false);
    }

    this.loadStatus();
    this.query(false);
  },
  query: function (isLoadGrids) {
    if (isLoadGrids) {
      this.loadGrids(this.getPlanningTabPanel());
    } else {
      var panel = this.getMainGrid();
      if (panel != null) {
        panel.loadData();
      }
    }
  },
  loadStatus: function (func) {
    if (func != null) {
      func();
    }
  },
  loadGrids: function (tabPanel) {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var organizationSortCode = params.organizationSortCode;
    var organizationLeaf = params.organizationLeaf;
    var budgetGroupViewType = params.budgetGroupViewType;

    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }

    var activePanelXtype = null;
    var activePanel = null;
    if (tabPanel.getActiveTab() != null) {
      activePanelXtype = tabPanel.getActiveTab().xtype;
    }

    var items = [];
    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary") {
      $this.createNewPanel(items, "预算汇总", "Budget.view.version.PlanningSummaryGrid", true, false);
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "MonthlySummary"
      || budgetGroupViewType == "BudgetMonthlyAllocation" || budgetGroupViewType == "BudgetMonthlySummary") {
      $this.createNewPanel(items, "非项目预算", "Budget.view.version.PlanningGrid", true, true);
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "MonthlySummary"
      || budgetGroupViewType == "ProjectMonthlyAllocation" || budgetGroupViewType == "ProjectMonthlySummary") {
      $this.createNewPanel(items, "项目预算", "Budget.view.version.ProjectSummaryGrid", true, false);
      $this.createNewPanel(items, "关联项目", "Budget.view.version.ProjectGrid", true, false);
    }

    if (budgetGroupViewType == "HumanResourcesSummary") {
      $this.createNewPanel(items, "人力汇总", "Budget.view.version.HumanResourcesSummaryGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesMedical") {
      $this.createNewPanel(items, "医疗人员", "Budget.view.version.HumanResourcesMedicalGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesNursing") {
      $this.createNewPanel(items, "护理人员", "Budget.view.version.HumanResourcesNursingGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesTechnology") {
      $this.createNewPanel(items, "医技人员", "Budget.view.version.HumanResourcesTechnologyGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesFunction") {
      $this.createNewPanel(items, "职能人员", "Budget.view.version.HumanResourcesFunctionGrid", true, true);
    }

    $this.isTabchangeHandle = false;
    tabPanel.removeAll();
    $this.isTabchangeHandle = true;

    for (var i = 0; i < items.length; i++) {
      tabPanel.add(items[i]);
      if (items[i].xtype == activePanelXtype) {
        activePanel = items[i];
      }
    }

    if (activePanel != null) {
      tabPanel.setActiveTab(activePanel);
    } else {
      tabPanel.setActiveTab(items[0]);
    }
  },
  createNewPanel: function (items, title, type, show, editable, closable) {
    if (show) {
      var initConfig = {border: false, editable: editable, title: title, closable: closable};
      var item = Ext.create(type, initConfig);
      if (items != null) {
        items.push(item);
      }
      return item;
    }
  },
  createProjectTabPanel: function (id, title, budgetItemId) {
    var $this = this;
    var params = $this.getParams();
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }

    var tabPanel = $this.getPlanningTabPanel();
    var items = tabPanel.items;
    var isAdd = true;
    for (var i = 0; i < items.length; i++) {
      if (items.get(i).id == id) {
        isAdd = false;
        tabPanel.setActiveTab(items.get(i));
      }
    }

    if (isAdd) {
      var projectItem = {
        id: id,
        xtype: "projectGrid",
        title: title,
        budgetItemId: budgetItemId,
        closable: true
      };

      tabPanel.add(projectItem);
      tabPanel.setActiveTab(Ext.getCmp(id));
    }
  },
  itemNameClick: function (budgetItemId, budgetItemName) {
    this.createProjectTabPanel("projectGrid-budgetItem-" + budgetItemId, "【" + budgetItemName + "】关联项目", budgetItemId);
  },
  getParams: function () {
    return Budget.controller.VersionController.getParams();
  },
  showProject: function (projectId) {
    var $this = this;
    var tabPanel = $this.getPlanningTabPanel();
    var params = this.getParams();
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == undefined || budgetGroupViewType == null) {
      budgetGroupViewType == "";
    }
    var canSave = Common.CommonManager.editor.canSave;
    params.projectId = projectId;

    var isEdit = false;

    Ext.widget('projectInfoWindow').showAndRevertFunc(params, isEdit,
      function () {
        mainApplication.getController('VersionController').loadGrid(true);
      }
    );
  },
  statics: {
    getParams: function () {
      var params = {};
      for (var str in pageInitParam) {
        params[str] = pageInitParam[str];
      }

      //获取预算模版相关信息
      var budgetGroupId = -1;
      var budgetGroupCode = '';
      var budgetGroupViewType = '';
      var budgetGroup = Ext.ComponentQuery.query('budgetGroupGrid')[0];
      if (budgetGroup != null && budgetGroup != undefined) {
        var budgetGroupRecord = budgetGroup.getSelectionModel().getLastSelected();
        if (budgetGroupRecord != null && budgetGroupRecord != undefined) {
          budgetGroupId = budgetGroupRecord.data.id;
          budgetGroupCode = budgetGroupRecord.data.code;
          budgetGroupViewType = budgetGroupRecord.data.viewType;
        }
      }
      params.budgetGroupId = budgetGroupId;
      params.budgetGroupCode = budgetGroupCode;
      params.budgetGroupViewType = budgetGroupViewType;

      return params;
    }
  }
});