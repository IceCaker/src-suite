Ext.define('Budget.view.version.PlanningGrid', {
  extend: 'Budget.view.grid.PlanningGrid',
  alias: "widget.planningGrid",
  loadData: function() {

    var $this = this;
    if ($this.isLoading) {
      return false;
    } else {
      $this.isLoading = true;
    }

    Common.CommonManager.showMask("数据加载中，请稍候...");

    var dataType = $this.dataType;
    var params = Common.CommonManager.getParams();

    //加载Grid
    $this.loadGrid();

    var dynamicQuery = {
      queryType: "read",
      type: "budgetItemGroupItem",
      fields: {
        domainId: "domainId",
        alias: "alias",
        sortCode: "sortCode",
        budgetItemGroupId: "budgetItemGroupId"
      },
      tree: true,
      joins: [
        {
          type: "BudgetItem",
          fields: {
            id: "id",
            code: "code",
            name: "name",
            budgetType: "budgetType",
            leaf: "leaf",
            parentId: "parentId"
          },
          conditions: [
            {
              field: "id",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }
      ],
      associations: [],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: params.budgetGroupId
        }
      ]
    };

    //查询年度数据
    dynamicQuery.associations.push({
      type: "budgetItemGroupItem",
      fields: {
        alias: "alias",
        sortCode: "sortCode",
        budgetItemGroupId: "budgetItemGroupId",
        budgetItemId: "budgetItemId"
      },
      references: {
        budgetItemId: "id"
      },
      joins: [
        {
          type: "annualCostPlanningDataVersion",
          fields: {
            amount: "yearAmount",
            versionId: "versionId"
          },
          conditions: [
            {
              field: "setOfBooksId",
              condition: "EQUALS",
              type: "Integer",
              value: params.setOfBooksId
            },
            {
              field: "period",
              condition: "EQUALS",
              type: "String",
              value: params.period
            },
            {
              field: "organizationId",
              condition: "EQUALS",
              type: "Integer",
              value: params.organizationId
            },
            {
              field: "dataType",
              condition: "STARTS_WITH",
              type: "String",
              value: dataType
            },
            {
              field: "budgetItemGroupId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemGroupId"
            },
            {
              field: "budgetObjectType",
              condition: "EQUALS",
              type: "String",
              value: "BudgetItem"
            },
            {
              field: "budgetObjectId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            },
            {
              field: "versionId",
              condition: "EQUALS",
              value: params.versionId
            }
          ]
        }
      ],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: params.budgetGroupId
        }
      ]
    });

    //查询月度数据
    for (var i = 1; i <= 12; i++) {
      var monthStr = i < 10 ? "0" + i : i;
      dynamicQuery.associations[0].joins.push({
        type: "monthlyCostPlanningDataVersion",
        fields: {
          amount: 'm' + monthStr
        },
        conditions: [
          {
            field: "setOfBooksId",
            condition: "EQUALS",
            type: "Integer",
            value: params.setOfBooksId
          },
          {
            field: "period",
            condition: "EQUALS",
            type: "String",
            value: params.period.substring(0, 4) + monthStr
          },
          {
            field: "organizationId",
            condition: "EQUALS",
            type: "Integer",
            value: params.organizationId
          },
          {
            field: "dataType",
            condition: "STARTS_WITH",
            type: "String",
            value: dataType
          },
          {
            field: "budgetItemGroupId",
            condition: "EQUALS",
            type: "Field",
            value: "budgetItemGroupId"
          },
          {
            field: "budgetObjectType",
            condition: "EQUALS",
            type: "String",
            value: "BudgetItem"
          },
          {
            field: "budgetObjectId",
            condition: "EQUALS",
            type: "Field",
            value: "budgetItemId"
          },
          {
            field: "versionId",
            condition: "EQUALS",
            value: params.versionId
          }
        ]
      });
    }

    //加载数据
    Ext.Ajax.request({
      url: contextPath + '/api/2.0.0/cql',
      method: 'POST',
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      params: Ext.encode(dynamicQuery),
      paramsAsJson: true,
      success: function(response, operation) {
        var responseJson = Ext.JSON.decode(response.responseText);
        $this.setRootNode({
          text: '根节点',
          expanded: true,
          children: responseJson.children
        });

        $this.isLoading = false;
        Common.CommonManager.hideMask();
      }
    });
  }
});