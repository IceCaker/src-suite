Ext.define('Budget.view.panel.PlanningTabPanel', {
  extend: 'Ext.tab.Panel',
  alias: "widget.planningTabPanel",
  processPage: false,
  padding: [0, 0, 0, 0],
  items: []
});