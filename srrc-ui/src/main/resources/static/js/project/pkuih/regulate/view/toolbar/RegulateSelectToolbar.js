Ext.define("Regulate.view.toolbar.RegulateSelectToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.regulateSelectToolbar",
  margin: '0 0 5 0',
  items: [{
    xtype: "textfield",
    name: "code",
    fieldLabel: "项目编码",
    labelSeparator: ' ',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 70
  }, {
    xtype: "textfield",
    name: "name",
    fieldLabel: "项目名称",
    labelSeparator: ' ',
    labelStyle: 'font-weight:bold;',
    labelAlign: 'right',
    labelWidth: 70
  }, {
    id: 'regulate-select-queryButton', text: '查询', xtype: "button",
    handler: function () {
      alert("11");
    }
  }]
});
