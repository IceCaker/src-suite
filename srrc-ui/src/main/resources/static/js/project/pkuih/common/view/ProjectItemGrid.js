Ext.require([
  "Pkuih.Common.view.ProjectItemWindow",
  "Pkuih.Common.view.ProjectItemPanel",
  "Pkuih.Common.view.ProjectItemForm"
]);
Ext.define('Pkuih.Common.view.ProjectItemGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.projectItemGrid',
  minHeight: 150,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  border: false,
  projectId: -1,
  versionId: -1,
  readOnly: true,
  columns: [{
    text: '费用行编号',
    dataIndex: 'code',
    style: 'text-align:center;',
    align: 'center',
    width: 160,
    menuDisabled: true,
    sortable: false
  }, {
    text: '费用行名称',
    dataIndex: 'description',
    flex: 1,
    style: 'text-align:center;',
    align: 'left',
    minWidth: 120,
    menuDisabled: true,
    sortable: false,
    renderer: function (value, meta, record) {
      return '<a href="#" onclick="Pkuih.Common.view.ProjectItemGrid.edit('
        + record.data.id + ',\'' + this.id + '\');"><font title="' + value + '">' + value + '</font></a>';
    }
  }, {
    text: '费用项',
    dataIndex: 'expenseItemName',
    style: 'text-align:center;',
    align: 'left',
    width: 120,
    menuDisabled: true,
    sortable: false
  }, {
    text: '承担部门',
    dataIndex: 'organizationName',
    style: 'text-align:center;',
    align: 'left',
    width: 140,
    menuDisabled: true,
    sortable: false
  }, {
    text: '所属年份',
    dataIndex: 'period',
    style: 'text-align:center;',
    align: 'center',
    width: 100,
    menuDisabled: true,
    sortable: false,
    renderer: function (value, meta, record) {
      var str = '';
      if (value != null && value != '') {
        str = value.substring(0, 4);
      }
      return str;
    }
  }, {
    text: '项目金额',
    dataIndex: 'amount',
    style: 'text-align:center;',
    align: 'right',
    width: 160,
    menuDisabled: true,
    sortable: false,
    renderer: function (value, meta, record) {
      if (value != null) {
        return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
          + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
      } else {
        return '0.00';
      }
    }
  }, {
    text: '执行进度',
    dataIndex: 'completePercent',
    style: 'text-align:center;',
    align: 'center',
    width: 160,
    hidden: true,
    menuDisabled: true,
    sortable: false,
    renderer: function (value, meta, record) {
      var str = '0.00%';
      if (value != null && value != '') {
        str = value * 100 + '%';
      }
      return str;
    }
  }, {
    text: '操作',
    style: 'text-align:center;',
    align: 'center',
    width: 80,
    menuDisabled: true,
    sortable: false,
    renderer: function (value, meta, record) {
      var operateStr = '';

      operateStr = operateStr + '&nbsp;&nbsp;<a onclick="Pkuih.Common.view.ProjectItemGrid.remove('
        + record.data.id + ',\'' + this.id + '\')">删除</a>';

      return operateStr;
    }
  }],
  dockedItems: [{
    xtype: 'toolbar',
    style: 'background-color: white;',
    padding: 6,
    border: true,
    items: [{
      action: 'add',
      xtype: 'button',
      text: '添加费用项',
      handler: function () {
        this.up("projectItemGrid").showProjectItemWindow(-1, true);
      }
    }]
  }],
  initComponent: function () {
    Ext.create("Pkuih.Common.view.ProjectItemWindow");

    this.store = Ext.create('Common.data.ApiGridStore', {
      fields: [
        {name: 'id', type: 'long'},
        {name: 'code', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'expenseItemId', type: 'long'},
        {name: 'expenseItemCode', type: 'long'},
        {name: 'expenseItemName', type: 'string'},
        {name: 'organizationId', type: 'long'},
        {name: 'organizationName', type: 'string'},
        {name: 'period', type: 'string'},
        {name: 'amount', type: 'double'},
        {name: 'completePercent', type: 'double'},
        {name: 'note', type: 'string'},
        {name: 'createTime', type: 'long'},
        {name: 'deletable', type: 'boolean'},
        {name: 'versionId', type: 'long'}
      ],
      api: 'projectItem',
      joins: [{
        type: 'organization',
        fields: {
          name: 'organizationName'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      }, {
        type: 'expenseItem',
        fields: {
          code: 'expenseItemCode',
          name: 'expenseItemName'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'expenseItemId'
          }
        ]
      }],
      conditions: [{
        field: 'projectId',
        condition: 'EQUALS',
        value: '#projectId#'
      }]
    });

    this.callParent(arguments);
  },
  loadData: function () {
    if (this.versionId > 0) {
      this.getStore().api = 'projectItemVersion';
      this.getStore().conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: this.versionId
      });
    }

    this.getStore().load({
      params: {
        projectId: this.projectId
      }
    });
  },
  loadGrid: function (projectId, versionId) {
    var readOnly = this.readOnly;
    if (readOnly) {
      this.down('toolbar').hide();
      this.columns[7].setVisible(false);
    } else {
      this.down('toolbar').show();
      this.columns[7].setVisible(true);
    }
    if (projectId != null) {
      this.projectId = projectId;
    }
    if (versionId != null) {
      this.versionId = versionId;
    }
    this.loadData();
  },
  showProjectItemWindow: function (projectItemId, isEdit) {
    var $this = this;
    var projectId = $this.projectId;
    var versionId = $this.versionId;
    if (projectId == undefined || projectId <= 0) {
      return false;
    }

    if (projectItemId == null) {
      projectItemId = -1;
    }
    if (isEdit == null) {
      if ($this.readOnly) {
        isEdit = false;
      } else {
        isEdit = true;
      }
    }

    Ext.widget('projectItemWindow').showAndRevertFunc(projectItemId, projectId, versionId, isEdit,
      function () {
        if (Ext.ComponentQuery.query('projectInfoWindow')[0] != undefined) {
          Ext.ComponentQuery.query('projectInfoWindow')[0].isUpdate = true;
        }
        $this.loadGrid();
      }
    );
  },
  statics: {
    edit: function (id, gridId) {
      if (gridId == null) {
        gridId = Ext.ComponentQuery.query('projectInfoWindow')[0].id;
      }
      Ext.getCmp(gridId).showProjectItemWindow(id);
    },
    remove: function (id, gridId) {
      if (gridId == null) {
        gridId = Ext.ComponentQuery.query('projectInfoWindow')[0].id;
      }
      var projectItemGrid = Ext.getCmp(gridId);
      var record = projectItemGrid.getStore().getById(id);
      var callback = function () {
        if (Ext.ComponentQuery.query('projectInfoWindow')[0] != undefined) {
          Ext.ComponentQuery.query('projectInfoWindow')[0].isUpdate = true;
        }
        projectItemGrid.loadData();
      };

      Ext.create("Common.data.ApiReader").deleteFunc('projectItem', [record], callback);
    }
  }
});