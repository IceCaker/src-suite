Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.form.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.button.Query',
  "Common.combo.ApiCombox",
  "Common.combo.Organization",
  "Common.combo.SetOfBook",
  "Common.combo.Period",
  "Common.data.ApiReader"
]);

Ext.application({
  name: 'Divide',
  appFolder: contextPath + '/static/js/project/pkuih/divide',
  controllers: ["DivideController"],
  launch: function () {
    Ext.tip.QuickTipManager.init();

    mainApplication = this;

    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add({
      xtype: 'divideMainPanel'
    });
  }
});