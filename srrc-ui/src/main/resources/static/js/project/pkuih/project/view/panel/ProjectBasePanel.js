Ext.define('Project.view.panel.ProjectBasePanel', {
  extend: 'Ext.panel.Panel',
  alias: "widget.projectBasePanel",
  title: '基本信息',
  bodyStyle: 'overflow-y: auto; overflow-x: hidden',
  values: {},
  items: [],
  initComponent: function() {
    var $this = this;

    this.items.push({
      xtype: "panel",
      border: true,
      margin: "5 5 5 5",
      items: [{xtype: "projectFormPanel"}]
    });

    this.items.push({
      xtype: "panel",
      border: true,
      margin: "5 5 5 5",
      items: [{xtype: "projectItemGrid"}]
    });

    this.items.push({
      xtype: "panel",
      border: true,
      margin: "5 5 5 5",
      items: [{xtype: "projectFileGrid"}]
    });

    this.callParent(arguments);
  },
  getProjectId: function() {
    return this.up("projectInfoPanel").projectId;
  },
  getResourceId: function() {
    return this.up("projectInfoPanel").resourceId;
  },
  isReadOnly: function() {
    return this.up("projectInfoPanel").readOnly;
  },
  loadProject: function() {
    var $this = this;
    var projectId = $this.getProjectId();
    var form = this.down("form");
    form.getForm().reset();

    var readOnly = $this.isReadOnly();
    if (readOnly) {
      this.down("toolbar").hide();
    } else {
      this.down("toolbar").show();
    }

    if (readOnly) {
      this.setEditedFieldStyle(form, 'background-color: white');
      this.setReadOnly(form);
    } else {
      this.setEditedFieldStyle(form, 'background-color: #FFFACD;');
      this.setEditable(form);
    }

    if (projectId > 0) {
      var projects = Ext.create("Common.data.ApiReader").load({
        api: 'project',
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          value: projectId
        }]
      });
      if(projects != null && projects[0] != null) {
        var project = projects[0];
        //项目期间
        var startTime = moment(project.startTime).format('YYYY-MM-DD HH:mm:ss');
        if (startTime != null && startTime != "" && startTime.length >= 4) {
          project.period = startTime.substring(0, 4) + "00";
        }
        form.getForm().setValues(project);
        $this.up("projectInfoPanel").resourceId = project.resourceId;
      }
    } else {
      var values = {};
      var params = Common.CommonManager.getParams();
      values.period = params.period;
      values.organizationId = params.organizationId;
      values.creatorOrgId = params.organizationId;
      if (params.projectCategoryId > 0) {
        values.projectCategoryId = params.projectCategoryId;
      }
      values.amount = 0;
      form.getForm().setValues(values);
    }
  },
  saveProject: function(isShowSuccessAlert) {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var projectId = $this.getProjectId();

    if(isShowSuccessAlert == undefined || isShowSuccessAlert == null){
      isShowSuccessAlert = true;
    }

    var isSuccess = false;
    var isUpdate = false;
    if (projectId > 0) {
      isUpdate = true;
    }

    if ($this.down("form").getForm().isValid()) {
      var project= $this.getFormValues();
      project.domainId = params.domainId;
      project.setOfBooksId = params.setOfBooksId;
      if (projectId > 0) {
        project.id = projectId;
      }

      var callback = function(result) {
        project = result[0].data;
        $this.down("form").getForm().setValues(project);
        if (!isUpdate) {
          $this.up("projectInfoPanel").projectId = project.id;
          $this.up("projectInfoPanel").resourceId = project.resourceId;
          $this.up("projectInfoPanel").showTabs();
        }

        if (isShowSuccessAlert) {
          Common.Msg.Alert("保存成功！");
        }
        isSuccess = true;
      };

      Ext.create("Common.data.ApiReader")
        .createOrUpdate('project', [project], callback, false);
    } else {
      Common.Msg.Alert("请选择或输入必填项！");
    }

    return isSuccess;
  },
  getFormValues: function() {
    var form = this.down("form");
    var values = form.getForm().getValues();
    Common.text.CommonAmount.getValuesFromForm(values, form);

    //项目开始结束时间
    if (values.period != null && values.period != "" && values.period.length >= 4) {
      values.startTime = values.period.substring(0, 4) + "-01-01T00:00:00.000+0800";
      values.endTime = values.period.substring(0, 4) + "-12-31T23:59:59.000+0800";
    }

    return values;
  },
  setEditedFieldStyle: function(form, style) {
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item["editable"]) != "undefined" && item.editable && typeof(item["setFieldStyle"]) != "undefined") {
        item.setFieldStyle(style);
      }
    }
  },
  setReadOnly: function(form) {
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item["editable"]) != "undefined" && item.editable) {
        item.setReadOnly(true);
      }
    }
  },
  setEditable: function(form) {
    for (var i = 0; i < form.items.items.length; i++) {
      var item = form.items.items[i];
      if (typeof(item["editable"]) != "undefined" && item.editable) {
        item.setReadOnly(false);
      }
    }
  },
  loadData: function() {
    var $this = this;

    $this.loadProject();
    $this.down("projectItemGrid").readOnly = $this.isReadOnly();
    $this.down("projectItemGrid").loadGrid($this.getProjectId());
    $this.down("projectFileGrid").readOnly = $this.isReadOnly();
    $this.down("projectFileGrid").loadGrid($this.getResourceId());
  }
});