Ext.define("Budget.view.version.HumanResourcesSummaryGrid", {
  extend: "Budget.view.version.PlanningGrid",
  alias: "widget.humanResourcesSummaryGrid",
  title: "人力汇总",
  dataType: "@@HumanResourcesSummary@@Cost@@"
});