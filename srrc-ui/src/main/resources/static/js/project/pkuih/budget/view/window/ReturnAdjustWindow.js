Ext.define("Budget.view.window.ReturnAdjustWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.returnAdjustWindow",
  title: "退回调整",
  plain: true,
  width: 500,
  height: 300,
  layout: 'fit',
  closeAction: "hide",
  autoShow: true,
  modal: true,
  params: [],
  items: [{
    xtype: "grid",
    simpleSelect: false,
    multiSelect: true,
    viewConfig: {
      loadMask: false,
      forceFit: true
    },
    params: {},
    store: Ext.create('Ext.data.Store', {
      fields: [
        {name: 'id', type: 'string'},
        {name: 'organizationId', type: 'int'},
        {name: 'organizationName', type: 'string'},
        {name: 'organizationLevel', type: 'string'},
        {name: 'statusName', type: 'string'}
      ],
      data: []
    }),
    plugins: [
      Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1,
        listeners: {
          beforeedit: function (editor, e) {
            var rows = e.grid.getSelectionModel().getSelection();
            e.grid.getSelectionModel().select(rows);
          }
        }
      })
    ],
    selModel: Ext.create('Ext.selection.CheckboxModel', {
      mode: 'SIMPLE'
    }),
    columns: [
      {
        text: '组织',
        dataIndex: 'organizationName',
        align: 'left',
        flex: 1,
        minWidth: 150,
        sortable: false
      },
      {
        text: '状态',
        dataIndex: 'statusName',
        align: 'left',
        width: 150,
        sortable: false
      }
    ]
  }],
  buttons: [
    {
      text: "确定",
      handler: function () {
        var $this = this;
        var params = Common.CommonManager.getParams();
        var budgetDataStatuses = [];

        var selections = $this.up("window").down('grid').getSelectionModel().getSelection();
        if (selections == 0) {
          Common.Msg.Alert("请选择需要退回调整的组织！");
          return false;
        }

        if (params.organizationSortCode.length == 4) {
          var budgetDataStatus = Ext.create('Common.data.ApiReader').load({
            api: 'budgetDataStatus',
            conditions: [{
              field: 'domainId',
              condition: 'EQUALS',
              type: 'String',
              value: params.domainId
            }, {
              field: 'setOfBooksId',
              condition: 'EQUALS',
              type: 'Integer',
              value: params.setOfBooksId
            }, {
              field: 'organizationId',
              condition: 'EQUALS',
              type: 'Integer',
              value: params.organizationId
            }, {
              field: 'period',
              condition: 'EQUALS',
              type: 'String',
              value: params.period
            }, {
              field: 'businessType',
              condition: 'EQUALS',
              type: 'String',
              value: params.businessType
            }, {
              field: 'dataType',
              condition: 'STARTS_WITH',
              type: 'String',
              value: '@@Planning@@'
            }]
          });

          if (budgetDataStatus != null && budgetDataStatus[0] != null) {
            if (budgetDataStatus[0].statusCode == 'Approving') {
              Common.Msg.Error("当前组织在流程审批中，不允许退回调整！");
              return false;
            }
            if (budgetDataStatus[0].statusCode == 'FinalJudgmentApproved') {
              Common.Msg.Error("预算编制已经终审通过，不允许再退回调整！");
              return false;
            }
            if (budgetDataStatus[0].statusCode == 'Approved' || budgetDataStatus[0].statusCode == 'Submitted'
              || budgetDataStatus[0].statusCode == 'FirstTrailApproved' || budgetDataStatus[0].statusCode == 'ReexamineApproved') {
              budgetDataStatuses[budgetDataStatuses.length] = {
                id: budgetDataStatus[0].id,
                statusCode: 'ReturnAdjust'
              };
            }
          }
        }

        for (var i = 0; i < selections.length; i++) {
          var budgetDataStatus = {
            id: selections[i].data.id,
            statusCode: 'ReturnAdjust'
          };
          budgetDataStatuses[budgetDataStatuses.length] = budgetDataStatus;
        }

        var callback = function () {
          Common.Msg.Alert("选择的组织退回调整成功！");
          $this.up("window").loadGrid();
          $this.up("window").close();
        };

        Ext.create("Common.data.ApiReader")
          .createOrUpdate('budgetDataStatus', budgetDataStatuses, callback, false);

        //发送待阅
        var messages = [];
        for (var i = 0; i < selections.length; i++) {
          if (selections[i].data.organizationId != params.organizationId) {
            var roleCode = '';
            if (selections[i].data.organizationLevel == 'Department') {
              roleCode = 'ROLE_COMPILERS';
            } else if (selections[i].data.organizationLevel == 'Company') {
              roleCode = 'ROLE_BUDGETER'
            }
            var resource = Ext.create('Common.data.ApiReader').load({
              api: 'resourcePermission',
              fields: {
                userId: 'userId'
              },
              joins: [{
                type: 'organization',
                fields: {
                  id: 'organizationId',
                  code: 'organizationCode',
                  name: 'organizationName',
                  organizationLevel: 'organizationLevel'
                },
                conditions: [{
                  field: 'domainId',
                  condition: 'EQUALS',
                  type: 'Field',
                  value: 'domainId'
                }, {
                  field: 'code',
                  condition: 'EQUALS',
                  type: 'Field',
                  value: 'resourceId'
                }]
              }, {
                type: 'role',
                fields: {
                  code: 'roleCode'
                },
                conditions: [{
                  field: 'domainId',
                  condition: 'EQUALS',
                  type: 'Field',
                  value: 'domainId'
                }, {
                  field: 'id',
                  condition: 'EQUALS',
                  type: 'Field',
                  value: 'roleId'
                }]
              }],
              conditions: [{
                field: 'domainId',
                condition: 'EQUALS',
                type: 'String',
                value: params.domainId
              }, {
                field: 'resourceType',
                condition: 'EQUALS',
                type: 'String',
                value: 'com.bmit.platform.soupe.model.Organization'
              }, {
                field: 'organizationLevel',
                condition: 'EQUALS',
                type: 'String',
                value: selections[i].data.organizationLevel
              }, {
                field: 'roleCode',
                condition: 'EQUALS',
                type: 'String',
                value: roleCode
              }, {
                field: 'organizationId',
                condition: 'EQUALS',
                type: 'Integer',
                value: selections[i].data.organizationId
              }]
            });
            if (resource.length == 0) {
              continue;
            }

            var paramObj = {};
            // paramObj.organizationId = resource[0].organizationId;
            // paramObj.period = params.period;
            paramObj.url = '';
            paramObj.returnAdjust = true;
            var paramStr = JSON.stringify(paramObj);

            var message = {};
            message.domainId = domainId;
            message.businessType = params.businessType;
            message.receiverId = resource[0].userId;
            message.status = 'Open';
            message.param = paramStr;
            message.subject = resource[0].organizationName + params.period.substr(0, 4) + '年预算被退回';
            messages.push(message);
          }
        }

        Ext.create("Common.data.ApiReader")
          .createOrUpdate('message', messages);
      }
    },
    {
      text: "取消",
      handler: function () {
        this.up("window").close();
      }
    }
  ],
  loadGrid: function () {

  },
  showAndRevertFunc: function (func) {
    var $this = this;

    var subOrganizationStatuses = this.loadData();
    $this.down("grid").store.loadData(subOrganizationStatuses);

    if (func != null) {
      this.loadGrid = func;
    }

    $this.show();
  },
  loadData: function () {
    var params = Common.CommonManager.getParams();
    var dataType = '@@Planning@@';

    //顶级组织复审通过时，需要退回调整当前组织
    var statusCode = "Draft";
    if (params.organizationSortCode.length == 4) {
      var budgetDataStatus = Ext.create("Common.data.ApiReader").load({
        api: 'budgetDataStatus',
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        }, {
          field: 'period',
          condition: 'EQUALS',
          type: 'String',
          value: params.period
        }, {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        }, {
          field: 'businessType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: params.businessType
        }, {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: dataType
        }]
      });

      if (budgetDataStatus != null && budgetDataStatus[0] != null
        && budgetDataStatus[0].statusCode != undefined && budgetDataStatus[0].statusCode != '') {
        statusCode = budgetDataStatus[0].statusCode;
      }
    }

    var dynamicQuery = {
      api: 'budgetDataStatus',
      joins: [{
        type: 'organization',
        fields: {
          parentId: 'parentId',
          sortCode: 'sortCode',
          name: 'organizationName',
          organizationLevel: 'organizationLevel'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'organizationId'
        }]
      }, {
        type: 'dictionaryOption',
        fields: {
          names: 'statusName'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'code',
          condition: 'EQUALS',
          type: 'Field',
          value: 'statusCode'
        }, {
          field: 'dictionaryCode',
          condition: 'EQUALS',
          value: 'BudgetStatus'
        }]
      }],
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: params.domainId
      }, {
        field: 'setOfBooksId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.setOfBooksId
      }, {
        field: 'period',
        condition: 'EQUALS',
        type: 'String',
        value: params.period
      }, {
        field: 'statusCode',
        condition: 'IN',
        type: 'String',
        value: ['Approved', 'Submitted', 'FirstTrailApproved', 'ReexamineApproved', 'FinalJudgmentApproved']
      }, {
        field: 'businessType',
        condition: 'EQUALS',
        type: 'String',
        value: params.businessType
      }, {
        field: 'dataType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: '@@Planning@@'
      }, {
        conditions: [{
          operator: 'OR',
          field: 'parentId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        }]
      }],
      sorts: [{
        field: 'sortCode',
        direction: 'ASC'
      }]
    };

    if (statusCode == "ReexamineApproved") {
      dynamicQuery.conditions[6].conditions.push({
        operator: 'OR',
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.organizationId
      });
    }

    return Ext.create('Common.data.ApiReader').load(dynamicQuery);
  }
});