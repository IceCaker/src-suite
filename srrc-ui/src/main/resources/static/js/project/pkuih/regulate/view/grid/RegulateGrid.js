Ext.define('Regulate.view.grid.RegulateGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.regulateGrid',
  autoScroll: true,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  autoWidth: true,
  regulateType: "",
  viewConfig: {
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  tbar: {
    xtype: 'regulateOperateToolbar'
  },
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'code', type: 'string'},
      {name: 'organizationId', type: 'int'},
      {name: 'budgetItemName', type: 'string'},
      {name: 'organizationName', type: 'string'},
      {name: 'budgetItemId', type: 'int'},
      {name: 'budgetAmount', type: 'double'},
      {name: 'adjustAmount', type: 'double'},
      {name: 'status', type: 'string'}
    ],
    api: "budgetRegulate",
    joins: [
      {
        type: 'organization',
        fields: {
          name: 'organizationName',
          sortCode: 'organizationSortCode'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      },
      {
        type: 'budgetItem',
        fields: {
          name: 'budgetItemName'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'budgetItemId'
          }
        ]
      }
    ],
    sorts: [
      {
        field: 'createTime',
        direction: 'DESC'
      }
    ],
    listeners: {
      beforeload: function (store, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      load: function (store) {
        Common.CommonManager.hideMask();
      }
    }
  }),
  selModel: Ext.create('Ext.selection.CheckboxModel',
    {mode: 'SIMPLE', checkOnly: true}
  ),
  columns: [
    {text: '申请编码', dataIndex: 'code', align: 'left', width: 150, sortable: false},
    {text: '申请部门', dataIndex: 'organizationName', align: 'left', width: 150},
    {text: '支出科目', dataIndex: 'budgetItemName', align: 'left', minWidth: 150, flex: 1},
    {
      text: '调整前预算', dataIndex: 'budgetAmount', align: 'left', width: 130, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    },
    {
      text: '调整值', dataIndex: 'adjustAmount', align: 'left', width: 130, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    },
    {
      text: '调整后预算', align: 'adjustAfterAmount', align: 'left', width: 130, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    },
    {
      text: '状态', dataIndex: 'status', align: 'center', width: 130, sortable: false,
      renderer: function (value, meta, record) {
        if (value == "Draft") {
          return '草稿';
        } else if (value == "Approving") {
          return '审批中';
        } else if (value == "Approved") {
          return '审批通过';
        }
      }
    },
    {
      text: '操作', dataIndex: 'operate', align: 'center', width: 130, sortable: false,
      renderer: function (value, meta, record) {
        var operateStr = '';
        if (record.data.status == 'Draft')
          operateStr = operateStr + '<a onclick="Regulate.view.grid.RegulateGrid.remove('
            + record.data.id + ')">删除</a>';

        return operateStr;
      }
    }
  ],
  initComponent: function () {
    this.bbar = Ext.create('Ext.PagingToolbar', {
      store: this.store,
      displayInfo: true,
      displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
      emptyMsg: '没有数据'
    });
    this.addEvents({'nameClickDelete': true});
    this.callParent(arguments);
  },
  loadGrid: function (params) {
    var $this = this;
    var conditions = [
      {
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: domainId
      },
      {
        field: 'period',
        condition: 'EQUALS',
        type: 'String',
        value: params.period
      },
      {
        field: 'dataType',
        condition: 'EQUALS',
        type: 'String',
        value: $this.regulateType == 'inside' ? 'BudgetInternal' : 'BudgetExternal'
      },
      {
        field: 'organizationSortCode',
        condition: 'STARTS_WITH',
        value: params.organizationSortCode
      }
    ];
    this.getStore().conditions = conditions;
    this.getStore().loadData({
      params: {
        domainId: domainId,
        params: params
      }
    });
  },
  deleteData: function (id) {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var record = $this.getStore().getById(id);
    var callback = function () {
      $this.loadGrid(params);
    };
    Ext.create("Common.data.ApiReader").deleteFunc('budgetRegulate', [record], callback);
  },
  subscription: {
    observers: ['regulate-search-queryButton'],
    operate: {
      onNext: function (params) {
        this.loadGrid(params);
      }
    }
  },
  statics: {
    remove: function (id) {
      Ext.ComponentQuery.query('regulateGrid')[0].deleteData(id);
    }
  }
});
