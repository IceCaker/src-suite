Ext.define('Budget.view.version.PlanningSummaryGrid', {
  extend: 'Budget.view.version.PlanningGrid',
  alias: "widget.planningSummaryGrid",
  title: "预算汇总",
  dataType: '@@SummaryPlanning@@'
});