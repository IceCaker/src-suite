Ext.define('Activity.view.ActivityGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.activityGrid',
    viewConfig: {
        loadMask: false,
        forceFit: true
    },
    store: Ext.create('Common.data.ApiDirectGridStore', {
        fields: [
            {name: 'id', type: 'string'},
            {name: 'organizationName', type: 'string'},
            {name: 'organizationId', type: 'string'},
            {name: 'code', type: 'string'},
            {name: 'name', type: 'string'},
            {name: 'managerId', type: 'string'},
            {name: 'managerName', type: 'string'},
            {name: 'amount', type: 'double'},
            {name: 'yearAmount', type: 'double'},
            {name: 'startTime', type: 'date'},
            {name: 'endTime', type: 'date'},
            {name: 'status', type: 'string'},
            {name: 'projectCategoryId', type: 'string'}
        ],
        pageSize: 50,
        api: "project",
        joins: [
            {
                type: "organization",
                fields: {
                    name: "organizationName"
                },
                conditions: [
                    {
                        field: "id",
                        condition: "EQUALS",
                        type: "Field",
                        value: "organizationId"
                    }
                ]
            },
            {
                type: "user",
                fields: {
                    name: "managerName"
                },
                conditions: [
                    {
                        field: "id",
                        condition: "EQUALS",
                        type: "Field",
                        value: "managerId"
                    }
                ]
            },
            {
                type: "projectCategory",
                fields: {
                    code: "projectCategoryCode"
                },
                conditions: [
                    {
                        field: "id",
                        condition: "EQUALS",
                        type: "Field",
                        value: "projectCategoryId"
                    }
                ]
            }
        ],
        sorts: [
            {
                field: 'createTime',
                direction: 'DESC'
            },
            {
                field: 'name',
                direction: 'ASC'
            }
        ]
    }),
    plugins: [],
    selModel: Ext.create('Ext.selection.CheckboxModel', {
        mode: 'SIMPLE'
    }),
    columns: [
        {
            text: '编号',
            dataIndex: 'code',
            align: 'center',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false
        },
        {
            text: '名称',
            dataIndex: 'name',
            align: 'center',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false
        },
        {
            text: '申请部门',
            dataIndex: 'organizationName',
            align: 'center',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false
        },
        {
            text: '经办人',
            dataIndex: 'managerName',
            align: 'center',
            flex: 1,
            minWidth: 100,
            locked: true,
            menuDisabled: true,
            sortable: false
        },
        {
            text: '总金额',
            dataIndex: 'amount',
            align: 'right',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false,
            renderer: amountRenderer
        },
        {
            text: '本年总金额',
            dataIndex: 'amount',
            align: 'right',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false,
            renderer: amountRenderer
        },
        {
            text: '开始日期',
            dataIndex: 'startTime',
            align: 'center',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false,
            renderer: dateRender
        },
        {
            text: '结束日期',
            dataIndex: 'endTime',
            align: 'center',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false,
            renderer: dateRender
        },
        {
            text: '状态',
            dataIndex: 'status',
            align: 'center',
            flex: 1,
            minWidth: 200,
            locked: true,
            menuDisabled: true,
            sortable: false,
            renderer: statusRenderer
        }
    ],
    initComponent: function (arguments) {
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: '显示 {0} - {1} 条，共计 {2} 条'
        });
        this.callParent(arguments);
    },
    subscription: {
        observers: ['activity-search-query'],
        operate: {
            onNext: function (params) {
                this.loadGrid(params);
            }
        }
    },
    loadGrid: function (params) {
        var $this = this;

        var conditions = [
            {
                field: "domainId",
                condition: "EQUALS",
                type: "String",
                value: domainId
            }, {
                field: "projectCategoryCode",
                condition: "EQUALS",
                type: "String",
                value: projectCategoryCode
            }, {
                field: 'organizationId',
                condition: 'IN',
                value: params.organizationId
            }, {
                field: 'startTime',
                condition: 'LESS_OR_EQUALS',
                type: 'Date',
                value: params.period.substring(0, 4) + "-12-31T23:59:59.000+0800"
            }, {
                field: 'endTime',
                condition: 'GREATER_OR_EQUALS',
                type: 'Date',
                value: params.period.substring(0, 4) + "-01-01T00:00:00.000+0800"
            },{
                field: 'managerName',
                condition: 'CONTAINS',
                value: params.managerName
            },{
                conditions: [
                    {
                        operator: 'OR',
                        field: 'name',
                        condition: 'CONTAINS',
                        value: params.nameCode
                    },
                    {
                        operator: 'OR',
                        field: 'code',
                        condition: 'CONTAINS',
                        value: params.nameCode
                    }
                ]
            }
        ];
        if(params.status != null && params.status != ''){
            conditions.push({
                field: 'status',
                condition: 'EQUALS',
                type: 'String',
                value: params.status
            });
        }
        this.getStore().conditions = conditions;

        this.getStore().loadData({
            params: {
                domainId: domainId,
                params: params
            }
        });
    }
});

function amountRenderer(value, meta, record) {
    if (value != null) {
        return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
    } else {
        return '0.00';
    }
};

function statusRenderer(value, meta, record) {
    switch (value) {
        case "Draft":
            return "草稿";
        case "DraftApproval":
            return "草稿审批中";
        case "Approved":
            return "已完成";
        case "Adjusting":
            return "调整中";
        case "AdjustApproval":
            return "调整审批中";
        case "Closed":
            return "已结束";
        case "CloseApproval":
            return "结束审批中";
    }
    return value;
};

function dateRender(v) {
    return moment(v).format('YYYY-MM-DD');
}
