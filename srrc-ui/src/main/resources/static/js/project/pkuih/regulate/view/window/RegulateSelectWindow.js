Ext.define("Regulate.view.window.RegulateSelectWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.regulateSelectWindow",
  title: '选择项目',
  modal: true,
  shadow: false,
  plain: true,
  resizable: false,
  width: 800,
  height: 360,
  closeAction: "destroy",
  maximizable: false,
  regulateType: "",
  items: [],
  buttons: [
    {
      text: "确定",
      action: "save",
      handler: function () {
        this.up("window").saveRegulateDetail();
      }
    },
    {
      text: "取消",
      action: "close",
      handler: function () {
        this.up("window").close();
      }
    }
  ],
  initComponent: function () {
    var $this = this;

    this.callParent(arguments);
  },
  loadGrid: function () {
  },
  showAndRevertFunc: function () {
    this.show();

    var items = [];
    items[items.length] = {
      xtype: "regulateSelectToolbar",
      border: true,
      margin: "5 5 5 5"
    };
    items[items.length] = {
      xtype: 'regulateSelectGrid',
      border: true,
      margin: "5 5 5 5",
    };
    this.add(items);
  },
  saveRegulateDetail: function () {
  }
});
