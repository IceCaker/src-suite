Ext.define('Budget.view.version.ProjectSummaryGrid', {
  extend: 'Budget.view.version.PlanningGrid',
  alias: "widget.projectSummaryGrid",
  title: "项目预算",
  isProjectBudget: true,
  dataType: '@@ProjectPlanning@@'
});