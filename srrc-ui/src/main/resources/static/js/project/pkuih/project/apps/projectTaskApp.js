Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  "Common.FlexibleToolbar"
]);

Ext.application({
  name: 'Project',
  appFolder: contextPath + '/static/js/project/pkuih/project',
  controllers: [
    "ProjectTaskController",
    "ProjectTaskInfoController"
  ],
  launch: function () {
    Common.CommonManager.init(pageInitParam);

    Ext.tip.QuickTipManager.init();
    var initParams = {}

    for(var obj in pageInitParam){
      initParams[obj] = pageInitParam[obj];
    }

    var panel = Ext.widget("projectTaskGrid",{
      height: 340,
      margin: false,
      border: "1 1 0 1",
      region: 'north'
    });

    Common.workflow.MainInfoData.items.push(panel);

    Common.CommonManager.showMask("正在查询项目数据...");
    panel.getStore().load({
      params: {
        projectIds: pageInitParam.projectIds
      }
    });

    Ext.application('WorkFlow.DefaultApplication');
  }
});