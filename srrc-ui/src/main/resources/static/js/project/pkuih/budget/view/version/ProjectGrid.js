Ext.define('Budget.view.version.ProjectGrid', {
  extend: 'Budget.view.grid.ProjectGrid',
  alias: 'widget.projectGrid',
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'projectId', type: 'string'},
      {name: 'projectCode', type: 'string'},
      {name: 'projectName', type: 'string'},
      {name: 'projectItemId', type: 'int'},
      {name: 'projectItemCode', type: 'string'},
      {name: 'projectItemName', type: 'string'},
      {name: 'expenseItemId', type: 'string'},
      {name: 'expenseItemName', type: 'string'},
      {name: 'organizationId', type: 'int'},
      {name: 'organizationName', type: 'string'},
      {name: 'amount', type: 'double'}
    ],
    api: "relatedProjectItemVersion",
    sorts: [{
      field: 'projectCode',
      direction: 'DESC'
    }],
    pageSize: 50,
    listeners: {
      beforeload: function (store, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      load: function (store) {
        Common.CommonManager.hideMask();
      }
    },
    groupField: "projectCode"
  }),
  features: [{
    ftype: 'groupingsummary',
    groupHeaderTpl: "<a onclick=\"Budget.view.version.ProjectGrid.edit('{[values.rows[0].data[\"projectId\"]]}')\">{[values.rows[0].data['projectName']]}[{[values.rows[0].data['projectCode']]}]</a>",
    hideGroupedHeader: true,
    enableGroupingMenu: false
  }],
  loadData: function () {
    var params = Common.CommonManager.getParams();

    //组装数据过滤条件
    this.getStore().conditions = [{
      field: 'domainId',
      condition: 'EQUALS',
      type: 'String',
      value: '#domainId#'
    }, {
      field: 'setOfBooksId',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#setOfBooksId#'
    }, {
      field: 'period',
      condition: 'EQUALS',
      type: 'String',
      value: '#period#'
    }, {
      field: 'budgetObjectId',
      enabled: '"#budgetItemId#" != ""',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#budgetItemId#'
    }, {
      field: 'budgetItemGroupId',
      enabled: '"#budgetGroupId#" != ""',
      condition: 'EQUALS',
      type: 'Integer',
      value: '#budgetGroupId#'
    },{
      field: 'versionId',
      condition: 'EQUALS',
      value: params.versionId
    }];

    if (params.organizationLeaf) {
      this.getStore().api = "relatedProjectItemVersion";

      //组装表连接
      this.getStore().joins = [{
        type: 'project',
        fields: {
          code: 'projectCode',
          name: 'projectName'
        },
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectId'
        }]
      }, {
        type: 'projectItem',
        fields: {
          code: 'projectItemCode',
          description: 'projectItemName',
          expenseItemId: 'expenseItemId'
        },
        joins: [{
          type: 'expenseItem',
          fields: {
            name: 'expenseItemName'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'expenseItemId'
            }
          ]
        }],
        conditions: [{
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectItemId'
        }]
      }];

      this.getStore().joins.push({
        type: 'organization',
        fields: {
          name: 'organizationName',
          sortCode: 'organizationSortCode'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      });

      this.getStore().conditions.push({
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: '#organizationId#'
      });
    } else {
      this.getStore().api = "relatedProjectItemVersion";

      //组装表连接
      this.getStore().joins = [{
        type: 'projectVersion',
        fields: {
          code: 'projectCode',
          name: 'projectName'
        },
        conditions: [{
          field: 'versionId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'versionId'
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectId'
        }]
      }, {
        type: 'projectItemVersion',
        fields: {
          code: 'projectItemCode',
          description: 'projectItemName',
          expenseItemId: 'expenseItemId'
        },
        joins: [{
          type: 'expenseItem',
          fields: {
            name: 'expenseItemName'
          },
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'expenseItemId'
            }
          ]
        }],
        conditions: [{
          field: 'versionId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'versionId'
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'projectItemId'
        }]
      }];

      this.getStore().joins.push({
        type: 'organization',
        fields: {
          name: 'organizationName',
          sortCode: 'organizationSortCode'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      });

      this.getStore().conditions.push({
        field: 'organizationSortCode',
        condition: 'STARTS_WITH',
        value: '#organizationSortCode#'
      });

      this.getStore().conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        type: 'Field',
        value: params.versionId
      });
    }

    var budgetItemId = '';
    if (this.budgetItemId > 0) {
      budgetItemId = this.budgetItemId;
    }

    var budgetGroupId = '';
    if (params.budgetGroupId > 0
      && (params.budgetGroupViewType != undefined && params.budgetGroupViewType != "BudgetSummary")) {
      budgetGroupId = params.budgetGroupId;
    }

    this.getStore().loadData({
      params: {
        domainId: params.domainId,
        setOfBooksId: params.setOfBooksId,
        period: params.period,
        organizationId: params.organizationId,
        organizationSortCode: params.organizationSortCode,
        budgetItemId: budgetItemId,
        budgetGroupId: budgetGroupId
      }
    });
  },
  statics: {
    edit: function(projectId) {
      Ext.ComponentQuery.query('projectGrid')[0].fireEvent('nameClickEdit', projectId);
    }
  }
});