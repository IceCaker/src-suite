Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');
Ext.Loader.setPath('Pkuih.Common', contextPath + '/static/js/project/pkuih/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.WorkFlowManager',
  "Common.data.ApiReader",
  "Common.combo.ApiCombox",
  "Common.combo.Period",
  "Common.combo.Organization",
  "Common.combo.SetOfBook",
  "Common.combo.ApiDictionaryCombox",
  "Pkuih.Common.view.ProjectInfoWindow",
  "Pkuih.Common.view.FileUploadWindow"
]);

Ext.application({
  name: 'Budget',
  appFolder: contextPath + '/static/js/project/pkuih/budget',
  controllers: ["VersionController"],
  launch: function() {
    Common.CommonManager.init(pageInitParam);
    mainApplication = this;

    var height = window.document.body.clientHeight;
    var width = window.document.body.clientWidth;

    Ext.tip.QuickTipManager.init();
    var initParams = {};

    for (var obj in pageInitParam) {
      initParams[obj] = pageInitParam[obj];
    }

    var panel = Ext.create('Ext.panel.Panel', {
      autoScroll: false,
      layout: 'border',
      border: false,
      bodyStyle: {
        background: "#FFFFFF"
      },
      bodyPadding: 5,
      width: width,
      height: height,
      items: [{
        xtype: "planningSearchToolbar",
        name: "toolbarOne",
        margin: '0 0 5 0',
        hidden: true,
        region: 'north'
      }, {
        xtype: 'budgetGroupGrid',
        title: '预算模版',
        border: false,
        collapsible: true,
        split: true,
        scroll: true,
        hideHeaders: true,
        hideCollapseTool: true,
        width: 250,
        region: 'west'
      }, {
        xtype: 'planningTabPanel',
        scroll: true,
        region: 'center'
      }]
    });

    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add(panel);
  }
});