Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.form.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.button.Query',
  "Common.combo.ApiCombox",
  "Common.combo.CommonCombox",
  "Common.combo.SetOfBook",
  "Common.combo.Period",
  "Common.combo.Organization",
  "Common.combo.BudgetItem"
]);

Ext.application({
  name: 'Regulate',
  appFolder: contextPath + '/static/js/project/pkuih/regulate',
  controllers: ["OutsideRegulateController"],
  launch: function () {
    Ext.tip.QuickTipManager.init();

    mainApplication = this;

    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add({
      xtype: 'regulateMainPanel'
    });
  }
});