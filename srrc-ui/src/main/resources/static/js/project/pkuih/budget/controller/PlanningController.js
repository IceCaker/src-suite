Ext.define('Budget.controller.PlanningController', {
  extend: 'Ext.app.Controller',
  views: ["toolbar.SearchToolbar", "toolbar.OperateToolbar", "grid.BudgetGroupGrid", "panel.PlanningTabPanel",
    "grid.PlanningSummaryGrid", "grid.PlanningGrid", "grid.ProjectSummaryGrid", "grid.ProjectGrid",
    "grid.HumanResourcesSummaryGrid", "grid.HumanResourcesMedicalGrid", "grid.HumanResourcesNursingGrid",
    "grid.HumanResourcesTechnologyGrid", "grid.HumanResourcesFunctionGrid",
    "window.ReturnAdjustWindow", "window.PlanningSituationWindow"],
  refs: [
    {ref: "planningSearchToolbar", selector: 'planningSearchToolbar'},
    {ref: 'operateToolbar', selector: 'operateToolbar'},
    {ref: 'budgetGroupGrid', selector: 'budgetGroupGrid'},
    {ref: 'planningTabPanel', selector: 'planningTabPanel'},
    {ref: 'planningSummaryGrid', selector: 'planningSummaryGrid'},
    {ref: 'planningGrid', selector: 'planningGrid'},
    {ref: 'projectSummaryGrid', selector: 'projectSummaryGrid'},
    {ref: 'projectGrid', selector: 'projectGrid'}
  ],
  businessTypeName: "预算编制",
  defaultTimeout: 600000,
  isTabchangeHandle: true,
  init: function() {
    var $this = this;
    $this.control({
      'budgetGroupGrid': {
        select: $this.selectBudgetGroupRow
      },
      'planningTabPanel': {beforetabchange: $this.beforetabchange, tabchange: $this.tabchange},
      'operateToolbar button[action=query]': {
        click: function() {
          $this.query(false)
        }
      },
      'operateToolbar button[action=save]': {click: $this.save},
      'operateToolbar button[action=verifyDivide]': {click: $this.verifyDivide},
      'operateToolbar button[action=commit]': {click: $this.commit},
      'operateToolbar button[action=archive]': {click: $this.archive},
      'operateToolbar button[action=returnAdjust]': {click: $this.returnAdjust},
      'operateToolbar button[xtype=versionSaveButton]': {click: $this.verifyVersionSave},
      'operateToolbar button[xtype=versionManageButton]': {click: $this.verifyVersionManage},
      'operateToolbar button[action=planningSituation]': {click: $this.planningSituation},
      'planningTabPanel button[action=fileUpload]': {click: $this.fileUpload},
      'planningGrid': {
        itemNameClick: this.itemNameClick
      }
    });
    Common.CommonManager.getParams = $this.getParams;
    Common.CommonManager.loadStatus = $this.loadStatus;
    Common.CommonManager.loadGrid = function(isCalculate) {
      $this.loadGrid(isCalculate);
    };
    Common.CommonManager.showDifferent = $this.showDifferent;
    Common.CommonManager.editor.isShowPlanningForLevelOrganizationFlag = true;
  },
  selectBudgetGroupRow: function(grid, record, index, eOpts) {
    this.query(true);
  },
  getMainGrid: function() {
    if (this.getPlanningTabPanel() != undefined) {
      return this.getPlanningTabPanel().getActiveTab();
    } else {
      return null;
    }
  },
  beforetabchange: function(tabPanel, newCard, oldCard, eOpts) {
    var $this = this;
    var panel = tabPanel.up("panel");
    if (oldCard != null && (oldCard.xtype == 'planningGrid') && oldCard.editable && !panel.readOnly) {
      var updatedRecords = oldCard.getStore().getUpdatedRecords();
      if (updatedRecords.length > 0) {
        $this.save();
      }
    }
  },
  tabchange: function(tabPanel, newCard, oldCard, eOpts) {
    var $this = this;
    if ($this.isTabchangeHandle) {
      var params = $this.getParams();
      var budgetGroupViewType = params.budgetGroupViewType;
      if (budgetGroupViewType == undefined || budgetGroupViewType == null) {
        budgetGroupViewType == "";
      }
      var canSave = Common.CommonManager.editor.canSave;
      var canCommit = Common.CommonManager.editor.canCommit;

      tabPanel.getActiveTab().loadData();

      if (canSave && !tabPanel.processPage && newCard.editable
        && budgetGroupViewType != "BudgetSummary" && budgetGroupViewType != "HumanResourcesSummary") {
        this.getOperateToolbar().down("button[action=save]").setVisible(true);
      } else {
        this.getOperateToolbar().down("button[action=save]").setVisible(false);
      }

      if (canSave && !tabPanel.processPage && newCard.isProjectBudget && (budgetGroupViewType == ""
        || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "MonthlySummary"
        || budgetGroupViewType == "ProjectMonthlyAllocation" || budgetGroupViewType == "ProjectMonthlySummary")) {
        this.getOperateToolbar().down("button[action=create]").setVisible(true);
      } else {
        this.getOperateToolbar().down("button[action=create]").setVisible(false);
      }
    }
  },
  loadGrid: function(isCalculate) {
    //修改计算状态
    if (isCalculate) {
      this.updateStatus(null, true);
    } else {
      this.updateStatus(null, false);
    }

    this.loadStatus();
    this.query(false);
  },
  updateStatus: function(newStatus, isCalculate) {
    var $this = this;
    var params = $this.getParams();
    var dataType = '@@Planning@@';

    if (isCalculate == null) {
      isCalculate = false;
    }

    var budgetDataStatuses = Ext.create("Common.data.ApiReader").load({
      api: 'budgetDataStatus',
      joins: [{
        type: 'dictionaryOption',
        fields: {
          names: 'statusName'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'code',
          condition: 'EQUALS',
          type: 'Field',
          value: 'statusCode'
        }, {
          field: 'dictionaryCode',
          condition: 'EQUALS',
          value: 'BudgetStatus'
        }]
      }],
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: params.domainId
      }, {
        field: 'setOfBooksId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.setOfBooksId
      }, {
        field: 'period',
        condition: 'EQUALS',
        type: 'String',
        value: params.period
      }, {
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.organizationId
      }, {
        field: 'businessType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: params.businessType
      }, {
        field: 'dataType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: dataType
      }]
    });

    var isUpdate = false;
    var isAddStatus = true;
    var statusCode = 'Draft';
    var budgetDataStatus;
    if (budgetDataStatuses != null && budgetDataStatuses[0] != null) {
      if (budgetDataStatuses[0].statusCode != undefined && budgetDataStatuses[0].statusCode != '') {
        statusCode = budgetDataStatuses[0].statusCode;
      }
      budgetDataStatus = budgetDataStatuses[0];
      isAddStatus = false;
    }

    if (newStatus != null && newStatus != '') {
      statusCode = newStatus;
      budgetDataStatus.statusCode = newStatus;
      isUpdate = true;
    }

    if (isAddStatus) {
      budgetDataStatus = {
        domainId: params.domainId,
        setOfBooksId: params.setOfBooksId,
        period: params.period,
        organizationId: params.organizationId,
        businessType: params.businessType,
        dataType: dataType,
        statusCode: statusCode
      };
      isUpdate = true;
    }

    if (isCalculate && (statusCode == 'Draft' || statusCode == 'Adjusting'
      || statusCode == 'Aborted' || statusCode == 'ReturnAdjust')) {
      budgetDataStatus.statusCode = statusCode + "Calculate";
      isUpdate = true;
    }
    if (isCalculate != null && !isCalculate && statusCode.indexOf("Calculate") > -1) {
      budgetDataStatus.statusCode = statusCode.substring(0, statusCode.indexOf("Calculate"));
      isUpdate = true;
    }

    if (isUpdate) {
      Ext.create("Common.data.ApiReader")
        .createOrUpdate('budgetDataStatus', [budgetDataStatus], null, false);
    }
  },
  query: function(isLoadGrids) {
    if (isLoadGrids) {
      this.loadGrids(this.getPlanningTabPanel());
    } else {
      var panel = this.getMainGrid();
      if (panel != null) {
        panel.loadData();
      }
    }
  },
  loadStatus: function(func) {
    var $this = this;
    var params = $this.getParams();
    var organizationSortCode = params.organizationSortCode;
    var organizationLeaf = params.organizationLeaf;
    var dataType = '@@Planning@@';

    var budgetDataStatus = Ext.create("Common.data.ApiReader").load({
      api: 'budgetDataStatus',
      joins: [{
        type: 'dictionaryOption',
        fields: {
          names: 'statusName'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'code',
          condition: 'EQUALS',
          type: 'Field',
          value: 'statusCode'
        }, {
          field: 'dictionaryCode',
          condition: 'EQUALS',
          value: 'BudgetStatus'
        }]
      }],
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: params.domainId
      }, {
        field: 'setOfBooksId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.setOfBooksId
      }, {
        field: 'period',
        condition: 'EQUALS',
        type: 'String',
        value: params.period
      }, {
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.organizationId
      }, {
        field: 'businessType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: params.businessType
      }, {
        field: 'dataType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: dataType
      }]
    });

    var statusCode = 'Draft';
    if (budgetDataStatus != null && budgetDataStatus[0] != null
      && budgetDataStatus[0].statusCode != undefined && budgetDataStatus[0].statusCode != '') {
      statusCode = budgetDataStatus[0].statusCode;
    }
    var statusCombo = Ext.getCmp('planning-search-status');
    if (statusCombo != undefined) {
      statusCombo.setValue(statusCode);
    }

    if (statusCode == 'Draft' || statusCode == 'Adjusting'
      || statusCode == 'DraftCalculate' || statusCode == 'AdjustingCalculate') {
      if (organizationLeaf) {
        Common.CommonManager.editor.canSave = true;
      } else {
        Common.CommonManager.editor.canSave = false;
      }
      Common.CommonManager.editor.canCommit = true;
    } else if (statusCode == 'Aborted' || statusCode == 'ReturnAdjust'
      || statusCode == 'AbortedCalculate' || statusCode == 'ReturnAdjustCalculate') {
      if (organizationLeaf) {
        Common.CommonManager.editor.canSave = true;
      } else {
        Common.CommonManager.editor.canSave = false;
      }
      Common.CommonManager.editor.canCommit = true;
    } else if (statusCode == 'Approving') {
      Common.CommonManager.editor.canSave = false;
      Common.CommonManager.editor.canCommit = false;
    } else if (statusCode == 'Approved' || statusCode == 'Submitted' || statusCode == 'FirstTrailApproved'
      || statusCode == 'ReexamineApproved' || statusCode == 'FinalJudgmentApproved') {
      Common.CommonManager.editor.canSave = false;
      Common.CommonManager.editor.canCommit = false;
    }

    if (organizationSortCode.length == 4 && statusCode == 'ReexamineApproved') {
      Common.CommonManager.editor.isShowProcessButton = true;
    } else {
      Common.CommonManager.editor.isShowProcessButton = false;
    }

    if (func != null) {
      func();
    }
  },
  loadGrids: function(tabPanel) {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var organizationSortCode = params.organizationSortCode;
    var organizationLeaf = params.organizationLeaf;
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }

    //设置按钮显示
    if (tabPanel.processPage) {
      Common.CommonManager.editor.canCommit = false;
    }

    if (Common.CommonManager.editor.isShowPlanningForLevelOrganizationFlag) {
      if (params.setOfBooksReadOnly != undefined && params.setOfBooksReadOnly
        && Ext.getCmp('planning-search-setOfBooks') != undefined) {
        Ext.getCmp('planning-search-setOfBooks').setValue(params.setOfBooksId);
        Ext.getCmp('planning-search-setOfBooks').setReadOnly(true);
      }
      if (params.periodReadOnly != undefined && params.periodReadOnly
        && Ext.getCmp('planning-search-period') != undefined) {
        Ext.getCmp('planning-search-period').setValue(params.period);
        Ext.getCmp('planning-search-period').setReadOnly(true);
      }
      if (params.organizationReadOnly != undefined && params.organizationReadOnly
        && Ext.getCmp('planning-search-organizationId') != undefined) {
        var organization = Ext.getCmp('planning-search-organizationId');
        organization.setValue(parseInt(params.organizationReadOnlyId));
        var organizationReadOnlySortCode = '';
        var selectRecord = organization.getStore().getById(parseInt(params.organizationReadOnlyId));
        if (selectRecord != null && typeof(selectRecord["data"]) != 'undefined' && selectRecord.data != null) {
          organizationReadOnlySortCode = '@@' + selectRecord.data.sortCode;
        }
        if (organizationReadOnlySortCode != null && organizationReadOnlySortCode != '') {
          var deleteRecords = [];
          organization.getStore().each(function(record) {
            if (record != null && typeof(record["data"]) != 'undefined' && record.data != null) {
              var sortCode = '@@' + record.data.sortCode;
              if (sortCode.indexOf(organizationReadOnlySortCode) == -1) {
                deleteRecords.push(record);
              }
            }
          });
          for (var i = 0; i < deleteRecords.length; i++) {
            organization.getStore().remove(deleteRecords[i]);
          }
        }
      }
      Common.CommonManager.editor.isShowPlanningForLevelOrganizationFlag = false;
    }

    var canSave = Common.CommonManager.editor.canSave;
    var canCommit = Common.CommonManager.editor.canCommit;

    if (!tabPanel.processPage) {
      this.getOperateToolbar().down("button[action=query]").setVisible(true);
    } else {
      this.getOperateToolbar().down("button[action=query]").setVisible(false);
    }

    if (!tabPanel.processPage && (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary")) {
      if (canCommit) {
        this.getOperateToolbar().down("button[action=commit]").setVisible(true);
      } else {
        this.getOperateToolbar().down("button[action=commit]").setVisible(false);
      }
      if (Common.CommonManager.editor.isShowProcessButton) {
        this.getOperateToolbar().down("button[action=archive]").setVisible(true);
      } else {
        this.getOperateToolbar().down("button[action=archive]").setVisible(false);
      }
      if (organizationLeaf) {
        this.getOperateToolbar().down("button[xtype=exportButton]").setVisible(true);
        this.getOperateToolbar().down("button[action=planningSituation]").setVisible(false);
      } else {
        this.getOperateToolbar().down("button[xtype=exportButton]").setVisible(false);
        this.getOperateToolbar().down("button[action=planningSituation]").setVisible(true);
      }
      if (canSave) {
        this.getOperateToolbar().down("button[xtype=importButton]").setVisible(true);
      } else {
        this.getOperateToolbar().down("button[xtype=importButton]").setVisible(false);
      }
      this.getOperateToolbar().down("button[xtype=versionSaveButton]").setVisible(true);
      this.getOperateToolbar().down("button[xtype=versionManageButton]").setVisible(true);
    } else {
      this.getOperateToolbar().down("button[action=commit]").setVisible(false);
      this.getOperateToolbar().down("button[action=archive]").setVisible(false);
      this.getOperateToolbar().down("button[xtype=exportButton]").setVisible(false);
      this.getOperateToolbar().down("button[xtype=importButton]").setVisible(false);
      this.getOperateToolbar().down("button[xtype=versionSaveButton]").setVisible(false);
      this.getOperateToolbar().down("button[xtype=versionManageButton]").setVisible(false);
      this.getOperateToolbar().down("button[action=planningSituation]").setVisible(false);
    }

    if (canSave && !tabPanel.processPage) {
      this.getOperateToolbar().down("button[xtype=calculateButton]").setVisible(true);
    } else {
      this.getOperateToolbar().down("button[xtype=calculateButton]").setVisible(false);
    }

    //显示和隐藏退回调整按钮
    var statusCode = '';
    var statusCombo = Ext.getCmp('planning-search-status');
    if (statusCombo != undefined) {
      statusCode = statusCombo.getValue();
    }
    if ((canCommit || (organizationSortCode.length == 4 && statusCode == 'ReexamineApproved'))
      && !organizationLeaf && !tabPanel.processPage) {
      var subOrganizationStatuses = Ext.create('Common.data.ApiReader').load({
        api: 'budgetDataStatus',
        fields: {statusCode: 'statusCode'},
        joins: [{
          type: 'organization',
          fields: {
            parentId: 'parentId',
            name: 'organizationName',
            sortCode: 'sortCode'
          },
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'domainId'
          }, {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }]
        }],
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        }, {
          field: 'parentId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        }, {
          field: 'period',
          condition: 'EQUALS',
          type: 'String',
          value: params.period
        }, {
          field: 'statusCode',
          condition: 'IN',
          type: 'String',
          value: ['Approved', 'Submitted', 'FirstTrailApproved', 'ReexamineApproved', 'FinalJudgmentApproved']
        }, {
          field: 'businessType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: params.businessType
        }, {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: '@@Planning@@'
        }]
      });

      if (subOrganizationStatuses != null && subOrganizationStatuses.length > 0) {
        this.getOperateToolbar().down("button[action=returnAdjust]").setVisible(true);
      } else {
        this.getOperateToolbar().down("button[action=returnAdjust]").setVisible(false);
      }
    } else {
      this.getOperateToolbar().down("button[action=returnAdjust]").setVisible(false);
    }

    if (canSave && !tabPanel.processPage && (budgetGroupViewType == "HumanResourcesSummary"
      || budgetGroupViewType == "HumanResourcesMedical" || budgetGroupViewType == "HumanResourcesNursing"
      || budgetGroupViewType == "HumanResourcesTechnology" || budgetGroupViewType == "HumanResourcesFunction")) {
      this.getOperateToolbar().down("button[action=verifyDivide]").setVisible(true);
    } else {
      this.getOperateToolbar().down("button[action=verifyDivide]").setVisible(false);
    }

    var isVisible = false;
    var toolbarItems = this.getOperateToolbar().items;
    for (var i = 0; i < toolbarItems.length; i++) {
      if (toolbarItems.get(i).text != undefined && !toolbarItems.get(i).hidden) {
        isVisible = true;
        break;
      }
    }
    this.getOperateToolbar().setVisible(isVisible);

    //显示和隐藏附件按钮
    if (organizationSortCode.length == 8) {
      tabPanel.down("button[action=fileUpload]").setVisible(true);
    } else {
      tabPanel.down("button[action=fileUpload]").setVisible(false);
    }

    var activePanelXtype = null;
    var activePanel = null;
    if (tabPanel.getActiveTab() != null) {
      activePanelXtype = tabPanel.getActiveTab().xtype;
    }

    var items = [];
    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary") {
      $this.createNewPanel(items, "预算汇总", "Budget.view.grid.PlanningSummaryGrid", true, false);
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "MonthlySummary"
      || budgetGroupViewType == "BudgetMonthlyAllocation" || budgetGroupViewType == "BudgetMonthlySummary") {
      $this.createNewPanel(items, "非项目预算", "Budget.view.grid.PlanningGrid", true, true);
    }

    if (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary"
      || budgetGroupViewType == "MonthlyAllocation" || budgetGroupViewType == "MonthlySummary"
      || budgetGroupViewType == "ProjectMonthlyAllocation" || budgetGroupViewType == "ProjectMonthlySummary") {
      $this.createNewPanel(items, "项目预算", "Budget.view.grid.ProjectSummaryGrid", true, false);
      $this.createNewPanel(items, "关联项目", "Budget.view.grid.ProjectGrid", true, false);
    }

    if (budgetGroupViewType == "HumanResourcesSummary") {
      $this.createNewPanel(items, "人力汇总", "Budget.view.grid.HumanResourcesSummaryGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesMedical") {
      $this.createNewPanel(items, "医疗人员", "Budget.view.grid.HumanResourcesMedicalGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesNursing") {
      $this.createNewPanel(items, "护理人员", "Budget.view.grid.HumanResourcesNursingGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesTechnology") {
      $this.createNewPanel(items, "医技人员", "Budget.view.grid.HumanResourcesTechnologyGrid", true, true);
    }

    if (budgetGroupViewType == "HumanResourcesFunction") {
      $this.createNewPanel(items, "职能人员", "Budget.view.grid.HumanResourcesFunctionGrid", true, true);
    }

    if (!tabPanel.processPage && (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary")) {
      $this.createNewPanel(items, "审批信息", "Budget.view.grid.ProcessGird", true, false);
    }

    $this.isTabchangeHandle = false;
    tabPanel.removeAll();
    $this.isTabchangeHandle = true;

    for (var i = 0; i < items.length; i++) {
      tabPanel.add(items[i]);
      if (items[i].xtype == activePanelXtype) {
        activePanel = items[i];
      }
    }

    if (activePanel != null) {
      tabPanel.setActiveTab(activePanel);
    } else {
      tabPanel.setActiveTab(items[0]);
    }
  },
  createNewPanel: function(items, title, type, show, editable, closable) {
    if (show) {
      var initConfig = {border: false, editable: editable, title: title, closable: closable};
      var item = Ext.create(type, initConfig);
      if (items != null) {
        items.push(item);
      }
      return item;
    }
  },
  createProjectTabPanel: function(id, title, budgetItemId) {
    var $this = this;
    var params = $this.getParams();
    var budgetGroupViewType = params.budgetGroupViewType;
    if (budgetGroupViewType == null || budgetGroupViewType == undefined) {
      budgetGroupViewType = "";
    }

    var tabPanel = $this.getPlanningTabPanel();
    var items = tabPanel.items;
    var isAdd = true;
    for (var i = 0; i < items.length; i++) {
      if (items.get(i).id == id) {
        isAdd = false;
        tabPanel.setActiveTab(items.get(i));
      }
    }

    if (isAdd) {
      if (!tabPanel.processPage && (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary")) {
        var processItem = items.get(items.length - 1);
        tabPanel.remove(processItem);
      }

      var projectItem = {
        id: id,
        xtype: "projectGrid",
        title: title,
        budgetItemId: budgetItemId,
        closable: true
      };

      tabPanel.add(projectItem);
      if (!tabPanel.processPage && (budgetGroupViewType == "" || budgetGroupViewType == "BudgetSummary")) {
        tabPanel.add(processItem);
      }
      tabPanel.setActiveTab(Ext.getCmp(id));
    }
  },
  itemNameClick: function(budgetItemId, budgetItemName) {
    this.createProjectTabPanel("projectGrid-budgetItem-" + budgetItemId, "【" + budgetItemName + "】关联项目", budgetItemId);
  },
  save: function() {
    var $this = this;
    var panel = $this.getMainGrid();
    var params = $this.getParams();
    if (panel != null) {
      //处理预算编制数据
      var result = panel.handlePlanningData();
      if (result.message != '') {
        Common.Msg.Alert(result.message);
        return false;
      }

      //删除年度数据
      if (result.deleteAnnualData.length > 0) {
        Ext.create("Common.data.ApiReader")
          .directDelete_('annualCostPlanningData', result.deleteAnnualData, null);
      }

      //删除月度数据
      if (result.deleteMonthlyData.length > 0) {
        Ext.create("Common.data.ApiReader")
          .directDelete_('monthlyCostPlanningData', result.deleteMonthlyData, null);
      }

      //保存年度数据
      if (result.annualEditData.length > 0) {
        Ext.create("Common.data.ApiReader")
          .createOrUpdate('annualCostPlanningData', result.annualEditData, null, false);
      }

      //保存月度数据
      if (result.monthlyEditData.length > 0) {
        Ext.create("Common.data.ApiReader")
          .createOrUpdate('monthlyCostPlanningData', result.monthlyEditData, null, false);
      }

      //修改为计算状态
      this.updateStatus(null, true);
      this.loadStatus();
      $this.query(false);
    }
  },
  commit: function() {
    var $this = this;
    var params = $this.getParams();
    var dataType = '@@Planning@@';

    //验证状态非审批中，才能提交
    if (!$this.verifyOrganizationStatus("提交")) {
      return false;
    }

    //验证非末级组织的下级部门是否全部审批通过或已提交
    if (!params.organizationLeaf && !$this.verifySubOrganizationStatus("提交")) {
      return false;
    }

    //验证人力资源部的人力汇总和预算汇总是否相等
    if (params.organizationCode == '301' && !$this.verifyHumanResourcesData()) {
      return false;
    }

    //检验附件
    if (params.organizationSortCode.length == 8) {
      var document = [];
      var organizations = Ext.create("Common.data.ApiReader").load({
        api: "organization",
        fields: {
          resourceId: "resourceId"
        },
        conditions: [
          {
            field: 'domainId',
            condition: 'EQUALS',
            value: params.domainId
          }, {
            field: 'id',
            condition: 'EQUALS',
            value: params.organizationId
          }
        ]
      });

      Ext.Ajax.request({
        method: "GET",
        async: false,
        url: contextPath + '/foundation/service/document',
        params: {resourceId: organizations[0].resourceId},
        success: function(response, operation) {
          var result = Ext.JSON.decode(response.responseText);
          if (result.success) {
            document = result.data;
          } else {
            Common.Msg.Alert(result.message);
          }
        }
      });

      if (document.length == 0) {
        Common.Msg.Alert("请点击【附件】按钮上传附件，再提交！");
        return false;
      }
    }

    //末级部门不需要审批，直接审批通过
    if (params.organizationSortCode.length == 12) {
      var versionId;
      var parameters = ",organizationId=" + params.organizationId
        + ",period=" + params.period
        + ",setOfBooksId=" + params.setOfBooksId
        + ",dataType=@@SummaryPlanning@@Cost@@,dataType=@@SummaryPlanning@@Expenses@@"
        + ",dataType=@@Planning@@Cost@@,dataType=@@Planning@@Expenses@@,dataType=@@Planning@@Public@@"
        + ",dataType=@@ProjectPlanning@@Cost@@,dataType=@@ProjectPlanning@@Expenses@@"
        + ",dataType=@@Project@@Cost@@,dataType=@@Project@@Expenses@@,dataType=@@Project@@Public@@"
        + ",dataType=@@Project@@"
        + ",dataType=@@HumanResourcesSummary@@Cost@@,dataType=@@HumanResourcesMedical@@Cost@@"
        + ",dataType=@@HumanResourcesNursing@@Cost@@,dataType=@@HumanResourcesTechnology@@Cost@@"
        + ",dataType=@@HumanResourcesFunction@@Expenses@@";

      var callback = function(result) {
        versionId = result[0].data.id;
      };

      Ext.create("Common.data.ApiReader").createOrUpdate("version", [{
        type: "approved",
        parameters: parameters,
        domainId: params.domainId,
        domainTypes: "annualCostPlanningData,monthlyCostPlanningData,relatedProjectItem,project"
      }], callback);

      var budgetDataStatus = Ext.create("Common.data.ApiReader").load({
        api: 'budgetDataStatus',
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        }, {
          field: 'period',
          condition: 'EQUALS',
          type: 'String',
          value: params.period
        }, {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        }, {
          field: 'businessType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: params.businessType
        }, {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: dataType
        }]
      });

      if (budgetDataStatus.length != 0) {
        budgetDataStatus[0].statusCode = "Submitted";
        budgetDataStatus[0].versionId = versionId;
      } else {
        budgetDataStatus[0] = {
          period: params.period,
          statusCode: "Submitted",
          versionId: versionId,
          dataType: "@@Planning@@",
          domainId: params.domainId,
          businessType: params.businessType,
          setOfBooksId: params.setOfBooksId,
          organizationId: params.organizationId
        }
      }
      Ext.create("Common.data.ApiReader").createOrUpdate("budgetDataStatus", [budgetDataStatus[0]]);

      var callbackToQuery = function(result) {
        $this.loadStatus();
        $this.query(true);
      };

      var organization = Ext.create("Common.data.ApiReader").load({
        api: 'organization',
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.organizationId
        }]
      });

      Ext.create("Common.data.ApiReader").createOrUpdate("budgetSummaryRecord", [{
        period: params.period,
        dataType: "@@Planning@@",
        domainId: params.domainId,
        setOfBooksId: params.setOfBooksId,
        organizationId: organization[0].parentId
      }], callbackToQuery);

      return false;
    }

    Common.CommonManager.showMask();
    Ext.Ajax.request({
      url: contextPath + '/bbudget/service/planning/task/start',
      timeout: 60000,
      params: params,
      method: 'POST',
      callback: function(options, success, response) {
        Common.CommonManager.hideMask();
        var responseJson = Ext.JSON.decode(response.responseText);
        if (responseJson.success) {
          for (var i = 0; i < responseJson.data.length; i++) {
            var processInstanceId = responseJson.data[i].id;
            var taskId = Common.WorkFlowManager.getTodoTask(processInstanceId);
            /*openBrowserWindow({
             isFull: true,
             url: contextPath + "/bbudget/service/planning/task/ui" + "?taskId=" + taskId
             });*/
            //getTaskDirection(taskId);
          }
          $this.loadStatus();
          $this.query(true);
        } else {
          Common.Msg.Error(responseJson.message);
        }
      }
    });
  },
  archive: function() {
    var $this = this;
    var params = $this.getParams();
    params.isArchive = true;

    //只有顶级组织才能发起终审批
    if (params.organizationSortCode.length != 4) {
      return false;
    }

    //验证状态非审批中，才能提交
    if (!$this.verifyOrganizationStatus("终审")) {
      return false;
    }

    Common.CommonManager.showMask();
    Ext.Ajax.request({
      url: contextPath + '/bbudget/service/planning/task/start',
      timeout: 60000,
      params: params,
      method: 'POST',
      callback: function(options, success, response) {
        Common.CommonManager.hideMask();
        var responseJson = Ext.JSON.decode(response.responseText);
        if (responseJson.success) {
          for (var i = 0; i < responseJson.data.length; i++) {
            var processInstanceId = responseJson.data[i].id;
            var taskId = Common.WorkFlowManager.getTodoTask(processInstanceId);
            //getTaskDirection(taskId);
          }
          $this.loadStatus();
          $this.query(true);
        } else {
          Common.Msg.Error(responseJson.message);
        }
      }
    });
  },
  verifyOrganizationStatus: function(operation) {
    var $this = this;
    var params = $this.getParams();
    var dataType = '@@Planning@@';
    if (operation == null) {
      operation = "提交";
    }

    //验证当前部门是否可以提交
    var budgetDataStatus = Ext.create("Common.data.ApiReader").load({
      api: 'budgetDataStatus',
      joins: [{
        type: 'dictionaryOption',
        fields: {
          names: 'statusName'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'code',
          condition: 'EQUALS',
          type: 'Field',
          value: 'statusCode'
        }, {
          field: 'dictionaryCode',
          condition: 'EQUALS',
          value: 'BudgetStatus'
        }]
      }],
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: params.domainId
      }, {
        field: 'setOfBooksId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.setOfBooksId
      }, {
        field: 'period',
        condition: 'EQUALS',
        type: 'String',
        value: params.period
      }, {
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.organizationId
      }, {
        field: 'businessType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: params.businessType
      }, {
        field: 'dataType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: dataType
      }]
    });

    if (budgetDataStatus != null && budgetDataStatus[0] != null
      && budgetDataStatus[0].statusCode != undefined && budgetDataStatus[0].statusCode != "") {
      if (operation != null && operation == "终审") {
        if (budgetDataStatus[0].statusCode != "ReexamineApproved") {
          Common.Msg.Alert(Ext.String.format("当前流程状态为{0}，请刷新页面！", budgetDataStatus[0].statusName));
          return false;
        }
      } else {
        if (budgetDataStatus[0].statusCode.indexOf("Calculate") > -1) {
          Common.Msg.Alert("请点击【汇总计算】按钮汇总计算，再" + operation + "！");
          return false;
        }

        if (budgetDataStatus[0].statusCode != "Draft" && budgetDataStatus[0].statusCode != "Aborted"
          && budgetDataStatus[0].statusCode != "Adjusting" && budgetDataStatus[0].statusCode != "ReturnAdjust"
          && operation != "保存版本") {
          Common.Msg.Alert(Ext.String.format("当前流程状态为{0}，请刷新页面！", budgetDataStatus[0].statusName));
          return false;
        }
      }
    }

    return true;
  },
  verifySubOrganizationStatus: function(operation, isShowAlert) {
    var $this = this;
    var params = $this.getParams();
    var dataType = '@@Planning@@';
    if (operation == null) {
      operation = "提交";
    }
    if (isShowAlert == null) {
      isShowAlert = true;
    }

    //检查系统配置，是否校验下级组织审批通过
    var resourceExtInstance = Ext.create("Common.data.ApiReader").load({
      api: "resourceExtInstance",
      conditions: [
        {
          field: "resourceExtOptionId",
          condition: "EQUALS",
          type: "String",
          value: "enabled-verify-subOrganization-approved"
        },
        {
          field: "resourceExtSchemaId",
          condition: "EQUALS",
          type: "String",
          value: "pkuih-configuration"
        }
      ]
    });
    if (resourceExtInstance != null && resourceExtInstance[0] != null
      && resourceExtInstance[0].instanceValue != undefined && resourceExtInstance[0].instanceValue == "false") {
      return true;
    }

    //非末级组织，验证下级部门，全部初审通过或已提交
    var subOrganizationStatuses = Ext.create("Common.data.ApiReader").load({
      api: 'organization',
      fields: {
        id: 'id',
        name: 'name',
        parentId: 'parentId'
      },
      joins: [{
        type: 'budgetDataStatus',
        fields: {
          period: 'period',
          dataType: 'dataType',
          statusCode: 'statusCode',
          setOfBooksId: 'setOfBooksId',
          businessType: 'businessType'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'organizationId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'id'
        }, {
          field: 'setOfBooksId',
          condition: 'EQUALS',
          type: 'Integer',
          value: params.setOfBooksId
        }, {
          field: 'period',
          condition: 'EQUALS',
          type: 'String',
          value: params.period
        }, {
          field: 'businessType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: params.businessType
        }, {
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: dataType
        }]
      }],
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: params.domainId
      }, {
        field: 'parentId',
        condition: 'IN',
        type: 'Integer',
        value: params.organizationId
      }]
    });

    var errorMessage = "";
    for (var i = 0; i < subOrganizationStatuses.length; i++) {
      var subOrganizationStatus = subOrganizationStatuses[i];
      if (!(subOrganizationStatus != null && subOrganizationStatus.statusCode != undefined
        && (subOrganizationStatus.statusCode == 'Approved' || subOrganizationStatus.statusCode == 'Submitted'
        || subOrganizationStatus.statusCode == 'FirstTrailApproved' || subOrganizationStatus.statusCode == 'ReexamineApproved'
        || subOrganizationStatus.statusCode == 'FinalJudgmentApproved'))) {
        if (errorMessage != null && errorMessage != "") {
          errorMessage += Ext.String.format("、【{0}】", subOrganizationStatus.name);
        } else {
          errorMessage += Ext.String.format("【{0}】", subOrganizationStatus.name);
        }
      }
    }

    if (errorMessage != "") {
      if (isShowAlert) {
        if (params.organizationSortCode.length == 4) {
          errorMessage += "未审批通过，当前组织暂时不能" + operation + "！<br/>";
        } else if (params.organizationSortCode.length == 8) {
          errorMessage += "未提交，当前组织暂时不能" + operation + "！<br/>";
        }
        Common.Msg.Alert(errorMessage);
      }
      return false;
    }

    return true;
  },
  verifyDivide: function() {
    //验证状态非审批中，才能提交
    if (!this.verifyOrganizationStatus("验证划分")) {
      return false;
    }

    if (this.verifyHumanResourcesData()) {
      Common.Msg.Alert("验证通过！");
    }
  },
  verifyHumanResourcesData: function() {
    var $this = this;
    var params = $this.getParams();

    var findMonthlyCostPlanningData = function(budgetItemGroupCode, budgetDataGroupCode, dataType, isNum) {
      var dynamicQuery = {
        api: 'budgetItemGroupItem',
        fields: {
          alias: 'alias',
          sortCode: 'sortCode'
        },
        joins: [{
          type: 'budgetItemGroup',
          fields: {
            code: 'budgetItemGroupCode'
          },
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'domainId'
          }, {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'budgetItemGroupId'
          }]
        }, {
          type: "BudgetItem",
          fields: {
            name: "name",
            code: "code",
            leaf: "leaf"
          },
          conditions: [
            {
              field: "id",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }, {
          type: 'monthlyCostPlanningData',
          fields: {
            amount: 'amount',
            period: 'period',
            budgetObjectId: 'budgetObjectId'
          },
          joins: [{
            type: 'budgetItemGroup',
            fields: {
              code: 'budgetDataGroupCode'
            },
            conditions: [{
              field: 'domainId',
              condition: 'EQUALS',
              type: 'Field',
              value: 'domainId'
            }, {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'budgetItemGroupId'
            }]
          }],
          conditions: [{
            field: 'domainId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'domainId'
          }, {
            field: 'setOfBooksId',
            condition: 'EQUALS',
            type: 'Integer',
            value: params.setOfBooksId
          }, {
            field: 'period',
            condition: 'STARTS_WITH',
            type: 'String',
            value: params.period.substr(0, 4)
          }, {
            field: 'organizationId',
            condition: 'EQUALS',
            type: 'Integer',
            value: params.organizationId
          }, {
            field: 'budgetObjectType',
            condition: 'EQUALS',
            type: 'String',
            value: 'BudgetItem'
          }, {
            field: 'dataType',
            condition: 'STARTS_WITH',
            type: 'String',
            value: dataType
          }, {
            field: 'budgetObjectId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'budgetItemId'
          }]
        }],
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'String',
          value: params.domainId
        }, {
          field: 'budgetItemGroupCode',
          condition: 'EQUALS',
          type: 'String',
          value: budgetItemGroupCode
        }, {
          field: 'budgetDataGroupCode',
          condition: 'EQUALS',
          type: 'String',
          value: budgetDataGroupCode
        }],
        sorts: [{
          field: 'sortCode',
          direction: 'ASC'
        }, {
          field: 'period',
          direction: 'ASC'
        }]
      };

      if(isNum) {
        dynamicQuery.conditions.push({
          field: 'code',
          condition: 'EQUALS',
          type: 'String',
          value: 'YS5000'
        });
      }else {
        dynamicQuery.conditions.push({
          field: 'code',
          condition: 'NOT_EQUALS',
          type: 'String',
          value: 'YS5000'
        });
      }

      return Ext.create("Common.data.ApiReader").load(dynamicQuery);
    };

    var errorMessage = "";

    var summaryPlanningList = findMonthlyCostPlanningData("R00", "Z00", "@@SummaryPlanning@@", false);
    var humanResourcesSummaryList = findMonthlyCostPlanningData("R00", "R00", "@@HumanResourcesSummary@@", false);
    for (var i = 0; i < summaryPlanningList.length; i++) {
      var summaryPlanning = summaryPlanningList[i];
      if (summaryPlanning.leaf && summaryPlanning.period != null && summaryPlanning.period != '') {
        var name = summaryPlanning.name;
        var month = summaryPlanning.period.charAt(4) == 0
          ? summaryPlanning.period.substr(5) : summaryPlanning.period.substr(4);
        var planningAmount = 0.00;
        if (planningAmount != null || planningAmount != '') {
          planningAmount = summaryPlanning.amount;
        }
        var humanResourcesAmount = 0.00;
        for (var j = 0; j < humanResourcesSummaryList.length; j++) {
          var humanResourcesSummary = humanResourcesSummaryList[j];
          if (humanResourcesSummary.period != null && humanResourcesSummary.period != ''
            && humanResourcesSummary.period == summaryPlanning.period
            && humanResourcesSummary.budgetObjectId == summaryPlanning.budgetObjectId
            && humanResourcesSummary.amount != null && humanResourcesSummary.amount != '') {
            humanResourcesAmount = humanResourcesSummary.amount;
            humanResourcesSummaryList[j].isVerify = true;
            break;
          }
        }
        if (parseFloat(humanResourcesAmount).toFixed(2) != parseFloat(planningAmount).toFixed(2)) {
          errorMessage += Ext.String.format("【{0}】{1}月人力资源汇总 {2} 不等于预算汇总 {3}；<br/>", name, month,
            Ext.util.Format.number(humanResourcesAmount, '0,0.00'),
            Ext.util.Format.number(planningAmount, '0,0.00'));
        }
      }
    }

    for (var j = 0; j < humanResourcesSummaryList.length; j++) {
      var humanResourcesSummary = humanResourcesSummaryList[j];
      if (humanResourcesSummary.leaf && humanResourcesSummary.period != null && humanResourcesSummary.period != ''
        && (humanResourcesSummary.isVerify == undefined || !humanResourcesSummary.isVerify)) {
        var name = humanResourcesSummary.name;
        var month = humanResourcesSummary.period.charAt(4) == 0
          ? humanResourcesSummary.period.substr(5) : humanResourcesSummary.period.substr(4);
        var humanResourcesAmount = 0.00;
        if (humanResourcesAmount != null || humanResourcesAmount != '') {
          humanResourcesAmount = humanResourcesSummary.amount;
        }
        var planningAmount = 0.00;
        for (var i = 0; i < summaryPlanningList.length; i++) {
          var summaryPlanning = summaryPlanningList[i];
          if (summaryPlanning.period != null && summaryPlanning.period != ''
            && summaryPlanning.period == humanResourcesSummary.period
            && summaryPlanning.budgetObjectId == humanResourcesSummary.budgetObjectId
            && summaryPlanning.amount != null && summaryPlanning.amount != '') {
            planningAmount = summaryPlanning.amount;
            break;
          }
        }
        if (parseFloat(humanResourcesAmount).toFixed(2) != parseFloat(planningAmount).toFixed(2)) {
          errorMessage += Ext.String.format("【{0}】{1}月人力资源汇总 {2} 不等于预算汇总 {3}；<br/>", name, month,
            Ext.util.Format.number(humanResourcesAmount, '0,0.00'),
            Ext.util.Format.number(planningAmount, '0,0.00'));
        }
      }
    }

    var summaryPlanningNum = findMonthlyCostPlanningData("R00", "Z01", "@@Planning@@", true);
    var humanResourcesSummaryNum = findMonthlyCostPlanningData("R00", "R00", "@@HumanResourcesSummary@@", true);
    for (var i = 0; i < summaryPlanningNum.length; i++) {
      var summaryPlanning = summaryPlanningNum[i];
      if (summaryPlanning.leaf && summaryPlanning.period != null && summaryPlanning.period != '') {
        var name = summaryPlanning.name;
        var month = summaryPlanning.period.charAt(4) == 0
          ? summaryPlanning.period.substr(5) : summaryPlanning.period.substr(4);
        var planningAmount = 0.00;
        if (planningAmount != null || planningAmount != '') {
          planningAmount = summaryPlanning.amount;
        }
        var humanResourcesAmount = 0.00;
        for (var j = 0; j < humanResourcesSummaryNum.length; j++) {
          var humanResourcesSummary = humanResourcesSummaryNum[j];
          if (humanResourcesSummary.period != null && humanResourcesSummary.period != ''
            && humanResourcesSummary.period == summaryPlanning.period
            && humanResourcesSummary.budgetObjectId == summaryPlanning.budgetObjectId
            && humanResourcesSummary.amount != null && humanResourcesSummary.amount != '') {
            humanResourcesAmount = humanResourcesSummary.amount;
            humanResourcesSummaryNum[j].isVerify = true;
            break;
          }
        }
        if (parseFloat(humanResourcesAmount).toFixed(2) != parseFloat(planningAmount).toFixed(2)) {
          errorMessage += Ext.String.format("【{0}】{1}月人力资源汇总 {2} 不等于人员费用 {3}；<br/>", name, month,
            Ext.util.Format.number(humanResourcesAmount, '0,0.00'),
            Ext.util.Format.number(planningAmount, '0,0.00'));
        }
      }
    }

    for (var j = 0; j < humanResourcesSummaryNum.length; j++) {
      var humanResourcesSummary = humanResourcesSummaryNum[j];
      if (humanResourcesSummary.leaf && humanResourcesSummary.period != null && humanResourcesSummary.period != ''
        && (humanResourcesSummary.isVerify == undefined || !humanResourcesSummary.isVerify)) {
        var name = humanResourcesSummary.name;
        var month = humanResourcesSummary.period.charAt(4) == 0
          ? humanResourcesSummary.period.substr(5) : humanResourcesSummary.period.substr(4);
        var humanResourcesAmount = 0.00;
        if (humanResourcesAmount != null || humanResourcesAmount != '') {
          humanResourcesAmount = humanResourcesSummary.amount;
        }
        var planningAmount = 0.00;
        for (var i = 0; i < summaryPlanningNum.length; i++) {
          var summaryPlanning = summaryPlanningNum[i];
          if (summaryPlanning.period != null && summaryPlanning.period != ''
            && summaryPlanning.period == humanResourcesSummary.period
            && summaryPlanning.budgetObjectId == humanResourcesSummary.budgetObjectId
            && summaryPlanning.amount != null && summaryPlanning.amount != '') {
            planningAmount = summaryPlanning.amount;
            break;
          }
        }
        if (parseFloat(humanResourcesAmount).toFixed(2) != parseFloat(planningAmount).toFixed(2)) {
          errorMessage += Ext.String.format("【{0}】{1}月人力资源汇总 {2} 不等于人员费用 {3}；<br/>", name, month,
            Ext.util.Format.number(humanResourcesAmount, '0,0.00'),
            Ext.util.Format.number(planningAmount, '0,0.00'));
        }
      }
    }

    if (errorMessage != "") {
      Common.Msg.Alert(errorMessage);
      return false;
    }

    return true;
  },
  returnAdjust: function() {
    var $this = this;

    Ext.widget('returnAdjustWindow').showAndRevertFunc(
      function() {
        $this.loadStatus();
        $this.query(true);
      }
    );
  },
  verifyVersionSave: function() {
    var $this = this;
    var params = $this.getParams();

    //验证状态非审批中，才能提交
    if (!$this.verifyOrganizationStatus("保存版本")) {
      return false;
    }

    //验证非末级组织的下级部门是否全部审批通过或已提交
    if (!params.organizationLeaf && !$this.verifySubOrganizationStatus("保存版本")) {
      return false;
    }
  },
  verifyVersionManage: function() {
    var $this = this;
    var params = $this.getParams();
    var canCommit = Common.CommonManager.editor.canCommit;
    var isEdit = true;

    //验证非末级组织的下级部门是否全部审批通过或已提交
    if (!params.organizationLeaf && !$this.verifySubOrganizationStatus("应用版本", false)) {
      isEdit = false;
    }

    if (canCommit && isEdit) {
      this.getOperateToolbar().down("button[xtype=versionManageButton]").showUseVersion = true;
    } else {
      this.getOperateToolbar().down("button[xtype=versionManageButton]").showUseVersion = false;
    }
  },
  planningSituation: function() {
    var $this = this;

    Ext.widget('planningSituationWindow').showAndRevertFunc(
      function() {

      }
    );
  },
  fileUpload: function() {
    var $this = this;
    var params = $this.getParams();
    var resourceId = '';
    var isEdit = false;
    if (Common.CommonManager.editor.canCommit) {
      isEdit = true;
    }

    var organizations = Ext.create("Common.data.ApiReader").load({
      api: "organization",
      fields: {
        resourceId: "resourceId"
      },
      conditions: [
        {
          field: 'domainId',
          condition: 'EQUALS',
          value: params.domainId
        }, {
          field: 'id',
          condition: 'EQUALS',
          value: params.organizationId
        }
      ]
    });

    if (organizations != null && organizations[0] != null
      && organizations[0].resourceId != null && organizations[0].resourceId != '') {
      resourceId = organizations[0].resourceId;
    }

    if (resourceId != "") {
      Ext.widget('fileUploadWindow').showAndRevertFunc(resourceId, isEdit, null);
    }
  },
  showDifferent: function(ids) {
    var $this = this;
    var params = $this.getParams();
    var versionIds = [];

    if (ids.length == 0) {
      Common.Msg.Alert("请至少选择一个版本进行对比！");
      return;
    }

    if (ids.length <= 2) {
      for (var i = 0; i < ids.length; i++) {
        versionIds.push(ids[i].data.id);
      }
      var params = Common.CommonManager.getParams();
      var versionSetOfBooksId = params.setOfBooksId;
      var versionPeriod = params.period;
      var versionOrganizationId = params.organizationId;
      var url = contextPath + "/pkuih/budget/compare/ui?versionIds=" + versionIds
        + "&setOfBooksId="+versionSetOfBooksId+"&period="+versionPeriod+"&organizationId="+versionOrganizationId;
      openBrowserWindow({isFull: true, url: url});
    } else {
      Common.Msg.Alert("请选择最多两个版本进行对比，如选一个将与当前版本进行对比！");
    }
  },
  getParams: function() {
    return Budget.controller.PlanningController.getParams();
  },
  statics: {
    getParams: function() {
      var params = {};
      for (var str in pageInitParam) {
        params[str] = pageInitParam[str];
      }

      //是否只读
      if (Common.CommonManager.editor.isShowPlanningForLevelOrganizationFlag) {
        if (params.setOfBooksId != undefined && params.setOfBooksId != null && params.setOfBooksId > 0) {
          params.setOfBooksReadOnly = true;
        }
        if (params.period != undefined && params.period != null && params.period != '') {
          params.periodReadOnly = true;
        }
        if (params.organizationId != undefined && params.organizationId != null && params.organizationId > 0) {
          params.organizationReadOnly = true;
          params.organizationReadOnlyId = params.organizationId;
        }
      }

      var queryButton = Ext.getCmp('planning-search-queryButton');
      if (queryButton != null && queryButton != undefined) {
        params.setOfBooksId = queryButton.getParams().setOfBooksId;
        params.period = queryButton.getParams().period;
        params.organizationId = queryButton.getParams().organizationId;
        params.organizationCode = queryButton.getParams().organizationCode;
        params.organizationSortCode = queryButton.getParams().organizationSortCode;
        params.organizationLeaf = queryButton.getParams().organizationLeaf;
        params.budgetType = queryButton.getParams().budgetType;
      }

      //获取预算模版相关信息
      var budgetGroupId = -1;
      var budgetGroupCode = '';
      var budgetGroupViewType = '';
      var budgetGroup = Ext.ComponentQuery.query('budgetGroupGrid')[0];
      if (budgetGroup != null && budgetGroup != undefined) {
        var budgetGroupRecord = budgetGroup.getSelectionModel().getLastSelected();
        if (budgetGroupRecord != null && budgetGroupRecord != undefined) {
          budgetGroupId = budgetGroupRecord.data.id;
          budgetGroupCode = budgetGroupRecord.data.code;
          budgetGroupViewType = budgetGroupRecord.data.viewType;
        }
      }
      params.budgetGroupId = budgetGroupId;
      params.budgetGroupCode = budgetGroupCode;
      params.budgetGroupViewType = budgetGroupViewType;

      return params;
    }
  }
});
