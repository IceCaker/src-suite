Ext.define("Pkuih.Common.view.ProjectItemWindow", {
  extend: 'Ext.window.Window',
  alias: "widget.projectItemWindow",
  title: '费用项',
  layout: 'fit',
  modal: true,
  shadow: false,
  plain: true,
  resizable: false,
  width: 800,
  height: 265,
  closeAction: "destroy",
  maximizable: false,
  items: [{
    xtype: "projectItemPanel",
    editable: true,
    margin: "0 0 0 0",
    border: false
  }],
  buttons: [
    {
      text: "保存",
      action: "save",
      handler: function () {
        this.up("window").saveProjectItem();
      }
    },
    {
      text: "取消",
      action: "close",
      handler: function () {
        this.up("window").close();
      }
    }
  ],
  initComponent: function () {
    Ext.create("Pkuih.Common.view.ProjectItemForm");
    Ext.create("Pkuih.Common.view.ProjectItemPanel");
    this.callParent(arguments);
  },
  loadProjectItemGrid: function () {

  },
  loadProjectItem: function(projectItemId, versionId) {
    var $this = this;
    var form = this.down("form");
    form.getForm().reset();

    var firstHalfData = [];
    var secondHalfData = [];
    if (projectItemId > 0) {
      var conditions = [{
        field: 'id',
        condition: 'EQUALS',
        value: projectItemId
      }];

      var joins = [{
        type: 'projectItemOther',
        fields: {
          levelCode: 'levelCode',
          levelName: 'levelName',
          classCode: 'classCode',
          className: 'className',
          days: 'days',
          num: 'num',
          cityCode: 'cityCode',
          cityName: 'cityName',
          benchmark: 'benchmark'
        },
        conditions: [{
          field: 'projectItemId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'id'
        }]
      }];

      if (versionId > 0) {
        conditions.push({
          field: 'versionId',
          condition: 'EQUALS',
          value: versionId
        });

        joins = [{
          type: 'projectItemOtherVersion',
          fields: {
            levelCode: 'levelCode',
            levelName: 'levelName',
            classCode: 'classCode',
            className: 'className',
            days: 'days',
            num: 'num',
            cityCode: 'cityCode',
            cityName: 'cityName',
            benchmark: 'benchmark'
          },
          conditions: [{
            field: 'projectItemId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'id'
          },{
            field: 'versionId',
            condition: 'EQUALS',
            type: 'Field',
            value: 'versionId'
          }]
        }];
      }

      var projectItems = Ext.create("Common.data.ApiReader").load({
        api: versionId > 0 ? 'projectItemVersion' : 'projectItem',
        conditions: conditions,
        joins: joins
      });

      var period = '';
      if (projectItems != null && projectItems[0] != null) {
        period = projectItems[0].period;
        var startTime = moment(projectItems[0].startTime).format('YYYY-MM-DD HH:mm:ss');
        var endTime = moment(projectItems[0].endTime).format('YYYY-MM-DD HH:mm:ss');
        if (startTime != undefined && startTime.length >= 10) {
          projectItems[0].startTime = startTime.substring(0, 10);
        } else {
          projectItems[0].startTime = period.substring(0, 4) + "-01-01";
        }
        if (endTime != undefined && endTime.length >= 10) {
          projectItems[0].endTime = endTime.substring(0, 10);
        } else {
          projectItems[0].endTime = period.substring(0, 4) + "-12-31";
        }
        form.getForm().setValues(projectItems[0]);
      }

      //查询费用行月度数据
      var dynamicQuery = {
        api: versionId > 0 ? 'monthlyCostPlanningDataVersion' : 'monthlyCostPlanningData',
        conditions: [{
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: '@@Project@@'
        }, {
          field: 'budgetObjectType',
          condition: 'EQUALS',
          type: 'String',
          value: 'ProjectItem'
        }, {
          field: 'budgetObjectId',
          condition: 'EQUALS',
          type: 'Integer',
          value: projectItemId
        }]
      };

      if(versionId > 0) {
        dynamicQuery.conditions.push({
          field: 'versionId',
          condition: 'EQUALS',
          type: 'Integer',
          value: versionId
        });
      }

      var monthlyData = Ext.create("Common.data.ApiReader").load(dynamicQuery);

      firstHalfData = [{M01: '', M02: '', M03: '', M04: '', M05: '', M06: ''}];
      secondHalfData = [{M07: '', M08: '', M09: '', M10: '', M11: '', M12: ''}];

      for (var i = 0; i < monthlyData.length; i++) {
        if (monthlyData[i].period != undefined && monthlyData[i].period != ''
          && period != '' && monthlyData[i].period.substring(0, 4) == period.substring(0, 4)) {
          var monthStr = monthlyData[i].period.substring(4, 6);
          var monthColumnStr = 'M' + monthStr;
          if (monthColumnStr == 'M01') {
            firstHalfData[0].M01 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M02') {
            firstHalfData[0].M02 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M03') {
            firstHalfData[0].M03 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M04') {
            firstHalfData[0].M04 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M05') {
            firstHalfData[0].M05 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M06') {
            firstHalfData[0].M06 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M07') {
            secondHalfData[0].M07 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M08') {
            secondHalfData[0].M08 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M09') {
            secondHalfData[0].M09 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M10') {
            secondHalfData[0].M10 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M11') {
            secondHalfData[0].M11 = monthlyData[i].amount;
          }
          if (monthColumnStr == 'M12') {
            secondHalfData[0].M12 = monthlyData[i].amount;
          }
        }
      }
    } else {
      var values = {};
      var params = Common.CommonManager.getParams();
      values.period = params.period;
      values.organizationId = params.organizationId;
      values.startTime = values.period.substring(0, 4) + "-01-01";
      values.endTime = values.period.substring(0, 4) + "-12-31";
      form.getForm().setValues(values);

      //初始化月度数据
      firstHalfData = [{M01: '', M02: '', M03: '', M04: '', M05: '', M06: ''}];
      secondHalfData = [{M07: '', M08: '', M09: '', M10: '', M11: '', M12: ''}];

      //当费用项只有一项数据时，默认选中
      var expenseItemCombox = Ext.getCmp('project-item-expenseItemId');
      if (expenseItemCombox != undefined && expenseItemCombox != null
        && expenseItemCombox.getStore() != undefined && expenseItemCombox.getStore() != null
        && expenseItemCombox.getStore().getTotalCount() == 1) {
        expenseItemCombox.select(expenseItemCombox.getStore().getAt(0));
      }
    }

    var firstHalfStore = Ext.data.StoreManager.lookup('firstHalfStore');
    firstHalfStore.add(firstHalfData);
    firstHalfStore.commitChanges();
    var secondHalfStore = Ext.data.StoreManager.lookup('secondHalfStore');
    secondHalfStore.add(secondHalfData);
    secondHalfStore.commitChanges();
  },
  saveProjectItem: function () {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var projectId = this.down("projectItemPanel").projectId;
    var projectItemId = this.down("projectItemPanel").projectItemId;
    //设置项目数据类型
    var dataType = "@@Project@@";
    if (params.budgetType != undefined && params.budgetType != null && params.budgetType == "Fund") {
      dataType += "Fund@@";
    } else if (params.budgetType != undefined && params.budgetType != null && params.budgetType == "Expenses") {
      dataType += "Expenses@@";
    } else {
      dataType += "Cost@@";
    }
    params.dataType = dataType;

    var items = Ext.ComponentQuery.query('projectItemGrid')[0].getStore().getRange();
    for (var i = 0; i < items.length; i++) {
      if (items[i].data.expenseItemId == $this.getFormValues().expenseItemId && projectItemId <= 0) {
        Common.Msg.Alert("项目已存在此费用项，不能添加相同费用项！");
        return false;
      }
    }

    if (this.down("form").getForm().isValid()) {
      var projectItem = $this.getFormValues();
      projectItem.domainId = params.domainId;
      projectItem.projectId = projectId;
      projectItem.status = 'Draft';
      if (projectItemId > 0) {
        projectItem.id = projectItemId;
      }

      var totalAmount = 0.00;
      var annualData = [];
      var monthlyData = [];

      //费用行年度和月度数据
      var firstHalfRecords = Ext.data.StoreManager.lookup('firstHalfStore').getRange();
      var secondHalfRecords = Ext.data.StoreManager.lookup('secondHalfStore').getRange();
      if (firstHalfRecords != null && firstHalfRecords[0] != null) {
        for (var i = 1; i <= 12; i++) {
          var monthStr = i < 10 ? "0" + i : i;
          var monthColumnStr = "M" + monthStr;
          if (i <= 6) {
            if (firstHalfRecords != null && firstHalfRecords[0] != null) {
              if (firstHalfRecords[0].get(monthColumnStr) != undefined && firstHalfRecords[0].get(monthColumnStr) != '') {
                monthlyData[monthlyData.length] = {
                  period: projectItem.period.substring(0, 4) + monthStr,
                  amount: firstHalfRecords[0].get(monthColumnStr)
                };
                totalAmount += firstHalfRecords[0].get(monthColumnStr);
              }
            }
          } else {
            if (secondHalfRecords != null && secondHalfRecords[0] != null) {
              if (secondHalfRecords[0].get(monthColumnStr) != undefined && secondHalfRecords[0].get(monthColumnStr) != '') {
                monthlyData[monthlyData.length] = {
                  period: projectItem.period.substring(0, 4) + monthStr,
                  amount: secondHalfRecords[0].get(monthColumnStr)
                };
                totalAmount += secondHalfRecords[0].get(monthColumnStr);
              }
            }
          }
        }
      }

      if (parseFloat(totalAmount).toFixed(2) != parseFloat(projectItem.amount).toFixed(2)) {
        var expenseItemName = Ext.getCmp('project-item-expenseItemId').getRawValue();
        var message = "【" + expenseItemName + "】预算金额 " + Ext.util.Format.number(projectItem.amount, '0,0.00');
        message += " 不等于月度合计 " + Ext.util.Format.number(totalAmount, '0,0.00') + "；";
        Common.Msg.Alert(message);
        return false;
      }

      var callback = function (result) {
        projectItem.id = result[0].data.id;
        $this.down("form").getForm().setValues(projectItem);
        projectItemId = projectItem.id;
        $this.down("projectItemPanel").projectItemId = projectItemId;
      }

      Ext.create("Common.data.ApiReader")
        .createOrUpdate('projectItem', [projectItem], callback, false);

      //保存费用行其他信息
      var projectItemOthers = Ext.create("Common.data.ApiReader").load({
        api: 'projectItemOther',
        conditions: [{
          field: 'projectItemId',
          condition: 'EQUALS',
          value: projectItemId
        }]
      });

      if (projectItemOthers != null && projectItemOthers[0] != null) {
        projectItemOthers[0].levelCode = projectItem.levelCode;
        projectItemOthers[0].levelName = projectItem.levelName;
        projectItemOthers[0].classCode = projectItem.classCode;
        projectItemOthers[0].className = projectItem.className;
        projectItemOthers[0].days = projectItem.days;
        projectItemOthers[0].num = projectItem.num;
        projectItemOthers[0].cityCode = projectItem.cityCode;
        projectItemOthers[0].cityName = projectItem.cityName;
        projectItemOthers[0].benchmark = projectItem.benchmark;
      } else {
        projectItemOthers[0] = projectItem;
        projectItemOthers[0].projectItemId = projectItemId;
        delete projectItemOthers[0].id;
      }

      Ext.create("Common.data.ApiReader")
        .createOrUpdate('projectItemOther', [projectItemOthers[0]], null, false);

      //查询费用行年度和月度数据
      var oldAnnualData = Ext.create("Common.data.ApiReader").load({
        api: 'annualCostPlanningData',
        conditions: [{
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: '@@Project@@'
        }, {
          field: 'budgetObjectType',
          condition: 'EQUALS',
          type: 'String',
          value: 'ProjectItem'
        }, {
          field: 'budgetObjectId',
          condition: 'EQUALS',
          type: 'Integer',
          value: projectItemId
        }]
      });

      var oldMonthlyData = Ext.create("Common.data.ApiReader").load({
        api: 'monthlyCostPlanningData',
        conditions: [{
          field: 'dataType',
          condition: 'STARTS_WITH',
          type: 'String',
          value: '@@Project@@'
        }, {
          field: 'budgetObjectType',
          condition: 'EQUALS',
          type: 'String',
          value: 'ProjectItem'
        }, {
          field: 'budgetObjectId',
          condition: 'EQUALS',
          type: 'Integer',
          value: projectItemId
        }]
      });

      var deleteAnnualData = [];
      var deleteMonthlyData = [];
      for (var i = 0; i < oldAnnualData.length; i++) {
        if (oldAnnualData[i] != null) {
          var isDelete = true;
          if (oldAnnualData[i].period != undefined && oldAnnualData[i].period == projectItem.period) {
            oldAnnualData[i].amount = projectItem.amount;
            annualData[annualData.length] = oldAnnualData[i];
            isDelete = false;
          }

          if (isDelete) {
            deleteAnnualData[deleteAnnualData.length] = oldAnnualData[i];
          }
        }
      }

      if (annualData.length == 0) {
        annualData[annualData.length] = {
          domainId: params.domainId,
          setOfBooksId: params.setOfBooksId,
          period: projectItem.period,
          organizationId: projectItem.organizationId,
          dataType: params.dataType,
          status: 'Draft',
          budgetItemGroupId: -1,
          budgetObjectType: 'ProjectItem',
          budgetObjectId: projectItemId,
          amount: projectItem.amount
        };
      }

      for (var i = 0; i < monthlyData.length; i++) {
        var isAdd = true;
        for (var j = 0; j < oldMonthlyData.length; j++) {
          if (oldMonthlyData[j].period != undefined && oldMonthlyData[j].period == monthlyData[i].period) {
            oldMonthlyData[j].amount = monthlyData[i].amount;
            oldMonthlyData[j].isUpdate = true;
            monthlyData[i] = oldMonthlyData[j];
            isAdd = false;
          }
        }

        if (isAdd) {
          monthlyData[i].domainId = params.domainId;
          monthlyData[i].setOfBooksId = params.setOfBooksId;
          monthlyData[i].organizationId = projectItem.organizationId;
          monthlyData[i].dataType = params.dataType;
          monthlyData[i].status = 'Draft';
          monthlyData[i].budgetItemGroupId = -1;
          monthlyData[i].budgetObjectType = 'ProjectItem';
          monthlyData[i].budgetObjectId = projectItemId;
        }
      }

      for (var j = 0; j < oldMonthlyData.length; j++) {
        if (oldMonthlyData[j].isUpdate == undefined || !oldMonthlyData[j].isUpdate) {
          deleteMonthlyData[deleteMonthlyData.length] = oldMonthlyData[j];
        }
      }

      //删除多余费用行年度和月度数据
      if (deleteAnnualData.length > 0) {
        Ext.create("Common.data.ApiReader").directDelete_('annualCostPlanningData', deleteAnnualData, null);
      }
      if (deleteMonthlyData.length > 0) {
        Ext.create("Common.data.ApiReader").directDelete_('monthlyCostPlanningData', deleteMonthlyData, null);
      }
      //保存费用行年度和月度数据
      Ext.create("Common.data.ApiReader")
        .createOrUpdate('annualCostPlanningData', annualData, null, false);
      Ext.create("Common.data.ApiReader")
        .createOrUpdate('monthlyCostPlanningData', monthlyData, null, false);

      $this.loadProjectItemGrid();
      $this.close();
    }
  },
  getFormValues: function () {
    var form = this.down("panel").down("form");
    var values = form.getValues();
    Common.text.CommonAmount.getValuesFromForm(values, form);

    //费用行开始结束时间
    if (values.startTime != undefined && values.startTime != '' && values.startTime.length == 10) {
      values.startTime = values.startTime + "T00:00:00.000+0800";
    }
    if (values.endTime != undefined && values.endTime != '' && values.endTime.length == 10) {
      values.endTime = values.endTime + "T23:59:59.000+0800";
    }
    if (values.period != null && values.period != "" && values.period.length >= 4
      && (values.startTime == undefined || values.startTime == ''
      || values.endTime == undefined || values.endTime == '')) {
      values.startTime = values.period.substring(0, 4) + "-01-01T00:00:00.000+0800";
      values.endTime = values.period.substring(0, 4) + "-12-31T23:59:59.000+0800";
    }

    return values;
  },
  showAndRevertFunc: function(projectItemId, projectId, versionId, isEdit, func) {
    this.down("projectItemPanel").projectItemId = projectItemId;
    this.down("projectItemPanel").projectId = projectId;
    this.down("projectItemPanel").versionId = versionId;

    var dynamicQuery = {
      api: versionId > 0 ? 'projectVersion' : 'project',
      joins: [
        {
          type: 'projectCategory',
          fields: {
            code: 'projectCategoryCode'
          },
          joins: [
            {
              type: 'dictionaryOption',
              fields: {
                names: 'projectForm'
              },
              conditions: [
                {
                  field: 'domainId',
                  condition: 'EQUALS',
                  value: domainId
                }, {
                  field: 'dictionaryCode',
                  condition: 'EQUALS',
                  value: 'ProjectCategoryRelationProjectForm'
                }, {
                  field: 'code',
                  condition: 'EQUALS',
                  type: 'Field',
                  value: 'code'
                }
              ]
            }
          ],
          conditions: [
            {
              field: 'id',
              condition: 'EQUALS',
              type: 'Field',
              value: 'projectCategoryId'
            }
          ]
        }
      ],
      conditions: [{
        field: 'id',
        condition: 'EQUALS',
        value: projectId
      }]
    };

    if (versionId > 0) {
      dynamicQuery.conditions.push({
        field: 'versionId',
        condition: 'EQUALS',
        value: versionId
      });
    }

    var projects = Ext.create("Common.data.ApiReader").load(dynamicQuery);

    if (projects != null && projects[0] != null
      && projects[0].projectCategoryId != undefined && projects[0].projectCategoryId != '') {
      this.down("projectItemPanel").projectCategoryId = projects[0].projectCategoryId;
      if (projects[0].projectForm != undefined && projects[0].projectForm != '') {
        this.down("projectItemPanel").formType = projects[0].projectForm;
      }
    }

    if (isEdit) {
      if (this.down("button[action=save]") != null) {
        this.down("button[action=save]").setVisible(true);
      }
      if (this.down("button[action=close]") != null) {
        this.down("button[action=close]").setText('取消');
      }
      this.down("projectItemPanel").readOnly = false;
    } else {
      if (this.down("button[action=save]") != null) {
        this.down("button[action=save]").setVisible(false);
      }
      if (this.down("button[action=close]") != null) {
        this.down("button[action=close]").setText('关闭');
      }
      this.down("projectItemPanel").readOnly = true;
    }

    this.down("projectItemPanel").loadView();
    this.loadProjectItem(projectItemId, versionId);
    if (func != null) {
      this.loadProjectItemGrid = func;
    }
    this.show();
  }
});
