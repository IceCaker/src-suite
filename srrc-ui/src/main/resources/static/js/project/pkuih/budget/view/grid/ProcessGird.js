Ext.define('Budget.view.grid.ProcessGird', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.processGird',
  title: '审批信息',
  storeAutoLoad: false,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  autoHeight: true,
  viewConfig: {
    loadMask: false,
    forceFit: true,
    preserveScrollOnRefresh: true
  },
  store: Ext.create('Ext.data.Store', {
    fields: ["processId", "subject", "statusName", "handleComment", "handlerName", "handleTime"],
    listeners: {
      'beforeload': function (obj, operation, eOpts) {
        Common.CommonManager.showMask("数据加载中，请稍候...");
      },
      'load': function (store) {
        Common.CommonManager.hideMask();
      }
    }
  }),
  columns: [{text: '描述', dataIndex: 'subject', minWidth: 200, flex: 1, align: "left"},
    {
      text: '状态', dataIndex: 'statusName', width: 180, align: "center",
    },
    {
      text: '审批意见', dataIndex: 'handleComment', flex: 1,
      renderer: function (value) {
        if (value == 'null' || value == null) {
          return "--";
        }

        if (value.length > 10) {
          return '<span  title="' + value + '" >' + value + '</span>';
        } else {
          return '<span>' + value + '</span>';
        }
      }
    },
    {
      text: '审批人员',
      dataIndex: 'handlerName',
      width: 150,
      renderer: function (value, metaData, record, rowIdx, colIdx, store) {
        return value;
      }
    },
    {
      text: '审批时间',
      dataIndex: 'handleTime',
      flex: 1,
      renderer: function (value, metaData, record, rowIdx, colIdx, store) {
        if (value == null || value == '') {
          return "--";
        }

        var d = new Date();
        d.setTime(value);
        return d.Format('yyyy-MM-dd hh:mm:ss');
      }
    }
  ],
  initComponent: function () {
    var $this = this;
    this.callParent(arguments);
  },
  loadData: function () {
    var $this = this;
    var params = Common.CommonManager.getParams();
    var budgetDataStatus = Ext.create("Common.data.ApiReader").load({
      api: 'budgetDataStatus',
      conditions: [{
        field: 'domainId',
        condition: 'EQUALS',
        type: 'String',
        value: params.domainId
      }, {
        field: 'setOfBooksId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.setOfBooksId
      }, {
        field: 'period',
        condition: 'EQUALS',
        type: 'String',
        value: params.period
      }, {
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: params.organizationId
      }, {
        field: 'businessType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: params.businessType
      }, {
        field: 'dataType',
        condition: 'STARTS_WITH',
        type: 'String',
        value: '@@Planning@@'
      }]
    });
    if (budgetDataStatus.length == 0 || budgetDataStatus[0].statusCode != 'Approving') {
      this.store.load();
      return;
    }

    var taskData = {};
    Ext.Ajax.request({
      url: contextPath + '/bworkflow/service/instance/tracking/' + budgetDataStatus[0].processId,
      method: 'GET',
      async: false,
      success: function (response, operation) {
        var result = Ext.JSON.decode(response.responseText);
        taskData = result.data;
      }
    });

    for (var i = 0; i < taskData.length; i++) {
      if (taskData[i].statusName == '终止') {
        this.store.loadData(taskData);
        return;
      }
    }
    var resource = [];
    if (params.organizationId == 1) {
      resource = $this.getResource('ROLE_PRINCIPAL', 'Company');
    } else {
      resource = $this.getResource('ROLE_AUDITOR', params.organizationId);
      resource = resource.concat($this.getResource('ROLE_BUDGETER', 'Company'));
    }

    /*resource.sorts(function (a, b) {
     if (a.roleCode = 'ROLE_AUDITOR') {
     return
     }
     });*/

    //去除resource中已通过的节点
    for (var i = 0; i < taskData.length; i++) {
      for (var j = 0; j < resource.length; j++) {
        if (resource[j] != undefined
          && taskData[i].name == resource[j].roleName
          && taskData[i].handlerName == resource[j].userName) {
          resource[j] = undefined;
        }
      }
    }
    for (var i = 0; i < resource.length; i++) {
      if (resource[i] != undefined) {
        taskData.splice(0, 0, {
          subject: resource[i].roleName,
          handlerName: resource[i].userName,
          statusName: '等待处理'
        });
      }
    }
    //合并审批信息中的审批人
    var repeatSubject = {};
    for (var i = 0; i < taskData.length; i++) {

      if (repeatSubject[taskData[i].subject] == undefined) {
        repeatSubject[taskData[i].subject] = taskData[i];
      } else {
        var handlerName = repeatSubject[taskData[i].subject].handlerName;
        repeatSubject[taskData[i].subject].handlerName = handlerName + ',' + taskData[i].handlerName;
        taskData[i] = undefined;
      }
    }
    var newTaskData = [];
    for (var i = 0; i < taskData.length; i++) {
      if (taskData[i] != undefined) {
        newTaskData.push(taskData[i]);
      }
    }
    this.store.loadData(newTaskData);
  },
  getResource: function (roleCode, organization) {
    var conditions = [{
      field: 'domainId',
      condition: 'EQUALS',
      type: 'String',
      value: domainId
    }, {
      field: 'resourceType',
      condition: 'EQUALS',
      type: 'String',
      value: 'com.bmit.platform.soupe.model.Organization'
    }, {
      field: 'roleCode',
      condition: 'IN',
      type: 'String',
      value: roleCode
    }];

    if (Ext.isNumber(organization)) {
      conditions.push({
        field: 'organizationId',
        condition: 'EQUALS',
        type: 'Integer',
        value: organization
      });
    } else {
      conditions.push({
        field: 'organizationLevel',
        condition: 'EQUALS',
        type: 'String',
        value: organization
      });
    }

    return Ext.create('Common.data.ApiReader').load({
      api: 'resourcePermission',
      fields: {
        userId: 'userId'
      },
      joins: [{
        type: 'organization',
        fields: {
          id: 'organizationId',
          code: 'organizationCode',
          name: 'organizationName',
          organizationLevel: 'organizationLevel'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'code',
          condition: 'EQUALS',
          type: 'Field',
          value: 'resourceId'
        }]
      }, {
        type: 'role',
        fields: {
          code: 'roleCode',
          name: 'roleName'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'roleId'
        }]
      }, {
        type: 'user',
        fields: {
          name: 'userName'
        },
        conditions: [{
          field: 'domainId',
          condition: 'EQUALS',
          type: 'Field',
          value: 'domainId'
        }, {
          field: 'id',
          condition: 'EQUALS',
          type: 'Field',
          value: 'userId'
        }]
      }],
      conditions: conditions
    });
  }
});