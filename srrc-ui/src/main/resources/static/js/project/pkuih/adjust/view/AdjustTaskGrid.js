Ext.define('Adjust.view.AdjustTaskGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.adjustTaskGrid',
  viewConfig: {
    loadMask: false,
    forceFit: true
  },
  params: {},
  store: Ext.create('Common.data.ApiDirectGridStore', {
    fields: [
      {name: 'id', type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'organizationId', type: 'string'},
      {name: 'organizationName', type: 'string'},
      {name: 'budgetAmount', type: 'double'},
      {name: 'adjustAmount', type: 'double'},
      {name: 'afterAmount', type: 'double'}
    ]
  }),
  plugins: [
    Ext.create('Ext.grid.plugin.CellEditing', {
      clicksToEdit: 1
    })
  ],
  selModel: Ext.create('Ext.selection.CheckboxModel', {checkOnly: true}),
  columns: [],
  loadGrid: function (organizations) {

    var fields = [
      {name: 'id', type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'organizationId', type: 'string'},
      {name: 'organizationName', type: 'string'},
    ];
    for (var i = 0; i < organizations.length; i++) {
      fields.push({name: 'budgetAmount' + organizations[i].id, type: 'double'});
      fields.push({name: 'adjustAmount' + organizations[i].id, type: 'double'});
      fields.push({name: 'afterAmount' + organizations[i].id, type: 'double'});
    }
    this.store.model.setFields(fields);

    this.columns = [];
    this.columns[this.columns.length] = {
      text: '预算项',
      dataIndex: 'alias',
      align: 'left',
      flex: 1,
      minWidth: 200,
      locked: true,
      menuDisabled: true,
      sortable: false
    };
    for (var i = 0; i < organizations.length; i++) {
      var organization = organizations[i];
      this.columns[this.columns.length] = {
        text: organization.name,
        menuDisabled: true,
        sortable: false,
        columns: [{
          text: '预算金额',
          dataIndex: 'budgetAmount' + organization.id,
          align: 'right',
          flex: 1,
          width: 120,
          menuDisabled: true,
          sortable: false,
          renderer: function (value, meta, record) {
            if (value != null) {
              return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
                + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
            } else {
              return '0.00';
            }
          }
        }, {
          text: '调整值',
          dataIndex: 'adjustAmount' + organization.id,
          align: 'right',
          width: 120,
          menuDisabled: true,
          sortable: false,
          renderer: function (value, meta, record) {
            if (value != null) {
              return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
                + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
            } else {
              return '0.00';
            }
          }
        }, {
          text: '调整后预算',
          oldText: null,
          dataIndex: 'afterAmount' + organization.id,
          align: 'right',
          width: 120,
          menuDisabled: true,
          sortable: false,
          renderer: function (value, meta, record) {
            if (value != null) {
              return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
                + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
            } else {
              return '0.00';
            }
          }
        }]
      };
    }
    this.reconfigure(this.store, this.columns);
  },
  loadData: function () {
    var $this = this;
    var param = pageInitParam;
    Common.CommonManager.showMask("数据加载中，请稍候...");

    var budgetItemGroup = Ext.create("Common.data.ApiReader").load({
      api: "resourceExtInstance",
      joins: [
        {
          type: "budgetItemGroup",
          fields: {
            id: "budgetItemGroupId"
          },
          conditions: [
            {
              field: "code",
              condition: "EQUALS",
              type: "Field",
              value: "instanceValue"
            }
          ]
        }
      ],
      conditions: [
        {
          field: "domainId",
          condition: "EQUALS",
          type: "String",
          value: domainId
        }, {
          field: "resourceExtOptionId",
          condition: "EQUALS",
          type: "String",
          value: "default-planning-calculate-itemGroup-code"
        },
        {
          field: "resourceExtSchemaId",
          condition: "EQUALS",
          type: "String",
          value: "budget-feature-setting-configuration"
        }
      ]
    });
    var budgetItemGroupId = budgetItemGroup[0].budgetItemGroupId;

    var budgetAdjustQuery = {
      api: "budgetAdjustDetail",
      joins: [
        {
          type: "organization",
          fields: {
            name: "organizationName"
          },
          conditions: [
            {
              field: "id",
              condition: "EQUALS",
              type: "Field",
              value: "organizationId"
            }
          ]
        }
      ],
      conditions: [
        {
          field: "domainId",
          condition: "EQUALS",
          type: "String",
          value: domainId
        }, {
          field: "budgetAdjustId",
          condition: "EQUALS",
          type: "Long",
          value: param.adjustId
        }
      ]
    };
    if (param.organizationId != -1) {
      budgetAdjustQuery.conditions.push({
        field: "organizationId",
        condition: "EQUALS",
        type: "Long",
        value: param.organizationId
      })
    }

    var budgetAdjustDetail = Ext.create("Common.data.ApiReader").load(budgetAdjustQuery);


    var organizations = [];
    var budgetItemIds = [];
    var removeRepeat = [];
    for (var i = 0; i < budgetAdjustDetail.length; i++) {
      budgetItemIds.push(budgetAdjustDetail[i].budgetItemId);

      var id = budgetAdjustDetail[i].organizationId;
      if (removeRepeat.indexOf(id) != -1) {
        continue;
      }
      removeRepeat.push(id);

      organizations.push({
        'id': budgetAdjustDetail[i].organizationId,
        'name': budgetAdjustDetail[i].organizationName
      });

    }
    var dynamicQuery = {
      queryType: "read",
      type: "budgetItemGroupItem",
      fields: {
        alias: "alias",
        sortCode: "sortCode",
        budgetItemId: "budgetItemId",
        budgetItemGroupId: "budgetItemGroupId"
      },
      associations: [],
      conditions: [
        {
          field: "budgetItemGroupId",
          condition: "EQUALS",
          type: "Integer",
          value: budgetItemGroupId
        }, {
          field: "budgetItemId",
          condition: "IN",
          type: "Integer",
          value: budgetItemIds
        }
      ],
      sorts: [{
        field: "sortCode",
        direction: 'ASC'
      }]
    };
    for (var i = 0; i < organizations.length; i++) {
      dynamicQuery.associations.push({
        type: "budgetItemGroupItem",
        fields: {
          alias: "alias",
          sortCode: "sortCode",
          budgetItemId: "budgetItemId",
          budgetItemGroupId: "budgetItemGroupId"
        },
        references: {
          budgetItemId: "budgetItemId"
        },
        joins: [{
          type: "budgetAdjustDetail",
          fields: {
            budgetItemId: 'itemId' + organizations[i].id,
            budgetAmount: 'budgetAmount' + organizations[i].id,
            adjustAmount: 'adjustAmount' + organizations[i].id
          },
          conditions: [
            {
              field: "domainId",
              condition: "EQUALS",
              type: "String",
              value: domainId
            }, {
              field: "budgetAdjustId",
              condition: "EQUALS",
              type: "Long",
              value: param.adjustId
            }, {
              field: "organizationId",
              condition: "EQUALS",
              type: "Long",
              value: organizations[i].id
            },
            {
              field: "budgetItemId",
              condition: "EQUALS",
              type: "Field",
              value: "budgetItemId"
            }
          ]
        }],
        conditions: [
          {
            field: "budgetItemGroupId",
            condition: "EQUALS",
            type: "Integer",
            value: budgetItemGroupId
          }, {
            field: "budgetItemId",
            condition: "IN",
            type: "Integer",
            value: budgetItemIds
          }
        ]
      });
    }

    var budgetAdjustDetail = [];
    Ext.Ajax.request({
      url: contextPath + '/api/2.0.0/cql',
      method: 'POST',
      async: false,
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      params: Ext.encode(dynamicQuery),
      paramsAsJson: true,
      success: function (response, operation) {
        var responseJson = Ext.JSON.decode(response.responseText);
        budgetAdjustDetail = responseJson.data;
      }
    });

    if (budgetAdjustDetail.length == 0) {
      Common.CommonManager.hideMask();
      return false;
    }

    $this.loadGrid(organizations);

    for (var i = 0; i < budgetAdjustDetail.length; i++) {
      for (var j = 0; j < organizations.length; j++) {
        var organizationId = organizations[j].id;

        var budgetAmount = budgetAdjustDetail[i]['budgetAmount' + organizationId];
        var adjustAmount = budgetAdjustDetail[i]['adjustAmount' + organizationId];
        budgetAdjustDetail[i]['afterAmount' + organizationId] = budgetAmount + adjustAmount;
      }
    }
    $this.getStore().removeAll();
    $this.getStore().add(budgetAdjustDetail);
    $this.getStore().commitChanges();

    Common.CommonManager.hideMask();
  },
});
