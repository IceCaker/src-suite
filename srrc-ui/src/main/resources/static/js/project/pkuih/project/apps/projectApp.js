Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');
Ext.Loader.setPath('Pkuih.Common', contextPath + '/static/js/project/pkuih/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  "Common.FlexibleToolbar",
  "Pkuih.Common.view.ProjectItemGrid",
  "Common.data.ApiReader",
  "Common.combo.ApiCombox",
  "Common.combo.Period",
  "Common.combo.Organization",
  "Common.combo.SetOfBook"
]);

Ext.application({
  name: 'Project',
  appFolder: contextPath + '/static/js/project/pkuih/project',
  controllers: [
    "ProjectListController","ProjectInfoController"
  ],
  launch: function () {
    Ext.tip.QuickTipManager.init();

    mainApplication = this;
    var height = window.document.body.clientHeight;

    var initParams = pageInitParam;

    var panel = Ext.create('Ext.panel.Panel', {
      id: 'project-panel',
      layout: 'border',
      border: false,
      height: 600,
      hidden: false,
      bodyStyle: {
        background: '#FFFFFF'
      },
      bodyPadding: 5,
      items: [{
        xtype: 'projectSearchToolbar',
        id:'project-searchToolbar',
        name: 'toolbarOne',
        margin: '0 0 5 0',
        region: 'north'
      }, {
        xtype: 'projectGrid',
        id:'project-grid-main',
        scroll: true,
        region: 'center'
      }, {
        xtype: 'projectInfoPanel',
        id:'project-info-main',
        scroll: true,
        hidden: true,
        region: 'center'
      }]
    });

    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add(panel);
  }
});