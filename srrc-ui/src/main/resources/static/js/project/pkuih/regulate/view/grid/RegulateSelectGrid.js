Ext.define('Regulate.view.grid.RegulateSelectGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.regulateSelectGrid',
  minHeight: 230,
  rowLines: true,
  useArrows: true,
  simpleSelect: false,
  multiSelect: false,
  border: false,
  readOnly: true,
  columns: [
    {
      text: '项目编号',
      dataIndex: 'projectCode',
      style: 'text-align:center;',
      align: 'center',
      width: 130,
      menuDisabled: true,
      sortable: false,
    },
    {
      text: '项目名称',
      dataIndex: 'projectName',
      style: 'text-align:center;',
      align: 'left',
      width: 140,
      menuDisabled: true,
      sortable: false
    },
    {
      text: '费用行编号',
      dataIndex: 'expenseCode',
      style: 'text-align:center;',
      align: 'center',
      width: 120,
      menuDisabled: true,
      sortable: false,
    },
    {
      text: '费用行名称',
      dataIndex: 'expenseName',
      style: 'text-align:center;',
      align: 'left',
      width: 140,
      menuDisabled: true,
      sortable: false
    },
    {
      text: '费用项',
      dataIndex: 'expenseItem',
      style: 'text-align:center;',
      align: 'left',
      flex: 1,
      minWidth: 140,
      menuDisabled: true,
      sortable: false
    },
    {
      text: '承担部门',
      dataIndex: 'organization',
      style: 'text-align:center;',
      align: 'left',
      flex: 1,
      minWidth: 140,
      menuDisabled: true,
      sortable: false
    },
    {
      text: '费用项预算', dataIndex: 'budgetAmount', align: 'left', width: 140, menuDisabled: true, sortable: false,
      renderer: function (value, meta, record) {
        if (value != null) {
          return '<font title="' + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '">'
            + Ext.util.Format.number(value / Common.CommonManager.conversionFactor, '0,000.00') + '</font>';
        } else {
          return '0.00';
        }
      }
    }
  ],
  initComponent: function () {

    this.store = Ext.create('Common.data.ApiGridStore', {
      fields: [
        {name: 'id', type: 'long'},
        {name: 'code', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'expenseItemId', type: 'long'},
        {name: 'expenseItemCode', type: 'long'},
        {name: 'expenseItemName', type: 'string'},
        {name: 'organizationId', type: 'long'},
        {name: 'organizationName', type: 'string'},
        {name: 'period', type: 'string'},
        {name: 'amount', type: 'double'},
        {name: 'completePercent', type: 'double'},
        {name: 'note', type: 'string'},
        {name: 'createTime', type: 'long'},
        {name: 'deletable', type: 'boolean'},
        {name: 'versionId', type: 'long'}
      ],
      api: 'projectItem',
      joins: [{
        type: 'organization',
        fields: {
          name: 'organizationName'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'organizationId'
          }
        ]
      }, {
        type: 'expenseItem',
        fields: {
          code: 'expenseItemCode',
          name: 'expenseItemName'
        },
        conditions: [
          {
            field: 'id',
            condition: 'EQUALS',
            type: 'Field',
            value: 'expenseItemId'
          }
        ]
      }],
      conditions: [{
        field: 'projectId',
        condition: 'EQUALS',
        value: '#projectId#'
      }]
    });
    this.callParent(arguments);
  },
  loadData: function () {
  },
  loadGrid: function (projectId, versionId) {
  },
  showProjectItemWindow: function () {
    Ext.widget('regulateDetailWindow').showAndRevertFunc(this.regulateType);
  }
});