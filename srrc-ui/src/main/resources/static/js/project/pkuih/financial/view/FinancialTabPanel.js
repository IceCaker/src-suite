Ext.define('Financial.view.FinancialTabPanel', {
  extend: 'Ext.tab.Panel',
  alias: "widget.financialTabPanel",
  processPage: false,
  padding: [0, 0, 0, 0],
  loadingPageTime: false,
  items: [],
  subscription: {
    observers: ['report-search-queryButton'],
    operate: {
      onNext: function(params) {
        if (window.location.pathname.indexOf("summary") > 0) {
          params.businessType = "financialSummary";
        } else {
          params.businessType = "financialStatements";
        }
        params.domainId = domainId;
        this.loadTabs(params);
      }
    }
  },
  loadTabs: function(params) {
    this.loadingPageTime = true;
    this.removeAll();
    var tabs = [];
    Ext.Ajax.request({
      url: contextPath + '/pkuih/budget/financial/tab',
      params: params,
      async: false,
      timeout: 30000000,
      method: 'POST',
      callback: function(options, success, response) {
        tabs = Ext.JSON.decode(response.responseText);
      }
    });
    if (tabs.length > 0) {
      //显示和隐藏操作按钮
      Ext.getCmp('report-search-saveButton').setVisible(true);
      Ext.getCmp('report-search-getButton').setVisible(true);
      Ext.getCmp('report-search-exportButton').setVisible(true);
      Ext.getCmp('report-search-importButton').setVisible(true);

      var activeTab = null;
      for (var i = 0; i < tabs.length; i++) {
        var panel = Ext.widget("financialDataPanel", {
          title: tabs[i],
          sheetIndex: i,
          params: params
        });
        if (i == 0) {
          activeTab = panel;
        }
        this.add(panel);
      }
      this.setActiveTab(activeTab);
      this.loadingPageTime = false;
      if (activeTab != null) {
        this.loadData(activeTab);
      }
    } else {
      //显示和隐藏操作按钮
      Ext.getCmp('report-search-saveButton').setVisible(false);
      Ext.getCmp('report-search-getButton').setVisible(false);
      Ext.getCmp('report-search-exportButton').setVisible(false);
      Ext.getCmp('report-search-importButton').setVisible(false);
    }
  },
  loadData: function(panel) {
    var d = new Ext.util.DelayedTask(function() {
      panel.load();
    });
    d.delay(50);
  },
  endsWith: function(mainStr, str) {
    if (str == null || str == "" || mainStr.length == 0 || str.length > mainStr.length) {
      return false;
    } else if (mainStr.substring(mainStr.length - str.length) == str) {
      return true;
    } else {
      return false;
    }
  },
  listeners: {
    beforetabchange: function(tabPanel, newCard, oldCard, eOpts) {
      if (tabPanel.loadingPageTime) {
        return;
      }
      if (oldCard != null) {
        oldCard.save();
      }
    },
    tabchange: function(tabPanel, newCard, oldCard, eOpts) {
      if (tabPanel.loadingPageTime) {
        return;
      }
      newCard.load();
    }
  }
});