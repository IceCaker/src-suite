Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Common', contextPath + '/static/js/common');

Ext.require([
  'Ext.grid.*',
  'Ext.data.*',
  'Ext.util.*',
  'Ext.toolbar.Paging',
  'Ext.ModelManager',
  'Ext.tip.QuickTipManager',
  'Common.Msg',
  'Common.Support',
  'Common.CommonManager',
  'Common.CommonFunc',
  'Common.data.ApiReader',
  'Common.combo.ApiCombox',
  'Common.combo.Period',
  "Common.combo.Organization",
  "Common.combo.SetOfBook",
  'Common.panel.HtmlPanelBuilder'
]);

Ext.application({
  name: 'AssetsBudget',
  appFolder: contextPath + '/static/js/project/pkuih/assets',
  views: ["AssetsBudgetTabPanel", "AssetsBudgetSearchToolbar", "AssetsBudgetDataPanel"],
  launch: function () {
    Ext.tip.QuickTipManager.init();

    mainApplication = this;

    var height = window.document.body.clientHeight;
    var initParams = {};

    for (var obj in pageInitParam) {
      if (pageInitParam.hasOwnProperty(obj)) {
        initParams[obj] = pageInitParam[obj];
      }
    }

    var panel = Ext.create("Ext.panel.Panel", {
      layout: 'border',
      border: false,
      bodyStyle: {
        background: "#FFFFFF"
      },
      bodyPadding: 5,
      items: [
        {
          xtype: "assetsBudgetSearchToolbar",
          margin: '0 0 5 0',
          region: 'north'
        },
        {
          xtype: 'assetsBudgetTabPanel',
          scroll: true,
          region: 'center'
        }
      ]
    });
    Ext.getCmp(Soupe.Layout.Base.CENTER_CONTAINER_ID).add(panel);
  }
});