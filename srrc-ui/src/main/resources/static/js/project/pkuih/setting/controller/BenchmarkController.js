Ext.define('Setting.controller.BenchmarkController', {
  extend: 'Ext.app.Controller',
  views: ['benchmark.MainPanel', 'benchmark.SearchToolbar', 'benchmark.DataGrid'],
  refs: [
    {ref: "benchmarkMainPanel", selector: 'benchmarkMainPanel'}
  ],
  init: function () {
    var $this = this;
    this.control({
      'benchmarkMainPanel': {
        'render': function () {
          $this.addPanelFunc();
        }
      }
    });
  },
  addPanelFunc: function() {
    var $this = this;
    $this.getBenchmarkMainPanel().add({
      id: 'benchmark-search-toolbar',
      xtype: 'benchmarkSearchToolbar',
      region: 'north'
    });
    $this.getBenchmarkMainPanel().add({
      id: 'main-benchmark-grid',
      xtype: 'benchmarkDataGrid',
      region: 'center'
    });
  }
});