Ext.define('Project.view.panel.ProjectProcessPanel', {
  extend: 'Ext.panel.Panel',
  alias: "widget.projectProcessPanel",
  layout: "border",
  title: '审批信息',
  items:[],
  bodyStyle: {
    background: 'white',
    padding: '2px'
  },
  initComponent: function(){
    var $this = this;

    var processStore = Ext.create('Ext.data.Store', {
      fields: ["processId", "status", "description", "creatorId", "creatorName", "createTime"],
      proxy: {
        type: 'ajax',
        url: "",
        method: "GET",
        reader: {
          type: 'json',
          root: 'data'
        }
      },
      listeners: {
        'beforeload': function (store) {
          obj.getProxy().url = contextPath + '/bproject/query/process/' + $this.projectId;
        },
        'load': function (store) {
          Common.CommonManager.hideMask();
        }
      }
    });

    var processGrid = Ext.create('Ext.grid.Panel', {
      title: '流程信息',
      store: processStore,
      columns: [
        { text: '描述', dataIndex: 'description', minWidth: 200, flex: 1, align: "left"},
        { text: '状态', dataIndex: 'status', width: 180, align: "center",
          renderer: function(value, meta, record){
            if(value == "Active"){
              return "流转中";
            }else if(value == "Aborted"){
              return "审批拒绝";
            }else if(value == "Completed"){
              return "审批通过";
            }
          }
        },
        { text: '发起人', dataIndex: 'creatorName', width: 140, align: "center"},
        { text: '发起时间', dataIndex: 'createTime', width: 180, align: "center", renderer: timestampRender}
      ],
      height: 200,
      region: "north",
      listeners: {
        select: function(grid, record, index, eOpts ){
          $this.nodeList.processInstanceId = record.data.processId;
          $this.nodeList.getStore().load();
        }
      }
    });

    var nodeGrid = Ext.widget("processTreeGrid", {
      region: "center",
      border: "0 1 1 1",
      processInstanceId: 330
    });

    this.processGrid = processGrid;
    this.nodeGrid = nodeGrid;
    this.items = [this.processGrid, this.nodeGrid];

    this.callParent(arguments);
  },
  getProjectId: function(){
    return this.up("projectInfoPanel").projectId;
  },
  isReadOnly: function(){
    return this.up("projectInfoPanel").readOnly;
  },
  loadData: function(){
    this.projectId = this.getProjectId();

    this.processGrid.getStore().load();
    this.nodeGrid.getStore().removeAll();
  }
});