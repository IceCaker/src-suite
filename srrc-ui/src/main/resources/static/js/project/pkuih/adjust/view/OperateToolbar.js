Ext.require([
  'Common.ToolbarUtil',
  'Common.button.VersionMge'
]);
Ext.define("Adjust.view.OperateToolbar", {
  extend: "Ext.toolbar.Toolbar",
  alias: "widget.adjustOperateToolbar",
  margin: '-5 0 5 0',
  style: 'border-top: 0px;',
  items: [{
    id: "adjust-operate-adjustWay",
    xtype: "commonCombo",
    emptyText: '请选择',
    width: 180,
    labelWidth: 65,
    editable: false,
    name: 'adjustWay',
    fieldLabel: '调整方式',
    store: Ext.create('Ext.data.Store', {
      fields: ['id', 'code', 'name'],
      data: {
        'data': [
          {id: '1', code: 'Amount', name: '按金额调整'},
          {id: '2', code: 'Proportion', name: '按比例调整'}
        ]
      },
      proxy: {
        type: 'memory',
        reader: {
          type: 'json',
          root: 'data'
        }
      }
    }),
    queryMode: 'local',
    displayField: 'name',
    valueField: 'code',
    observable: true
  }, {
    id: "adjust-operate-adjustValue",
    xtype: 'numberfield',
    name: 'adjustValue',
    fieldLabel: '调整值',
    width: 220,
    labelWidth: 50,
    selectOnFocus: true,
    hideTrigger: true,
    allowDecimals: true,
    decimalPrecision: 15,
    keyNavEnabled: false,
    mouseWheelEnabled: false,
    fieldStyle: 'text-align: right',
    observable: true
  }, {
    id: "adjust-operate-percent",
    xtype: 'label',
    margin: '0 0 0 -5',
    width: 20,
    text: '',
    subscription: {
      observers: ["adjust-operate-adjustWay"],
      operate: {
        onNext: function(param) {
          if (param.adjustWay == 'Proportion') {
            this.setText('%');
          } else {
            this.setText('');
          }
        }
      }
    }
  }, {
    id: 'adjust-operate-query', text: '预览', xtype: "queryButton", observable: true,
    paramsProperties: ["setOfBooksId", "period", "organizationId", "budgetItemId"],
    params: {},
    pushParam: function(param) {
      Ext.apply(this.params, param);
    },
    isLoadAll: function() {
      if (this.params.organizationId == undefined || this.params.organizationId == '') {
        Common.Msg.Alert("请选择组织！");
        return false;
      }
      if (this.params.budgetItemId == undefined || this.params.budgetItemId == '') {
        Common.Msg.Alert("请选择预算项！");
        return false;
      }
      for (var i = 0; i < this.paramsProperties.length; i++) {
        if (typeof(this.params[this.paramsProperties[i]]) == 'undefined') {
          return false;
        }
      }
      return true;
    },
    getParams: function() {
      return this.params;
    },
    subscription: {
      observers: ["adjust-search-setOfBook", "adjust-search-period", "adjust-search-multiOrganization",
        "adjust-search-multiBudgetItem", "adjust-operate-adjustWay", "adjust-operate-adjustValue"],
      operate: {
        onNext: function(param) {
          this.pushParam(param);
        }
      }
    },
    handler: function() {
      if (this.isLoadAll()) {
        this.publish(this.getParams());

        //设置调整按钮属性
        if (this.getParams().adjustWay != undefined && this.getParams().adjustValue != undefined
          && this.getParams().adjustWay != '' && this.getParams().adjustValue != ''
          && this.getParams().adjustValue != 0) {
          Ext.getCmp("adjust-operate-save").setDisabled(false);
          Ext.getCmp("adjust-operate-export").setDisabled(false);
        } else {
          Ext.getCmp("adjust-operate-save").setDisabled(true);
          Ext.getCmp("adjust-operate-export").setDisabled(true);
        }
      }
    }
  }, {
    id: 'adjust-operate-export', text: '导出', xtype: "button", action: 'export', disabled: true, observable: true,
    subscription: {
      observers: ["adjust-search-setOfBook", "adjust-search-period", "adjust-search-multiOrganization",
        "adjust-search-multiBudgetItem", "adjust-operate-adjustWay", "adjust-operate-adjustValue"],
      operate: {
        onNext: function(param) {
          this.setDisabled(true);
        }
      }
    }
  }, {
    id: 'adjust-operate-save', text: '调整', xtype: "button", action: 'save', disabled: true, observable: true,
    subscription: {
      observers: ["adjust-search-setOfBook", "adjust-search-period", "adjust-search-multiOrganization",
        "adjust-search-multiBudgetItem", "adjust-operate-adjustWay", "adjust-operate-adjustValue"],
      operate: {
        onNext: function(param) {
          this.setDisabled(true);
        }
      }
    }
  }, {
    id: 'adjust-operate-adjust-record', text: '调整记录', xtype: "button", action: 'adjustRecord', observable: true
  }, {
    id: 'adjust-operate-version-manager',
    xtype: "versionManageButton",
    showViewLink: true,
    showUseVersion: false,
    showCompareButton: true,
    queryParams: {
      domainId: "",
      setOfBooksId: -1,
      period: "",
      note: '',
      organizationId: -1,
      userId: userId,
      domainTypes: "annualCostPlanningData,monthlyCostPlanningData,relatedProjectItem,project"
    },
    versionSaveParams: {
      domainId: "",
      setOfBooksId: -1,
      period: "",
      organizationId: -1,
      includeSubOrganizations: true,
      domainTypes: "annualCostPlanningData,monthlyCostPlanningData,relatedProjectItem,project",
      dataType: ""
    },
    showVersion: function(versionId) {
      var organizations = Ext.create("Common.data.ApiReader").load({
        api: "organization",
        conditions: [
          {
            field: 'domainId',
            condition: 'EQUALS',
            value: domainId
          }, {
            field: 'parentId',
            condition: 'EQUALS',
            value: 0
          }
        ]
      });

      var versionOrganizationId = -1;
      if (organizations != null && organizations[0] != null
        && organizations[0].id != null && organizations[0].id != '') {
        versionOrganizationId = organizations[0].id;
      }
      var params = Common.CommonManager.getParams();
      var versionSetOfBooksId = params.setOfBooksId;
      var versionPeriod = params.period;
      var url = contextPath +"/pkuih/budget/version/ui?versionId=" + versionId
        + "&setOfBooksId="+versionSetOfBooksId+"&period="+versionPeriod+"&organizationId="+versionOrganizationId;
      openBrowserWindow({isFull: true, url: url});
    },
    afterUseVersion: function() {

    },
    subscription: {
      observers: ["adjust-search-setOfBook", "adjust-search-period", "adjust-search-multiOrganization",
        "adjust-search-multiBudgetItem", "adjust-operate-adjustWay", "adjust-operate-adjustValue"],
      operate: {
        onNext: function() {
          var dataType = '@@SummaryPlanning@@Cost@@&@@SummaryPlanning@@Expenses@@';
          dataType += '&@@Planning@@Cost@@&@@Planning@@Expenses@@&@@Planning@@Public@@';
          dataType += '&@@ProjectPlanning@@Cost@@&@@ProjectPlanning@@Expenses@@';
          dataType += '&@@Project@@Cost@@&@@Project@@Expenses@@&@@Project@@Public@@';
          dataType += '&@@Project@@';
          dataType += '&@@HumanResourcesSummary@@Cost@@&@@HumanResourcesMedical@@Cost@@&@@HumanResourcesNursing@@Cost@@&@@HumanResourcesTechnology@@Cost@@&@@HumanResourcesFunction@@Expenses@@';

          var organizations = Ext.create("Common.data.ApiReader").load({
            api: "organization",
            conditions: [
              {
                field: 'domainId',
                condition: 'EQUALS',
                value: domainId
              }, {
                field: 'parentId',
                condition: 'EQUALS',
                value: 0
              }
            ]
          });

          var organizationId = -1;
          var organizationCode = "";
          var organizationSortCode = "";
          if (organizations != null && organizations[0] != null
            && organizations[0].id != null && organizations[0].id != '') {
            organizationId = organizations[0].id;
            organizationCode = organizations[0].code;
            organizationSortCode = organizations[0].sortCode;
          }

          var params = Ext.getCmp('adjust-operate-query').getParams();
          this.queryParams.domainId = domainId;
          this.queryParams.setOfBooksId = params.setOfBooksId;
          this.queryParams.period = params.period;
          this.queryParams.organizationId = organizationId;
          this.queryParams.organizationCode = organizationCode;
          this.queryParams.organizationSortCode = organizationSortCode;

          this.versionSaveParams.domainId = params.domainId;
          this.versionSaveParams.setOfBooksId = params.setOfBooksId;
          this.versionSaveParams.period = params.period;
          this.versionSaveParams.organizationId = organizationId;
          this.versionSaveParams.organizationCode = organizationCode;
          this.versionSaveParams.organizationSortCode = organizationSortCode;
          this.versionSaveParams.dataType = dataType;
        }
      }
    }
  }]
});
