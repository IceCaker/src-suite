package com.bmit.project.srrc.util;

public class SrrcModule {
    public static final String MODULE_PATH = "srrc";
    public static final String MODULE_MAPPING_ROOT = "/srrc/";

    public static final String BMS_SRRC_EXPENSE_APPLY_FEATURE_KEY = "srrc.expense.apply";

    public static final String BMS_SRRC_EXPENSE_LIST_FEATURE_KEY = "srrc.expense.list";

    public static final String BMS_SRRC_EXPENSE_VIEW_FEATURE_KEY = "srrc.expense.view";

    public static final String BMS_SRRC_EXPENSE_PRINT_FEATURE_KEY = "srrc.expense.print";

    public static final String BMS_UI_COMPATIBILITY_VIEW_EXPENSE_COMMON_VIEW = "srrc.expense.common";
}
