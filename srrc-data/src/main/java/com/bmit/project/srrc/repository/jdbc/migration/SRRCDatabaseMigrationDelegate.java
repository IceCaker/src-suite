package com.bmit.project.srrc.repository.jdbc.migration;

import com.bmit.platform.soupe.component.AbstractSpringJdbcWithJOOQMigrationDelegate;
import com.bmit.platform.soupe.component.SpringJdbcWithJOOQMigrationDelegate;
import com.bmit.platform.soupe.ida.repository.jdbc.migration.IDADatabaseMigrationDelegate;
import org.springframework.stereotype.Component;

@Component
public class SRRCDatabaseMigrationDelegate extends AbstractSpringJdbcWithJOOQMigrationDelegate {

    public static final String SCHEMA_VERSION_TABLE_PREFIX = "SRRC_";

    @Override
    public String getSchemaVersionTablePrefix() {
        return SCHEMA_VERSION_TABLE_PREFIX;
    }

    @Override
    public Class<? extends SpringJdbcWithJOOQMigrationDelegate>[] getPreMigrationDelegates() {
        return new Class[]{
                IDADatabaseMigrationDelegate.class
        };
    }

}
