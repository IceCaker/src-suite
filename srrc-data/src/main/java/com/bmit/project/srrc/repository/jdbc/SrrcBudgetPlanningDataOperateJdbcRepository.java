package com.bmit.project.srrc.repository.jdbc;

import java.util.Map;

public interface SrrcBudgetPlanningDataOperateJdbcRepository {

    String BEAN_NAME = "SrrcBudgetPlanningDataOperateJdbcRepository";

    void getPlanningSummaryData(Map<String, Object> params);
}
